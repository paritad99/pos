package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmployeeEditActivity extends AppCompatActivity {

    Spinner ddlBranch, ddlStatus;
    EditText input_firstname, input_lastname, input_email, input_tel, input_username, input_password;
    Button btnSaveEmloyeeInfo;

    ArrayList<HashMap<String, String>> MyArrList;
    HashMap<String, String> map;

    String BranchId = "";
    String StatusId = "";
    String email = "";
    String employee_id = "";
    String level_position = "";
    String BranchIdchk = "";

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_add);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        btnSaveEmloyeeInfo = (Button) findViewById(R.id.btnSaveEmloyeeInfo);

        ddlBranch = (Spinner) findViewById(R.id.ddlBranch);
        ddlStatus = (Spinner) findViewById(R.id.ddlStatus);

        input_firstname = (EditText) findViewById(R.id.input_firstname);
        input_lastname = (EditText) findViewById(R.id.input_lastname);
        input_email = (EditText) findViewById(R.id.input_email);
        input_tel = (EditText) findViewById(R.id.input_tel);
        input_username = (EditText) findViewById(R.id.input_username);
        input_password = (EditText) findViewById(R.id.input_password);

        btnSaveEmloyeeInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String uname = input_username.getText().toString();
                String upass = input_password.getText().toString();

                if (uname.equals("")) {
                    input_username.setError("กรุณาใส่ Username");
                } else if (uname.length() < 8) {
                    input_username.setError("Username ต้องไม่น้อยกว่า 8 หลัก");
                } else if (upass.equals("")) {
                    input_password.setError("กรุณาใส่ Password");
                } else if (upass.length() < 8) {
                    input_password.setError("Password ต้องไม่น้อยกว่า 8 หลัก");
                }

                email = input_email.getText().toString().trim();
                boolean valid = validateEmailAddress(email);
                if (valid == true) {
                    email = input_email.getText().toString().trim();
                    new BookingAddTask().execute(StringUtil.URL + "gpos/api/EmployeeUpdate");

                } else {
                    input_email.setError("รูปแบบ Email ไม่ถูกต้อง");
                }


            }
        });


            /*
            ค่าที่ส่ง
        intent.putExtra("employee_id", feedItem.getEmployee_id());
        intent.putExtra("branch_id", feedItem.getBranch_id());
        intent.putExtra("level_position", feedItem.getLevel_position());
        intent.putExtra("firstname", feedItem.getFirstname());
        intent.putExtra("lastname", feedItem.getLastname());
        intent.putExtra("email", feedItem.getEmail());
        intent.putExtra("username", feedItem.getUsername());
        intent.putExtra("password", feedItem.getPassword());
        intent.putExtra("create_by", Profile.UserId);
        intent.putExtra("store_id", Profile.StoreId);
        */

        Intent intent = getIntent();
        input_firstname.setText(intent.getStringExtra("firstname"));
        input_lastname.setText(intent.getStringExtra("lastname"));
        input_email.setText(intent.getStringExtra("email"));
        input_tel.setText(intent.getStringExtra("tel"));
        input_username.setText(intent.getStringExtra("username"));
        input_password.setText(intent.getStringExtra("password"));
        employee_id = intent.getStringExtra("employee_id");
        BranchIdchk = intent.getStringExtra("branch_id");
        level_position = intent.getStringExtra("level_position");
        new AsyncLoadBranch().execute();
        new AsyncLoadStatus().execute();

    }

    private boolean validateEmailAddress(String emailAddress) {
        String expression = "^[\\w\\-]([\\.\\w])+[\\w]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = emailAddress;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        return matcher.matches();
    }


    private class BookingAddTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(EmployeeEditActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลังบันทึก");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();


            RequestBody formBody = new FormEncodingBuilder()

                    .add("employee_id", employee_id + "")
                    .add("branch_id", BranchId + "")
                    .add("level_position", StatusId + "")
                    .add("firstname", input_firstname.getText().toString())
                    .add("lastname", input_lastname.getText().toString())
                    .add("email", email)
                    .add("tel", input_tel.getText().toString())
                    .add("username", input_username.getText().toString().trim())
                    .add("password", input_password.getText().toString().trim())
                    .add("create_by", Profile.UserId + "")
                    .add("store_id", Profile.StoreId + "")
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(EmployeeEditActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(EmployeeEditActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    finish();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class AsyncLoadBranch extends AsyncTask<String, String, String> {
        //        ProgressDialog pdLoading = new ProgressDialog(FoodMenuAddActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
//            pdLoading.setMessage("\tกำลังโหลดข้อมูล...");
//            pdLoading.setCancelable(false);
//            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/BranchList/" + Profile.StoreId);
//                response = http.run(StringUtil.URL+"gpos/api/category/1");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread
            int selectIndex = 0;
//            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);
                MyArrList = new ArrayList<HashMap<String, String>>();
                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    map = new HashMap<String, String>();
                    if (BranchIdchk.equals(json_data.getString("branch_id"))) {
                        selectIndex = i;
                    }
                    map.put("MemberID", json_data.getString("branch_id"));
                    map.put("Name", json_data.getString("branch_name"));
                    map.put("Tel", json_data.getString("branch_address"));
                    MyArrList.add(map);

                }


                SimpleAdapter sAdap;
                sAdap = new SimpleAdapter(EmployeeEditActivity.this, MyArrList, R.layout.activity_menu_ddl_row,
                        new String[]{"Name"}, new int[]{R.id.ColName});

//                final AlertDialog.Builder viewDetail = new AlertDialog.Builder(FoodMenuAddActivity.this);
                ddlBranch.setAdapter(sAdap);
                ddlBranch.setSelection(selectIndex);
                ddlBranch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    public void onItemSelected(AdapterView<?> arg0, View selectedItemView,
                                               int position, long id) {


                        BranchId = MyArrList.get(position).get("MemberID")
                                .toString();
                        String sName = MyArrList.get(position).get("Name")
                                .toString();
                        String sTel = MyArrList.get(position).get("Tel")
                                .toString();


                    }

                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub
                        Toast.makeText(EmployeeEditActivity.this,
                                "Your Selected : Nothing",
                                Toast.LENGTH_SHORT).show();
                    }


                });


            } catch (JSONException e) {
                Toast.makeText(EmployeeEditActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    private class AsyncLoadStatus extends AsyncTask<String, String, String> {
        //        ProgressDialog pdLoading = new ProgressDialog(FoodMenuAddActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
//            pdLoading.setMessage("\tกำลังโหลดข้อมูล...");
//            pdLoading.setCancelable(false);
//            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/EmployeeStatusList");
//                response = http.run(StringUtil.URL+"gpos/api/category/1");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread
            int selectIndex = 0;
//            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);
                MyArrList = new ArrayList<HashMap<String, String>>();
                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    map = new HashMap<String, String>();
                    if (level_position.equals(json_data.getString("employee_sid"))) {
                        selectIndex = i;
                    }
                    map.put("MemberID", json_data.getString("employee_sid"));
                    map.put("Name", json_data.getString("name"));
                    map.put("Tel", json_data.getString("level"));
                    MyArrList.add(map);

                }


                SimpleAdapter sAdap;
                sAdap = new SimpleAdapter(EmployeeEditActivity.this, MyArrList, R.layout.activity_menu_ddl_row,
                        new String[]{"Name"}, new int[]{R.id.ColName});

//                final AlertDialog.Builder viewDetail = new AlertDialog.Builder(FoodMenuAddActivity.this);
                ddlStatus.setAdapter(sAdap);
                ddlStatus.setSelection(selectIndex);
                ddlStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    public void onItemSelected(AdapterView<?> arg0, View selectedItemView,
                                               int position, long id) {


                        StatusId = MyArrList.get(position).get("MemberID")
                                .toString();
                        String sName = MyArrList.get(position).get("Name")
                                .toString();
                        String sTel = MyArrList.get(position).get("Tel")
                                .toString();


                    }

                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub
                        Toast.makeText(EmployeeEditActivity.this,
                                "Your Selected : Nothing",
                                Toast.LENGTH_SHORT).show();
                    }


                });


            } catch (JSONException e) {
                Toast.makeText(EmployeeEditActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    private void showData(String jsonString) {
        Toast.makeText(this, jsonString, Toast.LENGTH_LONG).show();
    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

}
