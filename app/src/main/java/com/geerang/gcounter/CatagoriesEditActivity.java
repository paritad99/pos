package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.provider.MediaStore;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class CatagoriesEditActivity extends AppCompatActivity {

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    Bitmap FixBitmap;
    byte[] byteArray;
    String ConvertImage = "";
    URL url;
    private int GALLERY = 1, CAMERA = 2;
    Spinner ddlPrinter;
    Button buttonSelect, btnSaveCatMenu;
    ImageView imgStore;
    CheckBox chk_is_recommend;
    EditText input_cat_menu, input_desc_menu;
    ArrayList<HashMap<String, String>> MyArrList;
    HashMap<String, String> map;

    String printer = "";

    String categories_id;
    String categories_name;
    String categories_color;
    String categories_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catagories_edit);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        buttonSelect = (Button) findViewById(R.id.buttonSelect);
        buttonSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPictureDialog();
            }
        });

        btnSaveCatMenu = (Button) findViewById(R.id.btnSaveCatMenu);
        btnSaveCatMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CategoryAddTask().execute(StringUtil.URL + "gpos/api/CategoryEdit");
            }
        });
        /*
        ค่าที่ส่งมาจากหน้า List
        intent.putExtra("categories_id", feedItem.getId());
        intent.putExtra("categories_name",  feedItem.getTitle());
        intent.putExtra("categories_color",  feedItem.getDetail());
        intent.putExtra("categories_image",  feedItem.getProduct_image());
        */

        Intent intent = getIntent();
        categories_id = intent.getStringExtra("categories_id");
        categories_name = intent.getStringExtra("categories_name");
        categories_color = intent.getStringExtra("categories_color");
        categories_image = intent.getStringExtra("categories_image");
        printer = intent.getStringExtra("id_printer");

        input_cat_menu = (EditText) findViewById(R.id.input_cat_menu);
        input_cat_menu.setText(categories_name);

        input_desc_menu = (EditText) findViewById(R.id.input_desc_menu);
        input_desc_menu.setText(categories_color);

        ddlPrinter = (Spinner) findViewById(R.id.ddlPrinter);
        imgStore = (ImageView) findViewById(R.id.imgStore);

        new AsyncGetPrinter().execute();
        ShowImage();

        // select default
//        ddlPrinter.setSelection(MyArrList.indexOf(printer));
    }

    @Override
    protected void onResume() {
        super.onResume();
        new AsyncGetPrinter().execute();
    }

    public void ShowImage() {

        StringUtil stringUtil = new StringUtil();
        Picasso.with(CatagoriesEditActivity.this)
                .load(StringUtil.URL + "gpos/" + stringUtil.removeFirstChar(categories_image))
                .centerCrop()
                .fit()
                .error(R.drawable.ic_error)
                .into(imgStore);

    }

    private class AsyncGetPrinter extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(CatagoriesEditActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //this method will be running on UI thread
            pdLoading.setMessage("\tกำลังโหลดข้อมูล...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/GetPrinter/" + Profile.StoreId);
//                response = http.run(StringUtil.URL+"gpos/api/category/1");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;

        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);
                MyArrList = new ArrayList<HashMap<String, String>>();
                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    map = new HashMap<String, String>();
                    map.put("MemberID", json_data.getString("id_printer"));
                    map.put("Name", json_data.getString("printer_name"));
                    map.put("Tel", json_data.getString("printer_mac"));
                    MyArrList.add(map);

                }


                SimpleAdapter sAdap;
                sAdap = new SimpleAdapter(CatagoriesEditActivity.this, MyArrList, R.layout.activity_menu_ddl_row,
                        new String[]{"Name"}, new int[]{R.id.ColName});

//                final AlertDialog.Builder viewDetail = new AlertDialog.Builder(FoodMenuAddActivity.this);
                ddlPrinter.setAdapter(sAdap);

                ddlPrinter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    public void onItemSelected(AdapterView<?> arg0, View selectedItemView,
                                               int position, long id) {


                        printer = MyArrList.get(position).get("MemberID")
                                .toString();
                        String sName = MyArrList.get(position).get("Name")
                                .toString();
                        String sTel = MyArrList.get(position).get("Tel")
                                .toString();


                    }

                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub
                        Toast.makeText(CatagoriesEditActivity.this,
                                "Your Selected : Nothing",
                                Toast.LENGTH_SHORT).show();
                    }


                });

            } catch (JSONException e) {
                Toast.makeText(CatagoriesEditActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }


    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    private class CategoryAddTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(CatagoriesEditActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลังบันทึกข้อมูล...");
            pdLoading.setCancelable(false);
            pdLoading.show();

            Toast.makeText(CatagoriesEditActivity.this, "กำลังบันทึกข้อมูล", Toast.LENGTH_SHORT).show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            if(FixBitmap != null)
            {
                try {

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    FixBitmap.compress(Bitmap.CompressFormat.PNG, 20, stream);
                    byteArray = stream.toByteArray();
                    FixBitmap.recycle();
                    ConvertImage = Base64.encodeToString(byteArray, Base64.DEFAULT);

                } catch (Exception e) {

                    ConvertImage = "";
                }
            }else
            {
                ConvertImage = "";
            }


            RequestBody formBody = new FormEncodingBuilder()
                    .add("image_data", ConvertImage)
                    .add("store_id", Profile.StoreId)
                    .add("printer", printer)
                    .add("product_name", input_cat_menu.getText().toString())
                    .add("product_detail", input_desc_menu.getText().toString())
                    .add("categories_id", categories_id)
                    .build();

            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                String msg = object.getString("msg"); // success

//                StringUtil.product_menu_id = msg;
//                StringUtil.menu_store_id = msg;


                Toast.makeText(CatagoriesEditActivity.this, "" + msg, Toast.LENGTH_SHORT).show();
//                Intent in = new Intent(FoodMenuAddActivity.this,FoodOptionActivity.class);
////                in.putExtra("menu_id",msg);
//                startActivity(in);

                finish();


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public static Bitmap scaleBitmap(Bitmap bitmap, int wantedWidth, int wantedHeight) {
        Bitmap output = Bitmap.createBitmap(wantedWidth, wantedHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        Matrix m = new Matrix();
        m.setScale((float) wantedWidth / bitmap.getWidth(), (float) wantedHeight / bitmap.getHeight());
        canvas.drawBitmap(bitmap, m, new Paint());

        return output;
    }

    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Photo Gallery",
                "Camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    FixBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    // String path = saveImage(bitmap);
                    //Toast.makeText(MainActivity.this, "Image Saved!", Toast.LENGTH_SHORT).show();
                    imgStore.setImageBitmap(FixBitmap);
//                    UploadImageOnServerButton.setVisibility(View.VISIBLE);

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(CatagoriesEditActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {
            FixBitmap = (Bitmap) data.getExtras().get("data");
            imgStore.setImageBitmap(FixBitmap);
//            UploadImageOnServerButton.setVisibility(View.VISIBLE);
            //  saveImage(thumbnail);
            //Toast.makeText(ShadiRegistrationPart5.this, "Image Saved!", Toast.LENGTH_SHORT).show();
        }
    }

}
