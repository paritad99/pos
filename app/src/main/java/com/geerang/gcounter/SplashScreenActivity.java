package com.geerang.gcounter;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.epson.epos2.printer.Printer;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SplashScreenActivity extends AppCompatActivity {

    private Handler handler;
    private Runnable runnable;

    final String PREF_NAME = "LoginPreferences";
    final String KEY_USERNAME = "Username";
    final String KEY_USERID = "UserId";
    final String KEY_BranchId = "BranchId";
    final String KEY_StoreId = "StoreId";
    final String KEY_Status = "Status";
    final String KEY_REMEMBER = "RememberUsername";
    final String KEY_business_type_id = "business_type_id";
    final String KEY_StoreName = "StoreName";
    final String KEY_BranchName = "BranchName";
    final String KEY_Position = "Position";
    final String KEY_StoreExp = "StoreExp";
    final String KEY_Owner = "Owner";
    final String KEY_package = "package";
    final String KEY_StoreTax = "StoreTax";
    final String KEY_StorePhone = "StoreTax";


    SharedPreferences sp;
    SharedPreferences.Editor editor;
    int CheckCashier;
    private static final int REQUEST_PERMISSION = 100;
    ProgressBar progressBar;

    public static Printer mPrinter = null;
    private BroadcastReceiver MyReceiver = null;

    public void broadcastIntent() {

    }

    @Override
    protected void onPause() {
        super.onPause();
        // unregisterReceiver(MyReceiver);
    }

    private static final int PERMISSION_REQUEST_CODE = 200;
    private View view;

    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();

        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

//        requestRuntimePermission();

        progressBar = (ProgressBar) findViewById(R.id.progressBar2);

        // broadcastIntent();

        //String status = NetworkUtil.getConnectivityStatusString(SplashScreenActivity.this);
        if (!haveNetworkConnection()) {
            //status = "No Internet Connection";
            Intent in = new Intent(SplashScreenActivity.this, NointernetActivity.class);
            in.putExtra("Key", "text");
            in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(in);

        } else {

            sp = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
            editor = sp.edit();
            handler = new Handler();
            runnable = new Runnable() {
                public void run() {
                    try {

                        Thread.sleep(1000);
                        Profile.BranchId = sp.getString(KEY_BranchId, "");
                        Profile.StoreId = sp.getString(KEY_StoreId, "");
                        Profile.Username = sp.getString(KEY_USERNAME, "");
                        Profile.StoreName = sp.getString(KEY_StoreName, "");
                        Profile.BranchName = sp.getString(KEY_BranchName, "");
                        Profile.Position = sp.getString(KEY_Position, "");
                        Profile.Manager = sp.getString(KEY_BranchId, "");
                        Profile.business_type_id = sp.getString(KEY_business_type_id, "");
                        Profile.UserId = sp.getString(KEY_USERID, "");
                        Profile.store_exp = sp.getString(KEY_StoreExp, "");
                        String Login = sp.getString(KEY_REMEMBER, "");
                        String Owner = sp.getString(KEY_Owner, "");

                        if(!Profile.StoreId.equals(""))
                        {
                            if (!haveNetworkConnection()) {
                                //status = "No Internet Connection";
                                Intent in = new Intent(SplashScreenActivity.this, NointernetActivity.class);
                                in.putExtra("Key", "text");
                                in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(in);

                            }else
                            {
                                //load package
                                new AsyncLoadCat().execute(Profile.StoreId);
                            }

                        }

                        Profile.StoreTax = sp.getString(KEY_StoreTax, "");
                        Profile.StorePhone = sp.getString(KEY_StorePhone, "");

                        String store_package = sp.getString(KEY_package, "");
                        if (!store_package.equals("")) {
                            JSONArray jArray = new JSONArray(store_package);
                            // Extract data from json and store into ArrayList as class objects
                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject json_data = jArray.getJSONObject(i);
                                StringUtil.package_name = json_data.getString("package_name");
                                StringUtil.package_product = json_data.getString("package_product");
                                StringUtil.package_branch = json_data.getString("package_branch");
                                StringUtil.package_cashier = json_data.getString("package_cashier");
                                StringUtil.package_users = json_data.getString("package_users");
                                StringUtil.package_zone = json_data.getString("package_zone");
                                StringUtil.package_table = json_data.getString("package_table");
                            }

                        }

                        if (Owner.equals("Owner")) {
                            Profile.IsOwner = true;
                        } else {
                            Profile.IsOwner = false;
                        }

                        try {

                            String FinalDate = Profile.store_exp;

                            if (!FinalDate.equals("")) {
                                //Dates to compare
                                String CurrentDate = getDateTime(); // "09/24/2015";
                                Date date1;
                                Date date2;

                                SimpleDateFormat dates = new SimpleDateFormat("dd/MM/yyyy");

                                //Setting dates
                                try {
                                    date1 = dates.parse(CurrentDate);
                                    date2 = dates.parse(FinalDate);
                                    float x = getDifferenceDays(date1, date2);
                                    // check date
                                    int b = Math.round(x);
                                    if (b <= 0) {
                                        Intent in = new Intent(SplashScreenActivity.this, StoreExpireActivity.class);
                                        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                        startActivity(in);
                                        finish();
                                    } else {
                                        if (Profile.IsOwner == true) {
                                            Intent in = new Intent(SplashScreenActivity.this, SelectBranchActivity.class);
                                            in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                            startActivity(in);
                                            finish();

                                        } else {

                                            if (Profile.Position.equals("1")) {
                                                StringUtil.employee_status = "Customer Service";
                                                Intent in = new Intent(SplashScreenActivity.this, StaffDeliveryActivity.class);
                                                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                                startActivity(in);
                                                finish();

                                                //คนรับ order
                                            } else if (Profile.Position.equals("6")) {
                                                StringUtil.employee_status = "Customer Service";
                                                Intent in = new Intent(SplashScreenActivity.this, StaffDeliveryActivity.class);
                                                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                                startActivity(in);
                                                finish();
                                                //drink
                                            } else if (Profile.Position.equals("3")) {
//                                                Intent in = new Intent(SplashScreenActivity.this, ETableSelectActivity.class);
//                                                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                                                startActivity(in);
//                                                finish();
                                                StringUtil.employee_status = "Customer Service";
                                                Intent in = new Intent(SplashScreenActivity.this, StaffDeliveryActivity.class);
                                                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                                startActivity(in);
                                                finish();

                                            } else if (Profile.Position.equals("4")) // แคชเชียร์
                                            {
                                                new AsyncLoadProduct().execute();

                                            } else if (Profile.Position.equals("2")) {
                                                StringUtil.employee_status = "ผู้จัดการร้าน";

                                                Intent in = new Intent(SplashScreenActivity.this, ManagerMainActivity.class);
                                                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                                startActivity(in);
                                                finish();

                                            } else {
                                                Intent in = new Intent(SplashScreenActivity.this, LoginActivity.class);
                                                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                                startActivity(in);
                                                finish();
                                            }


                                        }

                                    }

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }


                            } else {
                                Intent in = new Intent(SplashScreenActivity.this, LoginActivity.class);
                                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                startActivity(in);
                                finish();

                            }


                        } catch (Exception exception) {
                            Log.e("DIDN'T WORK", "exception " + exception);
                        }

                    } catch (InterruptedException | JSONException e) {
                        e.printStackTrace();

                        Intent in = new Intent(SplashScreenActivity.this, LoginActivity.class);
                        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(in);
                        finish();

                    }


                }
            };


        }


    }

    private class AsyncLoadCat extends AsyncTask<String, String, String> {
       // ProgressDialog pdLoading = new ProgressDialog(SplashScreenActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
//            pdLoading.setMessage("\tLoading...");
//            pdLoading.setCancelable(false);
//            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/StorePackage/" +urls[0]);
//                response = http.run(StringUtil.URL+"gpos/api/category/1");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;

        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            try {
                sp = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                editor = sp.edit();

                JSONArray jArray = new JSONArray(result);
                editor = sp.edit();
                editor.putString(KEY_package, result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {

                    JSONObject json_data = jArray.getJSONObject(i);
                    StringUtil.package_name = json_data.getString("package_name");
                    StringUtil.package_product = json_data.getString("package_product");
                    StringUtil.package_branch = json_data.getString("package_branch");
                    StringUtil.package_cashier = json_data.getString("package_cashier");
                    StringUtil.package_users = json_data.getString("package_users");
                    StringUtil.package_zone = json_data.getString("package_zone");
                    StringUtil.package_table = json_data.getString("package_table");
                    Profile.store_exp = json_data.getString("store_exp");

                    editor.putString(KEY_StoreExp, json_data.getString("store_exp"));
                }

                editor.commit();

            } catch (JSONException e) {
                Toast.makeText(SplashScreenActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(SplashScreenActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    public static long getDifferenceDays(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    private String getDateTime() {
        // get date time in custom format
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(new Date());
    }

    private class AsyncLoadProduct extends AsyncTask<String, String, String> {

        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/CashierCheck/" + Profile.StoreId + "/" + Profile.BranchId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            try {

                JSONArray jArray = new JSONArray(result);
                if (jArray.length() > 0) {
                    Intent in = new Intent(SplashScreenActivity.this, StaffCashireActivity.class);
                    in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(in);
                    finish();

                } else {

                    Intent in = new Intent(SplashScreenActivity.this, CashierStartActivity.class);
                    in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(in);
                    finish();

                }


            } catch (JSONException e) {
                Toast.makeText(SplashScreenActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }


    private void requestRuntimePermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return;
        }

        int permissionStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permissionLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);

        List<String> requestPermissions = new ArrayList<>();

        if (permissionStorage == PackageManager.PERMISSION_DENIED) {
            requestPermissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionLocation == PackageManager.PERMISSION_DENIED) {
            requestPermissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }

        if (!requestPermissions.isEmpty()) {
            ActivityCompat.requestPermissions(this, requestPermissions.toArray(new String[requestPermissions.size()]), REQUEST_PERMISSION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode != REQUEST_PERMISSION || grantResults.length == 0) {
            return;
        }

        List<String> requestPermissions = new ArrayList<>();

        for (int i = 0; i < permissions.length; i++) {
            if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    && grantResults[i] == PackageManager.PERMISSION_DENIED) {
                requestPermissions.add(permissions[i]);
            }
            if (permissions[i].equals(Manifest.permission.ACCESS_COARSE_LOCATION)
                    && grantResults[i] == PackageManager.PERMISSION_DENIED) {
                requestPermissions.add(permissions[i]);
            }
        }

        if (!requestPermissions.isEmpty()) {
            ActivityCompat.requestPermissions(this, requestPermissions.toArray(new String[requestPermissions.size()]), REQUEST_PERMISSION);
        }
    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    public void onResume() {
        super.onResume();
        if (!haveNetworkConnection()) {
            //status = "No Internet Connection";
            Intent in = new Intent(SplashScreenActivity.this, NointernetActivity.class);
            in.putExtra("Key", "text");
            in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(in);

        } else {
            handler.postDelayed(runnable, 4000);
        }


    }

    public void onStop() {
        super.onStop();

        if (!haveNetworkConnection()) {
            //status = "No Internet Connection";
            Intent in = new Intent(SplashScreenActivity.this, NointernetActivity.class);
            in.putExtra("Key", "text");
            in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(in);

        } else {
            handler.removeCallbacks(runnable);
        }


    }

}
