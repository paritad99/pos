package com.geerang.gcounter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.widget.Button;
import android.widget.TextView;

public class StaffDeliveryActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;

    final String PREF_NAME = "LoginPreferences";
    final String KEY_USERNAME = "Username";
    final String KEY_USERID = "UserId";
    final String KEY_BranchId = "BranchId";
    final String KEY_StoreId = "StoreId";
    final String KEY_Status = "Status";
    final String KEY_REMEMBER = "RememberUsername";
    final String KEY_business_type_id = "business_type_id";
    final String KEY_StoreName = "StoreName";
    final String KEY_BranchName = "BranchName";
    final String KEY_Position = "Position";
    final String KEY_StoreExp = "StoreExp";
    final String KEY_Owner = "Owner";

    SharedPreferences sp;
    SharedPreferences.Editor editor;


    Profile profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_delivery);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        profile = new Profile(this);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each

        View headerView = navigationView.getHeaderView(0);

        sp = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sp.edit();


        TextView txtDateExp = (TextView) headerView.findViewById(R.id.txtDateExp);
        txtDateExp.setText("สาขา " + profile.BranchName);
        Button btnExitApp = (Button) headerView.findViewById(R.id.btnExitApp);
        btnExitApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final AlertDialog.Builder adb = new AlertDialog.Builder(StaffDeliveryActivity.this);
                adb.setTitle("ออกจากระบบ?");
                adb.setMessage("กรุณายืนยันการออกจากระบบ");
                adb.setNegativeButton("ยกเลิก", null);
                adb.setPositiveButton("ยืนยัน", new AlertDialog.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        // TODO Auto-generated method stub
                        stopService(new Intent(StaffDeliveryActivity.this, TimePrintService.class));
                        editor = sp.edit();
                        editor.putString(KEY_USERNAME, "");
                        editor.putString(KEY_USERID, "");
                        editor.putString(KEY_StoreId, "");
                        editor.putString(KEY_BranchId, "");
                        editor.putString(KEY_Status, "");
                        editor.putString(KEY_REMEMBER, "");
                        editor.putString(KEY_business_type_id, "");
                        editor.putString(KEY_StoreName, "");
                        editor.putString(KEY_BranchName, "");
                        editor.putString(KEY_Position, "");
                        editor.putString(KEY_Owner, "");
                        editor.commit();

                        Intent in = new Intent(StaffDeliveryActivity.this, LoginActivity.class);
                        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(in);
                        finish();

                    }
                });
                adb.show();


            }
        });

        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_attendance,R.id.nav_order_delivery, R.id.nav_staff_order_calcel, R.id.nav_staff_leave,
                R.id.nav_order_delivery, R.id.nav_order_delivery, R.id.nav_order_delivery)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

    }



    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
}
