package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class FoodMenuChoiceActivity extends AppCompatActivity {

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    ArrayList<HashMap<String, String>> MyArrList;
    ListView lisView1;

    String p_group_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_menu_choice);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // add back arrow to toolbar
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        // listView1
        lisView1 = (ListView)findViewById(R.id.listView1);

        // Check All
        Button btnCheckAll = (Button) findViewById(R.id.btnCheckAll);
        btnCheckAll.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int count = lisView1.getAdapter().getCount();
                for (int i = 0; i < count; i++) {
                    LinearLayout itemLayout = (LinearLayout)lisView1.getChildAt(i); // Find by under LinearLayout
                    CheckBox checkbox = (CheckBox)itemLayout.findViewById(R.id.ColChk);
                    checkbox.setChecked(true);
                }
            }
        });

        // Clear All
        Button btnClearAll = (Button) findViewById(R.id.btnClearAll);
        btnClearAll.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int count = lisView1.getAdapter().getCount();
                for (int i = 0; i < count; i++) {
                    LinearLayout itemLayout = (LinearLayout)lisView1.getChildAt(i); // Find by under LinearLayout
                    CheckBox checkbox = (CheckBox)itemLayout.findViewById(R.id.ColChk);
                    checkbox.setChecked(false);
                }
            }
        });



        // Get Item Checked
        Button btnGetItem = (Button) findViewById(R.id.btnGetItem);
        btnGetItem.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int count = MyArrList.size();
                for (int i = 0; i < count; i++) {
                    LinearLayout itemLayout = (LinearLayout)lisView1.getChildAt(i); // Find by under LinearLayout
                    CheckBox checkbox = (CheckBox)itemLayout.findViewById(R.id.ColChk);
                    if(checkbox.isChecked())
                    {
                        Log.d("Item "+String.valueOf(i), checkbox.getTag().toString());

                        p_group_id = MyArrList.get(i).get("p_group_id");
                        new BookingAddTask().execute(StringUtil.URL + "gpos/api/FoodMenuChoiceAdd",p_group_id);

//                        Toast.makeText(MenuIngredientsAddActivity.this, "", Toast.LENGTH_SHORT).show();
//                        Toast.makeText(MenuIngredientsAddActivity.this,checkbox.getTag().toString() ,Toast.LENGTH_LONG).show();
                    }
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(FoodMenuChoiceActivity.this);
                builder.setMessage("บันทึกข้อมูลเรียบร้อยแล้ว")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //do things
//                                StringUtil.product_menu_id

                                Intent in = new Intent(FoodMenuChoiceActivity.this,FoodOptionActivity.class);
                                startActivity(in);
//                                finish();
//                                FoodMenuChoiceEditPriceActivity

                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

            }
        });

        new AsyncLoadCat().execute();

    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    private class BookingAddTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(FoodMenuChoiceActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("Progress...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("product_id", StringUtil.product_menu_id+"")
                    .add("product_choice_group_id",  urls[1])
                    .add("create_by",Profile.UserId)
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

//                String status = object.getString("status"); // success
//                if (status.equals("0")) {
//                    String msg = object.getString("msg"); // success
//                    AlertDialog.Builder builder = new AlertDialog.Builder(FoodMenuChoiceActivity.this);
//                    builder.setMessage(msg)
//                            .setCancelable(false)
//                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    //do things
//                                }
//                            });
//                    AlertDialog alert = builder.create();
//                    alert.show();
//                } else {
//                    String msg = object.getString("msg"); // success
//
//                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    private class AsyncLoadCat extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(FoodMenuChoiceActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL+"gpos/api/MenuGroupChoice/"+ Profile.StoreId);
//                response = http.run(StringUtil.URL+"gpos/api/category/1");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {


            //this method will be running on UI thread

            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();

            MyArrList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map;



            lisView1.setAdapter(new CountryAdapter(FoodMenuChoiceActivity.this));

            try {

                JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);

                    map = new HashMap<String, String>();
                    map.put("p_group_id", json_data.getString("p_group_id"));
                    map.put("name", json_data.getString("name"));
                    map.put("min", json_data.getString("min"));
                    map.put("max", json_data.getString("max"));
                    map.put("description", json_data.getString("description"));
                    MyArrList.add(map);

                }


            } catch (JSONException e) {
                Toast.makeText(FoodMenuChoiceActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

        public class CountryAdapter extends BaseAdapter
        {
            private Context context;

            public CountryAdapter(Context c)
            {
                //super( c, R.layout.activity_column, R.id.rowTextView, );
                // TODO Auto-generated method stub
                context = c;
            }

            public int getCount() {
                // TODO Auto-generated method stub
                return MyArrList.size();
            }

            public Object getItem(int position) {
                // TODO Auto-generated method stub
                return position;
            }

            public long getItemId(int position) {
                // TODO Auto-generated method stub
                return position;
            }
            public View getView(final int position, View convertView, ViewGroup parent) {
                // TODO Auto-generated method stub

                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.activity_food_menu_choice_row, null);

                }

                // ColCode
                TextView txtCode = (TextView) convertView.findViewById(R.id.txtId);
                txtCode.setText(MyArrList.get(position).get("name"));

                // ColCountry
                TextView txtCountry = (TextView) convertView.findViewById(R.id.txtName);
                txtCountry.setText(MyArrList.get(position).get("description"));

                // ColChk
                CheckBox Chk = (CheckBox) convertView.findViewById(R.id.ColChk);
                Chk.setTag(MyArrList.get(position).get("p_group_id"));

                return convertView;

            }

        }

    }
}
