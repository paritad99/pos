package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class StockUpdateAddActivity extends AppCompatActivity {

    ArrayList<HashMap<String, String>> MyArrList;
    HashMap<String, String> map;
    Spinner ddlMenuCategory;

    String product_id;


    ListView lisView1;


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {

            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_exp_add);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // add back arrow to toolbar
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        ddlMenuCategory = (Spinner) findViewById(R.id.ddlMenuCategory);

//        new BookingAddTask().execute(StringUtil.URL + "gpos/api/BookingAdd");

        // listView1
        lisView1 = (ListView) findViewById(R.id.listView1);


        // Get Item Checked
        Button btnGetItem = (Button) findViewById(R.id.btnGetItem);
        btnGetItem.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                String order_id = "";
                String Customer = "";

                int count = MyArrList.size();
                for (int i = 0; i < count; i++) {
                    LinearLayout itemLayout = (LinearLayout) lisView1.getChildAt(i); // Find by under LinearLayout
                    EditText input_qty = (EditText) itemLayout.findViewById(R.id.input_qty);

                    Log.d("ProductID " + MyArrList.get(i).get("product_id"), input_qty.getText().toString());
                    String Qty = input_qty.getText().toString();
                    String input_qty_new;
                    if (Qty.equals("")) {
                        input_qty_new = "0";
                    } else {
                        input_qty_new = Qty;
                    }
                    String product_id = MyArrList.get(i).get("product_id").toString();
                    String product_price = MyArrList.get(i).get("product_price").toString();
                    new ProductExpAddTask().execute(StringUtil.URL + "gpos/api/ProductExpAdd", product_id, product_price, input_qty_new);

                }

                AlertDialog.Builder builder = new AlertDialog.Builder(StockUpdateAddActivity.this);
                builder.setMessage("ทำการบักทึกข้อมูลเรียบร้อยแล้ว")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //do things
                                finish();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();


            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        new AsyncLoadIngredientTable().execute();
    }

    private class AsyncLoadIngredientTable extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(StockUpdateAddActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tกำลังโหลดข้อมูล...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/MenuIngredientDropdown/" + Profile.StoreId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            //txtResult.setText(response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {

            pdLoading.dismiss();
            try {

                JSONArray jArray = new JSONArray(result);

                MyArrList = new ArrayList<HashMap<String, String>>();
                HashMap<String, String> map;

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    map = new HashMap<String, String>();
                    map.put("product_id", json_data.getString("product_id"));
                    map.put("product_name", json_data.getString("product_name"));
                    map.put("product_detail", json_data.getString("product_detail"));
                    map.put("product_price", json_data.getString("product_price"));
                    MyArrList.add(map);


                }

                lisView1.setAdapter(new CountryAdapter(StockUpdateAddActivity.this));


            } catch (JSONException e) {
                Toast.makeText(StockUpdateAddActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }


    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    public class CountryAdapter extends BaseAdapter {
        private Context context;

        public CountryAdapter(Context c) {
            //super( c, R.layout.activity_column, R.id.rowTextView, );
            // TODO Auto-generated method stub
            context = c;
        }

        public int getCount() {
            // TODO Auto-generated method stub
            return MyArrList.size();
        }

        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.product_exp_add_row, null);

            }

            // ColID
            TextView txtID = (TextView) convertView.findViewById(R.id.ColID);
            txtID.setText(MyArrList.get(position).get("product_id") + ".");

            // ColCode
            TextView txtCode = (TextView) convertView.findViewById(R.id.ColCode);
            txtCode.setText(MyArrList.get(position).get("product_name"));

            // ColCountry
            EditText input_qty = (EditText) convertView.findViewById(R.id.input_qty);
//            txtCountry.setText(MyArrList.get(position).get("status"));

            // ColChk
//            CheckBox Chk = (CheckBox) convertView.findViewById(R.id.ColChk);
//            Chk.setTag(MyArrList.get(position).get("product_id"));

            return convertView;

        }

    }


    private class ProductExpAddTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(StockUpdateAddActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลังบันทึก...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();
//
//            $product_id = $this->post("product_id");
//            $store_id = $this->post("store_id");
//            $create_by = $this->post("userid");
//            $branch_id = $this->post("branch_id");
//            $product_qty = $this->post("product_qty");
//            $product_price = $this->post("product_price");

            RequestBody formBody = new FormEncodingBuilder()
                    .add("product_id", urls[1])
                    .add("product_qty", urls[2])
                    .add("product_price", urls[3])
                    .add("store_id", Profile.StoreId)
                    .add("branch_id", Profile.BranchId)
                    .add("userid", Profile.UserId)
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
//                if (status.equals("0")) {
//                    String msg = object.getString("msg"); // success
//                    AlertDialog.Builder builder = new AlertDialog.Builder(ProductExpAddActivity.this);
//                    builder.setMessage(msg)
//                            .setCancelable(false)
//                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    //do things
//                                }
//                            });
//                    AlertDialog alert = builder.create();
//                    alert.show();
//                } else {
//                    String msg = object.getString("msg"); // success
//                    AlertDialog.Builder builder = new AlertDialog.Builder(ProductExpAddActivity.this);
//                    builder.setMessage(msg)
//                            .setCancelable(false)
//                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    //do things
////                                    Intent in = new Intent(ProductExpAddActivity.this, TableActivity.class);
////                                    startActivity(in);
////                                    finish();
//                                }
//                            });
//                    AlertDialog alert = builder.create();
//                    alert.show();
//                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private void showData(String jsonString) {
        Toast.makeText(this, jsonString, Toast.LENGTH_LONG).show();
    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }
}
