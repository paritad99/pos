package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.PictureDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.InputFilter;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CashierActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    private List<FeedItemOrder> feedsListOrder;
    String input_amount = "";
    String input_discount = "";

    Button btnSelectTableCustomer, btnBillMerge, btnCustomDiscount,btnPrintbill, btnDiscount, btnCash, btnCancelTable;

    public int total_qty = 0;
    public int sum_amount = 0;
    TextView tvTableName, tvTotal_qty, tvSum_amount, tvDiscount,txt_receive_total;

    private OrderRecyclerAdapter OrdermAdapter;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    static ArrayList<HashMap<String, String>> MyArrList;
    static ListView lisView1;
    static TextView txtShowChoice;

    static ProgressBar progressBar;

    Profile profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cashier);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // add back arrow to toolbar
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }



        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        profile = new Profile(this);

        tvTotal_qty = (TextView) findViewById(R.id.tvItemQty);
        tvSum_amount = (TextView) findViewById(R.id.tvSumAmount);
        tvDiscount = (TextView) findViewById(R.id.tvDiscount);
        tvTableName = (TextView) findViewById(R.id.tvTableName);
        txt_receive_total = (TextView) findViewById(R.id.txt_receive_total);


        btnCustomDiscount = (Button) findViewById(R.id.btnCustomDiscount);
        btnCustomDiscount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(CashierActivity.this, DiscountActivity.class);
                startActivity(in);

            }
        });



        Button btnMoveTable = (Button) findViewById(R.id.btnMoveTable);
        btnMoveTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(CashierActivity.this, MoveTableActivity.class);
                startActivity(in);
            }
        });

        btnCancelTable = (Button) findViewById(R.id.btnCancelTable);
        btnCancelTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                AlertDialog.Builder builder = new AlertDialog.Builder(CashierActivity.this);
                builder.setTitle(StringUtil.TableCustomerId+"#กรุณาระบุเหตุผลการยกเลิก\n(ความยาวไม่เกิน 120 ตัวอักษร)");
                final EditText input = new EditText(CashierActivity.this);
                input.setFilters(new InputFilter[] { new InputFilter.LengthFilter(120) });
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        m_Text = ;

                        if(input.length() < 5)
                        {
                            AlertError("กรุณาระบุเหตุผลของการยกเลิก");
                            dialog.cancel();

                        }else
                        {
                            new TableCancelOrderTask().execute(StringUtil.URL + "gpos/api/TableCancelOrder",input.getText().toString() );

                        }


                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();


            }
        });

        btnBillMerge = (Button) findViewById(R.id.btnBillMerge);
        btnBillMerge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 Intent in = new Intent(CashierActivity.this, MergeBillActivity.class);
//                    in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(in);
//                    finish();

            }
        });

        btnSelectTableCustomer = (Button) findViewById(R.id.btnSelectTableCustomer);
        btnSelectTableCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(profile.Position.equals("4"))
                {
                    Intent in = new Intent(CashierActivity.this, StaffCashireActivity.class);
                    in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(in);
                    finish();

                }else
                {
                   finish();
                }

            }
        });

        btnPrintbill = (Button) findViewById(R.id.btnPrintbill);
        btnPrintbill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(CashierActivity.this,ShowOrderPrintActivity.class);
                startActivity(in);

            }
        });


        btnDiscount = (Button) findViewById(R.id.btnDiscount);
        btnDiscount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(CashierActivity.this, PromotionActivity.class);
                startActivity(in);

            }
        });

        btnCash = (Button) findViewById(R.id.btnCash);
        btnCash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(CashierActivity.this, PaymentActivity.class);
                startActivity(in);

            }
        });


        recyclerView = (RecyclerView) findViewById(R.id.rvOrders);
    }




    private class TableCancelOrderTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(CashierActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tTable Cancel Order in progress...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("remain",  urls[1])
                    .add("order_id", StringUtil.OrderId )
                    .add("create_by",  profile.UserId)
                    .add("table_id",  StringUtil.TableCustomerId)
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(CashierActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(CashierActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    Intent in = new Intent(CashierActivity.this
                                            , StaffCashireActivity.class);
                                    in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                    startActivity(in);
                                    finish();

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public void AlertError(String msg)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(CashierActivity.this);
        builder.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(StringUtil.is_printer ==1 )
        {
            btnCash.setEnabled(true);
        } else
        {
            btnCash.setEnabled(false);
        }


        tvTableName.setText(StringUtil.TableCustomerName);
        new AsyncOrder().execute();

    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    private class AsyncOrder extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(CashierActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {

                response = http.run(StringUtil.URL + "gpos/api/DeliveryOrderActivity/"+ StringUtil.OrderId) ;//+ StringUtil.OrderId)
//                response = http.run(StringUtil.URL+"gpos/api/ListOrderItems/29");

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            //txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread
            pdLoading.dismiss();


            //List<DataFish> data=new ArrayList<>();

            int x = 0;
            int total_sale = 0;
            double total_sale_discount = 0.0;
            double receive_total = 0.0;

            feedsListOrder = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);

                int qty = 0;
                int price = 0;
                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++)
                {

                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemOrder item = new FeedItemOrder();
                    item.setOrders_item_id(json_data.getString("orders_item_id"));
                    item.setId(json_data.getString("product_id"));
                    item.setTitle(json_data.getString("product_name"));
                    item.setDetail(json_data.getString("product_detail"));
                    item.setPrice(json_data.getString("total_price"));
                    item.setQty(json_data.getString("qty"));
                    item.setOrderid(json_data.getString("orders_id"));
                    item.setProduct_choice(json_data.getString("product_choice_name"));
                    item.setProduct_image(json_data.getString("product_image"));
                    item.setChoice_group_name(json_data.getString("choice_group_name"));

                    try {
                        qty = Integer.parseInt(json_data.getString("qty"));
                        price = Integer.parseInt(json_data.getString("total_price"));

                        x = x + qty;
                        total_sale = total_sale +  price;

                        if(StringUtil.promotion_discount_percen_type.equals("เปอร์เซ็น")){
                        }else
                        {
//                            StringUtil.promotion_discount_percen_type = feedItem.getPromotion_type();
//                            StringUtil.promotion_discount_percen_total = Integer.parseInt(feedItem.getPromotion_qty());
                        }

//                        int dis = StringUtil.promotion_discount_percen_total;
//                        total_sale_discount  = Discount_Percen(total_sale,dis);
//                        receive_total = total_sale - total_sale_discount;
//
//                        StringUtil.discount_total_payment = total_sale_discount+"";

                        if(StringUtil.dicount_type.equals("เปอร์เซ็น"))
                        {
                            try {
                                double total = Double.parseDouble(total_sale+"");
                                int dis = StringUtil.dicount_total;
//                                double result = (total * dis) / 100;
                                StringUtil.discount_total_payment = ""+(total * dis) / 100;

                            }catch (Exception e)
                            {
                                StringUtil.discount_total_payment = e+"";
                            }

                        }else
                        {

                            try {

                                double total = Double.parseDouble(total_sale+"");
                                int dis = StringUtil.dicount_total;
//                                double result = total - dis;
                                StringUtil.discount_total_payment = ""+(total - dis);

                            }catch (Exception e)
                            {
                                StringUtil.discount_total_payment = e+"";
                            }


                        }


                    } catch (Exception e) {
                        Toast.makeText(CashierActivity.this, e.toString(), Toast.LENGTH_LONG).show();
                    }


                    feedsListOrder.add(item);
                }


                StringUtil.TotalPayment =  StringUtil.discount_total_payment + "";

                tvSum_amount.setText("ราคาสินค้ารวม " + total_sale+" บาท");
                tvTotal_qty.setText("รวม "+x + "รายการ");
                tvDiscount.setText("ส่วนลด "+  StringUtil.dicount_total + " "+StringUtil.dicount_type);
                txt_receive_total.setText("ยอดรวมสุทธิ "+ StringUtil.discount_total_payment+ " บาท");



                OrdermAdapter = new OrderRecyclerAdapter(CashierActivity.this, feedsListOrder);
                recyclerView.setAdapter(OrdermAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(CashierActivity.this, 1);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());

            } catch (JSONException e) {



                Toast.makeText(CashierActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    public double Discount_Percen(int price,int discount)
    {
        double res  = 0.0;

        res = (discount * price) / 100;

        return  res;
    }

    // Adapter
    public class OrderRecyclerAdapter extends RecyclerView.Adapter<OrderRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemOrder> feedsListOrder;
        private Context mContext;

        public OrderRecyclerAdapter(Context context, List<FeedItemOrder> feedItemList) {
            this.feedsListOrder = feedItemList;
            this.mContext = context;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.content_cashier_order_row, null);

            CustomViewHolder viewHolder = new CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(final CustomViewHolder customViewHolder, int i) {
            final FeedItemOrder feedItem = feedsListOrder.get(i);
            customViewHolder.feedItem = feedItem;

//            try {
            int qty = Integer.parseInt(feedItem.getQty());
            int price = Integer.parseInt(feedItem.getPrice());

            int sum = qty * price;

            //Setting text view title
            customViewHolder.textView.setText("สินค้า: " + feedItem.getTitle());
            customViewHolder.txt_price.setText( price+"฿");
//                customViewHolder.price.setText("ราคา/หน่วย: " + price);
            customViewHolder.txt_order_qty.setText("" + feedItem.getQty());
            customViewHolder.txt_choice_name.setText("Choice : "+feedItem.getChoice_group_name() +" : "+ feedItem.getProduct_choice());


        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedsListOrder ? feedsListOrder.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            protected ImageView imageView;
            protected TextView textView;
            protected TextView txt_price;
            protected TextView txt_choice_name,txt_order_qty;
            FeedItemOrder feedItem;

            public CustomViewHolder(View view) {
                super(view);
//                this.imageView = (ImageView) view.findViewById(R.id.imageView2);
                this.txt_choice_name = (TextView) view.findViewById(R.id.txt_choice_name);
                this.textView = (TextView) view.findViewById(R.id.txt_menu_name);
                this.txt_price = (TextView) view.findViewById(R.id.txt_price);
                this.txt_order_qty = (TextView) view.findViewById(R.id.txt_order_qty);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        StringUtil.Orders_item_id = feedItem.getOrders_item_id();
                        StringUtil.OrderId = feedItem.getOrderid();

//                        LayoutInflater layoutinflater = LayoutInflater.from(OpenTableOrderActivity.this);
//                        View promptUserView = layoutinflater.inflate(R.layout.content_product_qty, null);
//
//                        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(OpenTableOrderActivity.this);

//                        alertDialogBuilder.setView(promptUserView);
//                        TextView tvTitle = (TextView) promptUserView.findViewById(R.id.tvTitle);
//                        tvTitle.setText(feedItem.getTitle());

//                        final EditText input_product_qty = (EditText) promptUserView.findViewById(R.id.input_product_qty);


                    }
                });

            }
        }
    }

    // String
    public class FeedItemOrder {
        private String title;
        private String id;
        private String setDetail;
        private String price;
        private String qty;
        private String PricePerItem;
        private String orders_item_id;
        private String product_image;
        private String product_choice;
        private String choice_group_name;

        public String getChoice_group_name() {
            return choice_group_name;
        }

        public void setChoice_group_name(String choice_group_name) {
            this.choice_group_name = choice_group_name;
        }

        public String getProduct_choice() {
            return product_choice;
        }

        public void setProduct_choice(String product_choice) {
            this.product_choice = product_choice;
        }


        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }

        private String orderid;

        public String getOrderid() {
            return orderid;
        }

        public void setOrderid(String orderid) {
            this.orderid = orderid;
        }

        public String getOrders_item_id() {
            return orders_item_id;
        }

        public void setOrders_item_id(String orders_item_id) {
            this.orders_item_id = orders_item_id;
        }

        public String getPricePerItem() {
            return PricePerItem;
        }

        public void setPricePerItem(String pricePerItem) {
            this.PricePerItem = pricePerItem;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getPrice() {
            return price;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String name) {
            this.title = name;
        }


        public String getDetail() {
            return setDetail;
        }

        public void setDetail(String Detail) {
            this.setDetail = Detail;
        }


    }

}
