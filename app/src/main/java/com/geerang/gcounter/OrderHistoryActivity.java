package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import com.epson.epos2.printer.Printer;
import com.epson.epos2.printer.PrinterStatusInfo;
import com.epson.epos2.printer.ReceiveListener;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.IBinder;
import android.os.StrictMode;
import android.text.InputFilter;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection;
import recieptservice.com.recieptservice.PrinterInterface;

public class OrderHistoryActivity extends AppCompatActivity implements ReceiveListener {
    int check_receipt = 0, payment_type = 1;
    private Context mContext = null;
    public static EditText mEditTarget = null;
    public static Spinner mSpnSeries = null;
    public static Spinner mSpnLang = null;
    public static Printer mPrinter = null;
    public static ToggleButton mDrawer = null;

    EpsonMakeErrorMessage epsonMakeErrorMessage;
    final String KEY_Autoprint = "Autoprint";
    final String KEY_PrinterMain = "PrinterMain";
    final String KEY_Printer1 = "Printer1";
    final String KEY_Printer2 = "Printer2";
    final String KEY_Printer3 = "Printer3";
    final String KEY_Printer4 = "Printer4";

    Profile profile;
    IBinder service;
    PrinterInterface mAidl;
    String Resultbill;
    String PrinterAddress = "";
    RecyclerView mRecyclerView;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    TextView tvTableName, tvSum_Amount, txtItem_Qty, txtCustomer, txt_status;
    Button btnSelectMenu, btnCancel, btnGotoHome, btnDeliveryOrder;
    private SectionedRecyclerViewAdapter sectionAdapter;
    ArrayList<HashMap<String, String>> MyArrList_GroupChoice;

    ArrayList<HashMap<String, String>> ChoiceArrList = new ArrayList<HashMap<String, String>>();
    HashMap<String, String> map_choice;
    private SimpleAdapter mFirstHeader;

    int total_price = 0;
    int total_qty = 0;
    int check_order_place = 0;
    double vat_data = 0.0;
    double vat_total = 0.0;

    double total_payment = 0.0;
    double total_discount = 0.0;

    double total_sum = 0.0;
    final String PREF_NAME = "PrinterPreferences";
    final String KEY_Vat = "Vat";
    double total_service_charge;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    Button btnGotoCheckOut;
    int total_qty_check = 0;
    private CoordinatorLayout coordinatorLayout;

    Button btnPrint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        epsonMakeErrorMessage = new EpsonMakeErrorMessage(this);
        sp = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sp.edit();

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        profile = new Profile(this);

        btnPrint = (Button) findViewById(R.id.btnPrint);
        btnPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               // Toast.makeText(OrderHistoryActivity.this,"Printing...",Toast.LENGTH_SHORT).show();
                new AsyncChoiceProductPrint().execute();
            }
        });


        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);

        tvTableName = (TextView) findViewById(R.id.tvTableName);
        tvSum_Amount = (TextView) findViewById(R.id.tvSum_Amount);
        txtItem_Qty = (TextView) findViewById(R.id.txtItem_Qty);

        txtCustomer = (TextView) findViewById(R.id.txtCustomer);
        txtCustomer.setText(StringUtil.TotalCustomer);


        txt_status = (TextView) findViewById(R.id.txt_status);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        btnSelectMenu = (Button) findViewById(R.id.btnSelectMenu);
        btnSelectMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (StringUtil.payment_status.equals("CHECKOUT")) {

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "ออเดอร์กำลัง Check out ไม่สามารถเพิ่มออเดอร์ได้", Snackbar.LENGTH_LONG)
                            .setAction("OK", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                }
                            });
                    snackbar.show();

                } else {

                    Intent in = new Intent(OrderHistoryActivity.this, MenuCatagoriesActivity.class);
                    startActivity(in);
                }


            }
        });


        btnGotoCheckOut = (Button) findViewById(R.id.btnGotoCheckOut);
        btnGotoCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(OrderHistoryActivity.this, OrderCheckoutActivity.class);
                startActivity(in);

            }
        });


        btnDeliveryOrder = (Button) findViewById(R.id.btnCashireMethod);
        btnDeliveryOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                if (total_qty_check == 0) {
//                    AlertError("กรุณาเลือกสินค้า/บริการ ก่อนการชำระเงิน");
//                } else {
//                    Intent in = new Intent(OpenTableOrderActivity.this, CashierActivity.class);
//                    startActivity(in);

                AlertDialog.Builder builder = new AlertDialog.Builder(OrderHistoryActivity.this);
                builder.setTitle(StringUtil.TableCustomerId + "#กรุณายืนยันการส่งออเดอร์");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        new PlaceOrderConfirmTask().execute(StringUtil.URL + "gpos/api/ConfirmOrderItem", StringUtil.OrderId);

                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();


//                }

            }
        });


        if (StringUtil.payment_status.equals("CHECKOUT")) {

            btnDeliveryOrder.setVisibility(View.GONE);
            btnGotoCheckOut.setVisibility(View.GONE);

        }

        btnDeliveryOrder.setVisibility(View.GONE);
        btnGotoCheckOut.setVisibility(View.GONE);

        btnGotoHome = (Button) findViewById(R.id.btnGotoHome);
        btnGotoHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(OrderHistoryActivity.this);
                builder.setTitle(StringUtil.TableCustomerId + "#กรุณาระบุเหตุผลการยกเลิก\n(ความยาวไม่เกิน 120 ตัวอักษร)");
                final EditText input = new EditText(OrderHistoryActivity.this);
                input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(120)});
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (input.length() < 5) {
                            AlertError("กรุณาระบุเหตุผลของการยกเลิก");
                            dialog.cancel();

                        } else {
                            new TableCancelOrderTask().execute(StringUtil.URL + "gpos/api/TableCancelOrder", input.getText().toString());

                        }

                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();

            }
        });
    }

    public void AlertError(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(OrderHistoryActivity.this);
        builder.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things

                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        tvTableName.setText(StringUtil.TableCustomerName);
        new AsyncChoiceProduct().execute();


    }


    private class AsyncChoiceProduct extends AsyncTask<String, String, String> {


        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run("http://www.geerang.com/gpos/api/MenuOrderList/" + StringUtil.OrderId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {
            try {
//                progressBar.setVisibility(View.GONE);

                txt_status.setVisibility(View.GONE);

                total_price = 0;
                total_qty = 0;

                JSONArray jArray = new JSONArray(result);
                sectionAdapter = new SectionedRecyclerViewAdapter();
                for (int i = 0; i < jArray.length(); i++) {

                    total_qty_check++;

                    JSONObject json_data = jArray.getJSONObject(i);
                    //ประกาศ array string
                    ArrayList<String> arrSports = new ArrayList<>();
                    // array hasmap
                    MyArrList_GroupChoice = new ArrayList<HashMap<String, String>>();
                    HashMap<String, String> map;


                    int total_row_qty = 0;
                    total_row_qty = Integer.parseInt(json_data.getString("qty"));

                    String confirm = json_data.getString("is_confirm");
                    // เก็บตัวแปร Choice
                    String Choice = "";
                    int total_row_price = 0;
                    // เก็บราคาใน order
                    total_row_price = Integer.parseInt(json_data.getString("product_sale_price"));

                    int order_place = Integer.parseInt(confirm);
                    if (order_place == 0) {
                        check_order_place++;
                    }


                    JSONArray jArray2 = new JSONArray(json_data.getString("product_choice"));
                    int check_list = jArray2.length();
                    if (check_list > 0) {
                        for (int j = 0; j < jArray2.length(); j++) {

                            JSONObject json_data2 = jArray2.getJSONObject(j);
                            arrSports.add(json_data2.getString("product_choice_name"));

                            // หาราคารวมของแต่ละ row แล้วนำไปยัดใส่ใน header row
                            int choice_price = Integer.parseInt(json_data2.getString("choice_price"));
                            total_row_price += choice_price;

                            Choice += json_data2.getString("product_choice_name") + ",";

                            map = new HashMap<String, String>();
                            map.put("product_choice_name", json_data2.getString("product_choice_name"));
                            map.put("orders_item_group_id", json_data2.getString("orders_item_group_id"));
                            map.put("choice_price", json_data2.getString("choice_price"));
                            MyArrList_GroupChoice.add(map);
                        }
                    }


//                        int choice_price = Integer.parseInt(MyArrList_choice.get(position).get("choice_price"));
                    total_price = total_price + (total_row_price * total_row_qty);
                    total_qty = total_qty + total_row_qty;

                    mFirstHeader = new SimpleAdapter(MyArrList_GroupChoice
                            , "" + json_data.getString("product_name")
                            , (total_row_price * total_row_qty) + ""
                            , json_data.getString("qty") + ""
                            , json_data.getString("orders_item_id")
                            , Choice
                            , confirm
                            , json_data.getString("remain"));
                    sectionAdapter.addSection(mFirstHeader);

                }

                txtItem_Qty.setText(total_qty + "");
                tvSum_Amount.setText(total_price + "");


                StringUtil.TotalOrderQty = total_qty+"";

//                mRecyclerView.setLayoutManager(glm);
//
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(OrderHistoryActivity.this, LinearLayoutManager.VERTICAL, false);
                mRecyclerView.setLayoutManager(linearLayoutManager);
                mRecyclerView.setAdapter(sectionAdapter);


            } catch (JSONException e) {
                txt_status.setVisibility(View.VISIBLE);
                Toast.makeText(OrderHistoryActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }


    public class SimpleAdapter extends StatelessSection {

        //        private ArrayList<String> arrName = new ArrayList<>();
        ArrayList<HashMap<String, String>> MyArrList_choice = new ArrayList<HashMap<String, String>>();
        private String mTitle, Choice;
        private int price;
        private int qty;
        private String confirm;
        private String comment;
        private int select_item_count = 0;
        private int total_price_row;
        private String orders_item_id;

        public SimpleAdapter(ArrayList<HashMap<String, String>> arrName
                , String title, String price, String qty
                , String orders_item_id
                , String Choice
                , String confirm, String comment) {
            super(R.layout.layout_menu_order_item_header, R.layout.layout_menu_order_item);

            this.MyArrList_choice = arrName;
            this.mTitle = title;
            this.price = Integer.parseInt(price);
            this.qty = Integer.parseInt(qty);
            this.Choice = Choice;
            this.confirm = confirm;
            this.orders_item_id = orders_item_id;
            this.comment = comment;


        }

        @Override
        public int getContentItemsTotal() {
            return MyArrList_choice.size();
        }

        @Override
        public RecyclerView.ViewHolder getItemViewHolder(View view) {
            return new ItemViewHolder(view);
        }

        @Override
        public RecyclerView.ViewHolder getHeaderViewHolder(View view) {
            return new HeaderViewHolder(view);
        }

        @Override
        public void onBindItemViewHolder(RecyclerView.ViewHolder holder, final int position) {
            final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
            itemViewHolder.txtItem.setText("*" + MyArrList_choice.get(position).get("product_choice_name"));
            itemViewHolder.txtPrice.setText("+฿" + MyArrList_choice.get(position).get("choice_price"));
//            itemViewHolder.checkBox.setTag(MyArrList_choice.get(position).get("Code"));

        }

        @Override
        public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder) {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
            headerViewHolder.txtHeader.setText(mTitle);
            headerViewHolder.txtPrice.setText("฿" + (price));
            headerViewHolder.txtComment.setText(comment);

            headerViewHolder.txtItemQty.setText(qty + "");

            headerViewHolder.txtDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(OrderHistoryActivity.this);
                    builder.setTitle(StringUtil.TableCustomerId + "#ยืนยันการลบข้อมูล!\nกรุณาระบุเหตุผลการขอลบ\n(ความยาวไม่เกิน 120 ตัวอักษร)");
                    final EditText input = new EditText(OrderHistoryActivity.this);
                    input.setTextSize(14);
                    input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(120)});
                    input.setInputType(InputType.TYPE_CLASS_TEXT);
                    builder.setView(input);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            new ItemDeleteTask().execute(StringUtil.URL + "gpos/api/OrderItemDelete"
                                    , input.getText().toString()
                                    , orders_item_id);

                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    builder.show();


                }
            });
//            headerViewHolder.linearLayout1.Set
            if (!Choice.isEmpty()) {
                headerViewHolder.txtChoice.setText(Choice.substring(0, Choice.length() - 1) + "");
            } else {
                headerViewHolder.txtChoice.setVisibility(View.GONE);
            }


            headerViewHolder.btnReduce.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int sum_qty = qty - 1;
                    new ItemQtyUpdateTask().execute(StringUtil.URL + "gpos/api/OpenTable_UpdateOrderQty"
                            , orders_item_id, sum_qty + "");

                    new AsyncChoiceProduct().execute();
                }
            });

            headerViewHolder.btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int sum_qty = qty + 1;
                    new ItemQtyUpdateTask().execute(StringUtil.URL + "gpos/api/OpenTable_UpdateOrderQty"
                            , orders_item_id, sum_qty + "");

                    new AsyncChoiceProduct().execute();
                }
            });

            headerViewHolder.btnAddComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(OrderHistoryActivity.this);
                    builder.setTitle(StringUtil.TableCustomerId + "#กรุณาระบุหมายเหตุ\n(ความยาวไม่เกิน 120 ตัวอักษร)");
                    final EditText input = new EditText(OrderHistoryActivity.this);
                    input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(120)});
                    input.setInputType(InputType.TYPE_CLASS_TEXT);
                    builder.setView(input);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                            new AddRecommentOrderTask().execute(StringUtil.URL + "gpos/api/OrderRecomment", input.getText().toString(), orders_item_id);


                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    builder.show();
                }
            });


            // show status
            if (confirm.equals("1")) {

                headerViewHolder.btnAddComment.setVisibility(View.GONE);
                headerViewHolder.btnAdd.setVisibility(View.GONE);
                headerViewHolder.btnReduce.setVisibility(View.GONE);
                headerViewHolder.txtConfirStatus.setVisibility(View.VISIBLE);
                // show button + -
            } else {
//                headerViewHolder.btnAddComment.setVisibility(View.VISIBLE);
//                headerViewHolder.btnAdd.setVisibility(View.VISIBLE);
//                headerViewHolder.btnReduce.setVisibility(View.VISIBLE);
//                headerViewHolder.txtConfirStatus.setVisibility(View.GONE);
//                headerViewHolder.txtDelete.setVisibility(View.GONE);

                headerViewHolder.btnAddComment.setVisibility(View.GONE);
                headerViewHolder.btnAdd.setVisibility(View.GONE);
                headerViewHolder.btnReduce.setVisibility(View.GONE);
                headerViewHolder.txtConfirStatus.setVisibility(View.VISIBLE);

            }

//            txtConfirStatus


        }

        protected class HeaderViewHolder extends RecyclerView.ViewHolder {

            protected TextView txtHeader, txtDelete;
            protected TextView txtPrice, txtChoice, txtItemQty, txtConfirStatus, txtComment;

            protected Button btnReduce, btnAdd, btnAddComment;

            private HeaderViewHolder(View itemView) {
                super(itemView);
                txtDelete = (TextView) itemView.findViewById(R.id.txtDelete);

                txtHeader = (TextView) itemView.findViewById(R.id.txtHeader);
                txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
                txtItemQty = (TextView) itemView.findViewById(R.id.txtItemQty);
                txtConfirStatus = (TextView) itemView.findViewById(R.id.txtConfirStatus);
                txtComment = (TextView) itemView.findViewById(R.id.txtRemain);

                txtChoice = (TextView) itemView.findViewById(R.id.txtChoice);
                btnReduce = (Button) itemView.findViewById(R.id.btnReduce);
                btnAdd = (Button) itemView.findViewById(R.id.btnAdd);
                btnAddComment = (Button) itemView.findViewById(R.id.btnAddComment);


            }
        }

        protected class ItemViewHolder extends RecyclerView.ViewHolder {

            protected TextView txtItem, txtPrice;
            protected LinearLayout linearLayout1;

            //            protected CheckBox checkBox;
            private ItemViewHolder(View itemView) {
                super(itemView);
                txtItem = (TextView) itemView.findViewById(R.id.txtChoice);
                txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
                linearLayout1 = (LinearLayout) itemView.findViewById(R.id.linearLayout1);
                linearLayout1.setVisibility(View.GONE);

//                checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);


            }
        }
    }

    private class PlaceOrderConfirmTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(OrderHistoryActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลังส่งข้อมูล...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("orders_id", urls[1])
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);

            new AsyncChoiceProduct().execute();

            pdLoading.dismiss();


            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class ItemDeleteTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(OrderHistoryActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลังลบข้อมูล...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("remark", urls[1])
                    .add("orders_item_id", urls[2])
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);

            new AsyncChoiceProduct().execute();

            pdLoading.dismiss();


            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class ItemQtyUpdateTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(OrderHistoryActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tUpdating...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("orders_item_id", urls[1])
                    .add("qty", urls[2])
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);

            new AsyncChoiceProduct().execute();

            pdLoading.dismiss();


            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class AddRecommentOrderTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(OrderHistoryActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลังบันทึก...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("comment", urls[1])
                    .add("orders_item_id", urls[2])
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(OrderHistoryActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(OrderHistoryActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    new AsyncChoiceProduct().execute();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
    @Override
    public void onPtrReceive(final Printer printerObj, final int code, final PrinterStatusInfo status, final String printJobId) {
        runOnUiThread(new Runnable() {
            @Override
            public synchronized void run() {
              //  ShowMsg.showResult(code, epsonMakeErrorMessage.makeErrorMessage(status), OrderHistoryActivity.this);
                //epsonMakeErrorMessage.dispPrinterWarnings(status);
                disconnectPrinter();
            }
        });
    }

    private void disconnectPrinter() {
        if (mPrinter == null) {
            return;
        }

        try {
            mPrinter.endTransaction();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                  //  ShowMsg.showException(e, "endTransaction", OrderHistoryActivity.this);
                }
            });
        }

        try {
            mPrinter.disconnect();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                  //  ShowMsg.showException(e, "disconnect", OrderHistoryActivity.this);
                }
            });
        }

        finalizeObject();
    }
    private class AsyncChoiceProductPrint extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(OrderHistoryActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //this method will be running on UI thread

            pdLoading.setMessage("\tกำลังโหลดข้อมูล...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            OrderHistoryActivity.getHttp http = new OrderHistoryActivity.getHttp();
            try {
                response = http.run("http://www.geerang.com/gpos/api/ReceiptHistory/" + profile.StoreId + "/" + StringUtil.OrderId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {
            Resultbill = result;
            pdLoading.dismiss();

            editor = sp.edit();
            String PrinterMain = sp.getString(KEY_PrinterMain, "");
            if (!PrinterMain.equals("")) {
                //OpenPrinter+","+printer_title+","+printer_address+","+printer_mac_address+","+categories_id
                String[] namesList = PrinterMain.split(",");
                String status = namesList[0];
                String title = namesList[1];
                String address = namesList[2];
                String mac_address = namesList[3];
                String categories_id = namesList[4];

                PrinterAddress = address;
                //เลือกเครื่องปริ้นที่กำหนด
                if (!runPrintReceiptSequence()) {

                }


            } else {
                try {

                    mAidl.setAlignment(1);
                    mAidl.printText("ใบเสร็จรับเงิน / RECEIPT\n");
                    mAidl.printText("\n");
                    mAidl.setTextSize(20);
                    mAidl.printTableText(new String[]{"Invoice No." + StringUtil.OrderId, "Order Type. " + "Dine In "}, new int[]{1, 1}, new int[]{0, 0});
                    mAidl.printTableText(new String[]{"Invoice By." + profile.Username, "Table." + "" + StringUtil.TableCustomerName}, new int[]{1, 1}, new int[]{0, 0});
                    mAidl.printTableText(new String[]{"Staff." + profile.Username, getDateTime() + ""}, new int[]{1, 1}, new int[]{0, 0});
                    mAidl.printText("------------------------------------\n");
                    mAidl.printTableText(new String[]{"", "Qty", "Price", "Total"}, new int[]{1, 1, 1, 1}, new int[]{0, 2, 2, 2});
                    mAidl.setAlignment(1);
                    mAidl.printText("------------------------------------\n");

                    //columns.print();
                    total_price = 0;
                    total_qty = 0;

                    double sum_price = 0.0;
                    JSONArray jArray = null;
                    try {

                        double sum_row_price = 0.0;
                        jArray = new JSONArray(Resultbill);
                        // loop ของ report category
                        for (int f = 0; f < jArray.length(); f++) {
                            JSONObject json_data_report = jArray.getJSONObject(f);

                            JSONArray jArray_menu = new JSONArray(json_data_report.getString("products"));
                            //textData.append("------------------------------\n");
                            mAidl.setAlignment(0);
                            mAidl.setTextSize(18);
                            mAidl.printText(json_data_report.getString("report_cat_name") + "\n");

                            if (jArray_menu.length() > 0) {
                                // loop ของ products
                                int item_count_row = 0;
                                int total_row_qty = 0;
                                double total_price = 0.0;
                                double total_price_by_cat = 0.0;

                                int total_row_price = 0;
                                for (int i = 0; i < jArray_menu.length(); i++) {

                                    JSONObject json_data = jArray_menu.getJSONObject(i);
                                    total_row_qty = Integer.parseInt(json_data.getString("qty"));
                                    item_count_row += total_row_qty;
                                    String confirm = json_data.getString("is_confirm");
                                    // เก็บตัวแปร Choice
                                    String Choice = "";

                                    // เก็บราคาใน order
                                    total_row_price = Integer.parseInt(json_data.getString("product_sale_price"));

                                    int order_place = Integer.parseInt(confirm);
                                    if (order_place == 0) {
                                        check_order_place++;
                                    }

                                    // loop choice
                                    JSONArray jArray2 = new JSONArray(json_data.getString("product_choice"));
                                    int check_list = jArray2.length();
                                    if (check_list > 0) {
                                        for (int j = 0; j < jArray2.length(); j++) {
                                            JSONObject json_data2 = jArray2.getJSONObject(j);
                                            // หาราคารวมของแต่ละ row แล้วนำไปยัดใส่ใน header row
                                            int choice_price = Integer.parseInt(json_data2.getString("choice_price"));
                                            total_row_price += choice_price;
                                            Choice += json_data2.getString("product_choice_name") + ",";
                                        }
                                    }

                                    total_price =  total_row_price;
                                    total_qty = total_qty + total_row_qty;

                                    // หาผลร่วมระหว่าง หมวดหมู่
                                    total_price_by_cat += (total_price * total_row_qty);

                                    // หาผลร่วมระหว่าง ทั้งหมด
                                    sum_row_price = sum_row_price + total_price;
                                    String product_name = json_data.getString("product_name");

                                    mAidl.printTableText(new String[]{product_name, total_row_qty + "", String.format("%.02f", total_price) + "", String.format("%.02f", (total_price * total_row_qty)) + ""}, new int[]{1, 1, 1, 1}, new int[]{0, 2, 2, 2});

                                }

                                mAidl.printTableText(new String[]{"Total " + item_count_row + " items", String.format("%.02f", total_price_by_cat) + ""}, new int[]{1, 1}, new int[]{0, 2});
                                mAidl.setAlignment(1);
                                mAidl.printText("---------------------------------------\n");
                                // หาผลรวมทั้งหมด
                                sum_price += total_price_by_cat;

                                // reset ให้เป็น 0 เพื่อหา row ต่อไป
                                total_price_by_cat = 0;


                            } else {

                            }
                        }

                        mAidl.setAlignment(1);
                        mAidl.printText("---------------------------------------");
                        mAidl.nextLine(1);
                        mAidl.printTableText(new String[]{"Total " + total_qty, "", "", String.format("%.02f", sum_price) + ""}, new int[]{1, 1, 1, 1}, new int[]{0, 0, 0, 2});
                        mAidl.setAlignment(1);
                        mAidl.printText("\n");

                        try {
//                        Double total_price_result = 0.0;
//                        total_price_result = Double.parseDouble(sum_price+"")  - Double.parseDouble(StringUtil.discount_total_payment);


                            //// StringUtil.dicount_type StringUtil.dicount_total

                            // หาค่า ส่วนลด ว่าลดไปเท่าไร

                            double dis_res = sum_price - Double.parseDouble(StringUtil.TotalPayment);

                            mAidl.printTableText(new String[]{"SubTotal ", String.format("%.02f", sum_price) + ""}, new int[]{1, 1}, new int[]{0, 2});
                            mAidl.printTableText(new String[]{"Discount " + StringUtil.dicount_type, "" + String.format("%.02f", StringUtil.dicount_total)}, new int[]{1, 1}, new int[]{0, 2});
                            mAidl.printTableText(new String[]{"Total ", String.format("%.02f", StringUtil.discount_total_payment) + ""}, new int[]{1, 1}, new int[]{0, 2});
                            mAidl.nextLine(1);
                            mAidl.printText("\n");
                        } catch (Exception e) {
                            mAidl.printText(e.toString() + "\n");
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    mAidl.printText("\n");
                    mAidl.nextLine(2);


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }

    }

    private boolean runPrintReceiptSequence() {

        if (!initializeObject()) {
            return false;
        }

        if (!createReceiptData()) {
            finalizeObject();
            return false;
        }

        if (!printData()) {
            finalizeObject();
            return false;
        }

        return true;
    }


    private boolean printData() {
        if (mPrinter == null) {
            return false;
        }

        if (!connectPrinter()) {
            mPrinter.clearCommandBuffer();
            return false;
        }

        try {
            mPrinter.sendData(Printer.PARAM_DEFAULT);
        } catch (Exception e) {
            mPrinter.clearCommandBuffer();
           // ShowMsg.showException(e, "sendData", mContext);
            try {
                mPrinter.disconnect();
            } catch (Exception ex) {
                // Do nothing
            }
            return false;
        }

        return true;
    }

    private boolean connectPrinter() {
        if (mPrinter == null) {
            return false;
        }

        try {
            // mPrinter.connect("192.168.1.103", Printer.PARAM_DEFAULT);
            mPrinter.connect(PrinterAddress, Printer.PARAM_DEFAULT);

        } catch (Exception e) {
           // ShowMsg.showException(e, "connect", mContext);
            return false;
        }

        return true;
    }


    private void finalizeObject() {
        if (mPrinter == null) {
            return;
        }

        mPrinter.clearCommandBuffer();

        mPrinter.setReceiveEventListener(null);

        mPrinter = null;
    }

    private boolean initializeObject() {
        try {
            // ((SpnModelsItem) mSpnLang.getSelectedItem()).getModelConstant()
//            mPrinter = new Printer(((SpnModelsItem) mSpnSeries.getSelectedItem()).getModelConstant(),
            mPrinter = new Printer(Printer.TM_T88, Printer.MODEL_ANK, mContext);
            //

        } catch (Exception e) {
           // ShowMsg.showException(e, "Printer", mContext);
            return false;
        }

        mPrinter.setReceiveEventListener(this);

        return true;
    }

    private boolean createReceiptData() {
        String method = "";
        String RefNo = "";
        String payment_Amount = "";
        String payment_Change = "";
        String discount_type = "";
        int discount_data = 0;

        int service_data = 0;
        StringBuffer textData = new StringBuffer();
        final int barcodeWidth = 2;
        final int barcodeHeight = 100;

        if (mPrinter == null) {
            return false;
        }

        try {
            //语言需要
            mPrinter.addTextLang(Printer.LANG_TH);
            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            method = "addFeedLine";
            mPrinter.addFeedLine(1);
            textData.append("ใบเสร็จรับเงิน / RECEIPT\n");
            textData.append("\n");
            textData.append(String.format("%-20s %-20s \n", "Invoice No." + StringUtil.OrderId, "Order Type. " + "Dine In "));
            textData.append(String.format("%-20s %-20s \n", "Invoice By." + profile.Username, "Table." + "" + StringUtil.TableCustomerName));
            textData.append(String.format("%-20s %-20s \n", "Staff." + profile.Username, getDateTime() + ""));

            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            //columns.print();
            total_price = 0;
            total_qty = 0;
            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_RIGHT);
            textData.append("------------------------------------------\n");
            textData.append(String.format("%-15s %5s %5s %5s \n", " ", "Price", "Qty", "Total"));
            textData.append("------------------------------------------\n");
            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            double sum_price = 0.0;
            JSONArray jArray = null;
            try {


                double sum_row_price = 0.0;
                jArray = new JSONArray(Resultbill);
                // loop ของ report category
                for (int f = 0; f < jArray.length(); f++) {
                    JSONObject json_data_report = jArray.getJSONObject(f);

                    JSONArray jArray_menu = new JSONArray(json_data_report.getString("products"));
                    method = "addTextAlign";
                    mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                    textData.append(json_data_report.getString("report_cat_name") + "\n");


                    method = "addText";
                    mPrinter.addText(textData.toString());
                    textData.delete(0, textData.length());

                    if (jArray_menu.length() > 0) {

                        // loop ของ products
                        int item_count_row = 0;
                        int total_row_qty = 0;
                        double total_price = 0.0;
                        double total_price_by_cat = 0.0;
                        String line = "";
                        int total_row_price = 0;
                        for (int i = 0; i < jArray_menu.length(); i++) {

                            JSONObject json_data = jArray_menu.getJSONObject(i);
                            payment_type = json_data.getInt("payment_type");
                            discount_data = json_data.getInt("total_discount");
                            service_data = json_data.getInt("services");
                            if (payment_type == 1){
                                payment_Amount = json_data.getString("payment_amount");
                                payment_Change = json_data.getString("payment_change");
                            }
                            if (payment_type == 2){
                                RefNo = json_data.getString("creadit_card_ref");
                            }else if (payment_type == 3){
                                RefNo = json_data.getString("bank_ref_nbr");
                            }else{
                                RefNo = json_data.getString("qrcode_amount");
                            }
                            discount_type= json_data.getString("discount_type");
                            total_row_qty = Integer.parseInt(json_data.getString("qty"));
                            item_count_row += total_row_qty;
                            String confirm = json_data.getString("is_confirm");
                            // เก็บตัวแปร Choice
                            String Choice = "";

                            // เก็บราคาใน order
                            total_row_price = Integer.parseInt(json_data.getString("product_sale_price"));

                            int order_place = Integer.parseInt(confirm);
                            if (order_place == 0) {
                                check_order_place++;
                            }
                            // loop choice
                            JSONArray jArray2 = new JSONArray(json_data.getString("product_choice"));
                            int check_list = jArray2.length();
                            if (check_list > 0) {
                                for (int j = 0; j < jArray2.length(); j++) {
                                    JSONObject json_data2 = jArray2.getJSONObject(j);
                                    // หาราคารวมของแต่ละ row แล้วนำไปยัดใส่ใน header row
                                    int choice_price = Integer.parseInt(json_data2.getString("choice_price"));
                                    total_row_price += choice_price;
                                    Choice += json_data2.getString("product_choice_name") + ",";
                                }
                            }

                            total_price = total_row_price;
                            total_qty = total_qty + total_row_qty;
                            // หาผลร่วมระหว่าง หมวดหมู่
                            total_price_by_cat += (total_price * total_row_qty);

                            // หาผลร่วมระหว่าง ทั้งหมด
                            sum_row_price = sum_row_price + total_price;

                            String product_name = json_data.getString("product_name");
                            //mAidl.printTableText(new String[]{}, new int[]{ 1, 1, 1, 1}, new int[]{0, 2, 2, 2});
                            method = "addTextAlign";
                            mPrinter.addTextAlign(Printer.ALIGN_RIGHT);


//                            String data = String.format("%5s %5s %5s \n", total_price + "", total_row_qty + "", (total_price * total_row_qty) + "");
//                            String filled = StringUtils.rightPad(product_name, 20, ' ');
                            textData.append(String.format("%-20s %5s %5s %10s \n", " " + product_name, String.format("%.02f", total_price) + "", total_row_qty + "", String.format("%.02f", (total_price * total_row_qty)) + ""));


                            // textData.append(filled);
                            // textData.append(data);
                            method = "addText";
                            mPrinter.addText(textData.toString());
                            textData.delete(0, textData.length());
                        }

                        textData.append("------------------------------------------\n");
                        textData.append(String.format("%-20s %-20s \n", "Total " + item_count_row + " items", String.format("%.02f", total_price_by_cat) + ""));
                        total_payment = total_price_by_cat;
                        total_service_charge = (total_price_by_cat) * ((double)service_data / 100);
                        if (discount_type == "เปอร์เซ็น"){
                            total_discount = total_price_by_cat * ((double)discount_data / 100);
                        }else{
                            total_discount = discount_data;
                        }

                        sum_price += total_price_by_cat;

                        method = "addText";
                        mPrinter.addText(textData.toString());
                        textData.delete(0, textData.length());

                        // reset ให้เป็น 0 เพื่อหา row ต่อไป
                        total_price_by_cat = 0;


                    } else {

                    }


                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_RIGHT);
            textData.append("------------------------------------------\n");
            //textData.append("\n");
            textData.append(String.format("%-20s %-20s \n", "Sumtotal " + total_qty, " items", "", String.format("%.02f", sum_price) + ""));

            // เงินสด
            if (payment_type == 1) {


                double total = 0.0;
                double totalDiscount = 0.0;
                total = total_payment;
                totalDiscount = total_discount;
                // เช็คส่วนลดแยกตามประเภทอาหาร
                if (StringUtil.dicount_type.equals("แยกตามรายการ")) {
                    textData.append(String.format("%-20s %20s \n", "Discount " + StringUtil.dicount_total + StringUtil.report_cat_name, String.format("%.02f",totalDiscount) + ""));
                } else {
                    textData.append(String.format("%-20s %20s \n", "Discount " + StringUtil.dicount_total + StringUtil.dicount_type, String.format("%.02f",totalDiscount) + ""));
                }


                textData.append(String.format("%-20s %20s \n", "Total", String.format("%.02f", total_payment) + ""));

                double service_total = (total * Integer.parseInt(StringUtil.custom_service)) / 100;
                textData.append(String.format("%-20s %20s \n", "Service " + StringUtil.custom_service + "%", String.format("%.02f", total_service_charge) + ""));

                //double vat_total = (service_total * 7) / 100;

                if (StringUtil.vat == false) {
                    //textData.append(String.format("%-20s %20s \n", "Vat", vat_total + "%"));
                } else {

                    textData.append(String.format("%-20s %20s \n", "Vat " + vat_data + "%", String.format("%.02f", vat_total) + ""));
                }
                total = total + service_total + vat_total;

                textData.append(String.format("%-20s %20s \n", "Grand Total", String.format("%.02f",(total - totalDiscount)) + ""));
                textData.append(String.format("%-20s %20s \n", "Cash", payment_Amount + ""));

                int input_total_cash = Integer.parseInt(payment_Change );
                Double grand_total = (total - totalDiscount);

                int res_banlance = Integer.valueOf(grand_total.intValue());
                textData.append(String.format("%-20s %20s \n", "Change cash", payment_Change + ""));

                method = "addText";
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());

            } else {
                double total = 0.0;
                double totalDiscount = 0.0;
                total = total_payment;
                totalDiscount = total_discount;
                // เช็คส่วนลดแยกตามประเภทอาหาร
                if (StringUtil.dicount_type.equals("แยกตามรายการ")) {
                    textData.append(String.format("%-20s %20s \n", "Discount " + StringUtil.dicount_total + StringUtil.report_cat_name, String.format("%.02f",totalDiscount) + ""));
                } else {
                    textData.append(String.format("%-20s %20s \n", "Discount " + StringUtil.dicount_total + StringUtil.dicount_type, String.format("%.02f",totalDiscount) + ""));
                }


                textData.append(String.format("%-20s %20s \n", "Total", total_payment + ""));

                double service_total = (total * Integer.parseInt(StringUtil.custom_service)) / 100;
                textData.append(String.format("%-20s %20s \n", "Service " + StringUtil.custom_service + "%", String.format("%.02f", total_service_charge) + ""));

                //double vat_total = (service_total * 7) / 100;

                if (StringUtil.vat == false) {
                    //textData.append(String.format("%-20s %20s \n", "Vat", vat_total + "%"));
                } else {

                    textData.append(String.format("%-20s %20s \n", "Vat " + vat_data + "%", String.format("%.02f", vat_total) + ""));
                }
                total = total + service_total + vat_total;

                textData.append(String.format("%-20s %20s \n", "Grand Total", String.format("%.02f",(total - totalDiscount)) + ""));
                if (payment_type == 2){
                    textData.append(String.format("%-20s %20s \n", "Credit/Master Card",  ""));
                }else if(payment_type == 3){
                    textData.append(String.format("%-20s %20s \n", "โอน", ""));
                }else{
                    textData.append(String.format("%-20s %20s \n", "QR CODE", ""));

                }
                textData.append(String.format("%-20s %20s \n", "Ref No.", RefNo + ""));



                method = "addText";
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            }


            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            textData.append("THANK YOU\n");
            textData.append("POWER BY GPOS\n");
            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addCut";
            mPrinter.addCut(Printer.CUT_FEED);


        } catch (Exception e) {
            //ShowMsg.showException(e, method, mContext);
            return false;
        }

        textData = null;

        return true;
    }
    private String getDateTime() {
        // get date time in custom format
        SimpleDateFormat sdf = new SimpleDateFormat("[yyyy/MM/dd HH:mm]");
        return sdf.format(new Date());
    }

    private boolean isPrintable(PrinterStatusInfo status) {
        if (status == null) {
            return false;
        }

        if (status.getConnection() == Printer.FALSE) {
            return false;
        } else if (status.getOnline() == Printer.FALSE) {
            return false;
        } else {
            ;//print available
        }

        return true;
    }
    private class TableCancelOrderTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(OrderHistoryActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tTable Cancel Order in progress...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("remain", urls[1])
                    .add("order_id", StringUtil.OrderId)
                    .add("create_by", Profile.UserId)
                    .add("table_id", StringUtil.TableCustomerId)
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(OrderHistoryActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(OrderHistoryActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
//                                    Intent in = new Intent(OpenTableOrderActivity.this, StaffDeliveryActivity.class);
//                                    in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                                    startActivity(in);
                                    finish();

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


}
