package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import com.epson.epos2.printer.Printer;
import com.epson.epos2.printer.PrinterStatusInfo;
import com.epson.epos2.printer.ReceiveListener;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.os.IBinder;
import android.os.StrictMode;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import recieptservice.com.recieptservice.PrinterInterface;

public class DiscountActivity extends AppCompatActivity implements ReceiveListener {

    LinearLayout llCash, llPercen, llReportCat;
    static ListView lisView1;
    static ArrayList<HashMap<String, String>> MyArrList;

    Button btn5b, btn15b, btn10b, btn5p, btn10p, btn15p, btnSubmit;

    EditText input_custom_amount, input_custom_percen, input_report_discount_qty;
    TextView txt_total, txt_total_result;

    String total_payment = "";

    static String report_cat_id = "";
    static String report_cat_name = "";
    EpsonMakeErrorMessage epsonMakeErrorMessage;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {

            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    int check_receipt = 0;
    Button btnInvoice, btnShortInvoice, btnNoInvoice;

    Button btns5, btns10, btns15;
    EditText input_custom_service;
    int total_price = 0;
    int total_qty = 0;
    int check_order_place = 0;

    static AlertDialog dialogChoiceAdd;

    String Resultbill;

    static TextView txtReportTypeName;

    IBinder service;
    PrinterInterface mAidl;

    Profile profile;

    private Context mContext = null;
    public static EditText mEditTarget = null;
    public static Spinner mSpnSeries = null;
    public static Spinner mSpnLang = null;
    public static Printer mPrinter = null;
    public static ToggleButton mDrawer = null;

    final String KEY_Autoprint = "Autoprint";
    final String KEY_PrinterMain = "PrinterMain";
    final String KEY_Printer1 = "Printer1";
    final String KEY_Printer2 = "Printer2";
    final String KEY_Printer3 = "Printer3";
    final String KEY_Printer4 = "Printer4";

    String PrinterAddress = "";

    SharedPreferences sp;
    SharedPreferences.Editor editor;
    final String PREF_NAME = "PrinterPreferences";
    CheckBox checkBox;

    String service_charg_type = "";
    String is_service_include = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discount);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        epsonMakeErrorMessage = new EpsonMakeErrorMessage(this);
        sp = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sp.edit();

        profile = new Profile(this);

        StringUtil.dicount_type = "เปอร์เซ็น";

        checkBox = (CheckBox) findViewById(R.id.cheVat);
        //checkBox.setChecked(true);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // update your model (or other business logic) based on isChecked

                if (checkBox.isChecked() == true) {
                    StringUtil.vat = true;
                } else {
                    StringUtil.vat = false;
                }

                Cal();

                //CalWithService();
            }
        });

        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (checkBox.isChecked() == true) {
                    StringUtil.vat = true;
                } else {
                    StringUtil.vat = false;
                }

                // เช็คส่วนลดแยกตามประเภทอาหาร
                if (StringUtil.dicount_type.equals("แยกตามรายการ")) {
                    StringUtil.dicount_total = Integer.parseInt(input_report_discount_qty.getText().toString());

                }

                if (check_receipt == 2) {
                    finish();

                } else {
                    new AsyncChoiceProductPrint().execute();
                }

                new BookingAddTask().execute(StringUtil.URL + "gpos/api/InvoiceUpdate");

//                finish();
            }
        });

        llCash = (LinearLayout) findViewById(R.id.llCash);
        llPercen = (LinearLayout) findViewById(R.id.llPercen);
        llReportCat = (LinearLayout) findViewById(R.id.llReportCat);
        //lisView1 = (ListView) findViewById(R.id.listView1);

        txt_total = (TextView) findViewById(R.id.txt_total);
        txt_total.setText("รวมเงิน " + StringUtil.TotalPayment + " บาท ");

        txt_total_result = (TextView) findViewById(R.id.txt_total_result);
        txtReportTypeName = (TextView) findViewById(R.id.txtReportTypeName);

        RadioGroup rddiscount = (RadioGroup) this.findViewById(R.id.rddiscount);

        RadioGroup rddiscount_bytype = (RadioGroup) this.findViewById(R.id.rddiscount_bytype);

        llCash.setVisibility(View.GONE);
        llReportCat.setVisibility(View.GONE);

        input_custom_amount = (EditText) findViewById(R.id.input_custom_amount);
        input_custom_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (input_custom_amount.getText().toString().length() > 0) {
                    StringUtil.dicount_total = Integer.parseInt(input_custom_amount.getText().toString());
                    //input_custom_amount.setText(StringUtil.dicount_total);
                    Cal();

                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
        });

        input_custom_percen = (EditText) findViewById(R.id.input_custom_percen);
        input_custom_percen.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (input_custom_amount.getText().toString().length() > 0) {
                    StringUtil.dicount_total = Integer.parseInt(input_custom_amount.getText().toString());
                    //input_custom_amount.setText(StringUtil.dicount_total);
                    Cal();

                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
        });

        input_custom_service = (EditText) findViewById(R.id.input_custom_service);
        input_custom_service.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (input_custom_amount.getText().toString().length() > 0) {
                    StringUtil.custom_service = input_custom_service.getText().toString();
                    //input_custom_amount.setText(StringUtil.dicount_total);
                    Cal();

                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
        });

        input_report_discount_qty = (EditText) findViewById(R.id.input_report_discount_qty);
        input_report_discount_qty.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (input_report_discount_qty.getText().toString().length() > 0) {
                    StringUtil.dicount_total = Integer.parseInt(input_report_discount_qty.getText().toString());
                    //input_custom_amount.setText(StringUtil.dicount_total);
                    Cal();

                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
        });


        rddiscount_bytype.setOnCheckedChangeListener(
                new RadioGroup.OnCheckedChangeListener() {
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        RadioButton checkedRadio = (RadioButton) DiscountActivity.this.findViewById(checkedId);
                        String checkedText = checkedRadio.getText().toString();
                        if (checkedText.equals("เปอร์เซ็น")) {
                            StringUtil.dicount_type = "เปอร์เซ็น";

                        } else {

                            StringUtil.dicount_type = "เงินสด";
                        }

                        Cal();
                    }
                }
        );

        rddiscount.setOnCheckedChangeListener(
                new RadioGroup.OnCheckedChangeListener() {
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        RadioButton checkedRadio = (RadioButton) DiscountActivity.this.findViewById(checkedId);

                        String checkedText = checkedRadio.getText().toString();
                        if (checkedText.equals("เปอร์เซ็น")) {
                            llCash.setVisibility(View.GONE);
                            llPercen.setVisibility(View.VISIBLE);
                            llReportCat.setVisibility(View.GONE);
                            StringUtil.dicount_type = "เปอร์เซ็น";
                            StringUtil.dicount_type_report = "";

                            // เรียก function คำนวน
                            Cal();

                        } else if (checkedText.equals("เงินสด")) {
                            llCash.setVisibility(View.VISIBLE);
                            llPercen.setVisibility(View.GONE);
                            llReportCat.setVisibility(View.GONE);
                            StringUtil.dicount_type = "เงินสด";
                            StringUtil.dicount_type_report = "";
                            // เรียก function คำนวน
                            Cal();

                        } else {
                            llCash.setVisibility(View.GONE);
                            llPercen.setVisibility(View.GONE);
                            llReportCat.setVisibility(View.VISIBLE);
                            StringUtil.dicount_type_report = "แยกตามรายการ";
                            //StringUtil.dicount_type = "เปอร์เซ็น";
                            // ทำ Dialog เพื่อใช้ในการเลือก Choice ของเมนูนั้นๆ
                            LayoutInflater inflater = getLayoutInflater();
                            View alertLayout = inflater.inflate(R.layout.activity_discount_type_dialog, null);

                            lisView1 = (ListView) alertLayout.findViewById(R.id.listView1);
                            new AsyncChoiceProduct().execute();

//                            Button btnNewRowChoice = (Button) alertLayout.findViewById(R.id.btnNewRowChoice);
                            Button btnCancel = (Button) alertLayout.findViewById(R.id.btnCancel);

                            // load choice
                            AlertDialog.Builder alert = new AlertDialog.Builder(DiscountActivity.this);
                            //alert.setTitle("กรุณาเลือก Choice \n" + feedItem.getMenu_name());
                            // this is set the view from XML inside AlertDialog
                            alert.setView(alertLayout);
                            dialogChoiceAdd = alert.create();
                            //dialogChoiceAdd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

                            btnCancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialogChoiceAdd.dismiss();
                                }
                            });

                            dialogChoiceAdd.show();
                        }

                    }
                }
        );


        btn5b = (Button) findViewById(R.id.btn5b);
        btn5b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StringUtil.dicount_total = 5;
                input_custom_amount.setText(String.valueOf(StringUtil.dicount_total));
                //input_custom_amount.setText(StringUtil.dicount_total);
                Cal();
            }
        });

        btn10b = (Button) findViewById(R.id.btn10b);
        btn10b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StringUtil.dicount_total = 10;
                input_custom_amount.setText(String.valueOf(StringUtil.dicount_total));
                //input_custom_amount.setText(StringUtil.dicount_total);
                Cal();
            }
        });


        btn15b = (Button) findViewById(R.id.btn15b);
        btn15b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StringUtil.dicount_total = 15;
                input_custom_amount.setText(String.valueOf(StringUtil.dicount_total));
                // input_custom_amount.setText(StringUtil.dicount_total);
                Cal();
            }
        });

        btn5p = (Button) findViewById(R.id.btn5p);
        btn5p.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StringUtil.dicount_total = 5;
                input_custom_percen.setText(String.valueOf(StringUtil.dicount_total));
                //input_custom_percen.setText(StringUtil.dicount_total);
                Cal();
            }
        });

        btn10p = (Button) findViewById(R.id.btn10p);
        btn10p.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StringUtil.dicount_total = 10;
                input_custom_percen.setText(String.valueOf(StringUtil.dicount_total));
                //input_custom_percen.setText(StringUtil.dicount_total);
                Cal();
            }
        });

        btn15p = (Button) findViewById(R.id.btn15p);
        btn15p.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StringUtil.dicount_total = 15;
                input_custom_percen.setText(String.valueOf(StringUtil.dicount_total));
                //input_custom_percen.setText(StringUtil.dicount_total);
                Cal();
            }
        });

//        EnableButton();

        btnInvoice = (Button) findViewById(R.id.btnInvoice);
        btnInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check_receipt = 0;
                EnableButton();
            }
        });

        btnShortInvoice = (Button) findViewById(R.id.btnShortInvoice);
        btnShortInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check_receipt = 1;
                EnableButton();
            }
        });

        btnNoInvoice = (Button) findViewById(R.id.btnNoInvoice);
        btnNoInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check_receipt = 2;
                EnableButton();
            }
        });

        btns5 = (Button) findViewById(R.id.btns5);
        btns5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input_custom_service.setText("5");
                StringUtil.custom_service = input_custom_service.getText().toString();
                Cal();
            }
        });
        btns10 = (Button) findViewById(R.id.btns10);
        btns10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input_custom_service.setText("10");
                StringUtil.custom_service = input_custom_service.getText().toString();
                Cal();
            }
        });
        btns15 = (Button) findViewById(R.id.btns15);
        btns15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input_custom_service.setText("15");
                StringUtil.custom_service = input_custom_service.getText().toString();
                Cal();
                //CalWithService();
            }
        });


        EnableButton();

        Cal();

    }

    private void disconnectPrinter() {
        if (mPrinter == null) {
            return;
        }

        try {
            mPrinter.endTransaction();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                    //ShowMsg.showException(e, "endTransaction", DiscountActivity.this);
                }
            });
        }

        try {
            mPrinter.disconnect();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                    // ShowMsg.showException(e, "disconnect", DiscountActivity.this);
                }
            });
        }

        finalizeObject();
    }

    private String getDateTime() {
        // get date time in custom format
        SimpleDateFormat sdf = new SimpleDateFormat("[yyyy/MM/dd HH:mm]");
        return sdf.format(new Date());
    }

    @Override
    public void onPtrReceive(final Printer printerObj, final int code, final PrinterStatusInfo status, final String printJobId) {
        runOnUiThread(new Runnable() {
            @Override
            public synchronized void run() {
                //ShowMsg.showResult(code, epsonMakeErrorMessage.makeErrorMessage(status), DiscountActivity.this);
                // epsonMakeErrorMessage.dispPrinterWarnings(status);
                disconnectPrinter();
                finish();
            }
        });
    }


    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    private class BookingAddTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(DiscountActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลังบันทึก");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            String vat_status;
            if (StringUtil.vat == true) {
                vat_status = "yes";
            } else {
                vat_status = "no";
            }

            RequestBody formBody = new FormEncodingBuilder()
                    .add("order_id", StringUtil.OrderId + "")
                    .add("total_discount", StringUtil.dicount_total + "")
                    .add("discount_type", StringUtil.dicount_type)
                    .add("discount_by_menu", StringUtil.report_cat_name)
                    .add("vat", "7")
                    .add("services", input_custom_service.getText().toString())
                    .add("vat_include", vat_status)
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(DiscountActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(DiscountActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    finish();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


    private class AsyncChoiceProductPrint extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(DiscountActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //this method will be running on UI thread

            pdLoading.setMessage("\tกำลังโหลดข้อมูล...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run("http://www.geerang.com/gpos/api/ReportInvoice/" + profile.StoreId + "/" + StringUtil.OrderId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {
            Resultbill = result;
            pdLoading.dismiss();

            editor = sp.edit();
            String PrinterMain = sp.getString(KEY_PrinterMain, "");
            if (!PrinterMain.equals("")) {
                //OpenPrinter+","+printer_title+","+printer_address+","+printer_mac_address+","+categories_id
                String[] namesList = PrinterMain.split(",");
                String status = namesList[0];
                String title = namesList[1];
                String address = namesList[2];
                String mac_address = namesList[3];
                String categories_id = namesList[4];

                PrinterAddress = address;
                //เลือกเครื่องปริ้นที่กำหนด
                if (!runPrintReceiptSequence()) {

                }

                finish();


            } else {

            }
        }

    }

    private boolean runPrintReceiptSequence() {

        if (!initializeObject()) {
            return false;
        }

        if (!createReceiptData()) {
            finalizeObject();
            return false;
        }

        if (!printData()) {
            finalizeObject();
            return false;
        }

        return true;
    }

    private boolean printData() {
        if (mPrinter == null) {
            return false;
        }

        if (!connectPrinter()) {
            mPrinter.clearCommandBuffer();
            return false;
        }

        try {
            mPrinter.sendData(Printer.PARAM_DEFAULT);
        } catch (Exception e) {
            mPrinter.clearCommandBuffer();
            // ShowMsg.showException(e, "sendData", mContext);
            try {
                mPrinter.disconnect();
            } catch (Exception ex) {
                // Do nothing
            }
            return false;
        }

        return true;
    }

    private boolean connectPrinter() {
        if (mPrinter == null) {
            return false;
        }

        try {
            // mPrinter.connect("192.168.1.103", Printer.PARAM_DEFAULT);
            mPrinter.connect(PrinterAddress, Printer.PARAM_DEFAULT);


        } catch (Exception e) {
            // ShowMsg.showException(e, "connect", mContext);
            return false;
        }

        return true;
    }

    private void finalizeObject() {
        if (mPrinter == null) {
            return;
        }

        mPrinter.clearCommandBuffer();

        mPrinter.setReceiveEventListener(null);

        mPrinter = null;
    }

    private boolean initializeObject() {
        try {
            // ((SpnModelsItem) mSpnLang.getSelectedItem()).getModelConstant()
//            mPrinter = new Printer(((SpnModelsItem) mSpnSeries.getSelectedItem()).getModelConstant(),
            mPrinter = new Printer(Printer.TM_T88, Printer.MODEL_ANK, mContext);
            //

        } catch (Exception e) {
            //  ShowMsg.showException(e, "Printer", mContext);
            return false;
        }

        mPrinter.setReceiveEventListener(DiscountActivity.this);

        return true;
    }

    private boolean createReceiptData() {
        String method = "";

        StringBuffer textData = new StringBuffer();
        final int barcodeWidth = 2;
        final int barcodeHeight = 100;

        if (mPrinter == null) {
            return false;
        }

        try {
            //语言需要
            mPrinter.addTextLang(Printer.LANG_TH);
            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            method = "addFeedLine";
            mPrinter.addFeedLine(1);
            textData.append("ใบเรียกเก็บเงิน/INVOICE\n");
            textData.append("\n");
            textData.append(String.format("%-20s %-20s \n", "Invoice No." + StringUtil.OrderId, "Order Type. " + "Dine In "));
            textData.append(String.format("%-20s %-20s \n", "Invoice By." + profile.Username, "Table." + "" + StringUtil.TableCustomerName));
            textData.append(String.format("%-20s %-20s \n", "Staff." + profile.Username, getDateTime() + ""));

            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            //columns.print();
            total_price = 0;
            total_qty = 0;
            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_RIGHT);
            textData.append("------------------------------------------\n");
            textData.append(String.format("%-3s %-26s %5s %5s \n", "Qty", "Item", "Price", "Total"));
            //textData.append(String.format("%-15s %5s %5s %5s \n", " ", "Price", "Qty", "Total"));
            textData.append("------------------------------------------\n");
            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            double sum_price = 0.0;
            JSONArray jArray = null;
            try {


                double sum_row_price = 0.0;
                jArray = new JSONArray(Resultbill);
                // loop ของ report category
                for (int f = 0; f < jArray.length(); f++) {
                    JSONObject json_data_report = jArray.getJSONObject(f);

                    JSONArray jArray_menu = new JSONArray(json_data_report.getString("products"));
                    method = "addTextAlign";
                    mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                    textData.append("   " + json_data_report.getString("report_cat_name") + "\n");
                    //textData.append("------------------------------------------\n");
                    method = "addText";
                    mPrinter.addText(textData.toString());
                    textData.delete(0, textData.length());

                    if (jArray_menu.length() > 0) {
                        // loop ของ products
                        int item_count_row = 0;
                        int total_row_qty = 0;
                        double total_price = 0.0;
                        double total_price_by_cat = 0.0;
                        String line = "";
                        int total_row_price = 0;
                        int product_sale_price = 0;
                        for (int i = 0; i < jArray_menu.length(); i++) {

                            JSONObject json_data = jArray_menu.getJSONObject(i);
                            total_row_qty = Integer.parseInt(json_data.getString("qty"));
                            item_count_row += total_row_qty;
                            String confirm = json_data.getString("is_confirm");
                            String product_name = json_data.getString("product_name");
                            // เก็บตัวแปร Choice
                            String product_choice_name = "";

                            // เก็บราคาใน order
                            total_row_price = Integer.parseInt(json_data.getString("product_sale_price"));
                            product_sale_price = Integer.parseInt(json_data.getString("product_sale_price"));

                            int order_place = Integer.parseInt(confirm);
                            if (order_place == 0) {
                                check_order_place++;
                            }
                            // loop choice
                            JSONArray jArray2 = new JSONArray(json_data.getString("product_choice"));
                            int check_list = jArray2.length();
                            if (check_list > 0) {
                                for (int j = 0; j < jArray2.length(); j++) {
                                    JSONObject json_data2 = jArray2.getJSONObject(j);

                                    // หาราคารวมของแต่ละ row แล้วนำไปยัดใส่ใน header row
                                    int choice_price = Integer.parseInt(json_data2.getString("choice_price"));
                                    total_row_price += choice_price;

                                    String choice_name = json_data2.getString("product_choice_name");
                                    product_choice_name += choice_name + ",";

                                }
                            }


                            total_price = total_row_price;
                            total_qty = total_qty + total_row_qty;
                            // หาผลร่วมระหว่าง หมวดหมู่
                            total_price_by_cat += (total_price * total_row_qty);

                            // หาผลร่วมระหว่าง ทั้งหมด
                            sum_row_price = sum_row_price + total_price;
                            DecimalFormat formatter = new DecimalFormat("#,###.00");
                            method = "addTextAlign";
                            mPrinter.addTextAlign(Printer.ALIGN_RIGHT);
                            textData.append(String.format("%2s %-25s %7s %7s \n"
                                    , total_row_qty + ""
                                    , product_name
                                    , formatter.format(product_sale_price) + ""
                                    , formatter.format(total_row_qty * product_sale_price) + ""));

                            method = "addText";
                            mPrinter.addText(textData.toString());
                            textData.delete(0, textData.length());


                            if (check_list > 0) {

                                method = "addTextAlign";
                                mPrinter.addTextAlign(Printer.ALIGN_RIGHT);

                                for (int j = 0; j < jArray2.length(); j++) {
                                    JSONObject json_data2 = jArray2.getJSONObject(j);

                                    // หาราคารวมของแต่ละ row แล้วนำไปยัดใส่ใน header row
                                    int choice_price = Integer.parseInt(json_data2.getString("choice_price"));
                                    //total_row_price += choice_price;

                                    String choice_name = json_data2.getString("product_choice_name");
                                    //product_choice_name += choice_name + ",";

                                    //String x = String.format("%.02f", total_price);
                                    //String y = String.format("%.02f", (total_price * total_row_qty));
                                    int qty = (int) Math.round(total_row_qty);

                                    double amount = qty * choice_price;
                                    int IntValue = (int) Math.round(amount);

                                    textData.append(String.format("%2s %-25s %7s %7s \n"
                                            , " "
                                            , " -" + choice_name
                                            , formatter.format(choice_price)
                                            , formatter.format(IntValue) + ""));

                                    // textData.append(filled);
                                    // textData.append(data);

                                }

                                method = "addText";
                                mPrinter.addText(textData.toString());
                                textData.delete(0, textData.length());

                            }

                        }

                        textData.append("------------------------------------------------\n");
                        textData.append(String.format("%-20s %20s \n", "Total " + item_count_row + " items", String.format("%.02f", total_price_by_cat) + ""));
                        //textData.append(String.format("%-20s %-20s \n", "Total " + item_count_row + " items", String.format("%.02f", total_price_by_cat) + ""));
                        sum_price += total_price_by_cat;

                        method = "addText";
                        mPrinter.addText(textData.toString());
                        textData.delete(0, textData.length());

                        // reset ให้เป็น 0 เพื่อหา row ต่อไป
                        total_price_by_cat = 0;


                    } else {

                    }


                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_RIGHT);
            textData.append("------------------------------------------\n\n");
            //textData.append("\n");
            textData.append(String.format("%-20s %20s \n", "Total " + total_qty + " items", sum_price + ""));
            textData.append("\n");
            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_RIGHT);
            textData.append(String.format("%-20s %20s \n", "SubTotal ", sum_price + ""));
//            if (StringUtil.dicount_total > 0) {
//
//                textData.append(String.format("%-20s %-20s \n", "Discount " + StringUtil.dicount_type, " ", "", StringUtil.dicount_total + ""));
//                textData.append(String.format("%-20s %-20s \n", "Total " + StringUtil.discount_total_payment, " ", "", StringUtil.discount_total_payment + ""));
//
//            }

            if (StringUtil.vat == true) {
                textData.append(String.format("%-20s %20s \n", "Vat ", "7%"));
            }

            textData.append(String.format("%-20s %20s \n", "Service Charge ", input_custom_service.getText().toString() + "%"));

            // เช็คส่วนลดแยกตามประเภทอาหาร
            if (StringUtil.dicount_type_report.equals("แยกตามรายการ")) {
                textData.append(String.format("%-20s %20s \n", "   Discount " + StringUtil.dicount_total + " " + StringUtil.dicount_type + " " + StringUtil.report_cat_name, ""));
                DecimalFormat formatter = new DecimalFormat("#,###.00");
                double amount = Double.parseDouble(StringUtil.discount_total_payment);
                //System.out.println();
                int IntValue = (int) Math.round(amount);
                textData.append(String.format("%-20s %20s \n", "Total ", formatter.format(amount) + ""));
            } else {
                DecimalFormat formatter = new DecimalFormat("#,###.00");
                double amount = Double.parseDouble(StringUtil.discount_total_payment);
                int IntValue = (int) Math.round(amount);
                textData.append(String.format("%-20s %20s \n", "Discount " + StringUtil.dicount_total + " " + StringUtil.dicount_type, ""));
                textData.append(String.format("%-20s %20s \n", "Grand Total ", formatter.format(IntValue) + ""));

            }



            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            textData.append("\n");
            textData.append("THANK YOU\n");
            textData.append("POWER BY GPOS\n");
            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addCut";
            mPrinter.addCut(Printer.CUT_FEED);


        } catch (Exception e) {
            //ShowMsg.showException(e, method, mContext);
            return false;
        }

        textData = null;

        return true;
    }

    public void EnableButton() {
        if (check_receipt == 0) {
            //btnNo.setEnabled(true);
            btnInvoice.setBackground(ContextCompat.getDrawable(DiscountActivity.this, R.drawable.rounded_button_yellow));
            btnInvoice.setTextColor(Color.WHITE);
            //btnYes.setEnabled(false);
            btnShortInvoice.setBackground(ContextCompat.getDrawable(DiscountActivity.this, R.drawable.rounded_button_yellow_border));
            btnShortInvoice.setTextColor(Color.GRAY);

            btnNoInvoice.setBackground(ContextCompat.getDrawable(DiscountActivity.this, R.drawable.rounded_button_yellow_border));
            btnNoInvoice.setTextColor(Color.GRAY);

        } else if (check_receipt == 1) {
            //btnNo.setEnabled(true);
            btnInvoice.setBackground(ContextCompat.getDrawable(DiscountActivity.this, R.drawable.rounded_button_yellow_border));
            btnInvoice.setTextColor(Color.GRAY);
            //btnYes.setEnabled(false);
            btnShortInvoice.setBackground(ContextCompat.getDrawable(DiscountActivity.this, R.drawable.rounded_button_yellow));
            btnShortInvoice.setTextColor(Color.WHITE);

            btnNoInvoice.setBackground(ContextCompat.getDrawable(DiscountActivity.this, R.drawable.rounded_button_yellow_border));
            btnNoInvoice.setTextColor(Color.GRAY);

        } else if (check_receipt == 2) {

            //btnNo.setEnabled(true);
            btnInvoice.setBackground(ContextCompat.getDrawable(DiscountActivity.this, R.drawable.rounded_button_yellow_border));
            btnInvoice.setTextColor(Color.GRAY);
            //btnYes.setEnabled(false);
            btnShortInvoice.setBackground(ContextCompat.getDrawable(DiscountActivity.this, R.drawable.rounded_button_yellow_border));
            btnShortInvoice.setTextColor(Color.GRAY);

            btnNoInvoice.setBackground(ContextCompat.getDrawable(DiscountActivity.this, R.drawable.rounded_button_yellow));
            btnNoInvoice.setTextColor(Color.WHITE);

        }
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    // StringUtil.dicount_type StringUtil.dicount_total
    public void Cal() {
        double result = 0.0;
        double discount = 0.0;
        // ส่วนลดทั่วไป
        if (StringUtil.dicount_type_report.equals("")) {
            if (StringUtil.dicount_type.equals("เปอร์เซ็น")) {
                try {

                    double x = 0.0;
                    double total = Double.parseDouble(StringUtil.TotalPayment);
                    double dis = Double.parseDouble(input_custom_percen.getText().toString());
                    StringUtil.dicount_total = Integer.parseInt(input_custom_percen.getText().toString() + "");
                    discount = (total * dis) / 100;
                    StringUtil.discount_total_payment = "" + discount;


                } catch (Exception e) {
                    StringUtil.discount_total_payment = "0";
                    //StringUtil.TotalPayment = "0";
                }

            } else {
                try {

                    double total = Double.parseDouble(StringUtil.TotalPayment);
                    StringUtil.dicount_total = Integer.parseInt(input_custom_amount.getText().toString() + "");

                    double dis = StringUtil.dicount_total;
                    //result = total - dis;
                    discount = total - dis;
                    //double vat = (result * 7) / 100;
                    StringUtil.discount_total_payment = "" + discount;
                    //StringUtil.TotalPayment = "" + result;
                } catch (Exception e) {
                    //StringUtil.TotalPayment = "0";
                    StringUtil.discount_total_payment = "0";
                }

            }

            // ส่วนลดแยกตามประเภท อาหาร
        } else {

            //StringUtil.dicount_type = "เปอร์เซ็น";

            if (StringUtil.dicount_type.equals("เปอร์เซ็น")) {
                try {

                    double x = 0.0;
                    double total = Double.parseDouble(StringUtil.TotalPayment);
                    double dis = Double.parseDouble(input_report_discount_qty.getText().toString());

                    StringUtil.dicount_total = Integer.parseInt(input_report_discount_qty.getText().toString() + "");
                    discount = (total * dis) / 100;
                    StringUtil.discount_total_payment = "" + discount;

                    //StringUtil.TotalPayment = "" + result;

                } catch (Exception e) {
                    StringUtil.discount_total_payment = "0";
                    //StringUtil.TotalPayment = "0";
                }

            } else {
                try {

                    double total = Double.parseDouble(StringUtil.TotalPayment);
                    StringUtil.dicount_total = Integer.parseInt(input_report_discount_qty.getText().toString() + "");
                    int dis = StringUtil.dicount_total;
                    discount = total - dis;
                    StringUtil.discount_total_payment = "" + discount;
                } catch (Exception e) {
                    //StringUtil.TotalPayment = "0";
                    StringUtil.discount_total_payment = "0";
                }

            }

        }


        // check service charge include
        double service_charge = 0.0;
        double vat = 0.0;
        double service_charge_result = 0.0;
        double total_price_result = 0.0;
//        StringUtil.TotalPayment
        double TotalPayment  = Double.parseDouble(StringUtil.TotalPayment);
        try {
            service_charge = Integer.parseInt(input_custom_service.getText().toString());
        } catch (Exception e) {
            service_charge = 0;
        }

        if (service_charge > 0) {
            //double x = Double.parseDouble(StringUtil.discount_total_payment);
            //double y = 0.0;
            service_charge_result = (service_charge * TotalPayment) / 100;
            //service_charge_result = x + y;
            //StringUtil.discount_total_payment = service_charge_result + "";
        }

        if ( StringUtil.vat == true) {
            //StringUtil.vat = true;
            //result = total + x;
            vat = (TotalPayment * 7) / 100;
            // รวมค่า vat
            //result = result + vat;
        }


        total_price_result = TotalPayment + vat + service_charge_result;


        DecimalFormat formatter = new DecimalFormat("#,###.00");
        double amount = 0.0 ;//Double.parseDouble(StringUtil.discount_total_payment);
        //System.out.println();

        amount =  total_price_result - discount;

        StringUtil.discount_total_payment = amount+"";

        txt_total_result.setText("ยอดรวมสุทธิ " + formatter.format(amount) + " บาท ");

    }

    private class AsyncChoiceProduct extends AsyncTask<String, String, String> {

        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
//            progressBar.setVisibility(View.VISIBLE);

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/ReportCategory/" + profile.StoreId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {
            //this method will be running on UI thread
            try {

                JSONArray jArray = new JSONArray(result);

                MyArrList = new ArrayList<HashMap<String, String>>();
                HashMap<String, String> map;

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    map = new HashMap<String, String>();
                    map.put("product_id", json_data.getString("report_cat_id"));
                    map.put("product_name", json_data.getString("report_cat_name"));
                    map.put("product_detail", json_data.getString("report_sequence"));

                    MyArrList.add(map);

                }
                lisView1.setAdapter(new IngredientAdapter(DiscountActivity.this));


            } catch (JSONException e) {
                Toast.makeText(DiscountActivity.this, "477" + e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    public static class IngredientAdapter extends BaseAdapter {
        private Context context;

        public IngredientAdapter(Context c) {
            //super( c, R.layout.activity_column, R.id.rowTextView, );
            // TODO Auto-generated method stub
            context = c;
        }

        public int getCount() {
            // TODO Auto-generated method stub
            return MyArrList.size();
        }

        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.activity_discount_select_row, null);

            }


            Button btnSelect = (Button) convertView.findViewById(R.id.btnSelectReport);
            btnSelect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    report_cat_name = MyArrList.get(position).get("product_name");
                    report_cat_id = MyArrList.get(position).get("product_id");
                    txtReportTypeName.setText(report_cat_name);

                    StringUtil.report_cat_name = report_cat_name;

                    // close dialog
                    dialogChoiceAdd.dismiss();

                }
            });

            // ColCountry
            TextView txtCountry = (TextView) convertView.findViewById(R.id.ColCountry);
            txtCountry.setText(MyArrList.get(position).get("product_name"));

            return convertView;

        }

    }


    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


}
