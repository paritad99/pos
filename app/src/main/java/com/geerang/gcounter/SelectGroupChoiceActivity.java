package com.geerang.gcounter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.PictureDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import com.geerang.gcounter.ui.choice_group.ChoiceGroupFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class SelectGroupChoiceActivity extends AppCompatActivity {

    private final OkHttpClient client = new OkHttpClient();
    private List<FeedItemDrug> feedsList;
    private CategoriesRecyclerAdapter mAdapter;
    //final String KEY_BranchName = "BranchName";
    RecyclerView rvCatagory;

    String input_product_id = "";
    String group_choice = "";;
    String choice_min = "";;
    String choice_max = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_group_choice);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //profile = new Profile(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        Intent intent = getIntent();
        input_product_id = intent.getStringExtra("input_product_id");

        rvCatagory = (RecyclerView) findViewById(R.id.mRecyclerView);
        new AsyncLoadCat().execute();

    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    private class AsyncLoadCat extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(SelectGroupChoiceActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/MenuGroupChoice/" + Profile.StoreId);
//                response = http.run(StringUtil.URL+"gpos/api/category/1");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();

            feedsList = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemDrug item = new FeedItemDrug();
                    item.setEmployee_id(json_data.getString("p_group_id"));
                    item.setFirstname(json_data.getString("name"));
                    item.setLastname(json_data.getString("description"));

                    item.setP_group_id(json_data.getString("p_group_id"));
                    item.setName(json_data.getString("name"));
                    item.setDescription(json_data.getString("description"));
                    item.setMin(json_data.getString("min"));
                    item.setMax(json_data.getString("max"));

                    feedsList.add(item);
                }


                // Setup and Handover data to recyclerview

                mAdapter = new CategoriesRecyclerAdapter(SelectGroupChoiceActivity.this, feedsList);
                rvCatagory.setAdapter(mAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(SelectGroupChoiceActivity.this, 2);
                rvCatagory.setLayoutManager(mLayoutManager);
                rvCatagory.setItemAnimator(new DefaultItemAnimator());
                rvCatagory.setItemAnimator(new DefaultItemAnimator());


            } catch (JSONException e) {
                Toast.makeText(SelectGroupChoiceActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    public class CategoriesRecyclerAdapter extends RecyclerView.Adapter<CategoriesRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemDrug> feedItemList;
        private Context mContext;

        public CategoriesRecyclerAdapter(Context context, List<FeedItemDrug> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;
        }

        @Override
        public CategoriesRecyclerAdapter.CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.select_group_choice_row, null);

            CategoriesRecyclerAdapter.CustomViewHolder viewHolder = new CategoriesRecyclerAdapter.CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(CategoriesRecyclerAdapter.CustomViewHolder customViewHolder, int i) {
            FeedItemDrug feedItem = feedItemList.get(i);
            customViewHolder.feedItem = feedItem;


            //Setting text view title
            customViewHolder.txtChoice.setText(feedItem.getFirstname());
            //customViewHolder.tv_tel.setText(Html.fromHtml("วันที่สร้าง:"+feedItem.getTel()));
//            if (feedItem.getStatus_name().equals("1")) {
//                customViewHolder.tv_status.setText(Html.fromHtml("สถานะ: เปิดทำการ"));
//            }else
//            {
//                customViewHolder.tv_status.setText(Html.fromHtml("สถานะ: ปิดทำการ"));
//            }


            // goto this AddTableActivity
            //customViewHolder.imageView.setImageResource(StringUtil.arrImg[i]);


        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            protected ImageView imageView;
            protected TextView txtChoice;

            FeedItemDrug feedItem;

            public CustomViewHolder(View view) {
                super(view);

                this.txtChoice = (TextView) view.findViewById(R.id.txtChoice);
                //this.tv_tel = (TextView) view.findViewById(R.id.tv_tel);
                //this.tv_status = (TextView) view.findViewById(R.id.txtStatus);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

//
                        new AlertDialog.Builder(SelectGroupChoiceActivity.this)
                                .setTitle("เลือก GroupChoice")
                                .setMessage("ต้องการเลือก GroupChoice นี้ใช่หรือไม่?")
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {

                                        String group_choice = feedItem.getName();
                                        String choice_min = feedItem.getMin();
                                        String choice_max = feedItem.getMax();

                                        //Toast.makeText(SelectGroupChoiceActivity.this, "Yaay", Toast.LENGTH_SHORT).show();
                                        StringUtil.p_group_id = feedItem.getP_group_id();
                                        StringUtil.GroupName = feedItem.getName();
                                        StringUtil.GroupDescription = feedItem.getDescription();
                                        StringUtil.GroupMin = feedItem.getMin();
                                        StringUtil.GroupMax = feedItem.getMax();

                                        new GroupChoiceAddTask().execute(StringUtil.URL + "gpos/api/GroupChoiceUpdateAdd",feedItem.getP_group_id());

                                        //finish();

                                    }})
                                .setNegativeButton(android.R.string.no, null).show();



//                        StringUtil.CatId = feedItem.getId();
                        //new AsyncProduct().execute();
                        //Toast.makeText(v.getContext(), "Select is: " + feedItem.getId(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }
    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    private class GroupChoiceAddTask extends AsyncTask<String, Void, String> {

//        ProgressDialog pdLoading = new ProgressDialog(FoodMenuAddActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
//            pdLoading.setMessage("\tSaving...");
//            pdLoading.setCancelable(false);
//            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            // เพิ่ม GroupChoice


            RequestBody formBody = new FormEncodingBuilder()
                    .add("UserId", Profile.UserId)
                    .add("StoreId", Profile.StoreId)
                    .add("name", group_choice)
                    .add("min", choice_min)
                    .add("max", choice_max)
                    .add("description", "")
                    .add("product_id", input_product_id)
                    .add("p_group_id", urls[1])
                    .build();

            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
//            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(SelectGroupChoiceActivity.this);
                    builder.setMessage("ไม่สามารถบันทึกข้อมูลได้")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things

                                }
                            });
                    androidx.appcompat.app.AlertDialog alert = builder.create();
                    alert.show();
                } else {

                    String msg = object.getString("msg"); // success
                    androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(SelectGroupChoiceActivity.this);
                    builder.setMessage("บันทึกเรียบร้อยแล้ว")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    finish();
                                    // new AsyncLoadChoice().execute();
                                }
                            });
                    androidx.appcompat.app.AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    // String
    public class FeedItemDrug {

        private String employee_id;
        private String firstname;
        private String lastname;
        private String tel;
        private String status_name;

        private String p_group_id;
        private String name;
        private String description;
        private String min;
        private String max;

        public void setP_group_id(String p_group_id) {
            this.p_group_id = p_group_id;
        }

        public String getP_group_id() {
            return p_group_id;
        }

        public String getName() {
            return name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getMin() {
            return min;
        }

        public String getMax() {
            return max;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setMax(String max) {
            this.max = max;
        }

        public void setMin(String min) {
            this.min = min;
        }

        public String getEmployee_id() {
            return employee_id;
        }

        public void setEmployee_id(String employee_id) {
            this.employee_id = employee_id;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getStatus_name() {
            return status_name;
        }

        public void setStatus_name(String status_name) {
            this.status_name = status_name;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }
    }
}