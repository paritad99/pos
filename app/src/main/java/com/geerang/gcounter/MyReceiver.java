package com.geerang.gcounter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class MyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String status = NetworkUtil.getConnectivityStatusString(context);
        if(status.isEmpty() || status.equals("No internet is available")) {
            status="No Internet Connection";

            Intent in = new Intent(context,NointernetActivity.class);
            in.putExtra("Key", "text");
            in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(in);

        }

//        Toast.makeText(context, status, Toast.LENGTH_LONG).show();


    }
}