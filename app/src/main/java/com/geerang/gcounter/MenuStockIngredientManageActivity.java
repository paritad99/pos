package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class MenuStockIngredientManageActivity extends AppCompatActivity {

    Button btnIngredientAdd;
    ArrayList<HashMap<String, String>> MyArrList;
    ArrayList<HashMap<String, String>> MyArrListMenuIngredient;
    ListView lisView1, lvMenu_ingredients;
    Button btnAdd;
    Button btnCancel, btnGetItem;

    // CONNECTION_TIMEOUT and READ_TIMEOUT are in milliseconds
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;
    private RecyclerView recyclerView;
    TextView txtStoreName, txtUsername, tvSumAmount;

    Profile profile;

    String product_id, product_choice_name, p_group_add_id, choice_price, choice_stock;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_order_choice_manage);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }


        /*
        in.putExtra("product_choice_name",MyArrList_choice.get(position).get("product_choice_name"));
        in.putExtra("p_group_add_id",MyArrList_choice.get(position).get("p_group_add_id"));
        in.putExtra("choice_price",MyArrList_choice.get(position).get("choice_price"));
        in.putExtra("choice_stock",MyArrList_choice.get(position).get("choice_stock"));
        in.putExtra("product_id",product_id);
        */

        profile = new Profile(this);
        product_id = getIntent().getExtras().getString("product_id");
        product_choice_name = getIntent().getExtras().getString("product_choice_name");
        p_group_add_id = getIntent().getExtras().getString("p_group_add_id");

        btnIngredientAdd = (Button) findViewById(R.id.btnIngredientAdd);
        btnIngredientAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // ทำ Dialog เพื่อใช้ในการเลือก Choice ของเมนูนั้นๆ
                LayoutInflater inflater = getLayoutInflater();
                View alertLayout = inflater.inflate(R.layout.activity_stock_ingredient_dialog, null);
                btnAdd = (Button) alertLayout.findViewById(R.id.btnAdd);
                btnCancel = (Button) alertLayout.findViewById(R.id.btnCancel);
//                btnAdd.setEnabled(false);
//                btnAdd.setBackgroundColor(Color.GRAY);

                // listView1
                lisView1 = (ListView) alertLayout.findViewById(R.id.listView1);

                // call function load data
                loaddata();

                // load choice
                AlertDialog.Builder alert = new AlertDialog.Builder(MenuStockIngredientManageActivity.this);
                alert.setTitle("กรุณาเลือกวัตุดิบ");
                // this is set the view from XML inside AlertDialog
                alert.setView(alertLayout);
                final AlertDialog dialog = alert.create();
                btnAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int count = lisView1.getAdapter().getCount();
                        for (int i = 0; i < count; i++) {
                            LinearLayout itemLayout = (LinearLayout) lisView1.getChildAt(i); // Find by under LinearLayout
                            CheckBox checkbox = (CheckBox) itemLayout.findViewById(R.id.ColChk);
                            if (checkbox.isChecked()) {
                                Log.d("Item ", MyArrList.get(i).get("ID").toString());

                                new BookingAddTask().execute(StringUtil.URL + "gpos/api/MenuIngredientStockAdd", MyArrList.get(i).get("ID").toString());

                                //api  MenuIngredientStockAdd
//                                Toast.makeText(MenuStockIngredientManageActivity.this,checkbox.getTag().toString() ,Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });

                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });


                dialog.show();

            }
        });

        lvMenu_ingredients = (ListView) findViewById(R.id.lvMenu_ingredients);
        new AsyncLoadProduct().execute();

        btnGetItem = (Button) findViewById(R.id.btnGetItem);
        btnGetItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int count = lvMenu_ingredients.getAdapter().getCount();
                for (int i = 0; i < count; i++) {
                    LinearLayout itemLayout = (LinearLayout) lvMenu_ingredients.getChildAt(i); // Find by under LinearLayout
                    EditText input_use_qty = (EditText) itemLayout.findViewById(R.id.input_use_qty);
                    EditText input_per_qty = (EditText) itemLayout.findViewById(R.id.input_per_qty);

                    Log.d("Ingredient :", input_use_qty.getText().toString() + ":" + input_per_qty.getText().toString());

                    new MenuIngredientStockUpdateTask()
                            .execute(StringUtil.URL + "gpos/api/MenuIngredientStockUpdate/"
                                    , MyArrListMenuIngredient.get(i).get("ID")
                                    , input_use_qty.getText().toString()
                                    , input_per_qty.getText().toString()
                            );
                }

            }
        });


    }


    private class BookingAddTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(MenuStockIngredientManageActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลังบันทึกข้อมูล");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("menu_ingredients_id", urls[1])
                    .add("use_total", "")
                    .add("user_unit", "")
                    .add("p_group_add_id", p_group_add_id)
                    .add("create_by", profile.UserId)

                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            finish();
            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

//                choice_id = feedItem.getProduct_id();
//                new AsyncLoadIngredientTable().execute(choice_id);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuStockIngredientManageActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuStockIngredientManageActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
//                                    Intent in = new Intent(BookingUpdateActivity.this, TableActivity.class);
//                                    startActivity(in);
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        new AsyncLoadProduct().execute();

    }

    private class MenuIngredientStockUpdateTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(MenuStockIngredientManageActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลังบันทึกข้อมูล");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("menu_stock_ingredient_id", urls[1])
                    .add("use_total", urls[2])
                    .add("user_unit", urls[3])

                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

//                choice_id = feedItem.getProduct_id();
//                new AsyncLoadIngredientTable().execute(choice_id);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuStockIngredientManageActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things

                                    new AsyncLoadProduct().execute();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuStockIngredientManageActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
//                                    Intent in = new Intent(BookingUpdateActivity.this, TableActivity.class);
//                                    startActivity(in);

                                    new AsyncLoadProduct().execute();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    private class AsyncLoadProduct extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(MenuStockIngredientManageActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/MenuStockConfig/" + p_group_add_id);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {
            //this method will be running on UI thread
            pdLoading.dismiss();
            try {

                JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                MyArrListMenuIngredient = new ArrayList<HashMap<String, String>>();
                HashMap<String, String> map;

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);

                    /*** Rows 1 ***/
                    map = new HashMap<String, String>();
                    map.put("ID", json_data.getString("menu_stock_ingredient_id"));
                    map.put("detailProductName", json_data.getString("detailProductName"));
                    map.put("user_unit", json_data.getString("user_unit"));
                    map.put("use_total", json_data.getString("use_total"));
                    MyArrListMenuIngredient.add(map);

                }

                lvMenu_ingredients.setAdapter(new MenuIngredientAdapter(MenuStockIngredientManageActivity.this));

            } catch (JSONException e) {
                Toast.makeText(MenuStockIngredientManageActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }


    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    private class AsyncLoadIngredient extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(MenuStockIngredientManageActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/MenuIngredientList_stock/" + profile.StoreId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {
            //this method will be running on UI thread
            pdLoading.dismiss();
            try {

                JSONArray jArray = new JSONArray(result);

                MyArrList = new ArrayList<HashMap<String, String>>();
                HashMap<String, String> map;

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);

                    /*** Rows 1 ***/
                    map = new HashMap<String, String>();
                    map.put("ID", json_data.getString("product_id"));
                    map.put("Code", json_data.getString("product_name"));
                    MyArrList.add(map);

                }

                lisView1.setAdapter(new IngredientAdapter(MenuStockIngredientManageActivity.this));

            } catch (JSONException e) {
                Toast.makeText(MenuStockIngredientManageActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }


    public void loaddata() {

        new AsyncLoadIngredient().execute();
    }

    public class MenuIngredientAdapter extends BaseAdapter {
        private Context context;

        public MenuIngredientAdapter(Context c) {
            //super( c, R.layout.activity_column, R.id.rowTextView, );
            // TODO Auto-generated method stub
            context = c;
        }

        public int getCount() {
            // TODO Auto-generated method stub
            return MyArrListMenuIngredient.size();
        }

        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.activity_ingredient_stock_row, null);
            }

            // ColID
            TextView txtID = (TextView) convertView.findViewById(R.id.ColID);
            txtID.setText(MyArrListMenuIngredient.get(position).get("ID") + ".");

            // ColCode
            TextView txtCode = (TextView) convertView.findViewById(R.id.ColCode);
            txtCode.setText(MyArrListMenuIngredient.get(position).get("detailProductName"));


            EditText input_use_qty = (EditText) convertView.findViewById(R.id.input_use_qty);
            input_use_qty.setText(MyArrListMenuIngredient.get(position).get("use_total"));

            EditText input_per_qty = (EditText) convertView.findViewById(R.id.input_per_qty);
            input_per_qty.setText(MyArrListMenuIngredient.get(position).get("user_unit"));


            Button btnDelete = (Button) convertView.findViewById(R.id.btnDelete);
            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new MenuIngredientStockDeleteTask().execute(StringUtil.URL + "gpos/api/MenuStockConfigDelete/" + p_group_add_id, MyArrListMenuIngredient.get(position).get("ID"));
                }
            });

            return convertView;

        }
    }

    private class MenuIngredientStockDeleteTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(MenuStockIngredientManageActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลังบันทึกข้อมูล");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("menu_stock_ingredient_id", urls[1])
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

//                choice_id = feedItem.getProduct_id();
//                new AsyncLoadIngredientTable().execute(choice_id);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuStockIngredientManageActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    new AsyncLoadProduct().execute();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuStockIngredientManageActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
//                                    Intent in = new Intent(BookingUpdateActivity.this, TableActivity.class);
//                                    startActivity(in);
                                    new AsyncLoadProduct().execute();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


    public class IngredientAdapter extends BaseAdapter {
        private Context context;

        public IngredientAdapter(Context c) {
            //super( c, R.layout.activity_column, R.id.rowTextView, );
            // TODO Auto-generated method stub
            context = c;
        }

        public int getCount() {
            // TODO Auto-generated method stub
            return MyArrList.size();
        }

        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.activity_ingredient_column, null);

            }

            // ColID
            TextView txtID = (TextView) convertView.findViewById(R.id.ColID);
            txtID.setText(MyArrList.get(position).get("ID") + ".");

            // ColCode
            TextView txtCode = (TextView) convertView.findViewById(R.id.ColCode);
            txtCode.setText(MyArrList.get(position).get("Code"));


            return convertView;

        }
    }

}
