package com.geerang.gcounter.ui.employee;

import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.ViewModelProviders;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.PictureDrawable;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.geerang.gcounter.EmployeeAddActivity;
import com.geerang.gcounter.EmployeeEditActivity;
import com.geerang.gcounter.Profile;
import com.geerang.gcounter.R;
import com.geerang.gcounter.StringUtil;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class EmployeeFragment extends Fragment {

    private EmployeeViewModel mViewModel;

    public static EmployeeFragment newInstance() {
        return new EmployeeFragment();
    }

    private final OkHttpClient client = new OkHttpClient();
    private List<FeedItemDrug> feedsList;
    private CategoriesRecyclerAdapter mAdapter;

    RecyclerView rvCatagory;
    View root;

    TextView txtTotalEmp;
    int total_emp = 0;

    Profile profile;
    Button btnAddEmployee;
    String EmpID = "";

    final String PREF_NAME = "LoginPreferences";
    final String KEY_USERNAME = "Username";
    final String KEY_USERID = "UserId";
    final String KEY_BranchId = "BranchId";
    final String KEY_StoreId = "StoreId";
    final String KEY_Status = "Status";
    final String KEY_REMEMBER = "RememberUsername";
    final String KEY_business_type_id = "business_type_id";
    final String KEY_StoreName = "StoreName";
    final String KEY_BranchName = "BranchName";
    final String KEY_Position = "Position";
    final String KEY_StoreExp = "StoreExp";
    final String KEY_Owner = "Owner";
    final String KEY_package = "package";
    final String KEY_StoreTax = "StoreTax";
    final String KEY_StorePhone = "StoreTax";
    SharedPreferences sp;
    SharedPreferences.Editor editor;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {


        root = inflater.inflate(R.layout.employee_fragment, container, false);

        txtTotalEmp = (TextView) root.findViewById(R.id.txtTotalEmp);

        btnAddEmployee = (Button) root.findViewById(R.id.btnAddEmployee);
        btnAddEmployee.setText("เพิ่มพนักงาน");
        btnAddEmployee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(getActivity(), EmployeeAddActivity.class);
                startActivity(in);
            }
        });

        rvCatagory = (RecyclerView) root.findViewById(R.id.recyclerView);

        profile = new Profile(getActivity());
        sp = getActivity().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sp.edit();
        String store_package = sp.getString(KEY_package, "");
        try {
            if (!store_package.equals("")) {
                JSONArray jArray = new JSONArray(store_package);
                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    StringUtil.package_name = json_data.getString("package_name");
                    StringUtil.package_product = json_data.getString("package_product");
                    StringUtil.package_branch = json_data.getString("package_branch");
                    StringUtil.package_cashier = json_data.getString("package_cashier");
                    StringUtil.package_users = json_data.getString("package_users");
                    StringUtil.package_zone = json_data.getString("package_zone");
                    StringUtil.package_table = json_data.getString("package_table");
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new AsyncLoadCat().execute();

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        new AsyncLoadCat().execute();
    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    private class DeleteItemTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
        }

        protected String doInBackground(String... urls) {
            String response = null;
            EmployeeFragment.postHttp http = new EmployeeFragment.postHttp();


            RequestBody formBody = new FormEncodingBuilder()
                    .add("user_id", Profile.UserId)
                    .add("employee_id", EmpID)
                    .build();


            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success


                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    new AsyncLoadCat().execute();

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    new AsyncLoadCat().execute();

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                    //String order_id = object.getString("order_id"); // success

                    // keep to var ORderID
                    ///StringUtil.OrderId = order_id;

//                    Intent in = new Intent(ProductActivity.this, ListOrderActivity.class);
//                    startActivity(in);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class AsyncLoadCat extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(getActivity());
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();

            try {
//                if (Profile.Position.equals("9")) {
                response = http.run(StringUtil.URL + "gpos/api/employee/" + profile.StoreId);
//                } else {
//                    response = http.run(StringUtil.URL + "gpos/api/employee/" + Profile.BranchId);
//                }


//                response = http.run(StringUtil.URL+"gpos/api/category/1");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();

            feedsList = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);
                total_emp = jArray.length();
                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {

                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemDrug item = new FeedItemDrug();
                    item.setEmployee_id(json_data.getString("employee_id"));
                    item.setFirstname(json_data.getString("firstname"));
                    item.setLastname(json_data.getString("lastname"));
                    item.setTel(json_data.getString("tel"));
                    item.setStatus_name(json_data.getString("status_name"));
                    item.setBranch_id(json_data.getString("branch_id"));
                    item.setEmail(json_data.getString("email"));
                    item.setUsername(json_data.getString("username"));
                    item.setPassword(json_data.getString("password"));
                    item.setLevel_position(json_data.getString("level_position"));

                    feedsList.add(item);
                }

                int emp = 0;
                try {
                    emp = Integer.parseInt(StringUtil.package_users);

                } catch (Exception err) {
                    emp = 0;
                }

                txtTotalEmp.setText(total_emp + "/" + emp);
                if (total_emp >= emp) {
                    btnAddEmployee.setEnabled(false);
                } else {
                    btnAddEmployee.setEnabled(true);
                }

                // Setup and Handover data to recyclerview
                mAdapter = new CategoriesRecyclerAdapter(getActivity(), feedsList);
                rvCatagory.setAdapter(mAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
                rvCatagory.setLayoutManager(mLayoutManager);
                rvCatagory.setItemAnimator(new DefaultItemAnimator());
                rvCatagory.setItemAnimator(new DefaultItemAnimator());

            } catch (JSONException e) {
                Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    public class CategoriesRecyclerAdapter extends RecyclerView.Adapter<CategoriesRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemDrug> feedItemList;
        private Context mContext;

        public CategoriesRecyclerAdapter(Context context, List<FeedItemDrug> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.employee_list_row, null);

            CustomViewHolder viewHolder = new CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
            FeedItemDrug feedItem = feedItemList.get(i);
            customViewHolder.feedItem = feedItem;


            //Setting text view title
            customViewHolder.tv_employee_name.setText(Html.fromHtml("ขื่อ-นามสกุล:" + feedItem.getFirstname() + " " + feedItem.getLastname()));
            customViewHolder.tv_tel.setText(Html.fromHtml("เบอร์โทร:" + feedItem.getTel()));
            customViewHolder.tv_status.setText(Html.fromHtml("ตำแหน่ง:" + feedItem.getStatus_name()));


            customViewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    Intent in = new Intent(getActivity(), EmployeeEditActivity.class);
                    in.putExtra("employee_id", feedItem.getEmployee_id());
                    in.putExtra("branch_id", feedItem.getBranch_id());
                    in.putExtra("level_position", feedItem.getLevel_position());
                    in.putExtra("firstname", feedItem.getFirstname());
                    in.putExtra("lastname", feedItem.getLastname());
                    in.putExtra("email", feedItem.getEmail());
                    in.putExtra("username", feedItem.getUsername());
                    in.putExtra("password", feedItem.getPassword());
                    in.putExtra("create_by", Profile.UserId);
                    in.putExtra("store_id", Profile.StoreId);
                    in.putExtra("tel", feedItem.getTel());
                    startActivity(in);
                    //Toast.makeText(getActivity(),"Edit..." + feedItem.getId(),Toast.LENGTH_SHORT).show();
                }
            });
            customViewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());

                    adb.setTitle("ยืนยันการลบข้อมูล?");
                    adb.setMessage("ยืนยันการลบข้อมูล");
                    adb.setNegativeButton("Cancel", null);
                    adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
                        public void onClick(DialogInterface dialog, int arg1) {
                            // TODO Auto-generated method stub
                            EmpID = feedItem.getEmployee_id();
                            new DeleteItemTask().execute(StringUtil.URL + "gpos/api/EmployeeDelete");
                        }
                    });
                    adb.show();


                }
            });
            //customViewHolder.imageView.setImageResource(StringUtil.arrImg[i]);


        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            protected ImageView imageView;
            protected TextView tv_employee_name, tv_tel, tv_status;
            protected Button btnEdit;
            protected Button btnDelete;
            FeedItemDrug feedItem;

            public CustomViewHolder(View view) {
                super(view);

                this.tv_employee_name = (TextView) view.findViewById(R.id.txtCheckIn);
                this.tv_tel = (TextView) view.findViewById(R.id.tv_tel);
                this.tv_status = (TextView) view.findViewById(R.id.tv_status);
                this.btnEdit = (Button) view.findViewById(R.id.btnEdit);
                this.btnDelete = (Button) view.findViewById(R.id.btnDelete);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(getActivity(), EmployeeEditActivity.class);
                        intent.putExtra("employee_id", feedItem.getEmployee_id());
                        intent.putExtra("branch_id", feedItem.getBranch_id());
                        intent.putExtra("level_position", feedItem.getLevel_position());
                        intent.putExtra("firstname", feedItem.getFirstname());
                        intent.putExtra("lastname", feedItem.getLastname());
                        intent.putExtra("email", feedItem.getEmail());
                        intent.putExtra("username", feedItem.getUsername());
                        intent.putExtra("password", feedItem.getPassword());
                        intent.putExtra("create_by", Profile.UserId);
                        intent.putExtra("store_id", Profile.StoreId);
                        intent.putExtra("tel", feedItem.getTel());

                        startActivity(intent);


//                        StringUtil.CatId = feedItem.getId();
                        //new AsyncProduct().execute();
                        //Toast.makeText(v.getContext(), "Select is: " + feedItem.getId(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }
    }

    // String
    public class FeedItemDrug {

        private String employee_id;
        private String firstname;
        private String lastname;
        private String tel;
        private String status_name;
        private String branch_id;
        private String level_position;
        private String email;
        private String username;
        private String password;

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getLevel_position() {
            return level_position;
        }

        public void setLevel_position(String level_positionว) {
            this.level_position = level_positionว;
        }

        public String getBranch_id() {
            return branch_id;
        }

        public void setBranch_id(String branch_id) {
            this.branch_id = branch_id;
        }

        public String getEmployee_id() {
            return employee_id;
        }

        public void setEmployee_id(String employee_id) {
            this.employee_id = employee_id;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getStatus_name() {
            return status_name;
        }

        public void setStatus_name(String status_name) {
            this.status_name = status_name;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(EmployeeViewModel.class);
        // TODO: Use the ViewModel
    }

}
