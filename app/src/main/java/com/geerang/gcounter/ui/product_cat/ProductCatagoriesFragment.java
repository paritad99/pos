package com.geerang.gcounter.ui.product_cat;

import androidx.lifecycle.ViewModelProviders;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.PictureDrawable;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.geerang.gcounter.StockCategoryEditActivity;
import com.geerang.gcounter.ProductActivity;
import com.geerang.gcounter.Profile;
import com.geerang.gcounter.R;
import com.geerang.gcounter.StockCategoryActivity;
import com.geerang.gcounter.StringUtil;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ProductCatagoriesFragment extends Fragment {

    private ProductCatagoriesViewModel mViewModel;

    // CONNECTION_TIMEOUT and READ_TIMEOUT are in milliseconds
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;
    private List<FeedItemDrug> feedsList;
    private RecyclerView recyclerView;
    private DrugRecyclerAdapter mAdapter;

    public static ProductCatagoriesFragment newInstance() {
        return new ProductCatagoriesFragment();
    }

    View root;

    Button btnAddCat;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.product_catagories_fragment, container, false);

        btnAddCat = (Button) root.findViewById(R.id.btnAddCat);
        btnAddCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(getActivity(), StockCategoryActivity.class);
                startActivity(in);

            }
        });


        recyclerView = (RecyclerView) root.findViewById(R.id.recyclerView);
        new AsyncLogin().execute();


        return  root;
    }

    @Override
    public void onResume() {
        super.onResume();
        new AsyncLogin().execute();
    }

    private class AsyncLogin extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(getActivity());
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL+"gpos/api/category/"+ Profile.StoreId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();

            feedsList = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemDrug item = new FeedItemDrug();
                    item.setId(json_data.getString("categories_id"));
                    item.setTitle(json_data.getString("categories_name"));
                    item.setDetail(json_data.getString("categories_color"));
                    item.setProduct_image(json_data.getString("categories_image"));

                    feedsList.add(item);
                }

                // Setup and Handover data to recyclerview

                mAdapter = new DrugRecyclerAdapter(getActivity(), feedsList);
                recyclerView.setAdapter(mAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setItemAnimator(new DefaultItemAnimator());

            } catch (JSONException e) {
                Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    // Adapter
    public class DrugRecyclerAdapter extends RecyclerView.Adapter<DrugRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemDrug> feedItemList;
        private Context mContext;

        public DrugRecyclerAdapter(Context context, List<FeedItemDrug> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_stock_product_cat, null);

            CustomViewHolder viewHolder = new CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(CustomViewHolder customViewHolder, final int i) {
            final FeedItemDrug feedItem = feedItemList.get(i);
            customViewHolder.feedItem = feedItem;


            //Setting text view title
            customViewHolder.textView.setText(Html.fromHtml(feedItem.getTitle()));

            customViewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent in = new Intent(mContext, StockCategoryEditActivity.class);
                    in.putExtra("categories_id",feedItem.getId());
                    in.putExtra("categories_name",feedItem.getTitle());
                    in.putExtra("categories_color",feedItem.getDetail());
                    in.putExtra("categories_image",feedItem.getProduct_image());
                    startActivity(in);

                }
            });

            //customViewHolder.imageView.setImageResource(StringUtil.arrImg[i]);

            StringUtil stringUtil = new StringUtil();
            Picasso.with(mContext)
                    .load(StringUtil.URL+"gpos/"+ stringUtil.removeFirstChar(feedItem.getProduct_image()))
                    .centerCrop()
                    .fit()
                    .error(R.drawable.ic_error)
                    .into(customViewHolder.imageView);


        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            protected ImageView imageView;
            protected TextView textView;
            protected Button btnEdit;

            FeedItemDrug feedItem;

            public CustomViewHolder(View view) {
                super(view);
                this.imageView = (ImageView) view.findViewById(R.id.imageView);
                this.textView = (TextView) view.findViewById(R.id.title);
                this.btnEdit = (Button) view.findViewById(R.id.btnEdit);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        // StringUtil.title = feedItem.getTitle();
                        Intent intent = new Intent(v.getContext(), ProductActivity.class);
                        //intent.putExtra("Id", feedItem.getId());
                        //StringUtil.SelectCatImg = Integer.parseInt(feedItem.getId());
                        //intent.putExtra("Title", feedItem.getTitle());

                        //StringUtil.DrugDetail =feedItem.getDetail();
                        StringUtil.CatId = feedItem.getId();
                        v.getContext().startActivity(intent);

//                        Toast.makeText(v.getContext(), "Select is: " + feedItem.getTitle(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }
    }

    // String
    public class FeedItemDrug {
        private String title;
        private String id;
        private String setDetail;
        private String product_image;

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String name) {
            this.title = name;
        }


        public String getDetail() {
            return setDetail;
        }

        public void setDetail(String Detail) {
            this.setDetail = Detail;
        }


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(ProductCatagoriesViewModel.class);
        // TODO: Use the ViewModel
    }

}
