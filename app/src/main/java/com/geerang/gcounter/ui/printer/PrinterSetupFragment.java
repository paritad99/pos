package com.geerang.gcounter.ui.printer;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.PictureDrawable;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.geerang.gcounter.PrintDiscoveryActivity;
import com.geerang.gcounter.PrinterCashierActivity;
import com.geerang.gcounter.PrinterLocalActivity;
import com.geerang.gcounter.Profile;
import com.geerang.gcounter.R;
import com.geerang.gcounter.StringUtil;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class PrinterSetupFragment extends Fragment {

//    http://www.geerang.com/gpos/api/Menu/1


    public static PrinterSetupFragment newInstance() {
        return new PrinterSetupFragment();
    }

    // CONNECTION_TIMEOUT and READ_TIMEOUT are in milliseconds
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;
    private List<FeedItemDrug> feedsList;
    private RecyclerView recyclerView;
    private DrugRecyclerAdapter mAdapter;

    Button btnMainPrinter, btnPrinter1, btnPrinter2, btnPrinter3, btnPrinter4;

    final String KEY_Autoprint = "Autoprint";
    final String KEY_PrinterMain = "PrinterMain";
    final String KEY_Printer1 = "Printer1";
    final String KEY_Printer2 = "Printer2";
    final String KEY_Printer3 = "Printer3";
    final String KEY_Printer4 = "Printer4";
    final String KEY_Vat = "Vat";

    SharedPreferences sp;
    SharedPreferences.Editor editor;
    final String PREF_NAME = "PrinterPreferences";
    EditText input_vat;

    View root;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.printer_setup_fragment, container, false);
        recyclerView = (RecyclerView) root.findViewById(R.id.recyclerView);

        sp = getActivity().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sp.edit();


//        btnAddPrinter = (Button) root.findViewById(R.id.btnAddPrinter);
//        btnAddPrinter.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                    Intent in = new Intent(getActivity(),PrintDiscoveryActivity.class);
//                    startActivity(in);
//            }
//        });

        input_vat = (EditText) root.findViewById(R.id.input_vat);
        input_vat.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editor.putString(KEY_Vat, input_vat.getText().toString());
                editor.commit();

            }
        });


        btnMainPrinter = (Button) root.findViewById(R.id.btnMainPrinter);
        btnMainPrinter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(getActivity(), PrinterCashierActivity.class);
                in.putExtra("Printer", "MainPrinter");
                startActivity(in);

            }
        });

        btnPrinter1 = (Button) root.findViewById(R.id.btnPrinter1);
        btnPrinter1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getActivity(), PrinterLocalActivity.class);
                in.putExtra("Printer", "Printer1");
                startActivity(in);
            }
        });


        btnPrinter2 = (Button) root.findViewById(R.id.btnPrinter2);
        btnPrinter2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getActivity(), PrinterLocalActivity.class);
                in.putExtra("Printer", "Printer2");
                startActivity(in);
            }
        });

        btnPrinter3 = (Button) root.findViewById(R.id.btnPrinter3);
        btnPrinter3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getActivity(), PrinterLocalActivity.class);
                in.putExtra("Printer", "Printer3");
                startActivity(in);
            }
        });

        btnPrinter4 = (Button) root.findViewById(R.id.btnPrinter4);
        btnPrinter4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getActivity(), PrinterLocalActivity.class);
                in.putExtra("Printer", "Printer4");
                startActivity(in);
            }
        });


        // initiate a Switch
        final Switch simpleSwitch = (Switch) root.findViewById(R.id.swAutoPrint);
        // check current state of a Switch (true or false).
        Boolean switchState = simpleSwitch.isChecked();
        simpleSwitch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (simpleSwitch.isChecked()) {
                    editor.putString(KEY_Autoprint, "ON");
                    Toast.makeText(getActivity(), "Auto print is on", Toast.LENGTH_LONG).show();
                } else {
                    editor.putString(KEY_Autoprint, "OFF");
                    Toast.makeText(getActivity(), "Auto print is Off", Toast.LENGTH_LONG).show();
                }
                editor.commit();

            }
        });

        //new AsyncPrinter().execute();
        PrinterList();

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();

        editor = sp.edit();
        String vat = sp.getString(KEY_Vat, "");
        input_vat.setText(vat);
        //new AsyncPrinter().execute();
        PrinterList();
    }


    public void PrinterList() {

        editor = sp.edit();
        String PrinterMain = sp.getString(KEY_PrinterMain, "");
        if (!PrinterMain.equals("")) {
            //OpenPrinter+","+printer_title+","+printer_address+","+printer_mac_address+","+categories_id
            String[] namesList = PrinterMain.split(",");
            String status = namesList[0];
            String title = namesList[1];
            String address = namesList[2];
            String mac_address = namesList[3];
            String categories_id = namesList[4];

            btnMainPrinter.setText("เครื่องปริ้นหลัก (" + title + ") \nStatus " + categories_id + "");
        }

        String Printer1 = sp.getString(KEY_Printer1, "");
        if (!Printer1.equals("")) {
            String[] namesList = Printer1.split(",");
            String status = namesList[0];
            String title = namesList[1];
            String address = namesList[2];
            String mac_address = namesList[3];
            String categories_id = namesList[4];

            btnPrinter1.setText("Printer1 (" + title + ") \nStatus " + categories_id + "");
        }

        String Printer2 = sp.getString(KEY_Printer2, "");
        if (!Printer2.equals("")) {
            String[] namesList = Printer2.split(",");
            String status = namesList[0];
            String title = namesList[1];
            String address = namesList[2];
            String mac_address = namesList[3];
            String categories_id = namesList[4];

            btnPrinter2.setText("Printer2 (" + title + ") \nStatus " + categories_id + "");
        }

        String Printer3 = sp.getString(KEY_Printer3, "");
        if (!Printer3.equals("")) {

            String[] namesList = Printer3.split(",");
            String status = namesList[0];
            String title = namesList[1];
            String address = namesList[2];
            String mac_address = namesList[3];
            String categories_id = namesList[4];

            btnPrinter3.setText("Printer3 (" + title + ") \nStatus " + categories_id + "");
        }

        String Printer4 = sp.getString(KEY_Printer4, "");
        if (!Printer4.equals("")) {

            String[] namesList = Printer4.split(",");
            String status = namesList[0];
            String title = namesList[1];
            String address = namesList[2];
            String mac_address = namesList[3];
            String categories_id = namesList[4];

            btnPrinter4.setText("Printer4 (" + title + ") \nStatus " + categories_id + "");
        }

    }


    private void showData(String jsonString) {
        Toast.makeText(getActivity(), jsonString, Toast.LENGTH_LONG).show();
    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    private class AsyncPrinter extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(getActivity());
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/Printer/" + Profile.StoreId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();

            feedsList = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemDrug item = new FeedItemDrug();
                    item.setPrinter_address(json_data.getString("printer_ip"));
                    item.setPrinter_mac(json_data.getString("printer_mac"));
                    item.setPrinter_name(json_data.getString("printer_name"));
                    feedsList.add(item);
                }

                // Setup and Handover data to recyclerview

                mAdapter = new DrugRecyclerAdapter(getActivity(), feedsList);
                recyclerView.setAdapter(mAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setItemAnimator(new DefaultItemAnimator());

            } catch (JSONException e) {
                Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
            }


        }

    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    // Adapter
    public class DrugRecyclerAdapter extends RecyclerView.Adapter<DrugRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemDrug> feedItemList;
        private Context mContext;

        public DrugRecyclerAdapter(Context context, List<FeedItemDrug> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.printer_setup_row, null);

            CustomViewHolder viewHolder = new CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
            FeedItemDrug feedItem = feedItemList.get(i);
            customViewHolder.feedItem = feedItem;


            //Setting text view title
            customViewHolder.txtprinter_ip.setText("Ip Address:" + Html.fromHtml(feedItem.getPrinter_address()));
            customViewHolder.txtprinter_name.setText("Printer Name:" + Html.fromHtml(feedItem.getPrinter_name()));
            customViewHolder.txtprinter_mac.setText("Mac Address:" + Html.fromHtml(feedItem.getPrinter_mac()));

        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            protected TextView txtprinter_ip, txtprinter_name, txtprinter_mac;

            FeedItemDrug feedItem;

            public CustomViewHolder(View view) {
                super(view);
                this.txtprinter_ip = (TextView) view.findViewById(R.id.txtprinter_ip);
                this.txtprinter_name = (TextView) view.findViewById(R.id.txtprinter_name);
                this.txtprinter_mac = (TextView) view.findViewById(R.id.txtprinter_mac);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StringUtil.ProductId = feedItem.getId_printer();
                        Intent in = new Intent(getActivity(), PrintDiscoveryActivity.class);
                        startActivity(in);
                    }
                });

            }
        }
    }


    // String
    public class FeedItemDrug {
        private String printer_name;
        private String printer_address;
        private String printer_mac;
        private String id_printer;

        public String getId_printer() {
            return id_printer;
        }

        public void setId_printer(String id_printer) {
            this.id_printer = id_printer;
        }

        public String getPrinter_address() {
            return printer_address;
        }

        public String getPrinter_mac() {
            return printer_mac;
        }

        public String getPrinter_name() {
            return printer_name;
        }

        public void setPrinter_address(String printer_address) {
            this.printer_address = printer_address;
        }

        public void setPrinter_mac(String printer_mac) {
            this.printer_mac = printer_mac;
        }

        public void setPrinter_name(String printer_name) {
            this.printer_name = printer_name;
        }
    }


}
