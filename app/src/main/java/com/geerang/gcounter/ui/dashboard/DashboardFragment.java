package com.geerang.gcounter.ui.dashboard;

import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.geerang.gcounter.Profile;
import com.geerang.gcounter.R;
import com.geerang.gcounter.ReportDialyActivity;
import com.geerang.gcounter.ReportMonthlyActivity;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.DefaultAxisValueFormatter;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class DashboardFragment extends Fragment {


    public static DashboardFragment newInstance() {
        return new DashboardFragment();
    }


    TextView txtDateDay, txtStoreName, txtBranchName, txtUsername, txtTotalEmployee, txtDialySale, txtMonthlySale;

    //BarChart mChart;
    ArrayList<HashMap<String, Integer>> myList;
    BarChart chart, barchartCustomer;

    View root;
    Format formatter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.dashboard_fragment, container, false);


        formatter = new SimpleDateFormat("EEEE ที่ dd เดือน MMMM พ.ศ. yyyy", new Locale("th", "TH"));
        System.out.println(formatter.format(new Date()));

        txtDateDay = (TextView) root.findViewById(R.id.txtDateDay);
        txtDateDay.setText(formatter.format(new Date()));

        txtUsername = (TextView) root.findViewById(R.id.txtUsername);
        txtUsername.setText("ชื่อผู้ใช้งาน: " + Profile.Username);


        txtStoreName = (TextView) root.findViewById(R.id.txtStoreName);
        txtBranchName = (TextView) root.findViewById(R.id.txtBranchName);

        txtTotalEmployee = (TextView) root.findViewById(R.id.txtTotalEmployee);
        txtDialySale = (TextView) root.findViewById(R.id.txtDialySale);
        txtMonthlySale = (TextView) root.findViewById(R.id.txtMonthlySale);

        myList = new ArrayList<HashMap<String, Integer>>();

        chart = (BarChart) root.findViewById(R.id.barchart);

        barchartCustomer = (BarChart) root.findViewById(R.id.barchartCustomer);

        new AsyncChoiceProduct().execute("http://www.geerang.com/gpos/api/DashboardManager");


        return root;
    }

    public void create_graph_customer(List<String> graph_label, List<Integer> userScore) {

        try {
            barchartCustomer.setDrawBarShadow(false);
            barchartCustomer.setDrawValueAboveBar(true);
            barchartCustomer.getDescription().setEnabled(false);
            barchartCustomer.setPinchZoom(false);

            barchartCustomer.setDrawGridBackground(false);


            YAxis yAxis = barchartCustomer.getAxisLeft();
            yAxis.setValueFormatter(new IndexAxisValueFormatter() {

                public String getFormattedValueString(float value, AxisBase axis) {
                    return String.valueOf((int) value);
                }
            });

            yAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);


            yAxis.setGranularity(1f);
            yAxis.setGranularityEnabled(true);

            barchartCustomer.getAxisRight().setEnabled(false);


            XAxis xAxis = barchartCustomer.getXAxis();
            xAxis.setGranularity(1f);
            xAxis.setGranularityEnabled(true);
            xAxis.setCenterAxisLabels(false);

            xAxis.setDrawGridLines(true);

            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            //xAxis.setTextSize(7);
            xAxis.setValueFormatter(new IndexAxisValueFormatter(graph_label));

            List<BarEntry> yVals1 = new ArrayList<BarEntry>();

            for (int i = 0; i < userScore.size(); i++) {
                yVals1.add(new BarEntry(i, userScore.get(i)));
            }


            BarDataSet set1;

            if (barchartCustomer.getData() != null && barchartCustomer.getData().getDataSetCount() > 0) {
                set1 = (BarDataSet) barchartCustomer.getData().getDataSetByIndex(0);
                set1.setValues(yVals1);
                barchartCustomer.getData().notifyDataChanged();
                barchartCustomer.notifyDataSetChanged();
            } else {
                // create 2 datasets with different types
                set1 = new BarDataSet(yVals1, "จำนวนลูกค้ารายเดือน");
                set1.setColor(Color.rgb(0, 127, 63));


                ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
                dataSets.add(set1);

                BarData data = new BarData(dataSets);
                barchartCustomer.setData(data);


            }

            barchartCustomer.setFitBars(true);

            Legend l = barchartCustomer.getLegend();
//            l.setFormSize(12f); // set the size of the legend forms/shapes
//            l.setForm(Legend.LegendForm.SQUARE); // set what type of form/shape should be used
//
//            lsetPosition(Legend.LegendPosition.RIGHT_OF_CHART_INSIDE);
            l.setTextSize(10f);
            l.setTextColor(Color.BLACK);
            l.setXEntrySpace(5f); // set the space between the legend entries on the x-axis
            l.setYEntrySpace(5f); // set the space between the legend entries on the y-axis

            barchartCustomer.invalidate();

            barchartCustomer.animateY(2000);

        } catch (Exception ignored) {
        }
    }


    public void create_graph(List<String> graph_label, List<Integer> userScore) {

        try {
            chart.setDrawBarShadow(false);
            chart.setDrawValueAboveBar(true);
            chart.getDescription().setEnabled(false);
            chart.setPinchZoom(false);

            chart.setDrawGridBackground(false);


            YAxis yAxis = chart.getAxisLeft();
            yAxis.setValueFormatter(new IndexAxisValueFormatter() {

                public String getFormattedValueString(float value, AxisBase axis) {
                    return String.valueOf((int) value);
                }
            });

            yAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);


            yAxis.setGranularity(1f);
            yAxis.setGranularityEnabled(true);

            chart.getAxisRight().setEnabled(false);


            XAxis xAxis = chart.getXAxis();
            xAxis.setGranularity(1f);
            xAxis.setGranularityEnabled(true);
            xAxis.setCenterAxisLabels(false);

            xAxis.setDrawGridLines(true);

            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            //xAxis.setTextSize(7);
            xAxis.setValueFormatter(new IndexAxisValueFormatter(graph_label));

            List<BarEntry> yVals1 = new ArrayList<BarEntry>();

            for (int i = 0; i < userScore.size(); i++) {
                yVals1.add(new BarEntry(i, userScore.get(i)));
            }


            BarDataSet set1;

            if (chart.getData() != null && chart.getData().getDataSetCount() > 0) {
                set1 = (BarDataSet) chart.getData().getDataSetByIndex(0);
                set1.setValues(yVals1);
                chart.getData().notifyDataChanged();
                chart.notifyDataSetChanged();
            } else {
                // create 2 datasets with different types
                set1 = new BarDataSet(yVals1, "ยอดขายรายเดือน");
                set1.setColor(Color.rgb(255, 204, 0));


                ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
                dataSets.add(set1);

                BarData data = new BarData(dataSets);
                chart.setData(data);


            }

            chart.setFitBars(true);

            Legend l = chart.getLegend();
//            l.setFormSize(12f); // set the size of the legend forms/shapes
//            l.setForm(Legend.LegendForm.SQUARE); // set what type of form/shape should be used
//
//            lsetPosition(Legend.LegendPosition.RIGHT_OF_CHART_INSIDE);
            l.setTextSize(10f);
            l.setTextColor(Color.BLACK);
            l.setXEntrySpace(5f); // set the space between the legend entries on the x-axis
            l.setYEntrySpace(5f); // set the space between the legend entries on the y-axis

            chart.invalidate();

            chart.animateY(2000);

        } catch (Exception ignored) {
        }
    }


    @Override
    public void onResume() {
        super.onResume();

        new AsyncChoiceProduct().execute("http://www.geerang.com/gpos/api/DashboardManager");
    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    private class AsyncChoiceProduct extends AsyncTask<String, String, String> {

        ProgressDialog pdLoading = new ProgressDialog(getActivity());
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread

            pdLoading.setMessage("\tกำลังโหลดข้อมูล...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("store_id", Profile.StoreId)
                    .add("branch_id", Profile.BranchId)
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            try {

                pdLoading.dismiss();
                JSONArray jArray = new JSONArray(result);
                try {

                    //final ArrayList<BarEntry> entries = new ArrayList<>();
                    //final ArrayList<Student> listStudent = Student.getSampleStudentData(30);

                    // Extract data from json and store into ArrayList as class objects
                    for (int i = 0; i < jArray.length(); i++) {
                        JSONObject json_data = jArray.getJSONObject(i);
                        //ReportMonthlyActivity.FeedItemChoice item = new ReportMonthlyActivity.FeedItemChoice();

                        txtBranchName.setText("สาขา : " + json_data.getString("branch_name"));
                        txtStoreName.setText("ร้าน : " + json_data.getString("store_name"));


                        DecimalFormat formatter = new DecimalFormat("#,###");
                        double dialy_sale = 0.0, monthly_sale = 0.0;

                        try {
                            dialy_sale = Double.parseDouble(json_data.getString("dialy_sale"));
                            txtDialySale.setText(formatter.format(dialy_sale));
                        } catch (Exception e) {
                            txtDialySale.setText("0");
                        }

                        try {
                            monthly_sale = Double.parseDouble(json_data.getString("monthly_sale"));
                            txtMonthlySale.setText(formatter.format(monthly_sale));
                        } catch (Exception e) {
                            txtMonthlySale.setText("0");
                        }

                        //System.out.println(formatter.format(dialy_sale));

                        txtTotalEmployee.setText(json_data.getString("total_employee"));


                        // loop chart
                        //HashMap<String, Integer> data1 = new HashMap<String, Integer>();

                        List<Integer> entries = new ArrayList<>();
                        List<String> labels = new ArrayList<>();

                        JSONArray jArray2 = new JSONArray(json_data.getString("report_sale"));
                        for (int j = 0; j < jArray2.length(); j++) {
                            JSONObject json_data2 = jArray2.getJSONObject(j);

                            int x = Integer.parseInt(json_data2.getString("total"));
                            String day = json_data2.getString("payment_date");
                            entries.add(x);
                            labels.add("" + day);

                        }
                        create_graph(labels, entries);

                        // create customer total chart
                        List<Integer> entries2 = new ArrayList<>();
                        List<String> labels2 = new ArrayList<>();
                        JSONArray jArray3 = new JSONArray(json_data.getString("report_customer"));
                        for (int k = 0; k < jArray3.length(); k++) {
                            JSONObject json_data3 = jArray3.getJSONObject(k);

                            int x = Integer.parseInt(json_data3.getString("total_customer"));
                            String day = json_data3.getString("payment_date");
                            entries2.add(x);
                            labels2.add("" + day);

                        }

                        create_graph_customer(labels2, entries2);

                        //myList.add(data1);


                    }


                } catch (JSONException e) {
                    String x = "";
                    x = e.toString();
                    //Toast.makeText(ReportMonthlyActivity.this, e.toString(), Toast.LENGTH_LONG).show();
                }


            } catch (JSONException e) {
                String x = "";
                x = e.toString();
//                txt_status.setVisibility(View.VISIBLE);
                //Toast.makeText(ReportMonthlyActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }


}
