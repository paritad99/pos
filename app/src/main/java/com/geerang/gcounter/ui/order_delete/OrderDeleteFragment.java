package com.geerang.gcounter.ui.order_delete;

import androidx.lifecycle.ViewModelProviders;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.PictureDrawable;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.geerang.gcounter.OrderDeleteReviewActivity;
import com.geerang.gcounter.Profile;
import com.geerang.gcounter.R;
import com.geerang.gcounter.StringUtil;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class OrderDeleteFragment extends Fragment {

    private OrderDeleteViewModel mViewModel;

    public static OrderDeleteFragment newInstance() {
        return new OrderDeleteFragment();
    }

    private final OkHttpClient client = new OkHttpClient();
    private List<FeedItemDrug> feedsList;
    private CategoriesRecyclerAdapter mAdapter;

    RecyclerView rvCatagory;


    Profile profile;

    View root;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
//        http://www.geerang.com/gpos/api/OrderDeleting/1

        root = inflater.inflate(R.layout.fragment_order_delete, container, false);
        rvCatagory = (RecyclerView) root.findViewById(R.id.rvTable);

        profile = new Profile(getActivity());

        new AsyncLoadCat().execute();

        return root;
    }


    @Override
    public void onResume() {
        super.onResume();

        new AsyncLoadCat().execute();

    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    private class AsyncLoadCat extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(getActivity());
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL+"gpos/api/StaffOrderDeleteApproving/"+ profile.BranchId);
//                response = http.run(StringUtil.URL+"gpos/api/category/1");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();

            feedsList = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemDrug item = new FeedItemDrug();
                    item.setOrder_id(json_data.getString("orders_item_id"));
                    item.setMenu_name(json_data.getString("product_name"));
                    item.setUsername(json_data.getString("username"));
                    item.setTel(json_data.getString("tel"));
                    item.setRemain(json_data.getString("remain"));
                    item.setDateCancel(json_data.getString("is_cancel_date"));

                    feedsList.add(item);
                }

                // Setup and Handover data to recyclerview

                mAdapter = new CategoriesRecyclerAdapter(getActivity(), feedsList);
                rvCatagory.setAdapter(mAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
                rvCatagory.setLayoutManager(mLayoutManager);
                rvCatagory.setItemAnimator(new DefaultItemAnimator());
                rvCatagory.setItemAnimator(new DefaultItemAnimator());

            } catch (JSONException e) {
                Toast.makeText(getActivity(), "ไม่พบข้อมูล!", Toast.LENGTH_LONG).show();
            }

        }

    }

    public class CategoriesRecyclerAdapter extends RecyclerView.Adapter<CategoriesRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemDrug> feedItemList;
        private Context mContext;

        public CategoriesRecyclerAdapter(Context context, List<FeedItemDrug> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_order_delete, null);

            CustomViewHolder viewHolder = new CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
            FeedItemDrug feedItem = feedItemList.get(i);
            customViewHolder.feedItem = feedItem;

            //Setting text view title
            customViewHolder.txtUsername.setText(Html.fromHtml("ชื่อพนักงาน :"+feedItem.getUsername()));
            customViewHolder.txtMenu.setText(Html.fromHtml("รายการ:"+feedItem.getMenu_name()));
            customViewHolder.txtRemain.setText(Html.fromHtml("เหตุผล:"+feedItem.getRemain()));
            customViewHolder.txtDateCancel.setText(Html.fromHtml("วันที่เวลา:"+feedItem.getDateCancel()));

            //customViewHolder.imageView.setImageResource(StringUtil.arrImg[i]);


        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            protected ImageView imageView;
            protected TextView txtMenu,txtUsername,txtRemain,txtDateCancel;

            FeedItemDrug feedItem;

            public CustomViewHolder(View view) {
                super(view);

                this.txtMenu = (TextView) view.findViewById(R.id.txtMenu);
                this.txtUsername = (TextView) view.findViewById(R.id.txtUsername);
                this.txtRemain = (TextView) view.findViewById(R.id.txtRemain);
                this.txtDateCancel = (TextView) view.findViewById(R.id.txtDateCancel);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        StringUtil.OrderId_Delete = feedItem.getOrder_id();
                        StringUtil.OrderMenuname_Delete = feedItem.getMenu_name();
                        StringUtil.OrderRemain_Delete = feedItem.getRemain();
                        StringUtil.OrderUsername_Delete= feedItem.getUsername();
                        StringUtil.OrderTel_Delete= feedItem.getTel();


                        Intent intent = new Intent(getActivity(), OrderDeleteReviewActivity.class);
                        startActivity(intent);

//                        StringUtil.CatId = feedItem.getId();
                        //new AsyncProduct().execute();
                        //Toast.makeText(v.getContext(), "Select is: " + feedItem.getId(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }
    }

    // String
    public class FeedItemDrug {

        private String order_id;
        private String menu_name;
        private String username;
        private String remain;
        private String tel;
        private String DateCancel;

        public String getDateCancel() {
            return DateCancel;
        }

        public void setDateCancel(String dateCancel) {
            DateCancel = dateCancel;
        }

        public String getMenu_name() {
            return menu_name;
        }

        public void setMenu_name(String menu_name) {
            this.menu_name = menu_name;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getRemain() {
            return remain;
        }

        public void setRemain(String remain) {
            this.remain = remain;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }

    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(OrderDeleteViewModel.class);
        // TODO: Use the ViewModel
    }

}
