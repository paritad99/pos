package com.geerang.gcounter.ui.zone;

import androidx.appcompat.app.AlertDialog;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.PictureDrawable;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.geerang.gcounter.CashierActivity;
import com.geerang.gcounter.Profile;
import com.geerang.gcounter.R;
import com.geerang.gcounter.StringUtil;
import com.geerang.gcounter.TableAddActivity;
import com.geerang.gcounter.ZoneAddActivity;
import com.geerang.gcounter.ZoneEditActivity;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class ZoneFragment extends Fragment {

    private ZoneFragment mViewModel;

    public static ZoneFragment newInstance() {
        return new ZoneFragment();
    }

    private List<FeedItemDrug> feedsList;
    private RecyclerView recyclerView;
    private DrugRecyclerAdapter mAdapter;

    Handler handler = new Handler();
    Timer timer = new Timer();
    TimerTask timetask;
    Button btnAddZone;
    View root;
    String zone_ID = "";

    TextView txtTotalZone;
    int TotalZone = 0 ;
    Profile  profile;

    final String PREF_NAME = "LoginPreferences";
    final String KEY_USERNAME = "Username";
    final String KEY_USERID = "UserId";
    final String KEY_BranchId = "BranchId";
    final String KEY_StoreId = "StoreId";
    final String KEY_Status = "Status";
    final String KEY_REMEMBER = "RememberUsername";
    final String KEY_business_type_id = "business_type_id";
    final String KEY_StoreName = "StoreName";
    final String KEY_BranchName = "BranchName";
    final String KEY_Position = "Position";
    final String KEY_StoreExp = "StoreExp";
    final String KEY_Owner = "Owner";
    final String KEY_package = "package";
    final String KEY_StoreTax = "StoreTax";
    final String KEY_StorePhone = "StoreTax";
    SharedPreferences sp;
    SharedPreferences.Editor editor;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.zone_fragment, container, false);

        txtTotalZone = (TextView) root.findViewById(R.id.txtTotalZone);

        btnAddZone = (Button) root.findViewById(R.id.btnAddZone);
        btnAddZone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getActivity(), ZoneAddActivity.class);
                startActivity(in);
            }
        });
        recyclerView = (RecyclerView) root.findViewById(R.id.rvTable);

        Profile  profile = new Profile(getActivity());
        sp = getActivity().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sp.edit();
        String store_package = sp.getString(KEY_package, "");
        try {
            if (!store_package.equals("")) {
                JSONArray jArray = new JSONArray(store_package);
                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    StringUtil.package_name = json_data.getString("package_name");
                    StringUtil.package_product = json_data.getString("package_product");
                    StringUtil.package_branch = json_data.getString("package_branch");
                    StringUtil.package_cashier = json_data.getString("package_cashier");
                    StringUtil.package_users = json_data.getString("package_users");
                    StringUtil.package_zone = json_data.getString("package_zone");
                    StringUtil.package_table = json_data.getString("package_table");
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new AsyncLogin().execute();
        return  root;
    }

    @Override
    public void onResume() {
        super.onResume();

        Profile  profile = new Profile(getActivity());
        sp = getActivity().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sp.edit();
        String store_package = sp.getString(KEY_package, "");
        try {
            if (!store_package.equals("")) {
                JSONArray jArray = new JSONArray(store_package);
                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    StringUtil.package_name = json_data.getString("package_name");
                    StringUtil.package_product = json_data.getString("package_product");
                    StringUtil.package_branch = json_data.getString("package_branch");
                    StringUtil.package_cashier = json_data.getString("package_cashier");
                    StringUtil.package_users = json_data.getString("package_users");
                    StringUtil.package_zone = json_data.getString("package_zone");
                    StringUtil.package_table = json_data.getString("package_table");
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new AsyncLogin().execute();
    }

    private class AsyncLogin extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(getActivity());
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL+"gpos/api/Zone/"+ profile.BranchId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            //txtResult.setText(response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();

            feedsList = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);

                TotalZone = jArray.length();
                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemDrug item = new FeedItemDrug();
                    item.setId(json_data.getString("building_zone_id"));
                    item.setTitle(json_data.getString("zone_name"));
                    item.setBid(json_data.getString("bid"));
//                  item.setDetail(json_data.getString("building_address"));
                    feedsList.add(item);
                }
                int zone =0;
                try {
                     zone = Integer.parseInt(StringUtil.package_zone);
                }catch (Exception err)
                {
                    zone = 0;
                }

                if(TotalZone>=zone)
                {
                    btnAddZone.setEnabled(false);
                }else
                {
                    btnAddZone.setEnabled(true);
                }

                txtTotalZone.setText(TotalZone+"/"+zone);
                // Setup and Handover data to recyclerview

                mAdapter = new DrugRecyclerAdapter(getActivity(), feedsList);
                recyclerView.setAdapter(mAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setItemAnimator(new DefaultItemAnimator());

            } catch (JSONException e) {
                Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    private class DeleteItemTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
        }

        protected String doInBackground(String... urls) {
            String response = null;
            ZoneFragment.postHttp http = new ZoneFragment.postHttp();


            RequestBody formBody = new FormEncodingBuilder()
                    .add("building_zone_id", zone_ID)
                    .build();


            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success


                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    new ZoneFragment.AsyncLogin().execute();

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    new ZoneFragment.AsyncLogin().execute();

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                    //String order_id = object.getString("order_id"); // success

                    // keep to var ORderID
                    ///StringUtil.OrderId = order_id;

//                    Intent in = new Intent(ProductActivity.this, ListOrderActivity.class);
//                    startActivity(in);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
    // แบบ auto print task
    public void doTask(){

        timetask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {

                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String formattedDate = df.format(c.getTime());

                        // txtResult
                        //TextView result = (TextView) findViewById(R.id.txtResult);
                        //result.setText("TimeTask runing... Current : " + formattedDate);
                    }
                });
            }};
        timer.schedule(timetask, 0, 1000); // Every 1 second

    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    // Adapter
    public class DrugRecyclerAdapter extends RecyclerView.Adapter<DrugRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemDrug> feedItemList;
        private Context mContext;

        public DrugRecyclerAdapter(Context context, List<FeedItemDrug> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.building_zone_row, null);

            CustomViewHolder viewHolder = new CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
            FeedItemDrug feedItem = feedItemList.get(i);
            customViewHolder.feedItem = feedItem;


            //Setting text view title
            customViewHolder.textView.setText(Html.fromHtml("ชื่อโซน : " + feedItem.getTitle()));
//            customViewHolder.status.setText(Html.fromHtml(feedItem.getDetail()));

            customViewHolder.BtnTableAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    StringUtil.zone_id = feedItem.getId();
                    StringUtil.zone_name = feedItem.getTitle() ;
                    Intent in = new Intent(getActivity(), TableAddActivity.class);
                    startActivity(in);
                }
            });

            customViewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    StringUtil.zone_id = feedItem.getId();
                    StringUtil.zone_Name = feedItem.getTitle();
                    StringUtil.zone_Bid = feedItem.getBid();
                    Intent in = new Intent(getActivity(), ZoneEditActivity.class);
                    startActivity(in);
                    //Toast.makeText(getActivity(),"Edit..." + feedItem.getId(),Toast.LENGTH_SHORT).show();
                }
            });


            customViewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());

                    adb.setTitle("ยืนยันการลบข้อมูล?");
                    adb.setMessage("ยืนยันการลบข้อมูล");
                    adb.setNegativeButton("Cancel", null);
                    adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
                        public void onClick(DialogInterface dialog, int arg1) {
                            // TODO Auto-generated method stub
                            zone_ID = feedItem.getId();
                            new DeleteItemTask().execute(StringUtil.URL + "gpos/api/ZoneDelete");
                        }
                    });
                    adb.show();
                    //Toast.makeText(getActivity(),"Delete..." + feedItem.getId(),Toast.LENGTH_SHORT).show();
                }
            });

        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            protected ImageView imageView;
            protected TextView textView;
            protected TextView status;
            protected Button btnEdit;
            protected  Button  btnDelete;
            protected  Button  BtnTableAdd;
            FeedItemDrug feedItem;

            public CustomViewHolder(View view) {
                super(view);
                // this.imageView = (ImageView) view.findViewById(R.id.imageView2);
                this.textView = (TextView) view.findViewById(R.id.title);
                this.status = (TextView) view.findViewById(R.id.tv_status);
                this.BtnTableAdd = (Button) view.findViewById(R.id.btnAddTable);
                this.btnEdit = (Button) view.findViewById(R.id.btnEdit);
                this.btnDelete = (Button) view.findViewById(R.id.btnDelete);


//                view.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                        String status = StringUtil.Id = feedItem.getDetail();
//                        StringUtil.TableCustomerName = feedItem.getTitle();
//                        StringUtil.BranchId = feedItem.getId();
//                        StringUtil. TableCustomer = feedItem.getTitle();
//
//                        // ถ้าว่าง
//                        if (status.equals("มีการจอง")) {
//                            StringUtil.Id = feedItem.getId();
//                            new AsyncLoadOrderId().execute();
//
//                        } else {
//                            StringUtil.Id = feedItem.getId();
//                            StringUtil.TotalCustomer = "1";
//                            new AddOrderTask().execute(StringUtil.URL+"gpos/api/AddOrder");
//
//                        }
//
//                    }
//                });

            }
        }
    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();
        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    private class AddOrderTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
        }

        protected String doInBackground(String... urls)   {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("UserId", Profile.UserId)
                    .add("StoreId", Profile.StoreId)
                    .add("BranchId", Profile.BranchId)
                    .add("Customer",  StringUtil.TotalCustomer)
                    .add("CustomerId", StringUtil.Id)
                    .build();


            try {

                response = http.run(urls[0],formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString)  {
            // Dismiss ProgressBar
            //showData(jsonString);

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success

                if(status.equals("0"))
                {
                    String msg = object.getString("msg"); // success
                    androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

                }else
                {

                    String order_id = object.getString("order_id"); // success

                    // keep to var ORderID
                    StringUtil.OrderId = order_id;

                    Intent intent = new Intent(getActivity(), CashierActivity.class);
                    //StringUtil.Id = order_id;
                    StringUtil.OrderId = order_id;
                    startActivity(intent);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


    private class AsyncLoadOrderId extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(getActivity());
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL+"gpos/api/GetOrderList/"+ StringUtil.Id);
//                response = http.run(StringUtil.URL+"gpos/api/category/1");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();

            // feedsList = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);


                    StringUtil.TableCustomerName = json_data.getString("customer_name");
                    StringUtil.BranchId = json_data.getString("branch_id");
                    StringUtil.OrderId = json_data.getString("order_id");

                    //StringUtil.TableCustomer = json_data.getString("total_sale");

                    // Show total sale
                    //tvSumAmount.setText("ยอดขายวันนี้ "+json_data.getString("total_sale")+" บาท");

                }




                Intent intent = new Intent(getActivity(), CashierActivity.class);
                startActivity(intent);


            } catch (JSONException e) {
                Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }


    // String
    public class FeedItemDrug {
        private String title;
        private String id;
        private String setDetail;
        private  String bid;
        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String name) {
            this.title = name;
        }


        public String getDetail() {
            return setDetail;
        }

        public void setDetail(String Detail) {
            this.setDetail = Detail;
        }
        public String getBid() {
            return bid;
        }

        public void setBid(String bid) {
            this.bid = bid;
        }

    }



}
