package com.geerang.gcounter.ui.cashier;

import androidx.appcompat.app.AlertDialog;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.PictureDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.IBinder;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.epson.epos2.Epos2Exception;
import com.epson.epos2.printer.Printer;
import com.epson.epos2.printer.PrinterStatusInfo;
import com.epson.epos2.printer.ReceiveListener;
import com.geerang.gcounter.CashireCheckOutOrderActivity;
import com.geerang.gcounter.EpsonMakeErrorMessage;
import com.geerang.gcounter.MoveTableActivity;
import com.geerang.gcounter.NointernetActivity;
import com.geerang.gcounter.OpenTableOrderActivity;
import com.geerang.gcounter.Profile;
import com.geerang.gcounter.R;
import com.geerang.gcounter.ShowMsg;
import com.geerang.gcounter.SplashScreenActivity;
import com.geerang.gcounter.StringUtil;
import com.geerang.gcounter.TimePrintService;
import com.geerang.gcounter.ui.delivery.OrderDeliveryFragment;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import recieptservice.com.recieptservice.PrinterInterface;

public class CashireTableFragment extends Fragment implements ReceiveListener {


    public static CashireTableFragment newInstance() {
        return new CashireTableFragment();
    }


    private List<FeedItemZone> feedsListZone;
    private List<FeedItemDrug> feedsList;
    private RecyclerView recyclerViewTable, rvTable, rvZone;
    private DrugRecyclerAdapter mAdapter;
    private TableOrderRecyclerAdapter OrderAdapter;

    private static final int REQUEST_PERMISSION = 100;
    View root;

    Button btnSelectTable, btnAddTable;
    EditText input_customer_qty;

    ImageView imgEmpty;

    TextView txtShowEmpty, txtZone;
    AlertDialog dialog;
    int total_price = 0;
    int total_qty = 0;
    int check_order_place = 0;

    Profile profile;

    String Resultbill;

    ProgressBar progressBar3;

    //    ArrayList<String> ar;
//    List<List<String>> t2Rows;
    //IBinder service;
    PrinterInterface mAidl;

    private static final int DISCONNECT_INTERVAL = 500;//millseconds

    private Context mContext = null;
    public static EditText mEditTarget = null;
    public static Spinner mSpnSeries = null;
    public static Spinner mSpnLang = null;
    public static Printer mPrinter = null;
    public static ToggleButton mDrawer = null;

    EpsonMakeErrorMessage epsonMakeErrorMessage;
    final String KEY_Autoprint = "Autoprint";
    final String KEY_PrinterMain = "PrinterMain";
    final String KEY_Printer1 = "Printer1";
    final String KEY_Printer2 = "Printer2";
    final String KEY_Printer3 = "Printer3";
    final String KEY_Printer4 = "Printer4";

    String PrinterAddress = "";

    SharedPreferences sp;
    SharedPreferences.Editor editor;
    final String PREF_NAME = "PrinterPreferences";

    Handler handler = new Handler();
    Timer timer = new Timer();
    TimerTask timetask;
    String ZoneId = "";
    String ZoneName = "";
    TextView txtDatetime;
    private TableZoneRecyclerAdapter ZoneAdapter;
    final String PREF_NAME_PRINT = "PrinterPreferences";

    SharedPreferences sp_print;
    SharedPreferences.Editor editor_print;

    String product_name = "";
    String report_cat_name = "";
    String customer_name = "";
    String username = "";
    String kit_cat = "";
    String create_date = "";

    String orders_item_id_print;

    TextView txtLoadStatus;
    DecimalFormat formatter;

    //private ProgressBar progressBarTable;

    private boolean haveNetworkConnection() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

//    private boolean haveNetworkConnection() {
//        boolean haveConnectedWifi = false;
//        boolean haveConnectedMobile = false;
//
//        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
//
//        for (NetworkInfo ni : netInfo) {
//            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
//                if (ni.isConnected())
//                    haveConnectedWifi = true;
//            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
//                if (ni.isConnected())
//                    haveConnectedMobile = true;
//        }
//        return haveConnectedWifi || haveConnectedMobile;
//    }


    int discount_total = 0;
    String discount_type_menu = "";
    int include_vat = 7;

    int services = 0;
    boolean vat_check;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.cashire_table_fragment, container, false);

        epsonMakeErrorMessage = new EpsonMakeErrorMessage(getActivity());

        profile = new Profile(getActivity());

        rvTable = (RecyclerView) root.findViewById(R.id.rvTable);
        imgEmpty = (ImageView) root.findViewById(R.id.imgEmpty);
        txtShowEmpty = (TextView) root.findViewById(R.id.txtShowEmpty);

        txtDatetime = (TextView) root.findViewById(R.id.txtDatetime);

        formatter = new DecimalFormat("#,###.00");

        txtLoadStatus = (TextView) root.findViewById(R.id.txtLoadStatus);

        txtZone = (TextView) root.findViewById(R.id.txtZone);
        rvZone = (RecyclerView) root.findViewById(R.id.rvZone);

        btnAddTable = (Button) root.findViewById(R.id.btnAddTable);
        btnAddTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ZoneId.equals("")) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("กรุณาเลือกโซน")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

                } else {

                    LayoutInflater inflater = getLayoutInflater();
                    View alertLayout = inflater.inflate(R.layout.activity_select_table_dialog, null);
                    recyclerViewTable = (RecyclerView) alertLayout.findViewById(R.id.recyclerViewTable);

                    //progressBarTable = (ProgressBar) alertLayout.findViewById(R.id.progressBarTable);

                    TextView txtCustomerZone = (TextView) alertLayout.findViewById(R.id.txtCustomerZone);
                    txtCustomerZone.setText("โซน: " + ZoneName);
                    input_customer_qty = (EditText) alertLayout.findViewById(R.id.input_customer_qty);
                    //final Button btnSelectOk = (Button) alertLayout.findViewById(R.id.btnSelectOk);
                    // Load ข้อมูลโต๊ะ
                    new AsyncTableValid().execute();


                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    //alert.setTitle("");

                    // this is set the view from XML inside AlertDialog
                    alert.setView(alertLayout);
                    dialog = alert.create();
                    dialog.show();
                }

            }
        });

        btnSelectTable = (Button) root.findViewById(R.id.btnSelectTable);
        btnSelectTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ZoneId.equals("")) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("กรุณาเลือกโซน")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

                } else {

                    LayoutInflater inflater = getLayoutInflater();
                    View alertLayout = inflater.inflate(R.layout.activity_select_table_dialog, null);
                    recyclerViewTable = (RecyclerView) alertLayout.findViewById(R.id.recyclerViewTable);

                    TextView txtCustomerZone = (TextView) alertLayout.findViewById(R.id.txtCustomerZone);
                    txtCustomerZone.setText("โซน: " + ZoneName);
                    input_customer_qty = (EditText) alertLayout.findViewById(R.id.input_customer_qty);
                    //final Button btnSelectOk = (Button) alertLayout.findViewById(R.id.btnSelectOk);
                    // Load ข้อมูลโต๊ะ
                    new AsyncTableValid().execute();

                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    //alert.setTitle("");

                    // this is set the view from XML inside AlertDialog
                    alert.setView(alertLayout);
                    dialog = alert.create();
                    dialog.show();
                }

            }
        });

        sp = getActivity().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sp.edit();

        editor = sp.edit();
        String Autoprint = sp.getString(KEY_Autoprint, "");

        if (Autoprint.equals("ON")) {
            txtDatetime.setText("ON");
            txtDatetime.setTextColor(getResources().getColor(R.color.colorGreenFron));

        } else {
            txtDatetime.setText("OFF");
            txtDatetime.setTextColor(getResources().getColor(R.color.colorRed));
        }

        doTask();
        new AsyncTableZone().execute();
        new AsyncTableOrder().execute();
        return root;
    }


    public void doTask() {

        timetask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {

                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String formattedDate = df.format(c.getTime());

                        if (haveNetworkConnection()) {
                            //new AsyncTableZone().execute();
                            new AsyncTableOrder().execute();
                        } else {
                            Intent in = new Intent(getActivity(), NointernetActivity.class);
                            in.putExtra("Key", "text");
                            in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(in);
                        }
                        // txtResult
                        //txtDatetime.setText("TimeTask runing... : " + formattedDate);
                    }
                });
            }
        };
        timer.schedule(timetask, 0, 9000); // Reload Every 9 second

    }

    public void stopTask() {
        if (timetask != null) {
            timetask.cancel();
//            TextView result = (TextView) findViewById(R.id.txtResult);
//            result.setText("TimeTask stop.");
        }
    }


    @Override
    public void onResume() {
        super.onResume();

        new AsyncTableZone().execute();

        new AsyncTableOrder().execute();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        //Toast.makeText(getActivity(),"Stop Print",Toast.LENGTH_SHORT).show();

        stopTask();
    }

    @Override
    public void onPause() {
        super.onPause();
        //stopTask();

    }

    @Override
    public void onStop() {
        super.onStop();

        //stopTask();

    }

    private class AsyncOrderProductPrint extends AsyncTask<String, String, String> {


        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run("http://www.geerang.com/gpos/api/OrderMenuPrint/" + urls[0]);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;

        }

        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                // Do you work here on success
                Resultbill = result;

                double sum_price = 0.0;
                JSONArray jArray = null;
                double sum_row_price = 0.0;
                int countdata = 0;
                try {
                    jArray = new JSONArray(Resultbill);

                    for (int f = 0; f < jArray.length(); f++) {
                        JSONObject json_data_report = jArray.getJSONObject(f);
                        customer_name = json_data_report.getString("customer_name");
                        username = json_data_report.getString("username");
                        kit_cat = json_data_report.getString("kit_cat");
                        create_date = json_data_report.getString("create_date");
                        String Orderid = json_data_report.getString("orders_id");

                        product_name = "";
                        product_name += String.format("%-35s %5s \n", "items", "qty");
                        //textData.append("------------------------------------------\n");
                        product_name += "-----------------------------------------------\n";
                        //mAidl.printText("\n");
                        JSONArray jArray_menu = new JSONArray(json_data_report.getString("products"));
                        countdata = jArray_menu.length();

                        // เช็คว่ามีข้อมูลมั้ย
                        if (countdata > 0) {
                            if (initializeObject()) {
                                Boolean createOrder = createReceiptOrderData(jArray_menu);
                                if (createOrder == true) {

                                    // สั่งปริ้น
                                    if (!AutoprintData(kit_cat)) {
                                        finalizeObject();
                                    }

                                } else {
                                    finalizeObject();
                                }
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }

    }

//    private boolean runPrintReceiptSequenceOrder(String sorders_item_id) {
//
//        if (!initializeObject()) {
//            return false;
//        }
//
//        if (!createReceiptOrderData()) {
//            finalizeObject();
//            return false;
//        }
//
//        if (!printData()) {
//            finalizeObject();
//            return false;
//        }
//
//        return true;
//    }

    private boolean createReceiptOrderData(JSONArray jArray_menu) {

        String method = "";
        StringBuilder textData = new StringBuilder();
        final int barcodeWidth = 2;
        final int barcodeHeight = 100;
        if (mPrinter == null) {
            return false;
        }
        try {

            for (int i = 0; i < jArray_menu.length(); i++) {
                JSONObject json_data = jArray_menu.getJSONObject(i);
                //หมวดหมู่อาหาร
                //product_name = json_data.getString("product_name");
                String qty = json_data.getString("qty");
                String remain = json_data.getString("remain");
                String orders_item_id = json_data.getString("orders_item_id");
                String Choice = "";
                // loop product
                JSONArray jArray2 = new JSONArray(json_data.getString("product_choice"));
                int check_list = jArray2.length();
                if (check_list > 0) {
                    for (int k = 0; k < jArray2.length(); k++) {
                        JSONObject json_data3 = jArray2.getJSONObject(k);
                        Choice += json_data3.getString("product_choice_name") + ",";
                    }
                }

                if (remain.equals("")) {
                    //textData.append(String.format("%-20s %5s %5s %10s \n", " " + product_name, String.format("%.02f", total_price) + "", total_row_qty + "", String.format("%.02f", (total_price * total_row_qty)) + ""));
                    product_name += String.format("%-35s %5s \n", json_data.getString("product_name"), qty);

                    if (check_list > 0) {
                        for (int k = 0; k < jArray2.length(); k++) {
                            JSONObject json_data3 = jArray2.getJSONObject(k);
                            String s = json_data3.getString("product_choice_name");
                            product_name += String.format("%-35s \n", " -" + s);
                        }
                    }

                    // json_data.getString("product_name") + " " + Choice + " = " + qty + " \n";
                } else {
                    product_name += String.format("%-35s %5s \n", json_data.getString("product_name"), qty);
                    if (check_list > 0) {
                        for (int k = 0; k < jArray2.length(); k++) {
                            JSONObject json_data3 = jArray2.getJSONObject(k);
                            String s = json_data3.getString("product_choice_name");
                            product_name += String.format("%-35s \n", " -" + s);
                        }
                    }
                    //product_name += String.format("%-30s \n", ""+Choice);
                    product_name += String.format("%-35s \n", "หมายเหตุ: " + remain);

                    // json_data.getString("product_name") + " " + Choice + " = " + qty + " \n (" + remain + ") \n";
                }

                // update รายการปริ้นแล้ว
                new BookingAddTask().execute(StringUtil.URL + "gpos/api/AutoPrint_UpdateOrder", orders_item_id);

            }

            product_name += "-----------------------------------------------\n";


            mPrinter.addTextLang(Printer.LANG_TH);
            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            textData.append(String.format("%-35s %-10s \n", getDateTime() + " | " + username, kit_cat));
            textData.append("-----------------------------------------------\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            method = "addFeedLine";
            mPrinter.addFeedLine(1);

            textData.append("โต๊ะ " + customer_name + "");
            textData.append("\n");
            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());
            ///textData.append(String.format("%-30s %-20s \n", getDateTime(), kit_cat));
            textData.append("-----------------------------------------------\n");

            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            method = "addFeedLine";
            mPrinter.addFeedLine(1);
            textData.append(product_name + "\n");
            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());
            method = "addCut";
            mPrinter.addCut(Printer.CUT_FEED);
            //menu_order = "";


        } catch (Exception e) {
            mPrinter.clearCommandBuffer();
            //ShowMsg.showException(e, method, getApplicationContext());
            return false;
        }

        textData = null;

        return true;
    }


    public String CheckPrintAddress(String KitchenCat) {
        String status = "";
        String title = "";
        String address = "";
        String mac_address = "";
        String categories_id = "";

        sp_print = getActivity().getSharedPreferences(PREF_NAME_PRINT, Context.MODE_PRIVATE);
        editor_print = sp_print.edit();

        String PrinterMain = sp_print.getString(KEY_PrinterMain, "");
        String Printer1 = sp_print.getString(KEY_Printer1, "");

        if (!Printer1.equals("")) {
            String[] namesList = Printer1.split(",");
            status = namesList[0];
            title = namesList[1];
            address = namesList[2];
            if (address.equals(KitchenCat)) {
                address = namesList[2];
                System.out.println("Print 1 line 370" + address);
            }


            mac_address = namesList[3];
            categories_id = namesList[4];

            //btnPrinter1.setText("Printer1 (" + title + ") \nStatus " +status+"");
        }

        String Printer2 = sp_print.getString(KEY_Printer2, "");
        if (!Printer2.equals("")) {
            String[] namesList = Printer2.split(",");
            status = namesList[0];
            title = namesList[1];
            address = namesList[2];
            if (address.equals(KitchenCat)) {
                address = namesList[2];
                System.out.println("Print 1 line 370" + address);
            }


            mac_address = namesList[3];
            categories_id = namesList[4];

            //btnPrinter1.setText("Printer1 (" + title + ") \nStatus " +status+"");
        }

        String Printer3 = sp_print.getString(KEY_Printer3, "");
        if (!Printer3.equals("")) {
            String[] namesList = Printer3.split(",");
            status = namesList[0];
            title = namesList[1];
            address = namesList[2];
            if (address.equals(KitchenCat)) {
                address = namesList[2];
                System.out.println("Print 1 line 370" + address);
            }

            mac_address = namesList[3];
            categories_id = namesList[4];
            //สั้งปริ้น
//            if (!runPrintReceiptSequenceOrder()) {
//                System.out.println("Print Start");
//            }
            //btnPrinter1.setText("Printer1 (" + title + ") \nStatus " +status+"");
        }

        String Printer4 = sp_print.getString(KEY_Printer4, "");
        if (!Printer4.equals("")) {
            String[] namesList = Printer4.split(",");
            status = namesList[0];
            title = namesList[1];
            address = namesList[2];
            if (address.equals(KitchenCat)) {
                address = namesList[2];
                System.out.println("Print 1 line 370" + address);
            }

            //สั้งปริ้น
//            if (!runPrintReceiptSequenceOrder()) {
//                System.out.println("Print Start");
//            }
            mac_address = namesList[3];
            categories_id = namesList[4];

            //btnPrinter1.setText("Printer1 (" + title + ") \nStatus " +status+"");
        }


        return address;
    }


    private class AsyncTableZone extends AsyncTask<String, String, String> {
        // ProgressDialog pdLoading = new ProgressDialog(getActivity());

        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            //  pdLoading.setMessage("\tกำลังโหลดข้อมูล...");
            //  pdLoading.setCancelable(false);
            //  pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/Zone/" + Profile.BranchId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            //txtResult.setText(response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            //pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();

            feedsListZone = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {

                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemZone item = new FeedItemZone();

                    item.setId(json_data.getString("building_zone_id"));
                    item.setTitle(json_data.getString("zone_name"));
                    item.setBid(json_data.getString("bid"));
                    item.setDetail(json_data.getString("building_name"));

                    feedsListZone.add(item);
                }

                // Setup and Handover data to recyclerview

                ZoneAdapter = new TableZoneRecyclerAdapter(getActivity(), feedsListZone);
                rvZone.setAdapter(ZoneAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
                rvZone.setLayoutManager(mLayoutManager);
                rvZone.setItemAnimator(new DefaultItemAnimator());
                rvZone.setItemAnimator(new DefaultItemAnimator());

            } catch (JSONException e) {
                Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }


    public class TableZoneRecyclerAdapter extends RecyclerView.Adapter<TableZoneRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemZone> feedItemList;
        private Context mContext;

        public TableZoneRecyclerAdapter(Context context, List<FeedItemZone> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;
        }

        @Override
        public TableZoneRecyclerAdapter.CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.order_select_zone_row, null);

            TableZoneRecyclerAdapter.CustomViewHolder viewHolder = new TableZoneRecyclerAdapter.CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(TableZoneRecyclerAdapter.CustomViewHolder customViewHolder, int i) {
            FeedItemZone feedItem = feedItemList.get(i);
            customViewHolder.feedItem = feedItem;


            //Setting text view title
            customViewHolder.textView.setText(Html.fromHtml("อาคาร : " + feedItem.getDetail() + "โซน : " + feedItem.getTitle()));
            //customViewHolder.status.setText(Html.fromHtml(feedItem.getDetail()));

        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            protected ImageView imageView;
            protected TextView textView;
            protected TextView status;
            protected Button btnEdit;
            protected Button btnDelete;
            protected Button BtnTableAdd;
            FeedItemZone feedItem;

            public CustomViewHolder(View view) {
                super(view);
                // this.imageView = (ImageView) view.findViewById(R.id.imageView2);
                this.textView = (TextView) view.findViewById(R.id.title);
                this.status = (TextView) view.findViewById(R.id.tv_status);


                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        ZoneId = feedItem.getId();

                        txtZone.setText("โซน : " + feedItem.getTitle());
                        ZoneName = feedItem.getTitle();
                        new AsyncTableOrder().execute();
                    }
                });

            }
        }
    }

    private class BookingAddTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();


            RequestBody formBody = new FormEncodingBuilder()
                    .add("orders_item_id", urls[1])
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                String msg = object.getString("msg"); // success
                System.out.println(jsonString);

                disconnectPrinter();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class CheckOrderOrderTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(getActivity());

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลัง Check out...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("orders_id", urls[1])
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
//            new AsyncChoiceProduct().execute();
            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                //before inflating the custom alert dialog layout, we will get the current activity viewgroup
                ViewGroup viewGroup = root.findViewById(android.R.id.content);

                //then we will inflate the custom alert dialog xml that we created
                View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.layout_alert_dialog_payment_success, viewGroup, false);
                Button btnOk = (Button) dialogView.findViewById(R.id.btnOk);
                //Now we need an AlertDialog.Builder object
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                //setting the view of the builder to our custom view that we already inflated
                builder.setView(dialogView);

                //finally creating the alert dialog and displaying it
                final AlertDialog alertDialog = builder.create();
                alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                btnOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        alertDialog.dismiss();

                    }
                });
                alertDialog.show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private class AsyncTableOrder extends AsyncTask<String, String, String> {
        //ProgressDialog pdLoading = new ProgressDialog(getActivity());
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //this method will be running on UI thread
//            pdLoading.setMessage("\tกำลังโหลดข้อมูล...");
//            pdLoading.setCancelable(false);
//            pdLoading.show();

            txtLoadStatus.setVisibility(View.VISIBLE);
            txtLoadStatus.setText("Updating...");

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/CustomerTableOrder/" + profile.BranchId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            //txtResult.setText(response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            //this method will be running on UI thread
            //pdLoading.dismiss();
            //List<DataFish> data=new ArrayList<>();
            txtLoadStatus.setText("เสร็จสิ้น");
            txtLoadStatus.setVisibility(View.GONE);

            feedsList = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);
                int count_row = 0;
                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    count_row++;


                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemDrug item = new FeedItemDrug();
                    item.setId(json_data.getString("customer_id"));
                    item.setOrder_id(json_data.getString("order_id"));
                    item.setTitle(json_data.getString("customer_name"));
                    item.setCustomer_total(json_data.getString("total_customer"));
                    item.setTable_status(json_data.getString("table_status"));
                    item.setPayment_status(json_data.getString("payment_status"));
                    int total_item = 0;
                    int total_price = 0;

                    JSONArray jArray_2 = new JSONArray(json_data.getString("orders"));
                    for (int j = 0; j < jArray_2.length(); j++) {
                        JSONObject json_data_2 = jArray_2.getJSONObject(j);

                        // นับ Item QTY
                        int order_qty = Integer.parseInt(json_data_2.getString("qty"));
                        total_item += order_qty;

                        // Total Price
                        int product_sale_price = Integer.parseInt(json_data_2.getString("product_sale_price"));
                        //total_price += product_sale_price ;

                        JSONArray jArray_3 = new JSONArray(json_data_2.getString("order_choice"));
                        if (jArray_3.length() > 0) {
                            int allprice = 0;
                            for (int k = 0; k < jArray_3.length(); k++) {
                                JSONObject json_data_3 = jArray_3.getJSONObject(k);

                                int price = Integer.parseInt(json_data_3.getString("choice_price"));
                                allprice += price;

                            }
                            total_price += (product_sale_price + allprice) * order_qty;
                        } else {
                            total_price += product_sale_price * order_qty;
                        }


                    }

                    // สั่งปริ้น รายการอาหารตามจุดต่างๆ
                    editor = sp.edit();
                    String Autoprint = sp.getString(KEY_Autoprint, "");
                    if (Autoprint.equals("ON")) {
                        txtDatetime.setText("ON");

                        //txtDatetime.setTextColor(getResources().getColor(R.color.colorGreenFron));
//                        String orders_item_id = json_data_2.getString("orders_item_id");
//                        String employee_name = json_data_2.getString("employee_name");
                        String order_id = json_data.getString("order_id");
//                        String print_order_status = json_data_2.getString("print_order_status");
//                            if(!print_order_status.equals("printed"))
//                            {
                        new AsyncOrderProductPrint().execute(order_id);
                        // }

                    } else {
                        try {
                            txtDatetime.setText("OFF");
                            txtDatetime.setTextColor(getResources().getColor(R.color.colorRed));

                        }catch (Exception err)
                        {
                            txtDatetime.setText("OFF");
                        }

                    }

                    item.setTotal_qty(total_item + "");
                    item.setTotal_price(total_price + "");

                    feedsList.add(item);

                }

                if (count_row <= 0) {
                    imgEmpty.setVisibility(View.VISIBLE);
                    btnAddTable.setVisibility(View.VISIBLE);
                    txtShowEmpty.setVisibility(View.VISIBLE);
                    btnSelectTable.setVisibility(View.GONE);


                } else {
                    imgEmpty.setVisibility(View.GONE);
                    btnAddTable.setVisibility(View.GONE);
                    txtShowEmpty.setVisibility(View.GONE);
                    btnSelectTable.setVisibility(View.VISIBLE);
                }

                // Setup and Handover data to recyclerview

                OrderAdapter = new TableOrderRecyclerAdapter(getActivity(), feedsList);
                rvTable.setAdapter(OrderAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
                rvTable.setLayoutManager(mLayoutManager);
                rvTable.setItemAnimator(new DefaultItemAnimator());
                rvTable.setItemAnimator(new DefaultItemAnimator());


            } catch (JSONException e) {
                Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    private boolean isPrintable(PrinterStatusInfo status) {
        if (status == null) {
            return false;
        }

        if (status.getConnection() == Printer.FALSE) {
            return false;
        } else if (status.getOnline() == Printer.FALSE) {
            return false;
        } else {
            ;//print available
        }

        return true;
    }


    private String getDateTime() {
        // get date time in custom format
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        return sdf.format(new Date());
    }

    private class AsyncInvoiceDiscount extends AsyncTask<String, String, String> {

        ProgressDialog pdLoading = new ProgressDialog(getActivity());
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //this method will be running on UI thread
            pdLoading.setMessage("\tกำลังโหลดข้อมูล...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run("http://www.geerang.com/gpos/api/InvoiceDiscount/" + StringUtil.OrderId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {
            try {
                pdLoading.dismiss();
                String discount = "";
                JSONArray jArray = new JSONArray(result);
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    //total_row_qty = Integer.parseInt(json_data.getString("qty"));

                    String s = json_data.getString("services");

                    if (!s.equals("null")) {
                        services = Integer.parseInt(s);
                    } else {
                        services = 0;
                    }

                    String discount_type = json_data.getString("discount_type");
                    StringUtil.dicount_type = discount_type;

                    String vat_include = json_data.getString("vat_include");


                    if (discount_type.equals("") || discount_type.equals("null")) {
                        discount += "ประเภทส่วนลด : " + "-\n";
                    } else {
                        discount += "ประเภทส่วนลด : " + json_data.getString("discount_type") + "\n";
                    }

                    StringUtil.dicount_type_report = json_data.getString("discount_type");

                    String total_discount = json_data.getString("total_discount");
                    if (total_discount.equals("") || total_discount.equals("null")) {
                        discount += "จำนวนส่วนลด : " + "-\n";
                    } else {
                        StringUtil.discount_total_payment = json_data.getString("total_discount");
                        StringUtil.dicount_total = Integer.parseInt(total_discount);

                        discount += "จำนวนส่วนลด : " + json_data.getString("total_discount") + "\n";
                    }

                    if (vat_include.equals("yes")) {
                        vat_check = true;
                        String vat = json_data.getString("vat");
                        if (vat.equals("") || vat.equals("null")) {
                            discount += "Vat : " + "7%\n";
                            include_vat = Integer.parseInt("7");

                        } else {
                            discount += "Vat : " + json_data.getString("vat") + "%\n";
                            include_vat = Integer.parseInt(json_data.getString("vat"));
                        }

                    } else {
                        vat_check = false;
                    }

                    discount += "Services charge : " + services + "%";
                    StringUtil.custom_service = services + "";

                    try {
                        if (!StringUtil.discount_total_payment.equals("")) {
                            discount_total = Integer.parseInt(StringUtil.discount_total_payment);
                        }

                    } catch (Exception err) {
                        discount_total = 0;
                    }

                    discount_type_menu = discount_type;

                }

                //StringUtil.discount_total_payment = discount;

                //txtTotalDiscount.setText(discount);

            } catch (JSONException e) {
//                txt_status.setVisibility(View.VISIBLE);
                Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    private class AsyncChoiceProductPrint extends AsyncTask<String, String, String> {


        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //this method will be running on UI thread
        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run("http://www.geerang.com/gpos/api/ReportInvoice/" + profile.StoreId + "/" + StringUtil.OrderId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

//                txtResult.setText(response);
            return response;

        }

        @Override
        protected void onPostExecute(String result) {
            Resultbill = result;

            editor = sp.edit();
            String PrinterMain = sp.getString(KEY_PrinterMain, "");
            if (!PrinterMain.equals("")) {
                //OpenPrinter+","+printer_title+","+printer_address+","+printer_mac_address+","+categories_id
                String[] namesList = PrinterMain.split(",");
                String status = namesList[0];
                String title = namesList[1];
                String address = namesList[2];
                String mac_address = namesList[3];
                String categories_id = namesList[4];

                PrinterAddress = address;
                //
                if (!runPrintReceiptSequence()) {

                }

            } else {


            }
        }


    }

    private boolean runPrintReceiptSequence() {

        if (!initializeObject()) {
            return false;
        }

        if (!createReceiptData()) {
            finalizeObject();
            return false;
        }

        if (!printData()) {
            finalizeObject();
            return false;
        }

        return true;
    }

    private boolean initializeObject() {
        try {
            // ((SpnModelsItem) mSpnLang.getSelectedItem()).getModelConstant()
//            mPrinter = new Printer(((SpnModelsItem) mSpnSeries.getSelectedItem()).getModelConstant(),
            mPrinter = new Printer(Printer.TM_T88, Printer.MODEL_ANK, mContext);
            //

        } catch (Exception e) {
            //ShowMsg.showException(e, "Printer", mContext);
            return false;
        }

        mPrinter.setReceiveEventListener(this);

        return true;
    }

    private boolean createReceiptData() {
        String method = "";

        StringBuffer textData = new StringBuffer();
        final int barcodeWidth = 2;
        final int barcodeHeight = 100;

        if (mPrinter == null) {
            return false;
        }

        try {
            //语言需要
            mPrinter.addTextLang(Printer.LANG_TH);
            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            method = "addFeedLine";
            mPrinter.addFeedLine(1);
            textData.append("ใบเรียกเก็บเงิน/INVOICE\n");
            textData.append("\n");
            textData.append(String.format("%-20s %-20s \n", "Invoice No." + StringUtil.OrderId, "Order Type. " + "Dine In "));
            textData.append(String.format("%-20s %-20s \n", "Invoice By." + profile.Username, "Table." + "" + StringUtil.TableCustomerName));
            textData.append(String.format("%-20s %-20s \n", "Staff." + profile.Username, getDateTime() + ""));

            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            //columns.print();
            total_price = 0;
            total_qty = 0;
            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_RIGHT);
            textData.append("------------------------------------------------\n");
            textData.append(String.format("%-3s %-26s %5s %5s \n", "Qty", "Item", "Price", "Total"));
            //textData.append(String.format("%-15s %5s %5s %5s \n", " ", "Price", "Qty", "Total"));
            textData.append("------------------------------------------------\n");
            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            double sum_price = 0.0;
            JSONArray jArray = null;
            try {
                double sum_row_price = 0.0;
                jArray = new JSONArray(Resultbill);
                // loop ของ report category
                for (int f = 0; f < jArray.length(); f++) {
                    JSONObject json_data_report = jArray.getJSONObject(f);

                    JSONArray jArray_menu = new JSONArray(json_data_report.getString("products"));
                    method = "addTextAlign";
                    mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                    textData.append("   " + json_data_report.getString("report_cat_name") + "\n");
                    //textData.append("------------------------------------------\n");
                    method = "addText";
                    mPrinter.addText(textData.toString());
                    textData.delete(0, textData.length());

                    if (jArray_menu.length() > 0) {
                        // loop ของ products
                        int item_count_row = 0;
                        int total_row_qty = 0;
                        double total_price = 0.0;
                        double total_price_by_cat = 0.0;
                        String line = "";
                        int total_row_price = 0;
                        int product_sale_price = 0;
                        for (int i = 0; i < jArray_menu.length(); i++) {

                            JSONObject json_data = jArray_menu.getJSONObject(i);
                            total_row_qty = Integer.parseInt(json_data.getString("qty"));
                            item_count_row += total_row_qty;
                            String confirm = json_data.getString("is_confirm");
                            String product_name = json_data.getString("product_name");
                            // เก็บตัวแปร Choice
                            String product_choice_name = "";

                            // เก็บราคาใน order
                            total_row_price = Integer.parseInt(json_data.getString("product_sale_price"));
                            product_sale_price = Integer.parseInt(json_data.getString("product_sale_price"));

                            int order_place = Integer.parseInt(confirm);
                            if (order_place == 0) {
                                check_order_place++;
                            }
                            // loop choice
                            JSONArray jArray2 = new JSONArray(json_data.getString("product_choice"));
                            int check_list = jArray2.length();
                            if (check_list > 0) {
                                for (int j = 0; j < jArray2.length(); j++) {
                                    JSONObject json_data2 = jArray2.getJSONObject(j);
                                    // หาราคารวมของแต่ละ row แล้วนำไปยัดใส่ใน header row
                                    int choice_price = Integer.parseInt(json_data2.getString("choice_price"));
                                    total_row_price += choice_price;

                                    String choice_name = json_data2.getString("product_choice_name");
                                    product_choice_name += choice_name + ",";
                                }
                            }

                            total_price = total_row_price;
                            total_qty = total_qty + total_row_qty;
                            // หาผลร่วมระหว่าง หมวดหมู่
                            total_price_by_cat += (total_price * total_row_qty);

                            // หาผลร่วมระหว่าง ทั้งหมด
                            sum_row_price = sum_row_price + total_price;
                            DecimalFormat formatter = new DecimalFormat("#,###.00");
                            method = "addTextAlign";
                            mPrinter.addTextAlign(Printer.ALIGN_RIGHT);


                            if (product_sale_price == 0) {
                                textData.append(String.format("%2s %-25s %7s %7s \n"
                                        , total_row_qty + ""
                                        , product_name
                                        , ""
                                        , ""));
                            } else {
                                double p_total = total_row_qty * product_sale_price;
                                textData.append(String.format("%2s %-25s %7s %7s \n"
                                        , total_row_qty + ""
                                        , product_name
                                        , formatter.format(product_sale_price) + ""
                                        , formatter.format(p_total) + ""));

                            }


                            method = "addText";
                            mPrinter.addText(textData.toString());
                            textData.delete(0, textData.length());


                            if (check_list > 0) {

                                method = "addTextAlign";
                                mPrinter.addTextAlign(Printer.ALIGN_RIGHT);

                                for (int j = 0; j < jArray2.length(); j++) {
                                    JSONObject json_data2 = jArray2.getJSONObject(j);

                                    // หาราคารวมของแต่ละ row แล้วนำไปยัดใส่ใน header row
                                    int choice_price = Integer.parseInt(json_data2.getString("choice_price"));
                                    //total_row_price += choice_price;

                                    String choice_name = json_data2.getString("product_choice_name");
                                    //product_choice_name += choice_name + ",";

                                    //String x = String.format("%.02f", total_price);
                                    //String y = String.format("%.02f", (total_price * total_row_qty));
                                    int qty = (int) Math.round(total_row_qty);

                                    double amount = qty * choice_price;
                                    int IntValue = (int) Math.round(amount);

                                    textData.append(String.format("%2s %-25s %7s %7s \n"
                                            , " "
                                            , " -" + choice_name
                                            , formatter.format(choice_price)
                                            , formatter.format(IntValue) + ""));

                                    // textData.append(filled);
                                    // textData.append(data);

                                }

                                method = "addText";
                                mPrinter.addText(textData.toString());
                                textData.delete(0, textData.length());

                            }

                        }

                        textData.append("------------------------------------------------\n");
                        textData.append(String.format("%-20s %20s \n", "Total " + item_count_row + " items", String.format("%.02f", total_price_by_cat) + ""));
                        //textData.append(String.format("%-20s %-20s \n", "Total " + item_count_row + " items", String.format("%.02f", total_price_by_cat) + ""));
                        sum_price += total_price_by_cat;

                        method = "addText";
                        mPrinter.addText(textData.toString());
                        textData.delete(0, textData.length());

                        // reset ให้เป็น 0 เพื่อหา row ต่อไป
                        total_price_by_cat = 0;


                    } else {

                    }


                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                total_price = (int) sum_price;

                int result_net = 0;
                int total_net = 0;
                int total_invoice_with_vat = 0;
                int services_charg = 0;

                if (vat_check == true) {
                    StringUtil.vat = true;
                    total_invoice_with_vat = (total_price * include_vat) / 100;
                    total_net = total_price + total_invoice_with_vat;
                } else {
                    StringUtil.vat = false;
                    total_net = total_price;
                }

                int total_with_discount = 0;
                if (discount_type_menu.equals("เงินสด")) {
                    total_with_discount = total_net - discount_total;
                    result_net = total_with_discount;
                } else {
                    int x = ((total_net * discount_total) / 100);
                    int y = total_net - x;
                    result_net = y;
                }

                if (services > 0) {
                    int x = (result_net * services) / 100;
                    result_net = result_net + x;
                }
                StringUtil.discount_total_payment = result_net + "";
                //  total_net = total_net  - total_with_discount;
                //tvSum_Amount.setText("THB " + formatter.format(result_net) + "");

            } catch (Exception e) {
                // tvSum_Amount.setText("THB " + e + "");
            }

//                txtItem_Qty.setText("Total " + total_qty + " Items");
            StringUtil.TotalOrderQty = total_qty + "";
            StringUtil.TotalPayment = total_price + "";


            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_RIGHT);
            textData.append("\n------------------------------------------------\n\n");

            //textData.append("\n");
            textData.append(String.format("%-20s %20s \n", "Total " + total_qty + " items", sum_price + ""));
            textData.append("\n");
            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_RIGHT);
            textData.append(String.format("%-20s %20s \n", "SubTotal ", sum_price + ""));

            if (StringUtil.vat == true) {
                textData.append(String.format("%-20s %20s \n", "Vat ", "7%"));
            }

            textData.append(String.format("%-20s %20s \n", "Service Charge ", StringUtil.custom_service + "%"));

            // เช็คส่วนลดแยกตามประเภทอาหาร
            if (StringUtil.dicount_type_report.equals("แยกตามรายการ")) {
                textData.append(String.format("%-20s %20s \n", "   Discount " + StringUtil.dicount_total + " " + StringUtil.dicount_type + " " + StringUtil.report_cat_name, ""));
                DecimalFormat formatter = new DecimalFormat("#,###.00");
                double amount = Double.parseDouble(StringUtil.discount_total_payment);
                //System.out.println();
                int IntValue = (int) Math.round(amount);


                textData.append(String.format("%-20s %20s \n", "Total ", formatter.format(amount) + ""));
            } else {
                DecimalFormat formatter = new DecimalFormat("#,###.00");
                double amount = Double.parseDouble(StringUtil.discount_total_payment);
                int IntValue = (int) Math.round(amount);
                textData.append(String.format("%-20s %20s \n", "Discount " + StringUtil.dicount_total + " " + StringUtil.dicount_type, ""));
                textData.append(String.format("%-20s %20s \n", "Grand Total ", formatter.format(IntValue) + ""));

            }


            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            textData.append("\n");
            textData.append("THANK YOU\n");
            textData.append("POWER BY GPOS\n");
            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addCut";
            mPrinter.addCut(Printer.CUT_FEED);


        } catch (Exception e) {
            //ShowMsg.showException(e, method, mContext);
            return false;
        }

        textData = null;

        return true;
    }


    public static boolean isEnglish(String text) {

        boolean onlyEnglish = false;

        for (char character : text.toCharArray()) {

            if (Character.UnicodeBlock.of(character) == Character.UnicodeBlock.BASIC_LATIN
                    || Character.UnicodeBlock.of(character) == Character.UnicodeBlock.LATIN_1_SUPPLEMENT
                    || Character.UnicodeBlock.of(character) == Character.UnicodeBlock.LATIN_EXTENDED_A
                    || Character.UnicodeBlock.of(character) == Character.UnicodeBlock.GENERAL_PUNCTUATION) {

                onlyEnglish = true;
            } else {

                onlyEnglish = false;
            }
        }

        return onlyEnglish;
    }

    // gap -> Number of spaces between the left and the right string
    public void print(Printer epsonPrinter, String left, String right, int gap) throws Epos2Exception {
        int total = 42;

        // This is to give a gap between the 2 columns
        for (int i = 0; i < gap; i++) {
            right = " " + right;
        }

        // This will make sure that the right string is pushed to the right.
        // If the total sum of both the strings is less than the total length(42),
        // we are prepending extra spaces to the RIGHT string so that it is pushed to the right.
        if ((left.length() + right.length()) < total) {
            int extraSpace = total - left.length() - right.length();
            for (int i = 0; i < extraSpace; i++) {
                left = left + " ";
            }
        }

        int printableSpace = total - right.length(); // printable space left for the LEFT string

        //stringBuilder -> is the modified string which has RIGHT inserted in the LEFT at the appropriate position. And also contains the new line. This is the string which needs to be printed
        StringBuilder stringBuilder = new StringBuilder();
        // Below 2 statements make the first line of the print
        stringBuilder.append(left.substring(0, Math.min(printableSpace, left.length())));
        stringBuilder.append(right);

        // stringBuilder now contains the first line of the print


        // For appropriately printing the remaining lines of the LEFT string
        for (int index = printableSpace; index < left.length(); index = index + (printableSpace)) {
            stringBuilder.append(left.substring(index, Math.min(index + printableSpace, left.length())) + "\n");
        }
        epsonPrinter.addText(stringBuilder.toString());
    }

    /**
     * utility: pads two strings to columns per line
     */
    protected String padLine(@Nullable String partOne, @Nullable String partTwo, int columnsPerLine) {
        if (partOne == null) {
            partOne = "";
        }
        if (partTwo == null) {
            partTwo = "";
        }
        String concat;
        if ((partOne.length() + partTwo.length()) > columnsPerLine) {
            concat = partOne + " " + partTwo;
        } else {
            int padding = columnsPerLine - (partOne.length() + partTwo.length());
            concat = partOne + repeat(" ", padding) + partTwo;
        }
        return concat;
    }

    /**
     * utility: string repeat
     */
    protected String repeat(String str, int i) {
        return new String(new char[i]).replace("\0", str);
    }

    private boolean AutoprintData(String CatMenu) {
        if (mPrinter == null) {
            return false;
        }

        String GetIPPrinter = CheckPrintAddress(kit_cat);

        if (!AutoPrintconnectPrinter(GetIPPrinter)) {
            mPrinter.clearCommandBuffer();
            return false;
        }

        try {
            mPrinter.sendData(Printer.PARAM_DEFAULT);
        } catch (Exception e) {
            mPrinter.clearCommandBuffer();
            //ShowMsg.showException(e, "sendData", mContext);
            try {
                mPrinter.disconnect();
            } catch (Exception ex) {
                // Do nothing
            }
            return false;
        }

        return true;
    }


    private boolean printData() {
        if (mPrinter == null) {
            return false;
        }

        if (!connectPrinter()) {
            mPrinter.clearCommandBuffer();
            return false;
        }

        try {
            mPrinter.sendData(Printer.PARAM_DEFAULT);
        } catch (Exception e) {
            mPrinter.clearCommandBuffer();
            //ShowMsg.showException(e, "sendData", mContext);
            try {
                mPrinter.disconnect();
            } catch (Exception ex) {
                // Do nothing
            }
            return false;
        }

        return true;
    }

    private boolean AutoPrintconnectPrinter(String IPAddress) {
        if (mPrinter == null) {
            return false;
        }

        try {
            // mPrinter.connect("192.168.1.103", Printer.PARAM_DEFAULT);
            mPrinter.connect(IPAddress, Printer.PARAM_DEFAULT);

        } catch (Exception e) {
            //ShowMsg.showException(e, "connect", mContext);
            return false;
        }

        return true;
    }

    private boolean connectPrinter() {
        if (mPrinter == null) {
            return false;
        }

        try {
            // mPrinter.connect("192.168.1.103", Printer.PARAM_DEFAULT);
            mPrinter.connect(PrinterAddress, Printer.PARAM_DEFAULT);

        } catch (Exception e) {
            //ShowMsg.showException(e, "connect", mContext);
            return false;
        }

        return true;
    }

    @Override
    public void onPtrReceive(final Printer printerObj, final int code, final PrinterStatusInfo status, final String printJobId) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public synchronized void run() {
                //ShowMsg.showResult(code, epsonMakeErrorMessage.makeErrorMessage(status), getActivity());
                // epsonMakeErrorMessage.dispPrinterWarnings(status);
                disconnectPrinter();
            }
        });
    }


    private void disconnectPrinter() {
        if (mPrinter == null) {
            return;
        }

        try {
            mPrinter.endTransaction();
        } catch (final Exception e) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                    //ShowMsg.showException(e, "endTransaction", getActivity());
                }
            });
        }

        try {
            mPrinter.disconnect();
        } catch (final Exception e) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                    //ShowMsg.showException(e, "disconnect", getActivity());
                }
            });
        }

        finalizeObject();
    }

    private void finalizeObject() {
        if (mPrinter == null) {
            return;
        }

        mPrinter.clearCommandBuffer();

        mPrinter.setReceiveEventListener(null);

        mPrinter = null;
    }

    private void requestRuntimePermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return;
        }

        int permissionStorage = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permissionLocation = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION);

        List<String> requestPermissions = new ArrayList<>();

        if (permissionStorage == PackageManager.PERMISSION_DENIED) {
            requestPermissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionLocation == PackageManager.PERMISSION_DENIED) {
            requestPermissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }

        if (!requestPermissions.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), requestPermissions.toArray(new String[requestPermissions.size()]), REQUEST_PERMISSION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode != REQUEST_PERMISSION || grantResults.length == 0) {
            return;
        }

        List<String> requestPermissions = new ArrayList<>();

        for (int i = 0; i < permissions.length; i++) {
            if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    && grantResults[i] == PackageManager.PERMISSION_DENIED) {
                requestPermissions.add(permissions[i]);
            }
            if (permissions[i].equals(Manifest.permission.ACCESS_COARSE_LOCATION)
                    && grantResults[i] == PackageManager.PERMISSION_DENIED) {
                requestPermissions.add(permissions[i]);
            }
        }

        if (!requestPermissions.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), requestPermissions.toArray(new String[requestPermissions.size()]), REQUEST_PERMISSION);
        }
    }

    // Adapter
    public class TableOrderRecyclerAdapter extends RecyclerView.Adapter<TableOrderRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemDrug> feedItemList;
        private Context mContext;

        public TableOrderRecyclerAdapter(Context context, List<FeedItemDrug> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;


        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.delivery_table_order_row, null);

            CustomViewHolder viewHolder = new CustomViewHolder(view);
            return viewHolder;
        }


        @SuppressLint("ResourceAsColor")
        public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
            final FeedItemDrug feedItem = feedItemList.get(i);
            customViewHolder.feedItem = feedItem;
            //Setting text view title
            customViewHolder.txtTable.setText(Html.fromHtml(feedItem.getTitle()));
            customViewHolder.txtTotalPrice.setText("฿" + Html.fromHtml(feedItem.getTotal_price()));
            customViewHolder.txtOrderItem.setText(Html.fromHtml(feedItem.getTotal_qty() + " Items"));
            customViewHolder.txtCustomerTotal.setText(Html.fromHtml(feedItem.getCustomer_total()));

            customViewHolder.txtPaymentStatus.setVisibility(View.GONE);

            if (feedItem.getPayment_status().equals("CHECKOUT")) {
                customViewHolder.btnDel.setVisibility(View.VISIBLE);
                customViewHolder.btnMovetable.setVisibility(View.GONE);


                customViewHolder.view.setBackgroundColor(getResources().getColor(R.color.colorRed));
                customViewHolder.btnReorder.setVisibility(View.VISIBLE);
//                customViewHolder.txtPaymentStatus.setVisibility(View.VISIBLE);
                customViewHolder.txtPaymentStatus.setText(Html.fromHtml(feedItem.getPayment_status()));
                customViewHolder.cardView.setCardBackgroundColor(getResources().getColor(R.color.colorLinen));

            }

            customViewHolder.btnPrint.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    requestRuntimePermission();

                    StringUtil.OrderId = feedItem.getOrder_id();
                    StringUtil.TableCustomerName = feedItem.getTitle();

                    new AsyncInvoiceDiscount().execute();

                    new AsyncChoiceProductPrint().execute();


                }
            });

            customViewHolder.btnReorder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    StringUtil.OrderId = feedItem.getOrder_id();
                    new TableReOrderTask().execute(StringUtil.URL + "gpos/api/TableReOrder");

                }
            });

            customViewHolder.btnDel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(StringUtil.TableCustomerId + "#กรุณาระบุเหตุผลการยกเลิก\n(ความยาวไม่เกิน 120 ตัวอักษร)");
                    final EditText input = new EditText(getActivity());
                    input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(120)});
                    input.setInputType(InputType.TYPE_CLASS_TEXT);
                    builder.setView(input);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
//                        m_Text = ;
                            if (input.length() < 5) {
                                AlertError("กรุณาระบุเหตุผลของการยกเลิก");
                                dialog.cancel();

                            } else {
                                StringUtil.TableCustomerId = feedItem.getId();
                                StringUtil.OrderId = feedItem.getOrder_id();
                                new TableCancelOrderTask().execute(StringUtil.URL + "gpos/api/TableCancelOrder", input.getText().toString());

                            }
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    builder.show();
                }
            });

            customViewHolder.btnMovetable.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    StringUtil.TableCustomerName = feedItem.getTitle();
                    StringUtil.BranchId = StringUtil.BranchId;
                    StringUtil.TableCustomer = feedItem.getTitle();
                    StringUtil.TableCustomerId = feedItem.getId();
                    StringUtil.TotalCustomer = feedItem.getCustomer_total();
                    StringUtil.OrderId = feedItem.getOrder_id();


                    String status = StringUtil.Id = feedItem.getDetail();
                    StringUtil.TableCustomerName = feedItem.getTitle();
                    StringUtil.BranchId = feedItem.getId();
                    StringUtil.TableCustomer = feedItem.getTitle();

                    StringUtil.TableCustomerId_new = feedItem.getId();
                    StringUtil.Id = feedItem.getId();
                    StringUtil.TotalCustomer = "1";

                    Intent in = new Intent(getActivity(), MoveTableActivity.class);
                    startActivity(in);

                }
            });


        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            protected ImageView imageView;
            protected TextView txtTable, txtTotalPrice, txtPaymentStatus, txtOrderItem, txtCustomerTotal;
            //            protected TextView status;
            protected Button btnDel, btnMovetable, btnPrint;

            protected ImageButton btnReorder;

            FeedItemDrug feedItem;
            protected CardView cardView;

            protected View view;

            public CustomViewHolder(View view) {
                super(view);
                // this.imageView = (ImageView) view.findViewById(R.id.imageView2);
                this.txtTable = (TextView) view.findViewById(R.id.txtTable);
                this.txtTotalPrice = (TextView) view.findViewById(R.id.txtTotalPrice);
                this.txtOrderItem = (TextView) view.findViewById(R.id.txtOrderItem);
                this.txtCustomerTotal = (TextView) view.findViewById(R.id.txtCustomerTotal);
                this.txtPaymentStatus = (TextView) view.findViewById(R.id.txtPaymentStatus);
                this.view = (View) view.findViewById(R.id.divider46);
                cardView = (CardView) view.findViewById(R.id.cardView);

                this.btnReorder = (ImageButton) view.findViewById(R.id.btnReorder);
                this.btnPrint = (Button) view.findViewById(R.id.btnPrint);
                this.btnDel = (Button) view.findViewById(R.id.btnDel);
                this.btnMovetable = (Button) view.findViewById(R.id.btnMovetable);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        StringUtil.TableCustomerName = feedItem.getTitle();
                        StringUtil.BranchId = StringUtil.BranchId;
                        StringUtil.TableCustomer = feedItem.getTitle();
                        StringUtil.TableCustomerId = feedItem.getId();
                        StringUtil.TotalCustomer = feedItem.getCustomer_total();
                        StringUtil.OrderId = feedItem.getOrder_id();
                        StringUtil.payment_status = feedItem.getPayment_status();

//                        if(StringUtil.payment_status.equals("CHECKOUT"))
//                        {
                        Intent intent = new Intent(getActivity(), CashireCheckOutOrderActivity.class);
                        startActivity(intent);

//                        }else
//                        {
//                            Intent intent = new Intent(getActivity(), OpenTableOrderActivity.class);
//                            startActivity(intent);
//                        }


                    }
                });

            }
        }
    }

    private class TableReOrderTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(getActivity());

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลังดำเนินการ...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("remain", "")
                    .add("order_id", StringUtil.OrderId)
                    .add("create_by", profile.UserId)
                    .add("table_id", StringUtil.TableCustomerId)
                    .build();

            try {
                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();
            // reload order

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    new AsyncTableOrder().execute();

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class TableCancelOrderTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(getActivity());

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลังยกเลิกโต๊ะ...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("remain", urls[1])
                    .add("order_id", StringUtil.OrderId)
                    .add("create_by", profile.UserId)
                    .add("table_id", StringUtil.TableCustomerId)
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            // reload order
            new AsyncTableOrder().execute();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


    private class AsyncTableValid extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(getActivity());
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //progressBarTable.setVisibility(View.VISIBLE);

            //this method will be running on UI thread
            pdLoading.setMessage("\tกำลังโหลดข้อมูล...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
//                response = http.run(StringUtil.URL + "gpos/api/CustomerTable/" + profile.BranchId);
                response = http.run(StringUtil.URL + "gpos/api/CustomerTableZone/" + ZoneId + "/" + profile.BranchId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            //txtResult.setText(response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread
            //progressBarTable.setVisibility(View.GONE);

            pdLoading.dismiss();
            //if(getActivity().progressBarTable.getVisibility() == View.VISIBLE)
            //{
            //getActivity().progressBarTable.setVisibility(View.GONE);
            // }


//            List<DataFish> data=new ArrayList<>();

            feedsList = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);

                if (jArray.length() == 0) {
                    AlertError("ไม่พบข้อมูล");
                }

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemDrug item = new FeedItemDrug();
                    item.setId(json_data.getString("customer_id"));
                    item.setTitle(json_data.getString("customer_name"));
//                    item.setDetail(json_data.getString("order_id"));
                    item.setTable_status(json_data.getString("table_status"));

                    feedsList.add(item);
                }
                // Setup and Handover data to recyclerview
                mAdapter = new DrugRecyclerAdapter(getActivity(), feedsList);
                recyclerViewTable.setAdapter(mAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
                recyclerViewTable.setLayoutManager(mLayoutManager);
                recyclerViewTable.setItemAnimator(new DefaultItemAnimator());
                recyclerViewTable.setItemAnimator(new DefaultItemAnimator());

            } catch (JSONException e) {
                Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }


    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    // Adapter
    public class DrugRecyclerAdapter extends RecyclerView.Adapter<DrugRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemDrug> feedItemList;
        private Context mContext;

        public DrugRecyclerAdapter(Context context, List<FeedItemDrug> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.delivery_table_row, null);

            CustomViewHolder viewHolder = new CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
            FeedItemDrug feedItem = feedItemList.get(i);
            customViewHolder.feedItem = feedItem;
            //Setting text view title
            customViewHolder.textView.setText(Html.fromHtml(feedItem.getTitle()));

        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            protected ImageView imageView;
            protected TextView textView;
            //            protected TextView status;
            protected Button btnEdit;

            FeedItemDrug feedItem;

            public CustomViewHolder(View view) {
                super(view);
                // this.imageView = (ImageView) view.findViewById(R.id.imageView2);
                this.textView = (TextView) view.findViewById(R.id.title);
//                this.status = (TextView) view.findViewById(R.id.tv_status);
//                this.btnEdit = (Button) view.findViewById(R.id.btnEdit);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String CustomerQty = input_customer_qty.getText().toString();
                        if (CustomerQty.equals("")) {
                            CustomerQty = "0";
                        }

                        int total_customer = Integer.parseInt(CustomerQty);
                        if (total_customer < 0) {
                            input_customer_qty.setError("กรุณากรอกจำนวนลูกค้า");
                            // AlertError("กรุณาระบุเหตุผลของการยกเลิก");
                        } else {
                            String status = feedItem.getTable_status();
                            StringUtil.TableCustomerName = feedItem.getTitle();
                            StringUtil.BranchId = feedItem.getId();
                            StringUtil.TableCustomer = feedItem.getTitle();
                            StringUtil.TableCustomerId = feedItem.getId();
                            new AddOrderTask().execute(StringUtil.URL + "gpos/api/OpenOrder");
                        }
                    }
                });

            }
        }
    }

    public void AlertError(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things

                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    private class AddOrderTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();
            RequestBody formBody = new FormEncodingBuilder()
                    .add("UserId", profile.UserId)
                    .add("StoreId", profile.StoreId)
                    .add("BranchId", profile.BranchId)
                    .add("Customer", input_customer_qty.getText().toString())
                    .add("CustomerId", StringUtil.TableCustomerId)
                    .build();
            try {
                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            dialog.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success

                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

                } else {

                    StringUtil.TotalCustomer = input_customer_qty.getText().toString();
                    String order_id = object.getString("order_id"); // success
                    // keep to var ORderID
                    StringUtil.OrderId = order_id;
                    Intent intent = new Intent(getActivity(), OpenTableOrderActivity.class);
                    //StringUtil.Id = order_id;
                    StringUtil.OrderId = order_id;
                    startActivity(intent);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


    // String
    public class FeedItemDrug {
        private String title;
        private String id;
        private String setDetail;
        private String total_qty;
        private String customer_total;
        private String total_price;
        private String order_id;
        private String payment_status;

        public String getPayment_status() {
            return payment_status;
        }

        public void setPayment_status(String payment_status) {
            this.payment_status = payment_status;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getCustomer_total() {
            return customer_total;
        }

        public void setCustomer_total(String customer_total) {
            this.customer_total = customer_total;
        }

        public String getTotal_price() {
            return total_price;
        }

        public void setTotal_price(String total_price) {
            this.total_price = total_price;
        }

        private String table_status;

        public String getTable_status() {
            return table_status;
        }

        public void setTable_status(String table_status) {
            this.table_status = table_status;
        }

        public String getTotal_qty() {
            return total_qty;
        }

        public void setTotal_qty(String total_qty) {
            this.total_qty = total_qty;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String name) {
            this.title = name;
        }


        public String getDetail() {
            return setDetail;
        }

        public void setDetail(String Detail) {
            this.setDetail = Detail;
        }


    }


    // String
    public class FeedItemZone {
        private String title;
        private String id;
        private String setDetail;
        private String bid;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String name) {
            this.title = name;
        }


        public String getDetail() {
            return setDetail;
        }

        public void setDetail(String Detail) {
            this.setDetail = Detail;
        }

        public String getBid() {
            return bid;
        }

        public void setBid(String bid) {
            this.bid = bid;
        }

    }

}
