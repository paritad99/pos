package com.geerang.gcounter.ui.delivery;

import androidx.appcompat.app.AlertDialog;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.PictureDrawable;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.geerang.gcounter.MoveTableActivity;
import com.geerang.gcounter.OpenTableOrderActivity;
import com.geerang.gcounter.Profile;
import com.geerang.gcounter.R;
import com.geerang.gcounter.StringUtil;
import com.geerang.gcounter.TableAddActivity;
import com.geerang.gcounter.ZoneEditActivity;
import com.geerang.gcounter.ui.zone.ZoneFragment;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class OrderDeliveryFragment extends Fragment {


    public static OrderDeliveryFragment newInstance() {
        return new OrderDeliveryFragment();
    }

    private List<FeedItemDrug> feedsList;
    private List<FeedItemZone> feedsListZone;
    private RecyclerView recyclerViewTable, rvTable, rvZone;
    private DrugRecyclerAdapter mAdapter;
    private TableOrderRecyclerAdapter OrderAdapter;
    private TableZoneRecyclerAdapter ZoneAdapter;

    Handler handler = new Handler();
    Timer timer = new Timer();
    TimerTask timetask;

    View root;

    Button btnSelectTable, btnAddTable;
    EditText input_customer_qty;

    ImageView imgEmpty;

    TextView txtShowEmpty, txtZone;
    AlertDialog dialog;

    Profile profile;

    String ZoneId = "";
    String ZoneName = "";

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.order_delivery_fragment, container, false);

        profile = new Profile(getActivity());

        rvTable = (RecyclerView) root.findViewById(R.id.rvTable);
        rvZone = (RecyclerView) root.findViewById(R.id.rvZone);

        imgEmpty = (ImageView) root.findViewById(R.id.imgEmpty);
        txtShowEmpty = (TextView) root.findViewById(R.id.txtShowEmpty);
        txtZone = (TextView) root.findViewById(R.id.txtZone);

        btnAddTable = (Button) root.findViewById(R.id.btnAddTable);
        btnAddTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater inflater = getLayoutInflater();
                View alertLayout = inflater.inflate(R.layout.activity_select_table_dialog, null);
                recyclerViewTable = (RecyclerView) alertLayout.findViewById(R.id.recyclerViewTable);
                input_customer_qty = (EditText) alertLayout.findViewById(R.id.input_customer_qty);

                new AsyncTableValid().execute();
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setView(alertLayout);
                dialog = alert.create();
                dialog.show();
            }
        });

        btnSelectTable = (Button) root.findViewById(R.id.btnSelectTable);
        btnSelectTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ZoneId.equals("")) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("กรุณาเลือกโซน")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

                } else {

                    LayoutInflater inflater = getLayoutInflater();
                    View alertLayout = inflater.inflate(R.layout.activity_select_table_dialog, null);
                    recyclerViewTable = (RecyclerView) alertLayout.findViewById(R.id.recyclerViewTable);

                    TextView txtCustomerZone = (TextView) alertLayout.findViewById(R.id.txtCustomerZone);
                    txtCustomerZone.setText("โซน: "+ZoneName);
                    input_customer_qty = (EditText) alertLayout.findViewById(R.id.input_customer_qty);
                    //final Button btnSelectOk = (Button) alertLayout.findViewById(R.id.btnSelectOk);
                    // Load ข้อมูลโต๊ะ
                    new AsyncTableValid().execute();

                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    //alert.setTitle("");

                    // this is set the view from XML inside AlertDialog
                    alert.setView(alertLayout);
                    dialog = alert.create();
                    dialog.show();
                }

            }
        });

        new AsyncTableZone().execute();
        new AsyncTableOrder().execute();

        return root;
    }


    @Override
    public void onResume() {
        super.onResume();
        new AsyncTableZone().execute();
        new AsyncTableOrder().execute();

    }

    private class AsyncTableZone extends AsyncTask<String, String, String> {
        // ProgressDialog pdLoading = new ProgressDialog(getActivity());

        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            //  pdLoading.setMessage("\tกำลังโหลดข้อมูล...");
            //  pdLoading.setCancelable(false);
            //  pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/Zone/" + Profile.BranchId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            //txtResult.setText(response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            //pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();

            feedsListZone = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {

                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemZone item = new FeedItemZone();
                    item.setId(json_data.getString("building_zone_id"));
                    item.setTitle(json_data.getString("zone_name"));
                    item.setBid(json_data.getString("bid"));
//                  item.setDetail(json_data.getString("building_address"));

                    feedsListZone.add(item);
                }

                // Setup and Handover data to recyclerview

                ZoneAdapter = new TableZoneRecyclerAdapter(getActivity(), feedsListZone);
                rvZone.setAdapter(ZoneAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
                rvZone.setLayoutManager(mLayoutManager);
                rvZone.setItemAnimator(new DefaultItemAnimator());
                rvZone.setItemAnimator(new DefaultItemAnimator());

            } catch (JSONException e) {
                Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }


    private class AsyncTableOrder extends AsyncTask<String, String, String> {
        // ProgressDialog pdLoading = new ProgressDialog(getActivity());

        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            //  pdLoading.setMessage("\tกำลังโหลดข้อมูล...");
            //  pdLoading.setCancelable(false);
            //  pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/CustomerTableOrder/" + profile.BranchId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            //txtResult.setText(response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            //pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();

            feedsList = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);


                int count_row = 0;


                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    count_row++;
                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemDrug item = new FeedItemDrug();
                    item.setId(json_data.getString("customer_id"));
                    item.setOrder_id(json_data.getString("order_id"));
                    item.setTitle(json_data.getString("customer_name"));
                    item.setCustomer_total(json_data.getString("total_customer"));
                    item.setTable_status(json_data.getString("table_status"));
                    item.setPayment_status(json_data.getString("payment_status"));
                    int total_item = 0;
                    int total_price = 0;

                    JSONArray jArray_2 = new JSONArray(json_data.getString("orders"));
                    for (int j = 0; j < jArray_2.length(); j++) {
                        JSONObject json_data_2 = jArray_2.getJSONObject(j);

                        // นับ Item QTY
                        int order_qty = Integer.parseInt(json_data_2.getString("qty"));

                        total_item += order_qty;

                        // Total Price
                        int product_sale_price = Integer.parseInt(json_data_2.getString("product_sale_price"));

                        JSONArray jArray_3 = new JSONArray(json_data_2.getString("order_choice"));
                        if (jArray_3.length() > 0) {
                            for (int k = 0; k < jArray_3.length(); k++) {
                                JSONObject json_data_3 = jArray_3.getJSONObject(k);

                                int price = Integer.parseInt(json_data_3.getString("choice_price"));
                                total_price += product_sale_price + price * order_qty;


                            }
                        } else {
                            total_price += product_sale_price * order_qty;
                        }


                    }


                    item.setTotal_qty(total_item + "");
                    item.setTotal_price(total_price + "");

                    feedsList.add(item);


                }

                if (count_row <= 0) {
                    imgEmpty.setVisibility(View.VISIBLE);
                    btnAddTable.setVisibility(View.VISIBLE);
                    txtShowEmpty.setVisibility(View.VISIBLE);
                    btnSelectTable.setVisibility(View.GONE);


                } else {
                    imgEmpty.setVisibility(View.GONE);
                    btnAddTable.setVisibility(View.GONE);
                    txtShowEmpty.setVisibility(View.GONE);
                    btnSelectTable.setVisibility(View.VISIBLE);
                }

                // Setup and Handover data to recyclerview

                OrderAdapter = new TableOrderRecyclerAdapter(getActivity(), feedsList);
                rvTable.setAdapter(OrderAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
                rvTable.setLayoutManager(mLayoutManager);
                rvTable.setItemAnimator(new DefaultItemAnimator());
                //rvTable.setItemAnimator(new DefaultItemAnimator());

            } catch (JSONException e) {
                Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    // Adapter
    public class TableZoneRecyclerAdapter extends RecyclerView.Adapter<TableZoneRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemZone> feedItemList;
        private Context mContext;

        public TableZoneRecyclerAdapter(Context context, List<FeedItemZone> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;
        }

        @Override
        public TableZoneRecyclerAdapter.CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.order_select_zone_row, null);

            TableZoneRecyclerAdapter.CustomViewHolder viewHolder = new TableZoneRecyclerAdapter.CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(TableZoneRecyclerAdapter.CustomViewHolder customViewHolder, int i) {
            FeedItemZone feedItem = feedItemList.get(i);
            customViewHolder.feedItem = feedItem;


            //Setting text view title
            customViewHolder.textView.setText(Html.fromHtml("โซน : " + feedItem.getTitle()));
            //customViewHolder.status.setText(Html.fromHtml(feedItem.getDetail()));

        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            protected ImageView imageView;
            protected TextView textView;
            protected TextView status;
            protected Button btnEdit;
            protected Button btnDelete;
            protected Button BtnTableAdd;
            FeedItemZone feedItem;

            public CustomViewHolder(View view) {
                super(view);
                // this.imageView = (ImageView) view.findViewById(R.id.imageView2);
                this.textView = (TextView) view.findViewById(R.id.title);
                this.status = (TextView) view.findViewById(R.id.tv_status);


                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        ZoneId = feedItem.getId();

                        txtZone.setText("โซน : " + feedItem.getTitle());
                        ZoneName = feedItem.getTitle();
                        new AsyncTableOrder().execute();
                    }
                });

            }
        }
    }

    // Adapter
    public class TableOrderRecyclerAdapter extends RecyclerView.Adapter<TableOrderRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemDrug> feedItemList;
        private Context mContext;

        public TableOrderRecyclerAdapter(Context context, List<FeedItemDrug> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;


        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.delivery_table_order_row, null);

            CustomViewHolder viewHolder = new CustomViewHolder(view);
            return viewHolder;
        }


        @SuppressLint("ResourceAsColor")
        public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
            final FeedItemDrug feedItem = feedItemList.get(i);
            customViewHolder.feedItem = feedItem;
            //Setting text view title
            customViewHolder.txtTable.setText(Html.fromHtml(feedItem.getTitle()));
            customViewHolder.txtTotalPrice.setText("฿" + Html.fromHtml(feedItem.getTotal_price()));
            customViewHolder.txtOrderItem.setText(Html.fromHtml(feedItem.getTotal_qty() + " Items"));
            customViewHolder.txtCustomerTotal.setText(Html.fromHtml(feedItem.getCustomer_total()));

            customViewHolder.txtPaymentStatus.setText(Html.fromHtml(feedItem.getPayment_status()));

            if (feedItem.getPayment_status().equals("CHECKOUT")) {

                customViewHolder.btnDel.setVisibility(View.GONE);

                customViewHolder.btnMovetable.setVisibility(View.GONE);

                customViewHolder.btnPrint.setVisibility(View.GONE);

                customViewHolder.txtPaymentStatus.setVisibility(View.VISIBLE);


            }

            customViewHolder.btnDel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(StringUtil.TableCustomerId + "#กรุณาระบุเหตุผลการยกเลิก\n(ความยาวไม่เกิน 120 ตัวอักษร)");
                    final EditText input = new EditText(getActivity());
                    input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(120)});
                    input.setInputType(InputType.TYPE_CLASS_TEXT);
                    builder.setView(input);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
//                        m_Text = ;

                            if (input.length() < 5) {
                                AlertError("กรุณาระบุเหตุผลของการยกเลิก");
                                dialog.cancel();

                            } else {
                                StringUtil.TableCustomerId = feedItem.getId();
                                StringUtil.OrderId = feedItem.getOrder_id();
                                new TableCancelOrderTask().execute(StringUtil.URL + "gpos/api/TableCancelOrder", input.getText().toString());

                            }


                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    builder.show();
                }
            });

            customViewHolder.btnMovetable.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    StringUtil.TableCustomerName = feedItem.getTitle();
                    StringUtil.BranchId = StringUtil.BranchId;
                    StringUtil.TableCustomer = feedItem.getTitle();
                    StringUtil.TableCustomerId = feedItem.getId();
                    StringUtil.TotalCustomer = feedItem.getCustomer_total();
                    StringUtil.OrderId = feedItem.getOrder_id();


                    String status = StringUtil.Id = feedItem.getDetail();
                    StringUtil.TableCustomerName = feedItem.getTitle();
                    StringUtil.BranchId = feedItem.getId();
                    StringUtil.TableCustomer = feedItem.getTitle();

                    StringUtil.TableCustomerId_new = feedItem.getId();
                    StringUtil.Id = feedItem.getId();
                    StringUtil.TotalCustomer = "1";

                    Intent in = new Intent(getActivity(), MoveTableActivity.class);
                    startActivity(in);

                }
            });


        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            protected ImageView imageView;
            protected TextView txtTable, txtTotalPrice, txtPaymentStatus, txtOrderItem, txtCustomerTotal;
            //            protected TextView status;
            protected Button btnDel, btnMovetable, btnPrint;

            FeedItemDrug feedItem;

            public CustomViewHolder(View view) {
                super(view);
                // this.imageView = (ImageView) view.findViewById(R.id.imageView2);
                this.txtTable = (TextView) view.findViewById(R.id.txtTable);
                this.txtTotalPrice = (TextView) view.findViewById(R.id.txtTotalPrice);
                this.txtOrderItem = (TextView) view.findViewById(R.id.txtOrderItem);
                this.txtCustomerTotal = (TextView) view.findViewById(R.id.txtCustomerTotal);
                this.txtPaymentStatus = (TextView) view.findViewById(R.id.txtPaymentStatus);

                this.btnPrint = (Button) view.findViewById(R.id.btnPrint);
                this.btnDel = (Button) view.findViewById(R.id.btnDel);
                this.btnMovetable = (Button) view.findViewById(R.id.btnMovetable);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        StringUtil.TableCustomerName = feedItem.getTitle();
                        StringUtil.BranchId = StringUtil.BranchId;
                        //  StringUtil.total_payment_sum = feedItem.getTotal_price();
                        StringUtil.TableCustomer = feedItem.getTitle();
                        StringUtil.TableCustomerId = feedItem.getId();
                        StringUtil.TotalCustomer = feedItem.getCustomer_total();
                        StringUtil.OrderId = feedItem.getOrder_id();
                        StringUtil.payment_status = feedItem.getPayment_status();

                        Intent intent = new Intent(getActivity(), OpenTableOrderActivity.class);
                        startActivity(intent);

                    }
                });

            }
        }
    }

    private class TableCancelOrderTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(getActivity());

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tTable Cancel Order in progress...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("remain", urls[1])
                    .add("order_id", StringUtil.OrderId)
                    .add("create_by", profile.UserId)
                    .add("table_id", StringUtil.TableCustomerId)
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            // reload order
            new AsyncTableOrder().execute();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things


                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


    private class AsyncTableValid extends AsyncTask<String, String, String> {
        // ProgressDialog pdLoading = new ProgressDialog(getActivity());
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            //  pdLoading.setMessage("\tกำลังโหลดข้อมูล...");
            //  pdLoading.setCancelable(false);
            // pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/CustomerTableZone/"+ZoneId+"/" + profile.BranchId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            //txtResult.setText(response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            //pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();

            feedsList = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemDrug item = new FeedItemDrug();
                    item.setId(json_data.getString("customer_id"));
                    item.setTitle(json_data.getString("customer_name"));
//                    item.setDetail(json_data.getString("order_id"));
                    item.setTable_status(json_data.getString("table_status"));

                    feedsList.add(item);
                }

                // Setup and Handover data to recyclerview

                mAdapter = new DrugRecyclerAdapter(getActivity(), feedsList);
                recyclerViewTable.setAdapter(mAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
                recyclerViewTable.setLayoutManager(mLayoutManager);
                recyclerViewTable.setItemAnimator(new DefaultItemAnimator());
                recyclerViewTable.setItemAnimator(new DefaultItemAnimator());

            } catch (JSONException e) {
                Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }


    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    // Adapter
    public class DrugRecyclerAdapter extends RecyclerView.Adapter<DrugRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemDrug> feedItemList;
        private Context mContext;

        public DrugRecyclerAdapter(Context context, List<FeedItemDrug> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.delivery_table_row, null);

            CustomViewHolder viewHolder = new CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
            FeedItemDrug feedItem = feedItemList.get(i);
            customViewHolder.feedItem = feedItem;


            //Setting text view title
            customViewHolder.textView.setText(Html.fromHtml(feedItem.getTitle()));

        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            protected ImageView imageView;
            protected TextView textView;
            //            protected TextView status;
            protected Button btnEdit;

            FeedItemDrug feedItem;

            public CustomViewHolder(View view) {
                super(view);
                // this.imageView = (ImageView) view.findViewById(R.id.imageView2);
                this.textView = (TextView) view.findViewById(R.id.title);
//                this.status = (TextView) view.findViewById(R.id.tv_status);

//                this.btnEdit = (Button) view.findViewById(R.id.btnEdit);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String status = feedItem.getTable_status();
                        StringUtil.TableCustomerName = feedItem.getTitle();
                        StringUtil.BranchId = feedItem.getId();
                        StringUtil.TableCustomer = feedItem.getTitle();
                        StringUtil.TableCustomerId = feedItem.getId();
                        new AddOrderTask().execute(StringUtil.URL + "gpos/api/OpenOrder");
                    }
                });

            }
        }
    }

    public void AlertError(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things

                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    private class AddOrderTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();
            RequestBody formBody = new FormEncodingBuilder()
                    .add("UserId", profile.UserId)
                    .add("StoreId", profile.StoreId)
                    .add("BranchId", profile.BranchId)
                    .add("Customer", input_customer_qty.getText().toString())
                    .add("CustomerId", StringUtil.TableCustomerId)
                    .build();
            try {
                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            dialog.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success

                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

                } else {

                    StringUtil.TotalCustomer = input_customer_qty.getText().toString();
                    String order_id = object.getString("order_id"); // success
                    // keep to var ORderID
                    StringUtil.OrderId = order_id;
                    Intent intent = new Intent(getActivity(), OpenTableOrderActivity.class);
                    //StringUtil.Id = order_id;
                    StringUtil.OrderId = order_id;
                    startActivity(intent);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    // String
    public class FeedItemZone {
        private String title;
        private String id;
        private String setDetail;
        private String bid;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String name) {
            this.title = name;
        }


        public String getDetail() {
            return setDetail;
        }

        public void setDetail(String Detail) {
            this.setDetail = Detail;
        }

        public String getBid() {
            return bid;
        }

        public void setBid(String bid) {
            this.bid = bid;
        }

    }

    // String
    public class FeedItemDrug {
        private String title;
        private String id;
        private String setDetail;
        private String total_qty;
        private String customer_total;
        private String total_price;
        private String order_id;
        private String payment_status;

        public String getPayment_status() {
            return payment_status;
        }

        public void setPayment_status(String payment_status) {
            this.payment_status = payment_status;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getCustomer_total() {
            return customer_total;
        }

        public void setCustomer_total(String customer_total) {
            this.customer_total = customer_total;
        }

        public String getTotal_price() {
            return total_price;
        }

        public void setTotal_price(String total_price) {
            this.total_price = total_price;
        }

        private String table_status;

        public String getTable_status() {
            return table_status;
        }

        public void setTable_status(String table_status) {
            this.table_status = table_status;
        }

        public String getTotal_qty() {
            return total_qty;
        }

        public void setTotal_qty(String total_qty) {
            this.total_qty = total_qty;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String name) {
            this.title = name;
        }


        public String getDetail() {
            return setDetail;
        }

        public void setDetail(String Detail) {
            this.setDetail = Detail;
        }


    }


}
