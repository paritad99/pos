package com.geerang.gcounter.ui.attendance;

import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.ViewModelProviders;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.PictureDrawable;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.geerang.gcounter.Profile;
import com.geerang.gcounter.R;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class AttendanceFragment extends Fragment {

    private AttendanceViewModel mViewModel;

    TextView txtDateDay, txtTimeAttendance, txtStoreName, txtBranchName, txtUsername, txtTotalEmployee, txtDialySale, txtMonthlySale;


    Button btnCheckIn, btnCheckOut;

    public static AttendanceFragment newInstance() {
        return new AttendanceFragment();
    }

    View root;
    Format formatter;

    String CheckInOut = "";
    private List<FeedItemDrug> feedsList;
    private CategoriesRecyclerAdapter mAdapter;

    RecyclerView rvCatagory;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.attendance_fragment, container, false);

        formatter = new SimpleDateFormat("EEEE ที่ dd เดือน MMMM พ.ศ. yyyy", new Locale("th", "TH"));
        System.out.println(formatter.format(new Date()));

        txtDateDay = (TextView) root.findViewById(R.id.txtDateDay);
        txtDateDay.setText(formatter.format(new Date()));

        txtUsername = (TextView) root.findViewById(R.id.txtUsername);
        txtUsername.setText("ชื่อผู้ใช้งาน: " + Profile.Username);

        txtStoreName = (TextView) root.findViewById(R.id.txtStoreName);

        txtBranchName = (TextView) root.findViewById(R.id.txtBranchName);

        txtTimeAttendance = (TextView) root.findViewById(R.id.txtTimeAttendance);

        txtBranchName.setText("สาขา : " + Profile.BranchName);
        txtStoreName.setText("ร้าน : " + Profile.StoreName);

        SimpleDateFormat formatter_month = new SimpleDateFormat("เดือน MMMM พ.ศ. yyyy", new Locale("th", "TH"));
        TextView txtLogMonth = (TextView) root.findViewById(R.id.txtLogMonth);
        txtLogMonth.setText("" + formatter_month.format(new Date()));


        btnCheckIn = (Button) root.findViewById(R.id.btnCheckIn);
        btnCheckIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckInOut = "checkin";

                new AlertDialog.Builder(getActivity())
                        .setTitle("CheckIn เข้างาน")
                        .setMessage("คุณต้องการ Checkin ใช่หรือใหม่?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                new AsyncCheckInOut().execute();
                            }})
                        .setNegativeButton(android.R.string.no, null).show();



            }
        });

        btnCheckOut = (Button) root.findViewById(R.id.btnCheckOut);

        btnCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckInOut = "checkout";
                //new AsyncCheckInOut().execute();

                new AlertDialog.Builder(getActivity())
                        .setTitle("CheckOut เลิกงาน")
                        .setMessage("คุณต้องการ Checkout ใช่หรือใหม่?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                new AsyncCheckInOut().execute();
                            }})
                        .setNegativeButton(android.R.string.no, null).show();

            }
        });

        rvCatagory = (RecyclerView) root.findViewById(R.id.recyclerView);

        new AsyncChoiceProduct().execute();

        return root;
    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    private class AsyncCheckInOut extends AsyncTask<String, String, String> {

        ProgressDialog pdLoading = new ProgressDialog(getActivity());
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread

            pdLoading.setMessage("\tกำลัง" + CheckInOut + "...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("door", CheckInOut)
                    .add("employee_id", Profile.UserId)
                    .build();

            try {

                response = http.run("http://www.geerang.com/gpos/api/AttendanceCheckInOut", formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {

            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(result);

                String status = object.getString("status"); // success

                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things

                                    new AsyncChoiceProduct().execute();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

                } else {

                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things

                                    new AsyncChoiceProduct().execute();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

    }


    private class AsyncChoiceProduct extends AsyncTask<String, String, String> {

        ProgressDialog pdLoading = new ProgressDialog(getActivity());
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread

            pdLoading.setMessage("\tกำลังโหลดข้อมูล...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("store_id", Profile.StoreId)
                    .add("branch_id", Profile.BranchId)
                    .add("employee_id", Profile.UserId)
                    .build();
            try {

                response = http.run("http://www.geerang.com/gpos/api/Attendance", formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            try {

                pdLoading.dismiss();
                JSONArray jArray = new JSONArray(result);
                try {

                    //final ArrayList<BarEntry> entries = new ArrayList<>();
                    //final ArrayList<Student> listStudent = Student.getSampleStudentData(30);zxzzzzsss
                    // Extract data from json and store into ArrayList as class objects
                    for (int i = 0; i < jArray.length(); i++) {
                        JSONObject json_data = jArray.getJSONObject(i);
                        //ReportMonthlyActivity.FeedItemChoice item = new ReportMonthlyActivity.FeedItemChoice();

                        txtBranchName.setText("สาขา : " + json_data.getString("branch_name"));
                        txtStoreName.setText("ร้าน : " + json_data.getString("store_name"));

                        feedsList = new ArrayList<>();
                        try {

                            JSONArray jArrayAttendence = new JSONArray(json_data.getString("report_attendance"));
                            // Extract data from json and store into ArrayList as class objects
                            for (int a = 0; a < jArrayAttendence.length(); a++) {
                                JSONObject json_Attendence = jArrayAttendence.getJSONObject(a);
                                FeedItemDrug item = new FeedItemDrug();
                                item.setEmployee_id(json_Attendence.getString("username"));
                                item.setFirstname(json_Attendence.getString("checkin_time"));
                                item.setLastname(json_Attendence.getString("checkout_time"));
                                item.setStatus_name(json_Attendence.getString("work_hours"));// work_hours
                                item.setTel(json_Attendence.getString("create_date"));
                                feedsList.add(item);
                            }

                            // Setup and Handover data to recyclerview
                            mAdapter = new CategoriesRecyclerAdapter(getActivity(), feedsList);
                            rvCatagory.setAdapter(mAdapter);
                            //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
                            rvCatagory.setLayoutManager(mLayoutManager);
                            rvCatagory.setItemAnimator(new DefaultItemAnimator());
                            rvCatagory.setItemAnimator(new DefaultItemAnimator());


                            // txtTimeAttendance
                            JSONArray jArrayAttendenceSc = new JSONArray(json_data.getString("attendance_schedule"));
                            // Extract data from json and store into ArrayList as class objects
                            for (int sc = 0; sc < jArrayAttendenceSc.length(); sc++) {
                                JSONObject json_AttendenceSc = jArrayAttendenceSc.getJSONObject(sc);

                                String Time = json_AttendenceSc.getString("checkin_time") + " - " + json_AttendenceSc.getString("checkout_time");
                                txtTimeAttendance.setText(Time);

                            }


                        } catch (JSONException e) {
                            Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
                        }

                        //create_graph_customer(labels2,entries2);

                        //myList.add(data1);


                    }

                } catch (JSONException e) {
                    String x = "";
                    x = e.toString();
                    //Toast.makeText(ReportMonthlyActivity.this, e.toString(), Toast.LENGTH_LONG).show();
                }


            } catch (JSONException e) {
                String x = "";
                x = e.toString();
//                txt_status.setVisibility(View.VISIBLE);
                //Toast.makeText(ReportMonthlyActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(AttendanceViewModel.class);
        // TODO: Use the ViewModel
    }

    public class CategoriesRecyclerAdapter extends RecyclerView.Adapter<CategoriesRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemDrug> feedItemList;
        private Context mContext;

        public CategoriesRecyclerAdapter(Context context, List<FeedItemDrug> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;
        }

        @Override
        public CategoriesRecyclerAdapter.CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.attendance_list_row, null);

            CategoriesRecyclerAdapter.CustomViewHolder viewHolder = new CategoriesRecyclerAdapter.CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(CategoriesRecyclerAdapter.CustomViewHolder customViewHolder, int i) {
            FeedItemDrug feedItem = feedItemList.get(i);
            customViewHolder.feedItem = feedItem;


            //Setting text view title
            customViewHolder.txtCheckIn.setText("" + feedItem.getFirstname());
            String checkout = feedItem.getLastname();

            if(!checkout.equals("null"))
            {
                customViewHolder.txtCheckOut.setText("" + feedItem.getLastname());
            }else
            {
                customViewHolder.txtCheckOut.setText("-");
            }

            customViewHolder.tv_status.setText("" + feedItem.getStatus_name());

            customViewHolder.txtStampDate.setText(feedItem.getTel());

            //System.out.println();


            //customViewHolder.imageView.setImageResource(StringUtil.arrImg[i]);


        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            protected ImageView imageView;
            protected TextView txtCheckIn, txtCheckOut, tv_status,txtStampDate;

            FeedItemDrug feedItem;

            public CustomViewHolder(View view) {
                super(view);
                this.txtStampDate = (TextView) view.findViewById(R.id.txtStampDate);
                this.txtCheckIn = (TextView) view.findViewById(R.id.txtCheckIn);
                this.txtCheckOut = (TextView) view.findViewById(R.id.txtCheckout);
                this.tv_status = (TextView) view.findViewById(R.id.tv_status);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

//                        StringUtil.CatId = feedItem.getId();
                        //new AsyncProduct().execute();
                        //Toast.makeText(v.getContext(), "Select is: " + feedItem.getId(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }
    }

    public class FeedItemDrug {

        private String employee_id;
        private String firstname;
        private String lastname;
        private String tel;
        private String status_name;

        public String getEmployee_id() {
            return employee_id;
        }

        public void setEmployee_id(String employee_id) {
            this.employee_id = employee_id;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getStatus_name() {
            return status_name;
        }

        public void setStatus_name(String status_name) {
            this.status_name = status_name;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }
    }

}