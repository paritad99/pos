package com.geerang.gcounter.ui.staff_leave;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.PictureDrawable;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.geerang.gcounter.Profile;
import com.geerang.gcounter.R;
import com.geerang.gcounter.StaffLeaveAddActivity;
import com.geerang.gcounter.StringUtil;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class StaffLeaveFragment extends Fragment {


    public static StaffLeaveFragment newInstance() {
        return new StaffLeaveFragment();
    }

    private final OkHttpClient client = new OkHttpClient();
    private List<FeedItemLeave> feedsList;
    private CategoriesRecyclerAdapter mAdapter;

    RecyclerView rvCatagory;
    View root;

    Button btnLeave;

    TextView txt_leave_status;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {


        root = inflater.inflate(R.layout.fragment_staff_leave, container, false);


        txt_leave_status = (TextView) root.findViewById(R.id.txt_leave_status);

        btnLeave = (Button) root.findViewById(R.id.btnLeave);
        btnLeave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getActivity(), StaffLeaveAddActivity.class);
                startActivity(in);
            }
        });


        rvCatagory = (RecyclerView) root.findViewById(R.id.recyclerView);


        new AsyncLoadCat().execute();

        return root;
    }


    @Override
    public void onResume() {
        super.onResume();

        new AsyncLoadCat().execute();
    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    private class AsyncLoadCat extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(getActivity());
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/StaffLeave_Request/" + Profile.UserId);
//                response = http.run(StringUtil.URL+"gpos/api/category/1");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();



            feedsList = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemLeave item = new FeedItemLeave();
                    item.setEmployee_id(json_data.getString("employee_id"));
                    item.setFirstname(json_data.getString("firstname"));
                    item.setLastname(json_data.getString("lastname"));
                    item.setLeave_title(json_data.getString("leave_title"));
                    item.setStart_date(json_data.getString("start_date"));
                    item.setEnd_date(json_data.getString("end_date"));
                    item.setLeave_of_day(json_data.getString("leave_of_day"));
                    item.setLeave_of_type(json_data.getString("leave_of_type"));
                    item.setCreate_date(json_data.getString("create_date"));
                    item.setTel(json_data.getString("tel"));
                    item.setStatus_name(json_data.getString("status_name"));
                    item.setIs_approve(json_data.getString("is_approve"));

                    feedsList.add(item);
                }

                if(jArray.length() > 0)
                {
                    txt_leave_status.setVisibility(View.GONE);
                }

                // Setup and Handover data to recyclerview

                mAdapter = new CategoriesRecyclerAdapter(getActivity(), feedsList);
                rvCatagory.setAdapter(mAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
                rvCatagory.setLayoutManager(mLayoutManager);
                rvCatagory.setItemAnimator(new DefaultItemAnimator());
                rvCatagory.setItemAnimator(new DefaultItemAnimator());

            } catch (JSONException e) {



                Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    public class CategoriesRecyclerAdapter extends RecyclerView.Adapter<CategoriesRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemLeave> feedItemList;
        private Context mContext;

        public CategoriesRecyclerAdapter(Context context, List<FeedItemLeave> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_staff_leave_row, null);

            CustomViewHolder viewHolder = new CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
            FeedItemLeave feedItem = feedItemList.get(i);
            customViewHolder.feedItem = feedItem;


            String Leave_of_day[] = {"ครึ้งวันเช้า", "ครึ่งวันบ่าย", "เต็มวัน"};
            String leave_of_type[] = {"กิจ", "ป่วย", "พักร้อน"};

            try {
                //Setting text view title
                customViewHolder.txt_firstname.setText(Html.fromHtml("ชื่อ-นามสกุล" + feedItem.getFirstname() + "-" + feedItem.getLastname()));
                customViewHolder.txt_leave_of_type.setText(Html.fromHtml("ขอลา ("
                        + leave_of_type[Integer.parseInt(feedItem.leave_of_day + "")] + ")"));

                customViewHolder.txt_leave_of_day.setText(Html.fromHtml("รูปแบบการลา : " + Leave_of_day[Integer.parseInt(feedItem.getLeave_of_day() + "")]));

                customViewHolder.txt_leave_date.setText(Html.fromHtml("ตั้งแต่วันที่ : " + feedItem.getStart_date()
                        + "ถึงวันที่ " + feedItem.getEnd_date()));
                customViewHolder.txt_leave_title.setText(Html.fromHtml("เหตุผลการขอลา : " + feedItem.getLeave_title()));

                //รอการ approve ,1 Approved
                String s = feedItem.getIs_approve();
                if(s.equals("0"))
                {
                    customViewHolder.txt_is_approve.setText(Html.fromHtml("รอการอนุมัติ"));
                    customViewHolder.txt_is_approve.setTextColor(Color.parseColor("#FF0000"));
                }else
                {
                    customViewHolder.txt_is_approve.setText(Html.fromHtml("อนุมัติเรียบร้อย"));
                    customViewHolder.txt_is_approve.setTextColor(Color.parseColor("#228B22"));
                }





            } catch (Exception e) {

            }


        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            //            protected ImageView imageView;

            protected TextView txt_is_approve,txt_firstname, txt_leave_of_type, txt_leave_of_day, txt_leave_title, txt_leave_date;

            FeedItemLeave feedItem;

            public CustomViewHolder(View view) {
                super(view);

                this.txt_firstname = (TextView) view.findViewById(R.id.txt_firstname);
                this.txt_leave_of_type = (TextView) view.findViewById(R.id.txt_leave_of_type);
                this.txt_leave_of_day = (TextView) view.findViewById(R.id.txt_leave_of_day);
                this.txt_leave_title = (TextView) view.findViewById(R.id.txt_leave_title);
                this.txt_leave_date = (TextView) view.findViewById(R.id.txt_leave_date);
                this.txt_is_approve = (TextView) view.findViewById(R.id.txt_is_approve);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

//                        StringUtil.CatId = feedItem.getId();
                        //new AsyncProduct().execute();
                        //Toast.makeText(v.getContext(), "Select is: " + feedItem.getId(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }
    }

    // String
    public class FeedItemLeave {

        private String employee_id;
        private String leave_title;
        private String start_date;
        private String end_date;
        private String leave_of_day;
        private String leave_of_type;
        private String create_date;

        private String firstname;
        private String lastname;
        private String status_name;
        private String tel;
        private String is_approve;

        public String getIs_approve() {
            return is_approve;
        }

        public void setIs_approve(String is_approve) {
            this.is_approve = is_approve;
        }

        public void setEmployee_id(String employee_id) {
            this.employee_id = employee_id;
        }

        public String getEmployee_id() {
            return employee_id;
        }

        public String getCreate_date() {
            return create_date;
        }

        public String getEnd_date() {
            return end_date;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getFirstname() {
            return firstname;
        }

        public String getLastname() {
            return lastname;
        }

        public String getLeave_of_day() {
            return leave_of_day;
        }

        public String getLeave_of_type() {
            return leave_of_type;
        }

        public String getLeave_title() {
            return leave_title;
        }

        public String getStart_date() {
            return start_date;
        }

        public void setCreate_date(String create_date) {
            this.create_date = create_date;
        }

        public void setEnd_date(String end_date) {
            this.end_date = end_date;
        }

        public void setLeave_of_day(String leave_of_day) {
            this.leave_of_day = leave_of_day;
        }

        public void setLeave_of_type(String leave_of_type) {
            this.leave_of_type = leave_of_type;
        }

        public void setLeave_title(String leave_title) {
            this.leave_title = leave_title;
        }

        public void setStatus_name(String status_name) {
            this.status_name = status_name;
        }

        public void setStart_date(String start_date) {
            this.start_date = start_date;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }

        public String getStatus_name() {
            return status_name;
        }

        public String getTel() {
            return tel;
        }


    }


}
