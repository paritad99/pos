package com.geerang.gcounter.ui.ingredient;

import androidx.appcompat.app.AlertDialog;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.PictureDrawable;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.geerang.gcounter.IngredientGroupAddActivity;
import com.geerang.gcounter.IngredientGroupEditActivity;
import com.geerang.gcounter.Profile;
import com.geerang.gcounter.R;
import com.geerang.gcounter.StringUtil;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class IngredientCategoryFragment extends Fragment {

    public static IngredientCategoryFragment newInstance() {
        return new IngredientCategoryFragment();
    }

    // CONNECTION_TIMEOUT and READ_TIMEOUT are in milliseconds
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;
    private List<FeedItemDrug> feedsList;
    private RecyclerView recyclerView;
    private DrugRecyclerAdapter mAdapter;

    View root;

    Button btnMenuAdd;

    String delete_product_id;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.ingredient_category_fragment, container, false);

        recyclerView = (RecyclerView) root.findViewById(R.id.recyclerView);
        new AsyncLogin().execute();

        btnMenuAdd = (Button) root.findViewById(R.id.btnMenuAdd);
        btnMenuAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getActivity(), IngredientGroupAddActivity.class);
                startActivity(in);
            }
        });

        return  root;
    }


    @Override
    public void onResume() {
        super.onResume();
        new AsyncLogin().execute();

    }


    private class DeleteItemTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();


            RequestBody formBody = new FormEncodingBuilder()
                    .add("mic_id", delete_product_id)
                    .build();


            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                new AsyncLogin().execute();

                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things


                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

                } else {

                    //String order_id = object.getString("order_id"); // success

                    // keep to var ORderID
                    ///StringUtil.OrderId = order_id;

//                    Intent in = new Intent(ProductActivity.this, ListOrderActivity.class);
//                    startActivity(in);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private void showData(String jsonString) {
        Toast.makeText(getActivity(), jsonString, Toast.LENGTH_LONG).show();
    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    private class AsyncLogin extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(getActivity());
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL+"gpos/api/IngredientCategory/" + Profile.StoreId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();

            feedsList = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemDrug item = new FeedItemDrug();
                    item.setId(json_data.getString("mic_id"));
                    item.setTitle(json_data.getString("name"));
                    item.setDetail(json_data.getString("create_date"));
                    feedsList.add(item);
                }

                // Setup and Handover data to recyclerview
                mAdapter = new DrugRecyclerAdapter(getActivity(), feedsList);
                recyclerView.setAdapter(mAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setItemAnimator(new DefaultItemAnimator());

            } catch (JSONException e) {
                Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    // Adapter
    public class DrugRecyclerAdapter extends RecyclerView.Adapter<DrugRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemDrug> feedItemList;
        private Context mContext;

        public DrugRecyclerAdapter(Context context, List<FeedItemDrug> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.ingredient_cat_fragment_row, null);

            CustomViewHolder viewHolder = new CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
            final FeedItemDrug feedItem = feedItemList.get(i);
            customViewHolder.feedItem = feedItem;


            //Setting text view title
            customViewHolder.textView.setText(Html.fromHtml(feedItem.getTitle()));

            customViewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent in = new Intent(mContext, IngredientGroupEditActivity.class);
                    in.putExtra("mic_id",feedItem.getId());
                    in.putExtra("name",feedItem.getTitle());
                    startActivity(in);

                }
            });

            customViewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());

                    adb.setTitle("ยืนยันการลบข้อมูล?");
                    adb.setMessage("ยืนยันการลบข้อมูล");
                    adb.setNegativeButton("Cancel", null);
                    adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
                        public void onClick(DialogInterface dialog, int arg1) {
                            // TODO Auto-generated method stub
                            delete_product_id = feedItem.getId();
                            new DeleteItemTask().execute(StringUtil.URL + "gpos/api/IngredientCategoryDelete");
                        }
                    });
                    adb.show();


                }
            });




            //customViewHolder.imageView.setImageResource(StringUtil.arrImg[i]);

        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
//            protected ImageView imageView;
            protected TextView textView;
//            protected TextView tvPrice;

            FeedItemDrug feedItem;

            protected  Button  btnEdit,btnDelete;

            public CustomViewHolder(View view) {
                super(view);
                this.textView = (TextView) view.findViewById(R.id.title);
                this.btnEdit = (Button) view.findViewById(R.id.btnEdit);
                this.btnDelete = (Button) view.findViewById(R.id.btnDel);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

//                        StringUtil.ProductId = feedItem.getId();
//                        Intent in = new Intent(getActivity(), FoodOptionActivity.class);
//                        startActivity(in);


                    }
                });

            }
        }
    }


    // String
    public class FeedItemDrug {
        private String menu_image_data = "";
        private String menu_categories_id = "";
        private String menu_name = "";
        private String menu_product_detail = "";
        private String menu_product_price = "";
        private String menu_cookingtime = "";
        private String menu_calories = "";
        private String menu_is_recommend = "";

        public void setMenu_name(String menu_name) {
            this.menu_name = menu_name;
        }

        public String getMenu_name() {
            return menu_name;
        }

        public String getMenu_calories() {
            return menu_calories;
        }

        public String getMenu_categories_id() {
            return menu_categories_id;
        }

        public String getMenu_cookingtime() {
            return menu_cookingtime;
        }

        public String getMenu_image_data() {
            return menu_image_data;
        }

        public String getMenu_is_recommend() {
            return menu_is_recommend;
        }

        public String getMenu_product_detail() {
            return menu_product_detail;
        }

        public String getMenu_product_price() {
            return menu_product_price;
        }

        public String getSetDetail() {
            return setDetail;
        }

        public void setMenu_calories(String menu_calories) {
            this.menu_calories = menu_calories;
        }

        public void setMenu_categories_id(String menu_categories_id) {
            this.menu_categories_id = menu_categories_id;
        }

        public void setMenu_cookingtime(String menu_cookingtime) {
            this.menu_cookingtime = menu_cookingtime;
        }

        public void setMenu_image_data(String menu_image_data) {
            this.menu_image_data = menu_image_data;
        }

        public void setMenu_is_recommend(String menu_is_recommend) {
            this.menu_is_recommend = menu_is_recommend;
        }

        public void setMenu_product_detail(String menu_product_detail) {
            this.menu_product_detail = menu_product_detail;
        }

        public void setMenu_product_price(String menu_product_price) {
            this.menu_product_price = menu_product_price;
        }

        public void setSetDetail(String setDetail) {
            this.setDetail = setDetail;
        }

        private String title;
        private String id;
        private String setDetail;
        private String Price;
        private String product_image;

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }

        public String getPrice() {
            return Price;
        }

        public void setPrice(String price) {
            this.Price = price;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String name) {
            this.title = name;
        }


        public String getDetail() {
            return setDetail;
        }

        public void setDetail(String Detail) {
            this.setDetail = Detail;
        }


    }


}
