package com.geerang.gcounter.ui.store_info;

import androidx.lifecycle.ViewModelProviders;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.geerang.gcounter.Profile;
import com.geerang.gcounter.R;
import com.geerang.gcounter.StoreInfoActivity;
import com.geerang.gcounter.StringUtil;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class StoreInfoFragment extends Fragment {

    private StoreInfoViewModel mViewModel;

    public static StoreInfoFragment newInstance() {
        return new StoreInfoFragment();
    }


    RecyclerView rvCatagory;

    View root;
    TextView tvStorename, tvDescription, tvBusinessType, tvAddress, tvTax, tvPhone;
    ImageView imgStore;
    Button btnEditStore;

    String ImageStore;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.store_info_fragment, container, false);
        rvCatagory = (RecyclerView) root.findViewById(R.id.recyclerView);
        tvStorename = (TextView) root.findViewById(R.id.tvStorename);
        tvDescription = (TextView) root.findViewById(R.id.tvDescription);
        tvBusinessType = (TextView) root.findViewById(R.id.tvBusinessType);
        tvAddress = (TextView) root.findViewById(R.id.tvAddress);
        tvTax = (TextView) root.findViewById(R.id.tvTax);
        tvPhone = (TextView) root.findViewById(R.id.tvPhone);

        imgStore = (ImageView) root.findViewById(R.id.imgStore);

        new AsyncLoadCat().execute();
        btnEditStore = (Button) root.findViewById(R.id.btnEditStore);
        btnEditStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent in = new Intent(getActivity(), StoreInfoActivity.class);
                in.putExtra("store_name",tvStorename.getText().toString());
                in.putExtra("store_description",tvDescription.getText().toString());
                in.putExtra("business_type_name",tvBusinessType.getText().toString());
                in.putExtra("store_address",tvAddress.getText().toString());
                in.putExtra("tax",tvTax.getText().toString());
                in.putExtra("phone",tvPhone.getText().toString());
                in.putExtra("store_image",StringUtil.menu_image_data);

                startActivity(in);
            }
        });




        return root;
    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    private class AsyncLoadCat extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(getActivity());
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/StoreInfo/" + Profile.StoreId);
//                response = http.run(StringUtil.URL+"gpos/api/category/1");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();

            try {

                JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);

                    tvStorename.setText(json_data.getString("store_name"));
                    tvDescription.setText(json_data.getString("store_description"));
                    tvBusinessType.setText(json_data.getString("business_type_name"));
                    tvAddress.setText(json_data.getString("store_address"));
                    tvTax.setText(json_data.getString("tax"));
                    tvPhone.setText(json_data.getString("phone"));


                    if (!json_data.getString("logo").equals("")){

                        StringUtil.menu_image_data = ImageStore;
                        StringUtil stringUtil = new StringUtil();

                        ImageStore = json_data.getString("logo");
                        Picasso.with(getActivity())
                                .load(StringUtil.URL+"gpos/"+
                                        stringUtil.removeFirstChar(json_data.getString("logo")))
                                .fit()
                                .into(imgStore);
                    }

                }


            } catch (JSONException e) {
                Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }


    // String
    public class FeedItemDrug {

        private String employee_id;
        private String firstname;
        private String lastname;
        private String tel;
        private String status_name;

        public String getEmployee_id() {
            return employee_id;
        }

        public void setEmployee_id(String employee_id) {
            this.employee_id = employee_id;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getStatus_name() {
            return status_name;
        }

        public void setStatus_name(String status_name) {
            this.status_name = status_name;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(StoreInfoViewModel.class);
        // TODO: Use the ViewModel
    }

}
