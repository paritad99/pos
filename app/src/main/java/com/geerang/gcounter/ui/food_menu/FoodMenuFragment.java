package com.geerang.gcounter.ui.food_menu;

import androidx.appcompat.app.AlertDialog;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.PictureDrawable;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.geerang.gcounter.CatagoriesAddActivity;
import com.geerang.gcounter.CatagoriesEditActivity;
import com.geerang.gcounter.MenuStoreActivity;
import com.geerang.gcounter.Profile;
import com.geerang.gcounter.R;
import com.geerang.gcounter.StringUtil;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class FoodMenuFragment extends Fragment {



    // CONNECTION_TIMEOUT and READ_TIMEOUT are in milliseconds
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;
    private List<FeedItemDrug> feedsList;
    private RecyclerView recyclerView;
    private DrugRecyclerAdapter mAdapter;


    View root;

    Button btnAddCat;
    Profile profile;

    TextView txtMenuCat;
    int Total_cat = 0;
    FloatingActionButton fab;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_food_menu, container, false);

        recyclerView = (RecyclerView) root.findViewById(R.id.recyclerView);

        txtMenuCat = (TextView) root.findViewById(R.id.txtMenuCat);
        fab = (FloatingActionButton) root.findViewById(R.id.fab);

        new AsyncLogin().execute();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(getActivity(), CatagoriesAddActivity.class);
                startActivity(in);

            }
        });

        profile = new Profile(getActivity());


        return  root;
    }

    @Override
    public void onResume() {
        super.onResume();
        new AsyncLogin().execute();
    }

    private class AsyncLogin extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(getActivity());
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL+"gpos/api/category/"+ profile.StoreId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {
            //this method will be running on UI thread
            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();
            feedsList = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);
                Total_cat = jArray.length();
                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemDrug item = new FeedItemDrug();
                    item.setId(json_data.getString("categories_id"));
                    item.setTitle(json_data.getString("categories_name"));
                    item.setDetail(json_data.getString("categories_color"));
                    item.setProduct_image(json_data.getString("categories_image"));
                    item.setId_printer(json_data.getString("id_printer"));
                    feedsList.add(item);
                }
                int menu = 0;
                try {
                    menu = Integer.parseInt(StringUtil.package_product);
                }catch (Exception err)
                {
                    menu = 0;
                }

                txtMenuCat.setText(Total_cat+"/"+menu);
                if(Total_cat>menu)
                {
                    fab.setEnabled(false);
                }else
                {
                    fab.setEnabled(true);
                }

                // Setup and Handover data to recyclerview
                mAdapter = new DrugRecyclerAdapter(getActivity(), feedsList);
                recyclerView.setAdapter(mAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setItemAnimator(new DefaultItemAnimator());


            } catch (JSONException e) {
                Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
            }
        }
    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    // Adapter
    public class DrugRecyclerAdapter extends RecyclerView.Adapter<DrugRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemDrug> feedItemList;
        private Context mContext;

        public DrugRecyclerAdapter(Context context, List<FeedItemDrug> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.menu_category_row_for_edit, null);

            CustomViewHolder viewHolder = new CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(CustomViewHolder customViewHolder, final int i) {
            final FeedItemDrug feedItem = feedItemList.get(i);
            customViewHolder.feedItem = feedItem;


            //Setting text view title
            customViewHolder.textView.setText(Html.fromHtml(feedItem.getTitle()));

            customViewHolder.btnDeleteCat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    final AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());

                    adb.setTitle("ยืนยันการลบข้อมูล?");
                    adb.setMessage("ยืนยันการลบข้อมูล");
                    adb.setNegativeButton("Cancel", null);
                    adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
                        public void onClick(DialogInterface dialog, int arg1) {
                            // TODO Auto-generated method stub
                            new DeleteItemTask().execute(StringUtil.URL + "gpos/api/CatMenuDelete", feedItem.getId());
                        }
                    });
                    adb.show();

                }
            });
            customViewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mContext, CatagoriesEditActivity.class);
                    StringUtil.CatId = feedItem.getId();
                    intent.putExtra("categories_id", feedItem.getId());
                    intent.putExtra("categories_name",  feedItem.getTitle());
                    intent.putExtra("categories_color",  feedItem.getDetail());
                    intent.putExtra("categories_image",  feedItem.getProduct_image());
                    intent.putExtra("id_printer",  feedItem.getId_printer());
                    startActivity(intent);
                }
            });

            customViewHolder.btnMenuAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mContext, MenuStoreActivity.class);
                        StringUtil.CatId = feedItem.getId();
                      startActivity(intent);

                }
            });

            //customViewHolder.imageView.setImageResource(StringUtil.arrImg[i]);

            StringUtil stringUtil = new StringUtil();
            Picasso.with(mContext)
                    .load(StringUtil.URL+"gpos/"+ stringUtil.removeFirstChar(feedItem.getProduct_image()))
                    .centerCrop()
                    .fit()
                    .error(R.drawable.noimageavailable)
                    .into(customViewHolder.imageView);
        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }

        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }

        public class CustomViewHolder extends RecyclerView.ViewHolder {
            protected ImageView imageView;
            protected TextView textView;
            protected Button btnEdit,btnDeleteCat,btnMenuAdd;
            FeedItemDrug feedItem;

            public CustomViewHolder(View view) {
                super(view);
                this.imageView = (ImageView) view.findViewById(R.id.imageView);
                this.textView = (TextView) view.findViewById(R.id.title);
                this.btnEdit= (Button) view.findViewById(R.id.btnEdit);
                this.btnDeleteCat= (Button) view.findViewById(R.id.btnDeleteCat);
                this.btnMenuAdd= (Button) view.findViewById(R.id.btnMenuAdd);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

//                        Intent intent = new Intent(v.getContext(), MenuStoreActivity.class);
//                        StringUtil.CatId = feedItem.getId();
//                        v.getContext().startActivity(intent);

                    }
                });

            }
        }
    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    private class DeleteItemTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();


            RequestBody formBody = new FormEncodingBuilder()
                    .add("product_id", urls[1])
                    .build();


            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success


                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things

                                    new AsyncLogin().execute();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

                } else {
                    new AsyncLogin().execute();
                    //String order_id = object.getString("order_id"); // success

                    // keep to var ORderID
                    ///StringUtil.OrderId = order_id;

//                    Intent in = new Intent(ProductActivity.this, ListOrderActivity.class);
//                    startActivity(in);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


    // String
    public class FeedItemDrug {
        private String title;
        private String id;
        private String setDetail;
        private String product_image;
        private String id_printer;

        public void setId_printer(String id_printer) {
            this.id_printer = id_printer;
        }

        public String getId_printer() {
            return id_printer;
        }

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String name) {
            this.title = name;
        }


        public String getDetail() {
            return setDetail;
        }

        public void setDetail(String Detail) {
            this.setDetail = Detail;
        }


    }


}
