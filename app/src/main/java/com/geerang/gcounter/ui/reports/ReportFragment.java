package com.geerang.gcounter.ui.reports;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.geerang.gcounter.ReportDialyActivity;
import com.geerang.gcounter.R;
import com.geerang.gcounter.ReportMonthlyActivity;

public class ReportFragment extends Fragment {



    public static ReportFragment newInstance() {
        return new ReportFragment();
    }

    Button btnDialy,btnMonthly,btnStock;
    View root;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        root =  inflater.inflate(R.layout.report_fragment, container, false);

        btnDialy = (Button) root.findViewById(R.id.btnDialyReport);
        btnDialy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getActivity(), ReportDialyActivity.class);
                startActivity(in);
            }
        });

        btnMonthly = (Button) root.findViewById(R.id.btnMonthly);
        btnMonthly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(getActivity(), ReportMonthlyActivity.class);
                startActivity(in);

            }
        });

//        btnStock = (Button) root.findViewById(R.id.btnStock);
//        btnStock.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Intent in = new Intent(getActivity(), ReportDialyActivity.class);
//                startActivity(in);
//
//            }
//        });


        return  root;
    }



}
