package com.geerang.gcounter.ui.table;

import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.ViewModelProviders;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.PictureDrawable;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.geerang.gcounter.CashierActivity;
import com.geerang.gcounter.Profile;
import com.geerang.gcounter.R;
import com.geerang.gcounter.StringUtil;
import com.geerang.gcounter.TableEditActivity;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class TableFragment extends Fragment {

    private TableViewModel mViewModel;

    public static TableFragment newInstance() {
        return new TableFragment();
    }

    private List<FeedItemDrug> feedsList;
    private RecyclerView recyclerView;
    private DrugRecyclerAdapter mAdapter;

    Handler handler = new Handler();
    Timer timer = new Timer();
    TimerTask timetask;
    Button BtnTableAdd;
    View root;
    String Table_ID = "";

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.table_fragment, container, false);

        recyclerView = (RecyclerView) root.findViewById(R.id.rvTable);

        new AsyncLogin().execute();
        return  root;
    }

    @Override
    public void onResume() {

        super.onResume();
        new AsyncLogin().execute();
    }

    private class AsyncLogin extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(getActivity());
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL+"gpos/api/CustomerTable/"+ Profile.BranchId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            //txtResult.setText(response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();

            feedsList = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemDrug item = new FeedItemDrug();
                    item.setId(json_data.getString("customer_id"));
                    item.setTitle(json_data.getString("customer_name"));
                    item.setZoneId(json_data.getString("building_zone_id"));
                    item.setBranchId(json_data.getString("branch_id"));
//                    item.setDetail(json_data.getString("order_id"));

                    feedsList.add(item);
                }

                // Setup and Handover data to recyclerview

                mAdapter = new DrugRecyclerAdapter(getActivity(), feedsList);
                recyclerView.setAdapter(mAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setItemAnimator(new DefaultItemAnimator());

            } catch (JSONException e) {
                Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    private class DeleteItemTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
        }

        protected String doInBackground(String... urls) {
            String response = null;
            TableFragment.postHttp http = new TableFragment.postHttp();


            RequestBody formBody = new FormEncodingBuilder()
                    .add("customer_id", Table_ID)
                    .build();


            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success


                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    new AsyncLogin().execute();

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

                } else {

                    //String order_id = object.getString("order_id"); // success

                    // keep to var ORderID
                    ///StringUtil.OrderId = order_id;

//                    Intent in = new Intent(ProductActivity.this, ListOrderActivity.class);
//                    startActivity(in);
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    new AsyncLogin().execute();

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
    // แบบ auto print task
    public void doTask(){

        timetask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {

                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String formattedDate = df.format(c.getTime());

                        // txtResult
                        //TextView result = (TextView) findViewById(R.id.txtResult);
                        //result.setText("TimeTask runing... Current : " + formattedDate);
                    }
                });
            }};
        timer.schedule(timetask, 0, 1000); // Every 1 second

    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    // Adapter
    public class DrugRecyclerAdapter extends RecyclerView.Adapter<DrugRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemDrug> feedItemList;
        private Context mContext;

        public DrugRecyclerAdapter(Context context, List<FeedItemDrug> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.table_edit_row, null);

            CustomViewHolder viewHolder = new CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
            FeedItemDrug feedItem = feedItemList.get(i);
            customViewHolder.feedItem = feedItem;


            //Setting text view title
            customViewHolder.textView.setText(Html.fromHtml("ชื่อโต๊ะ : " + feedItem.getTitle()));
            customViewHolder.status.setVisibility(View.GONE);
//            if(!feedItem.getDetail().equals("null"))
//            {
//                customViewHolder.status.setText("มีการจอง");
//                customViewHolder.status.setTextColor(mContext.getResources().getColor(R.color.colorRed));
//            }else
//            {
//                customViewHolder.status.setTextColor(mContext.getResources().getColor(R.color.colorGreen));
//                customViewHolder.status.setText("ว่าง");
//            }

//            customViewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    Intent in = new Intent(mContext, TableEditActivity.class);
//                    startActivity(in);
//
//                }
//            });
            customViewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    StringUtil.TableCustomerId = feedItem.getId();
                    StringUtil.TableCustomerName = feedItem.getTitle();
                    StringUtil.zone_id = feedItem.getZoneId();
                    StringUtil.BranchId  = feedItem.getBranchId();
                    Intent in = new Intent(getActivity(), TableEditActivity.class);
                    startActivity(in);
                    //Toast.makeText(getActivity(),"Edit..." + feedItem.getId(),Toast.LENGTH_SHORT).show();
                }
            });

            customViewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());

                    adb.setTitle("ยืนยันการลบข้อมูล?");
                    adb.setMessage("ยืนยันการลบข้อมูล");
                    adb.setNegativeButton("Cancel", null);
                    adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
                        public void onClick(DialogInterface dialog, int arg1) {
                            // TODO Auto-generated method stub
                            Table_ID = feedItem.getId();
                            new DeleteItemTask().execute(StringUtil.URL + "gpos/api/TableDelete");
                        }
                    });
                    adb.show();
                    //Toast.makeText(getActivity(),"Delete..." + feedItem.getId(),Toast.LENGTH_SHORT).show();
                }
            });


        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            protected ImageView imageView;
            protected TextView textView;
            protected TextView status;
            protected Button btnEdit;
            protected Button btnDelete;
            FeedItemDrug feedItem;

            public CustomViewHolder(View view) {
                super(view);
                // this.imageView = (ImageView) view.findViewById(R.id.imageView2);
                this.textView = (TextView) view.findViewById(R.id.title);
                this.status = (TextView) view.findViewById(R.id.tv_status);

                this.btnEdit = (Button) view.findViewById(R.id.btnEdit);
                this.btnDelete = (Button) view.findViewById(R.id.btnDelete);
//                view.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                        String status = StringUtil.Id = feedItem.getDetail();
//                        StringUtil.TableCustomerName = feedItem.getTitle();
//                        StringUtil.BranchId = Profile.BranchId;
//                        StringUtil.TableCustomer = feedItem.getTitle();
//
//                        // ถ้าว่าง
//                        if (status.equals("มีการจอง")) {
//                            StringUtil.Id = feedItem.getId();
//                            new AsyncLoadOrderId().execute();
//
//                        } else {
//                            StringUtil.Id = feedItem.getId();
//                            StringUtil.TotalCustomer = "1";
//                            new AddOrderTask().execute(StringUtil.URL+"gpos/api/AddOrder");
//
//                        }
//
//                    }
//                });

            }
        }
    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    private class AddOrderTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
        }

        protected String doInBackground(String... urls)   {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("UserId", Profile.UserId)
                    .add("StoreId", Profile.StoreId)
                    .add("BranchId", Profile.BranchId)
                    .add("Customer",  StringUtil.TotalCustomer)
                    .add("CustomerId", StringUtil.Id)
                    .build();


            try {

                response = http.run(urls[0],formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString)  {
            // Dismiss ProgressBar
            //showData(jsonString);

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success

                if(status.equals("0"))
                {
                    String msg = object.getString("msg"); // success
                    androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

                }else
                {

                    String order_id = object.getString("order_id"); // success

                    // keep to var ORderID
                    StringUtil.OrderId = order_id;

                    Intent intent = new Intent(getActivity(), CashierActivity.class);
                    //StringUtil.Id = order_id;
                    StringUtil.OrderId = order_id;
                    startActivity(intent);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


    private class AsyncLoadOrderId extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(getActivity());
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL+"gpos/api/GetOrderList/"+ StringUtil.Id);
//                response = http.run(StringUtil.URL+"gpos/api/category/1");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();

            // feedsList = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);


                    StringUtil.TableCustomerName = json_data.getString("customer_name");
                    StringUtil.BranchId = json_data.getString("branch_id");
                    StringUtil.OrderId = json_data.getString("order_id");

                    //StringUtil.TableCustomer = json_data.getString("total_sale");

                    // Show total sale
                    //tvSumAmount.setText("ยอดขายวันนี้ "+json_data.getString("total_sale")+" บาท");

                }




                Intent intent = new Intent(getActivity(), CashierActivity.class);
                startActivity(intent);


            } catch (JSONException e) {
                Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }


    // String
    public class FeedItemDrug {
        private String title;
        private String id;
        private String setDetail;
        private String zoneid;
        private String branch_id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String name) {
            this.title = name;
        }


        public String getDetail() {
            return setDetail;
        }

        public void setDetail(String Detail) {
            this.setDetail = Detail;
        }

        public String getZoneId() {
            return zoneid;
        }

        public void setZoneId(String zoneid) {
            this.zoneid = zoneid;
        }
        public String getBranchId() {
            return branch_id;
        }

        public void setBranchId(String branch_id) {
            this.branch_id = branch_id;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(TableViewModel.class);
        // TODO: Use the ViewModel
    }

}
