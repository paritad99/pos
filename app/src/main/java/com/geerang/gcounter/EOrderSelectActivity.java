package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.PictureDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class EOrderSelectActivity extends AppCompatActivity {

    RecyclerView rvCatagory, rvProduct, rvOrders;
    private List<FeedItemDrug> feedsList;
    private List<FeedItemOrder> feedsListOrder;
    private List<FeedItemProduct> feedsListProduct;

    String input_amount = "";
    String input_discount = "";

    private CategoriesRecyclerAdapter mAdapter;
    private ProductRecyclerAdapter mAdapter_product;
    Button btnSelectTableCustomer, btnBillMerge, btnPrintbill, btnDiscount, btnCash, btnCancelTable;

    public int total_qty = 0;
    public int sum_amount = 0;
    TextView tvTableName, tvTotal_qty, tvSum_amount, tvDiscount;

    private OrderRecyclerAdapter OrdermAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eorder);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        tvTotal_qty = (TextView) findViewById(R.id.tvItemQty);
        tvSum_amount = (TextView) findViewById(R.id.tvSumAmount);
        tvDiscount = (TextView) findViewById(R.id.tvDiscount);


        tvTableName = (TextView) findViewById(R.id.tvTableName);



        btnCancelTable = (Button) findViewById(R.id.btnCancelTable);
        btnCancelTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(EOrderSelectActivity.this, CancelSelectTableActivity.class);
                startActivity(in);
            }
        });


        btnPrintbill = (Button) findViewById(R.id.btnPrintbill);
        btnPrintbill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(EOrderSelectActivity.this);
                builder.setMessage("ดำเนินการสั่งอาหารเรียบร้อย")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //do things
                                new UpdateOrderTask().execute(StringUtil.URL+"gpos/api/UpdateOrder");

                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();


            }
        });




        rvCatagory = (RecyclerView) findViewById(R.id.rvCatagory);
        rvProduct = (RecyclerView) findViewById(R.id.rvProduct);
        rvOrders = (RecyclerView) findViewById(R.id.rvOrders);

    }


    @Override
    protected void onResume() {
        super.onResume();

        tvTableName.setText(StringUtil.TableCustomerName);

        new AsyncLoadCat().execute();

        new AsyncOrder().execute();

    }

    private class AsyncOrder extends AsyncTask<String, String, String> {
        //        ProgressDialog pdLoading = new ProgressDialog(CashierActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
//            pdLoading.setMessage("\tLoading...");
//            pdLoading.setCancelable(false);
//            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();

            try {

                if(StringUtil.Is_bill_merge == 1)
                {
                    response = http.run(StringUtil.URL + "gpos/api/ListOrderItems/" + StringUtil.arr_orderid);

                }else
                {
                    response = http.run(StringUtil.URL + "gpos/api/ListOrderItems/" + StringUtil.OrderId);
                }


//                response = http.run(StringUtil.URL+"gpos/api/ListOrderItems/29");

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            //txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            //pdLoading.dismiss();


            //List<DataFish> data=new ArrayList<>();

            int x = 0;
            int total_sale = 0;

            feedsListOrder = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);

                int qty = 0;
                int price = 0;
                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemOrder item = new FeedItemOrder();
                    item.setOrders_item_id(json_data.getString("orders_item_id"));
                    item.setId(json_data.getString("product_id"));
                    item.setTitle(json_data.getString("product_name"));
                    item.setDetail(json_data.getString("product_detail"));
                    item.setPrice(json_data.getString("amount"));
                    item.setQty(json_data.getString("qty"));
                    item.setOrderid(json_data.getString("orders_id"));
                    item.setProduct_image(json_data.getString("product_image"));
                    item.setChoice_name(json_data.getString("choice_name"));


                    try {


                        qty = Integer.parseInt(json_data.getString("qty"));
                        price = Integer.parseInt(json_data.getString("amount"));

                        x = x + qty;

                        total_sale = total_sale + (qty * price);
                    } catch (Exception e) {
                    }


                    feedsListOrder.add(item);
                }

                // Setup and Handover data to recyclerview

                StringUtil.TotalPayment = total_sale + "";

                tvTotal_qty.setText("รวมรายการ : " + x);
                tvSum_amount.setText("จำนวนเงินรวม : " + total_sale);


                OrdermAdapter = new OrderRecyclerAdapter(EOrderSelectActivity.this, feedsListOrder);
                rvOrders.setAdapter(OrdermAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(EOrderSelectActivity.this, 1);
                rvOrders.setLayoutManager(mLayoutManager);
                rvOrders.setItemAnimator(new DefaultItemAnimator());

            } catch (JSONException e) {
//                Toast.makeText(CashierActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    // Adapter
    public class OrderRecyclerAdapter extends RecyclerView.Adapter<OrderRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemOrder> feedsListOrder;
        private Context mContext;

        public OrderRecyclerAdapter(Context context, List<FeedItemOrder> feedItemList) {
            this.feedsListOrder = feedItemList;
            this.mContext = context;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.order_product_row, null);

            CustomViewHolder viewHolder = new CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
            FeedItemOrder feedItem = feedsListOrder.get(i);
            customViewHolder.feedItem = feedItem;

            try {
                int qty = Integer.parseInt(feedItem.getQty());
                int price = Integer.parseInt(feedItem.getPrice());

                int sum = qty * price;

                //Setting text view title
                customViewHolder.textView.setText("สินค้า: " + feedItem.getTitle()+"("+feedItem.getChoice_name()+")");
                customViewHolder.total_price.setText("ราคารวม: " + sum);
                customViewHolder.price.setText("ราคา/หน่วย: " + price);
                customViewHolder.qty.setText("จำนวน: " + feedItem.getQty());

            } catch (Exception err) {

            }


        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedsListOrder ? feedsListOrder.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            protected ImageView imageView;
            protected TextView textView;
            protected TextView price;
            protected TextView qty;
            protected TextView total_price;
            FeedItemOrder feedItem;

            public CustomViewHolder(View view) {
                super(view);
//                this.imageView = (ImageView) view.findViewById(R.id.imageView2);
                this.textView = (TextView) view.findViewById(R.id.title);
                this.price = (TextView) view.findViewById(R.id.tvPrice);
                this.qty = (TextView) view.findViewById(R.id.tvQty);
                this.total_price = (TextView) view.findViewById(R.id.tvTotalPrice);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        StringUtil.Orders_item_id = feedItem.getOrders_item_id();
                        StringUtil.OrderId = feedItem.getOrderid();
                        StringUtil.OrderQty = feedItem.getQty();

                        Intent in = new Intent(EOrderSelectActivity.this,EditProductItemActivity.class);
                        startActivity(in);

                    }
                });

            }
        }
    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    private class UpdateOrderTask extends AsyncTask<String, Void, String> {
        ProgressDialog pdLoading = new ProgressDialog(EOrderSelectActivity.this);
        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tUpdating...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();


            RequestBody formBody = new FormEncodingBuilder()
                    .add("orders_id", StringUtil.OrderId)
                    .build();


            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();
            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success

                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(EOrderSelectActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things

                                    Intent in = new Intent(EOrderSelectActivity.this,ETableSelectActivity.class);
                                    startActivity(in);
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

                } else {

                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(EOrderSelectActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things

                                    Intent in = new Intent(EOrderSelectActivity.this,ETableSelectActivity.class);
                                    startActivity(in);
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

//                    Intent in = new Intent(EOrderSelectActivity.this,ETableSelectActivity.class);
//                    startActivity(in);

                    // update order items
//                    new AsyncOrder().execute();
                    // keep to var ORderID
                    ///StringUtil.OrderId = order_id;

//                    Intent in = new Intent(ProductActivity.this, ListOrderActivity.class);
//                    startActivity(in);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class AsyncLoadCat extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(EOrderSelectActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/category/" + Profile.StoreId);
//                response = http.run(StringUtil.URL+"gpos/api/category/1");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();

            feedsList = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemDrug item = new FeedItemDrug();
                    item.setId(json_data.getString("categories_id"));
                    item.setTitle(json_data.getString("categories_name"));
                    item.setDetail(json_data.getString("categories_name"));
                    item.setProduct_image(json_data.getString("categories_image"));
                    feedsList.add(item);
                }

                // Setup and Handover data to recyclerview

                mAdapter = new CategoriesRecyclerAdapter(EOrderSelectActivity.this, feedsList);
                rvCatagory.setAdapter(mAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(EOrderSelectActivity.this, 1);
                rvCatagory.setLayoutManager(mLayoutManager);
                rvCatagory.setItemAnimator(new DefaultItemAnimator());
                rvCatagory.setItemAnimator(new DefaultItemAnimator());

            } catch (JSONException e) {
                Toast.makeText(EOrderSelectActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    private class AsyncProduct extends AsyncTask<String, String, String> {
        //        ProgressDialog pdLoading = new ProgressDialog(CashierActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/products/" + Profile.StoreId + '/' + StringUtil.CatId);

//                response = http.run(StringUtil.URL+"gpos/api/products/1" + '/' + StringUtil.CatId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

//            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();

            feedsListProduct = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemProduct item = new FeedItemProduct();
                    item.setId(json_data.getString("product_id"));
                    item.setTitle(json_data.getString("product_name"));
                    item.setDetail(json_data.getString("product_detail"));
                    item.setPrice(json_data.getString("product_sale_price"));
                    item.setProduct_image(json_data.getString("product_image"));
                    feedsListProduct.add(item);
                }

                // Setup and Handover data to recyclerview

                mAdapter_product = new ProductRecyclerAdapter(EOrderSelectActivity.this, feedsListProduct);
                rvProduct.setAdapter(mAdapter_product);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(EOrderSelectActivity.this, 2);
                rvProduct.setLayoutManager(mLayoutManager);
                rvProduct.setItemAnimator(new DefaultItemAnimator());
                rvProduct.setItemAnimator(new DefaultItemAnimator());

            } catch (JSONException e) {
                Toast.makeText(EOrderSelectActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    public class ProductRecyclerAdapter extends RecyclerView.Adapter<ProductRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemProduct> feedsListProduct;
        private Context mContext;

        public ProductRecyclerAdapter(Context context, List<FeedItemProduct> feedItemList) {
            this.feedsListProduct = feedItemList;
            this.mContext = context;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.product_row, null);

            CustomViewHolder viewHolder = new CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
            FeedItemProduct feedItem = feedsListProduct.get(i);
            customViewHolder.feedsListProduct = feedItem;


            //Setting text view title
            customViewHolder.textView.setText(Html.fromHtml(feedItem.getTitle()));
            customViewHolder.tvPrice.setText(Html.fromHtml(feedItem.getPrice()));

            StringUtil stringUtil = new StringUtil();
            Picasso.with(mContext)
                    .load(StringUtil.URL + "gpos/" + stringUtil.removeFirstChar(feedItem.getProduct_image()))
                    .centerCrop()
                    .fit()
                    .error(R.drawable.ic_error)
                    .into(customViewHolder.imageView);


        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedsListProduct ? feedsListProduct.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            protected ImageView imageView;
            protected TextView textView;
            protected TextView tvPrice;

            FeedItemProduct feedsListProduct;

            public CustomViewHolder(View view) {
                super(view);
                this.imageView = (ImageView) view.findViewById(R.id.imageView);
                this.textView = (TextView) view.findViewById(R.id.title);
                this.tvPrice = (TextView) view.findViewById(R.id.tvTotalPrice);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StringUtil.ProductId = feedsListProduct.getId();
                        StringUtil.ProductAmount = feedsListProduct.getPrice();

                        Intent in = new Intent(EOrderSelectActivity.this,ChoiceActivity.class);
                        startActivity(in);
                    }
                });

            }
        }
    }

    public class CategoriesRecyclerAdapter extends RecyclerView.Adapter<CategoriesRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemDrug> feedItemList;
        private Context mContext;

        public CategoriesRecyclerAdapter(Context context, List<FeedItemDrug> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.product_cat_list_row, null);

            CustomViewHolder viewHolder = new CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
            FeedItemDrug feedItem = feedItemList.get(i);
            customViewHolder.feedItem = feedItem;


            //Setting text view title
            customViewHolder.textView.setText(Html.fromHtml(feedItem.getTitle()));
            //customViewHolder.imageView.setImageResource(StringUtil.arrImg[i]);

            StringUtil stringUtil = new StringUtil();
            Picasso.with(mContext)
                    .load(StringUtil.URL + "gpos/" + stringUtil.removeFirstChar(feedItem.getProduct_image()))
                    .fit()
                    .into(customViewHolder.imageView);
        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            protected ImageView imageView;
            protected TextView textView;
            protected TextView status;

            FeedItemDrug feedItem;

            public CustomViewHolder(View view) {
                super(view);
                this.imageView = (ImageView) view.findViewById(R.id.imageView);
                this.textView = (TextView) view.findViewById(R.id.title);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        StringUtil.CatId = feedItem.getId();
                        new AsyncProduct().execute();
                        //Toast.makeText(v.getContext(), "Select is: " + feedItem.getId(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }
    }


    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    // String
    public class FeedItemDrug {
        private String title;
        private String id;
        private String setDetail;
        private String price;
        private String qty;
        private String PricePerItem;
        private String orders_item_id;

        private String orderid;
        private String product_image;

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }

        public String getOrderid() {
            return orderid;
        }

        public void setOrderid(String orderid) {
            this.orderid = orderid;
        }

        public String getOrders_item_id() {
            return orders_item_id;
        }

        public void setOrders_item_id(String orders_item_id) {
            this.orders_item_id = orders_item_id;
        }

        public String getPricePerItem() {
            return PricePerItem;
        }

        public void setPricePerItem(String pricePerItem) {
            this.PricePerItem = pricePerItem;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getPrice() {
            return price;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String name) {
            this.title = name;
        }


        public String getDetail() {
            return setDetail;
        }

        public void setDetail(String Detail) {
            this.setDetail = Detail;
        }


    }

    // String
    public class FeedItemProduct {
        private String title;
        private String id;
        private String setDetail;
        private String Price;
        private String product_image;

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }

        public String getPrice() {
            return Price;
        }

        public void setPrice(String price) {
            this.Price = price;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String name) {
            this.title = name;
        }


        public String getDetail() {
            return setDetail;
        }

        public void setDetail(String Detail) {
            this.setDetail = Detail;
        }


    }


    // String
    public class FeedItemOrder {
        private String title;
        private String id;
        private String setDetail;
        private String price;
        private String qty;
        private String PricePerItem;
        private String orders_item_id;
        private String product_image;
        private String Choice_name;

        public String getChoice_name() {
            return Choice_name;
        }

        public void setChoice_name(String choice_name) {
            Choice_name = choice_name;
        }

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }

        private String orderid;

        public String getOrderid() {
            return orderid;
        }

        public void setOrderid(String orderid) {
            this.orderid = orderid;
        }

        public String getOrders_item_id() {
            return orders_item_id;
        }

        public void setOrders_item_id(String orders_item_id) {
            this.orders_item_id = orders_item_id;
        }

        public String getPricePerItem() {
            return PricePerItem;
        }

        public void setPricePerItem(String pricePerItem) {
            this.PricePerItem = pricePerItem;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getPrice() {
            return price;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String name) {
            this.title = name;
        }


        public String getDetail() {
            return setDetail;
        }

        public void setDetail(String Detail) {
            this.setDetail = Detail;
        }


    }

}
