package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.epson.epos2.cashchanger.CashChanger;
import com.epson.epos2.printer.Printer;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class PaymentActivity extends AppCompatActivity implements View.OnClickListener {

    private final OkHttpClient client = new OkHttpClient();

    EditText input_payment_amount;
    Button btnCash, btnBank, btnCredit, btnPaySbuy;
    String Payment_Status = "";
    String payment_ref_number = "";
    String payment_type = "";
    double payment_change = 0.0;

    TextView tvTotal;
    public static Printer mPrinter = null;
    static CashChanger mCashChanger = null;


    AlertDialog.Builder dDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        input_payment_amount = (EditText) findViewById(R.id.input_payment_amount);
        //input_payment_amount.setText(StringUtil.TotalPayment + "");

        tvTotal = (TextView) findViewById(R.id.tvTotal);
        tvTotal.setText("จำนวนเงินที่ต้องชำระ " + StringUtil.discount_total_payment + " บาท " +
                " \n เลขที่สั่งซื้อ "+StringUtil.OrderId +"");

        btnCash = (Button) findViewById(R.id.btnCash);
        btnCash.setOnClickListener(this);
        btnBank = (Button) findViewById(R.id.btnBank);
        btnBank.setOnClickListener(this);
        btnCredit = (Button) findViewById(R.id.btnCredit);
        btnCredit.setOnClickListener(this);
        btnPaySbuy = (Button) findViewById(R.id.btnPaySbuy);
        btnPaySbuy.setOnClickListener(this);

        dDialog = new AlertDialog.Builder(this);
    }

    @Override
    public void onClick(View view) {
        //btnCash,btnBank,btnCredit,btnPaySbuy
        //'โอนเงินผ่านธนาคาร','บัตรเดบิต/เครดิต','PayPal','PAYSBUY','เงินสด'
        switch (view.getId()) {
            case R.id.btnCash:
                Payment_Status = "2";
                String payment_amount = input_payment_amount.getText().toString();
                if(TextUtils.isEmpty(payment_amount)) {
                    input_payment_amount.setError("กรุณาระบุจำนวนเงินที่รับจากลูกค้า");
                    return;
                }else
                {
                    double amount = Double.parseDouble(payment_amount);
                    float receive =  Float.parseFloat(StringUtil.TotalPayment);//Integer.parseInt(StringUtil.TotalPayment);
                    if(amount < receive)
                    {
                        input_payment_amount.setError("จำนวนเงินที่รับน้อยกว่าจำนวนเงินเรียกเก็บ");
                        return;

                    }else
                    {
                        dDialog.setTitle("ยืนยันการชำระเงิน");
                        dDialog.setMessage("กรุณายืนยันการชำระเงิน.");
                        dDialog.setNegativeButton("Cancel", null);
                        dDialog.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
                            public void onClick(DialogInterface dialog, int arg1) {
                                // TODO Auto-generated method stub
                                try {
                                    double input_amount = Double.parseDouble(input_payment_amount.getText().toString().trim());
                                    double cash_payment = Double.parseDouble(StringUtil.TotalPayment);

                                    StringUtil.input_amount = input_amount+"";
                                    StringUtil.cash_payment = cash_payment+"";

                                    payment_change = input_amount - cash_payment;
                                    StringUtil.payment_change = payment_change+"";

                                    new AddOrderTask().execute(StringUtil.URL + "gpos/api/Cashier");


                                } catch (Exception error) {

                                }
                            }
                        });
                        dDialog.show();
                    }


                }
                break;
            case R.id.btnBank:
                Payment_Status = "3";
                input_payment_amount.setText(StringUtil.TotalPayment + "");
                new AddOrderTask().execute(StringUtil.URL + "gpos/api/Cashier");
                break;
            case R.id.btnCredit:
                Payment_Status = "1";
                input_payment_amount.setText(StringUtil.TotalPayment + "");
                new AddOrderTask().execute(StringUtil.URL + "gpos/api/Cashier");
                break;
            case R.id.btnPaySbuy:
                Payment_Status = "4";
                input_payment_amount.setText(StringUtil.TotalPayment + "");
                new AddOrderTask().execute(StringUtil.URL + "gpos/api/Cashier");
                break;

        }
    }


    private class AddOrderTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(PaymentActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();
            RequestBody formBody;
            if(StringUtil.Is_bill_merge == 1)
            {
//                response = http.run(StringUtil.URL + "gpos/api/ListOrderItems/" + StringUtil.arr_orderid);

                formBody = new FormEncodingBuilder()
                        .add("order_id",  StringUtil.arr_orderid)//รหัสสินค้า
                        .add("payment_change", payment_change + "")//เงินทอน
                        .add("payment_amount", input_payment_amount.getText().toString().trim())//เงินที่รับจากลูกค้า
                        .add("payment_type", Payment_Status)//รูปแบบการจ่ายเงิน
                        .add("payment_ref_number", payment_ref_number)//รูปแบบการจ่ายเงิน
                        .add("employee_id", Profile.UserId)
                        .add("customer_id", StringUtil.Id)
                        .add("is_bill_merge",StringUtil.Is_bill_merge+"")
                        //สำหรับโปรโมชั่น
                        .add("promotion_id", StringUtil.promotion_id)
                        .add("promotion_use_qty", StringUtil.promotion_use_qty+"")
                        .add("promotion_branch_id", StringUtil.promotion_branch_id)
                        .build();

            }else
            {
                 formBody = new FormEncodingBuilder()
                        .add("order_id", StringUtil.OrderId)//รหัสสินค้า
                        .add("payment_change", payment_change + "")//เงินทอน
                        .add("payment_amount", input_payment_amount.getText().toString().trim())//เงินที่รับจากลูกค้า
                        .add("payment_type", Payment_Status)//รูปแบบการจ่ายเงิน
                        .add("employee_id", Profile.UserId)
                        .add("customer_id", StringUtil.Id)
                        .add("is_bill_merge",StringUtil.Is_bill_merge+"")
                        //สำหรับโปรโมชั่น

                        .add("promotion_id", StringUtil.promotion_id)
                        .add("promotion_use_qty", StringUtil.promotion_use_qty+"")
                        .add("promotion_branch_id", StringUtil.promotion_branch_id)

                        .build();

            }


            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);

            JSONObject object = null;
            try {

                pdLoading.dismiss();

                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success

                String msg = object.getString("msg"); // success
                AlertDialog.Builder builder = new AlertDialog.Builder(PaymentActivity.this);
                builder.setMessage(msg)
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //do things
                                Intent in = new Intent(PaymentActivity.this, BalanceActivity.class);
                                startActivity(in);
                                finish();

                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private void showData(String jsonString) {
        Toast.makeText(this, jsonString, Toast.LENGTH_LONG).show();
    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

}
