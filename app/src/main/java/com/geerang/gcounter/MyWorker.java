package com.geerang.gcounter;

import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.epson.epos2.printer.Printer;
import com.epson.epos2.printer.PrinterStatusInfo;
import com.epson.epos2.printer.ReceiveListener;

import java.util.Timer;

public class MyWorker extends Worker implements ReceiveListener {

    // constant
    public static final long NOTIFY_INTERVAL = 10 * 1000; // 10 seconds

    // run on another Thread to avoid crash
    private Handler mHandler = new Handler();
    // timer handling
    private Timer mTimer = null;

    public IBinder onBind(Intent intent) {
        return null;
    }

    static private BluetoothSocket mbtSocket = null;
    public static Printer mPrinter = null;
    static String menu_order = "";

    //    IBinder service;
//    PrinterInterface mAidl;
    String Resultbill = "";

    Profile profile;

    private static final int DISCONNECT_INTERVAL = 1000;//millseconds

    private BroadcastReceiver MyReceiver = null;


    EpsonMakeErrorMessage epsonMakeErrorMessage;
    final String KEY_Autoprint = "Autoprint";
    final String KEY_PrinterMain = "PrinterMain";
    final String KEY_Printer1 = "Printer1";
    final String KEY_Printer2 = "Printer2";
    final String KEY_Printer3 = "Printer3";
    final String KEY_Printer4 = "Printer4";

    String PrinterAddress = "";
    String product_name = "";
    String report_cat_name = "";
    String customer_name = "";
    String username = "";
    String kit_cat = "";
    String create_date = "";

    final String PREF_NAME_PRINT = "PrinterPreferences";
    SharedPreferences sp_print;
    SharedPreferences.Editor editor_print;
    private Handler handler = new Handler();
    Context context;

    public MyWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);

    }

    @NonNull
    @Override
    public Result doWork() {
        return null;
    }


    @Override
    public void onPtrReceive(Printer printer, int i, PrinterStatusInfo printerStatusInfo, String s) {

    }
}