package com.geerang.gcounter;

public class EnglishCheck {

    public static boolean isEnglish(String text) {
        String IS_ENGLISH_REGEX = "^[ \\w \\d \\s \\. \\& \\+ \\- \\, \\! \\@ \\# \\$ \\% \\^ \\* \\( \\) \\; \\\\ \\/ \\| \\< \\> \\\" \\' \\? \\= \\: \\[ \\] ]*$";

        if (text == null) {
            return false;
        }
        return text.matches(IS_ENGLISH_REGEX);
    }

}
