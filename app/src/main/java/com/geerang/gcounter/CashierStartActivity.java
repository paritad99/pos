package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class CashierStartActivity extends AppCompatActivity {

    Button btnSave;
    EditText input_1000_note, input_50_note, input_500_note, input_100_note, input_20_note, input_10_bath, input_5_bath, input_1_bath, input_2_bath, input_50_stank, input_25_stank;
    TextView Total;
    int x1000 = 0;
    int x500 = 0;
    int x100 = 0;
    int x50 = 0;
    int x20 = 0;

    int b10 = 0;
    int b5 = 0;
    int b2 = 0;
    int b1 = 0;

    double s5 = 0;
    double s25 = 0;

    double total_money = 0.0;



    final String PREF_NAME = "LoginPreferences";
    final String KEY_USERNAME = "Username";
    final String KEY_USERID = "UserId";
    final String KEY_BranchId = "BranchId";
    final String KEY_StoreId = "StoreId";
    final String KEY_Status = "Status";
    final String KEY_REMEMBER = "RememberUsername";
    final String KEY_business_type_id = "business_type_id";
    final String KEY_StoreName = "StoreName";
    final String KEY_BranchName = "BranchName";
    final String KEY_Position = "Position";
    final String KEY_StoreExp = "StoreExp";
    SharedPreferences sp;
    SharedPreferences.Editor editor;

 Profile profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cashier_start);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        profile = new Profile(this);

        sp = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sp.edit();

        input_1000_note = (EditText) findViewById(R.id.input_1000_note);
        InputCal(input_1000_note);
        input_500_note = (EditText) findViewById(R.id.input_500_note);
        InputCal(input_500_note);
        input_100_note = (EditText) findViewById(R.id.input_100_note);
        InputCal(input_100_note);

        input_50_note = (EditText) findViewById(R.id.input_50_note);
        InputCal(input_50_note);

        input_20_note = (EditText) findViewById(R.id.input_20_note);
        InputCal(input_20_note);

        input_10_bath = (EditText) findViewById(R.id.input_10_bath);
        InputCal(input_10_bath);
        input_5_bath = (EditText) findViewById(R.id.input_5_bath);
        InputCal(input_5_bath);
        input_2_bath = (EditText) findViewById(R.id.input_2_bath);
        InputCal(input_2_bath);
        input_1_bath = (EditText) findViewById(R.id.input_1_bath);
        InputCal(input_1_bath);

        input_50_stank = (EditText) findViewById(R.id.input_50_stank);
        InputCal(input_50_stank);

        input_25_stank = (EditText) findViewById(R.id.input_25_stank);
        InputCal(input_25_stank);

        Total = (TextView) findViewById(R.id.txtTotalBaht);

        btnSave = (Button) findViewById(R.id.btnSaveCash);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new CashierAddTask().execute(StringUtil.URL + "gpos/api/CashierStartAdd");

            }
        });

        Button btnExit = (Button) findViewById(R.id.btnExit);
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                stopService(new Intent(CashierStartActivity.this, TimePrintService.class));

                editor = sp.edit();
                editor.putString(KEY_USERNAME, "");
                editor.putString(KEY_USERID, "");
                editor.putString(KEY_StoreId, "");
                editor.putString(KEY_BranchId, "");
                editor.putString(KEY_Status, "");
                editor.putString(KEY_REMEMBER, "");
                editor.putString(KEY_business_type_id, "");
                editor.putString(KEY_StoreName, "");
                editor.putString(KEY_BranchName, "");
                editor.putString(KEY_Position, "");
                editor.commit();

                Intent in = new Intent(CashierStartActivity.this, LoginActivity.class);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(in);
                finish();

            }
        });



    }

    private class CashierAddTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(CashierStartActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลังบันทึก...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("store_id", profile.StoreId)
                    .add("branch_id", profile.BranchId)
                    .add("employee_id", profile.UserId)
                    .add("user_id", profile.UserId)
                    .add("cash_money", total_money + "")
                    .add("cash_round", "1")
                    .add("remain", "เปิดร้าน")

                    .add("input_1000_note", x1000 + "")
                    .add("input_500_note", x500 + "")
                    .add("input_100_note", x100 + "")
                    .add("input_50_note", x50 + "")
                    .add("input_20_note", x20 + "")

                    .add("input_10_bath", b10 + "")
                    .add("input_5_bath", b5 + "")
                    .add("input_2_bath", b2 + "")
                    .add("input_1_bath", b1 + "")
                    .add("input_50_stank", s5 + "")
                    .add("input_25_stank", s25 + "")

                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success

                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(CashierStartActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    Intent in = new Intent(CashierStartActivity.this, StaffCashireActivity.class);
                                    in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                    startActivity(in);
                                    finish();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();



            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private void showData(String jsonString) {
        Toast.makeText(this, jsonString, Toast.LENGTH_LONG).show();
    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    public void InputCal(EditText input) {
        input.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
//                Toast.makeText(getApplicationContext(),"before text change",Toast.LENGTH_LONG).show();
                x1000 = ConvertEdiTextToInt(input_1000_note, 1000);
                x500 = ConvertEdiTextToInt(input_500_note, 500);
                x100 = ConvertEdiTextToInt(input_100_note, 100);
                x20 = ConvertEdiTextToInt(input_20_note, 20);
                x50 = ConvertEdiTextToInt(input_50_note, 50);
                b10 = ConvertEdiTextToInt(input_10_bath, 10);
                b5 = ConvertEdiTextToInt(input_5_bath, 5);
                b2 = ConvertEdiTextToInt(input_2_bath, 2);
                b1 = ConvertEdiTextToInt(input_1_bath, 1);
                s5 = ConvertEdiTextToFloat(input_50_stank, 0.5);
                s25 = ConvertEdiTextToFloat(input_25_stank, 0.25);
                total_money = (x1000 + x500 + x50 + x100 + x20 + b10 + b5 + b2 + b1 + s5 + s25);
                Total.setText("จำนวนเงินรวม " + total_money + " บาท");
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                x1000 = ConvertEdiTextToInt(input_1000_note, 1000);
                x500 = ConvertEdiTextToInt(input_500_note, 500);
                x100 = ConvertEdiTextToInt(input_100_note, 100);
                x20 = ConvertEdiTextToInt(input_20_note, 20);
                x50 = ConvertEdiTextToInt(input_50_note, 50);
                b10 = ConvertEdiTextToInt(input_10_bath, 10);
                b5 = ConvertEdiTextToInt(input_5_bath, 5);
                b2 = ConvertEdiTextToInt(input_2_bath, 2);
                b1 = ConvertEdiTextToInt(input_1_bath, 1);
                s5 = ConvertEdiTextToFloat(input_50_stank, 0.5);
                s25 = ConvertEdiTextToFloat(input_25_stank, 0.25);
                total_money = (x1000 + x500 + x50+x100 + x20 + b10 + b5 + b2 + b1 + s5 + s25);
                Total.setText("จำนวนเงินรวม " + total_money + " บาท");
//                Toast.makeText(getApplicationContext(),"after text change",Toast.LENGTH_LONG).show();
            }
        });

    }

    public int ConvertEdiTextToInt(EditText input, int val) {
        int sum = 0;
        try {
            sum = Integer.parseInt(input.getText().toString().trim()) * val;
        } catch (Exception error) {
            sum = 0;
        }
        return sum;
    }

    public double ConvertEdiTextToFloat(EditText input, Double val) {
        double sum = 0.0;
        try {
            sum = Integer.parseInt(input.getText().toString().trim()) * val;
        } catch (Exception error) {
            sum = 0.0;
        }
        return sum;
    }

}
