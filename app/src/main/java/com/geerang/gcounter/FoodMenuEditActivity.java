package com.geerang.gcounter;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.PictureDrawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FoodMenuEditActivity extends AppCompatActivity {

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    Bitmap FixBitmap;
    byte[] byteArray;
    String ConvertImage = "";
    URL url;
    private int GALLERY = 1, CAMERA = 2;
    Spinner ddlMenuCategory, ddlKitchen, ddlReport;

    Button buttonSelect, btnGotoAddChoice, btnOldChoice;
    ImageButton btnSaveMenu, btnCancel;

    ImageView imgStore;
    CheckBox chk_is_recommend;
    EditText input_menu, input_desc, input_price, input_calories, input_time_task;

    ArrayList<HashMap<String, String>> MyArrList;
    HashMap<String, String> map;

    ArrayList<HashMap<String, String>> MyArrListMenuCategory;
    HashMap<String, String> mapMenuCategory;

    ArrayList<HashMap<String, String>> MyArrListKitchen;
    HashMap<String, String> mapKitchen;

    ArrayList<HashMap<String, String>> MyArrListddlReport;
    HashMap<String, String> mapddlReport;

    ArrayList<HashMap<String, String>> MyArrListChoice;
    HashMap<String, String> map_choice;

    String categories_id, kitchen_id, report_id;

    LinearLayout llChoice;
    private List<FeedItemChoice> feedsList;
    private ChoiceRecyclerAdapter mAdapter;

    RecyclerView recyclerview_choice;
    static String choice_id, group_choice_id;
    EditText input_choice_name, input_choice_order;

    EditText input_group_choice, input_choice_desc, input_choice_min, input_choice_max;
    ListView lisView1;

    AlertDialog dialogChoiceAdd;
    String input_product_id = "";
    String p_group_id = "";

    Profile profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_add);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        profile = new Profile(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
/*
ข้อมูลทีส่งมา
 in.putExtra("product_name", feedItem.getMenu_name());
                    in.putExtra("product_detail", feedItem.getMenu_product_detail());
                    in.putExtra("product_sale_price", feedItem.getMenu_product_price());
                    in.putExtra("product_image", feedItem.getProduct_image());
                    in.putExtra("calories", feedItem.getMenu_calories());
                    in.putExtra("categories_id", feedItem.getMenu_categories_id());
                    in.putExtra("cookingtime", feedItem.getMenu_cookingtime());
                    in.putExtra("product_id", feedItem.getId());
 */
        Intent intent = getIntent();
        input_product_id = intent.getStringExtra("product_id");
        Log.d("product_id", input_product_id);

        categories_id = intent.getStringExtra("categories_id");
        kitchen_id = intent.getStringExtra("id_kit");
        report_id = intent.getStringExtra("report_cat_id");

        input_menu = (EditText) findViewById(R.id.input_menu);
        input_menu.setText(intent.getStringExtra("product_name"));
        input_desc = (EditText) findViewById(R.id.input_desc);
        input_desc.setText(intent.getStringExtra("product_detail"));
        input_price = (EditText) findViewById(R.id.input_price);
        input_price.setText(intent.getStringExtra("product_sale_price"));
        input_calories = (EditText) findViewById(R.id.input_calories);
        input_calories.setText(intent.getStringExtra("calories"));
        input_time_task = (EditText) findViewById(R.id.input_time_task);
        input_time_task.setText(intent.getStringExtra("cookingtime"));
        imgStore = (ImageView) findViewById(R.id.imgStore);

        StringUtil stringUtil = new StringUtil();

        if (!intent.getStringExtra("product_image").equals("")) {
            Picasso.with(FoodMenuEditActivity.this)
                    .load(StringUtil.URL + "gpos/" + stringUtil.removeFirstChar(intent.getStringExtra("product_image")))
                    .fit()
                    .placeholder(R.drawable.noimageavailable)
                    .error(R.drawable.noimageavailable)
                    .into(imgStore);
        }

        chk_is_recommend = (CheckBox) findViewById(R.id.chk_is_recommend);
        ddlMenuCategory = (Spinner) findViewById(R.id.ddlMenuCategory);
        ddlKitchen = (Spinner) findViewById(R.id.ddlKitchen);
        ddlReport = (Spinner) findViewById(R.id.ddlReport);

        btnGotoAddChoice = (Button) findViewById(R.id.btnGotoAddChoice);
        btnGotoAddChoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                StringUtil.GroupName = "";
                StringUtil.GroupMin = "";
                StringUtil.GroupMax = "";
                StringUtil.p_group_id = "";
                StringUtil.StateChoice = "ADD";

                Intent in = new Intent(FoodMenuEditActivity.this, FoodMenuChoiceAddActivity.class);
                in.putExtra("input_product_id", input_product_id);
                startActivity(in);
            }
        });

        buttonSelect = (Button) findViewById(R.id.buttonSelect);

        input_menu.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                new AsyncCheckMenuName().execute(StringUtil.URL + "gpos/api/CheckMenuAdd");

            }
        });


        buttonSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPictureDialog();
            }
        });

        btnSaveMenu = (ImageButton) findViewById(R.id.btnSaveMenu);
        btnSaveMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new BookingAddTask().execute(StringUtil.URL + "gpos/api/FoodMenuAdd");

            }
        });


        btnCancel = (ImageButton) findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

//                input_product_id

            }
        });


        input_choice_name = (EditText) findViewById(R.id.input_choice_name);
        input_choice_order = (EditText) findViewById(R.id.input_choice_order);


        recyclerview_choice = (RecyclerView) findViewById(R.id.recyclerview_choice);


        if (isNetworkConnected() == true) {
            new AsyncLoadKitchenCat().execute();
            new AsyncLoadReportCat().execute();
            new AsyncLoadCat().execute();
            new AsyncLoadChoice().execute();
        }

    }

    // check network connected
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isNetworkConnected() == true) {
            new AsyncLoadChoice().execute();
        }

    }

    private class AsyncCheckMenuName extends AsyncTask<String, Void, String> {

//        ProgressDialog pdLoading = new ProgressDialog(FoodMenuAddActivity.this);

        @Override
        protected void onPreExecute() {
//             Create Show ProgressBar
//            pdLoading.setMessage("\tกำลังตรวจสอบข้อมูลซ้ำ...");
//            pdLoading.setCancelable(false);
//            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("MenuName", input_menu.getText().toString() + "")
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
//            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(FoodMenuEditActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
//                    final String product_id = object.getString("product_id"); // success
                    // นำข้อมูลไปใส่เพื่อเตรียม input
//                    input_product_id = product_id;

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class AsyncCheckGroupChoiceName extends AsyncTask<String, Void, String> {

//        ProgressDialog pdLoading = new ProgressDialog(FoodMenuAddActivity.this);

        @Override
        protected void onPreExecute() {
//             Create Show ProgressBar
//            pdLoading.setMessage("\tกำลังตรวจสอบข้อมูลซ้ำ...");
//            pdLoading.setCancelable(false);
//            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("GroupChoiceName ", urls[1] + "")
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
//            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);
                String status = object.getString("status"); // success
                p_group_id = object.getString("p_group_id"); // success

                if (jsonString.length() > 0) {
                    btnOldChoice.setVisibility(View.VISIBLE);
                    btnOldChoice.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                        }
                    });

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public void CreateChoice() {

        Toast.makeText(FoodMenuEditActivity.this, "Update list", Toast.LENGTH_LONG).show();
        lisView1.setAdapter(new ChoiceAdapter(FoodMenuEditActivity.this));
    }

    public class ChoiceAdapter extends BaseAdapter {
        private Context context;

        public ChoiceAdapter(Context c) {
            //super( c, R.layout.activity_column, R.id.rowTextView, );
            // TODO Auto-generated method stub
            context = c;
        }

        public int getCount() {
            // TODO Auto-generated method stub
            return MyArrListChoice.size();
        }

        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.foodmenu_choice_row_add, null);

            }

            // ColID
            EditText txtID = (EditText) convertView.findViewById(R.id.input_choice);
            txtID.setText(MyArrListChoice.get(position).get("ID") + ".");

            // ColCode
            EditText txtCode = (EditText) convertView.findViewById(R.id.input_price);
            txtCode.setText(MyArrListChoice.get(position).get("Code"));

            ImageButton btnDelete_row = (ImageButton) convertView.findViewById(R.id.btnDelete_row);
            btnDelete_row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MyArrListChoice.remove(position);
                    CreateChoice();

                }
            });

            return convertView;

        }

    }

    private class GroupChoiceAddTask extends AsyncTask<String, Void, String> {

//        ProgressDialog pdLoading = new ProgressDialog(FoodMenuAddActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
//            pdLoading.setMessage("\tSaving...");
//            pdLoading.setCancelable(false);
//            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            // เพิ่ม GroupChoice
            String group_choice = input_group_choice.getText().toString();
            String choice_min = input_choice_min.getText().toString();
            String choice_max = input_choice_max.getText().toString();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("UserId", profile.UserId)
                    .add("StoreId", profile.StoreId)
                    .add("name", group_choice)
                    .add("min", choice_min)
                    .add("max", choice_max)
                    .add("product_id", input_product_id)
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
//            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(FoodMenuEditActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    group_choice_id = msg;

                    // บันทึก Choice ต่อหลังจากที่บันทึก GroupChoice เสด
                    int count = lisView1.getAdapter().getCount();
                    for (int i = 0; i < count; i++) {

                        LinearLayout itemLayout = (LinearLayout) lisView1.getChildAt(i); // Find by under LinearLayout
                        EditText input_choice = (EditText) itemLayout.findViewById(R.id.input_choice);
                        EditText input_price = (EditText) itemLayout.findViewById(R.id.input_price);
                        String Choice = input_choice.getText().toString();
                        String Price = input_price.getText().toString();

                        // เพิ่ม Choice ทีละรายการ
                        new ChoiceAddTask().execute(StringUtil.URL + "gpos/api/ChoiceAdd", Choice, Price, group_choice_id);

                        Log.d(Choice, Price);

                    }

                    AlertDialog.Builder builder = new AlertDialog.Builder(FoodMenuEditActivity.this);
                    builder.setMessage("บันทึกเรียบร้อยแล้ว")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
//                                    finish();

                                    new AsyncLoadChoice().execute();

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class ChoiceAddTask extends AsyncTask<String, Void, String> {

//        ProgressDialog pdLoading = new ProgressDialog(FoodMenuAddActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
//            pdLoading.setMessage("\tกำลังบันทึก...");
//            pdLoading.setCancelable(false);
//            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("UserId", profile.UserId + "")
                    .add("choice_name", urls[1])
                    .add("choice_price", urls[2])
                    .add("p_group_id", urls[3])
                    .add("StoreId", profile.StoreId)
                    .add("BranchId", profile.BranchId)

                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
//            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(FoodMenuEditActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
//                                    finish();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(FoodMenuEditActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    //                                    finish();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


                new AsyncLoadChoice().execute();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class ChoiceDeleteTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(FoodMenuEditActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลังลบ...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();


            RequestBody formBody = new FormEncodingBuilder()
                    .add("UserId", profile.UserId + "")
                    .add("p_group_id", urls[1])
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(FoodMenuEditActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    new AsyncLoadChoice().execute();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(FoodMenuEditActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
//                                    finish();
                                    new AsyncLoadChoice().execute();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private void showData(String jsonString) {
        Toast.makeText(FoodMenuEditActivity.this, jsonString, Toast.LENGTH_LONG).show();
    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    private class AsyncLoadChoice extends AsyncTask<String, String, String> {
        //        ProgressDialog pdLoading = new ProgressDialog(FoodMenuAddActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //this method will be running on UI thread
//            pdLoading.setMessage("\tLoading...");
//            pdLoading.setCancelable(false);
//            pdLoading.show();
        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/GroupChoiceList/" + input_product_id);
//                response = http.run(StringUtil.URL+"gpos/api/category/1");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            //this method will be running on UI thread
//            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();
            feedsList = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemChoice item = new FeedItemChoice();
                    item.setEmployee_id(json_data.getString("p_group_id"));
                    item.setFirstname(json_data.getString("group_name"));
                    item.setLastname(json_data.getString("group_name"));
                    item.setCmin(json_data.getString("min"));
                    item.setCmax(json_data.getString("max"));

                    feedsList.add(item);
                }

                // Setup and Handover data to recyclerview
                mAdapter = new ChoiceRecyclerAdapter(FoodMenuEditActivity.this, feedsList);
                recyclerview_choice.setAdapter(mAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(FoodMenuEditActivity.this, 1);
                recyclerview_choice.setLayoutManager(mLayoutManager);
                recyclerview_choice.setItemAnimator(new DefaultItemAnimator());
                recyclerview_choice.setItemAnimator(new DefaultItemAnimator());

            } catch (JSONException e) {
                Toast.makeText(FoodMenuEditActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    public class ChoiceRecyclerAdapter extends RecyclerView.Adapter<ChoiceRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemChoice> feedItemList;
        private Context mContext;

        public ChoiceRecyclerAdapter(Context context, List<FeedItemChoice> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.choice_list_row, null);

            CustomViewHolder viewHolder = new CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
            final FeedItemChoice feedItem = feedItemList.get(i);
            customViewHolder.feedItem = feedItem;


            //Setting text view title
            customViewHolder.title_choice.setText(Html.fromHtml("" + feedItem.getFirstname()));
            customViewHolder.txtPrice.setVisibility(View.GONE);

            customViewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent in = new Intent(mContext, MenuChoiceEditActivity.class);

                    in.putExtra("input_product_id", input_product_id);
                    in.putExtra("p_group_id", feedItem.getEmployee_id());
                    in.putExtra("title_choice", feedItem.getFirstname());
                    in.putExtra("title_choice_order", feedItem.getLastname());
                    in.putExtra("cmin", feedItem.getCmin());
                    in.putExtra("cmax", feedItem.getCmax());
                    startActivity(in);

                }
            });


            customViewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    final AlertDialog.Builder adb = new AlertDialog.Builder(FoodMenuEditActivity.this);

                    adb.setTitle("ยืนยันการลบข้อมูล?");
                    adb.setMessage("ยืนยันการลบข้อมูล");
                    adb.setNegativeButton("Cancel", null);
                    adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
                        public void onClick(DialogInterface dialog, int arg1) {
                            // TODO Auto-generated method stub

                            choice_id = feedItem.getEmployee_id();
                            new ChoiceDeleteTask().execute(StringUtil.URL + "gpos/api/MenuChoiceDelete", feedItem.getEmployee_id());


                        }
                    });
                    adb.show();

                }
            });


        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            //            protected ImageView imageView;
            protected TextView title_choice, txtPrice;
            protected Button btnEdit, btnDelete;

            FeedItemChoice feedItem;

            public CustomViewHolder(View view) {
                super(view);

                this.title_choice = (TextView) view.findViewById(R.id.txtCheckIn);
                this.txtPrice = (TextView) view.findViewById(R.id.txtPrice);

                this.btnEdit = (Button) view.findViewById(R.id.btnEdit);
                this.btnDelete = (Button) view.findViewById(R.id.btnDelete);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

//                        StringUtil.CatId = feedItem.getId();
                        //new AsyncProduct().execute();
                        //Toast.makeText(v.getContext(), "Select is: " + feedItem.getId(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }
    }

    // String
    public class FeedItemChoice {

        private String employee_id;
        private String firstname;
        private String lastname;
        private String tel;
        private String status_name;
        private String cmin;
        private String cmax;

        public String getCmax() {
            return cmax;
        }

        public String getCmin() {
            return cmin;
        }

        public void setCmax(String cmax) {
            this.cmax = cmax;
        }

        public void setCmin(String cmin) {
            this.cmin = cmin;
        }

        public String getEmployee_id() {
            return employee_id;
        }

        public void setEmployee_id(String employee_id) {
            this.employee_id = employee_id;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getStatus_name() {
            return status_name;
        }

        public void setStatus_name(String status_name) {
            this.status_name = status_name;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }
    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    private class AsyncLoadKitchenCat extends AsyncTask<String, String, String> {
        //        ProgressDialog pdLoading = new ProgressDialog(FoodMenuAddActivity.this);
        HttpURLConnection conn;
        URL url = null;

        int id = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
//            pdLoading.setMessage("\tกำลังโหลดข้อมูล...");
//            pdLoading.setCancelable(false);
//            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/KitchenCategories/" + profile.StoreId);
//                response = http.run(StringUtil.URL+"gpos/api/category/1");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

//            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);
                MyArrListKitchen = new ArrayList<HashMap<String, String>>();
                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    mapKitchen = new HashMap<String, String>();
                    mapKitchen.put("MemberID", json_data.getString("id_kit"));
                    mapKitchen.put("Name", json_data.getString("kit_cat"));
                    mapKitchen.put("Tel", json_data.getString("id_printer"));
                    MyArrListKitchen.add(mapKitchen);

                    String kid = json_data.getString("id_kit");

                    if (kid.equals(kitchen_id)) {
                        id = i;
                    }

                }


                SimpleAdapter sAdap;
                sAdap = new SimpleAdapter(FoodMenuEditActivity.this, MyArrListKitchen, R.layout.activity_menu_ddl_row,
                        new String[]{"Name"}, new int[]{R.id.ColName});

//                final AlertDialog.Builder viewDetail = new AlertDialog.Builder(FoodMenuAddActivity.this);
                ddlKitchen.setAdapter(sAdap);


                ddlKitchen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    public void onItemSelected(AdapterView<?> arg0, View selectedItemView,
                                               int position, long id) {
                        try {
                            kitchen_id = MyArrListKitchen.get(position).get("MemberID")
                                    .toString();

                            String sName = MyArrListKitchen.get(position).get("Name")
                                    .toString();
                            String sTel = MyArrListKitchen.get(position).get("Tel")
                                    .toString();
                        } catch (Exception e) {

                        }

                    }

                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub
                        Toast.makeText(FoodMenuEditActivity.this,
                                "Your Selected : Nothing",
                                Toast.LENGTH_SHORT).show();
                    }
                });

                // set default
                ddlKitchen.setSelection(id);

            } catch (JSONException e) {
                Toast.makeText(FoodMenuEditActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    private class AsyncLoadReportCat extends AsyncTask<String, String, String> {
        //        ProgressDialog pdLoading = new ProgressDialog(FoodMenuAddActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
//            pdLoading.setMessage("\tกำลังโหลดข้อมูล...");
//            pdLoading.setCancelable(false);
//            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/ReportCategory/" + profile.StoreId);
//                response = http.run(StringUtil.URL+"gpos/api/category/1");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

//            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();
            int id = 0;
            try {

                JSONArray jArray = new JSONArray(result);
                MyArrListddlReport = new ArrayList<HashMap<String, String>>();
                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    mapddlReport = new HashMap<String, String>();
                    mapddlReport.put("MemberID", json_data.getString("report_cat_id"));
                    mapddlReport.put("Name", json_data.getString("report_cat_name"));
                    mapddlReport.put("Tel", json_data.getString("report_sequence"));
                    MyArrListddlReport.add(mapddlReport);
                    String report_cat_id = json_data.getString("report_cat_id");
                    if (report_cat_id.equals(report_id)) {
                        id = i;
                    }

                }


                SimpleAdapter sAdap;
                sAdap = new SimpleAdapter(FoodMenuEditActivity.this, MyArrListddlReport, R.layout.activity_menu_ddl_row,
                        new String[]{"Name"}, new int[]{R.id.ColName});

//                final AlertDialog.Builder viewDetail = new AlertDialog.Builder(FoodMenuAddActivity.this);
                ddlReport.setAdapter(sAdap);


                ddlReport.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    public void onItemSelected(AdapterView<?> arg0, View selectedItemView,
                                               int position, long id) {


                        report_id = MyArrListddlReport.get(position).get("MemberID")
                                .toString();
                        String sName = MyArrListddlReport.get(position).get("Name")
                                .toString();
                        String sTel = MyArrListddlReport.get(position).get("Tel")
                                .toString();


                    }

                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub
                        Toast.makeText(FoodMenuEditActivity.this,
                                "Your Selected : Nothing",
                                Toast.LENGTH_SHORT).show();
                    }


                });

                ddlReport.setSelection(id);

            } catch (JSONException e) {
                Toast.makeText(FoodMenuEditActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    private class AsyncLoadCat extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(FoodMenuEditActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tกำลังโหลดข้อมูล...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/MenuCategory/" + profile.StoreId);
//                response = http.run(StringUtil.URL+"gpos/api/category/1");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            int defddl = 0;
            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);
                MyArrListMenuCategory = new ArrayList<HashMap<String, String>>();
                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    mapMenuCategory = new HashMap<String, String>();
                    mapMenuCategory.put("MemberID", json_data.getString("categories_id"));
                    mapMenuCategory.put("Name", json_data.getString("categories_name"));
                    mapMenuCategory.put("Tel", json_data.getString("categories_color"));
                    MyArrListMenuCategory.add(mapMenuCategory);

                    String catname = json_data.getString("categories_id");
                    if (catname.equals(categories_id)) {
                        defddl = i;
                    }

                }


                SimpleAdapter sAdap;
                sAdap = new SimpleAdapter(FoodMenuEditActivity.this, MyArrListMenuCategory, R.layout.activity_menu_ddl_row,
                        new String[]{"Name"}, new int[]{R.id.ColName});

//                final AlertDialog.Builder viewDetail = new AlertDialog.Builder(FoodMenuAddActivity.this);
                ddlMenuCategory.setAdapter(sAdap);


                ddlMenuCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    public void onItemSelected(AdapterView<?> arg0, View selectedItemView,
                                               int position, long id) {


                        categories_id = MyArrListMenuCategory.get(position).get("MemberID")
                                .toString();
                        String sName = MyArrListMenuCategory.get(position).get("Name")
                                .toString();
                        String sTel = MyArrListMenuCategory.get(position).get("Tel")
                                .toString();


                    }

                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub
                        Toast.makeText(FoodMenuEditActivity.this,
                                "Your Selected : Nothing",
                                Toast.LENGTH_SHORT).show();

                        //ddlMenuCategory.setd("Select Country");
                    }


                });

                ddlMenuCategory.setSelection(defddl);

            } catch (JSONException e) {
                Toast.makeText(FoodMenuEditActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }


    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Photo Gallery",
                "Camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    FixBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    // String path = saveImage(bitmap);
                    //Toast.makeText(MainActivity.this, "Image Saved!", Toast.LENGTH_SHORT).show();
                    imgStore.setImageBitmap(FixBitmap);
//                    UploadImageOnServerButton.setVisibility(View.VISIBLE);

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(FoodMenuEditActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {
            FixBitmap = (Bitmap) data.getExtras().get("data");
            imgStore.setImageBitmap(FixBitmap);
//            UploadImageOnServerButton.setVisibility(View.VISIBLE);
            //  saveImage(thumbnail);
            //Toast.makeText(ShadiRegistrationPart5.this, "Image Saved!", Toast.LENGTH_SHORT).show();
        }
    }

    private class BookingAddTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(FoodMenuEditActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลังบันทึกข้อมูล...");
            pdLoading.setCancelable(false);
            pdLoading.show();

            //Toast.makeText(FoodMenuEditActivity.this,"กำลังบันทึกข้อมูล",Toast.LENGTH_SHORT).show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            try {
//            FixBitmap.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
//
////            ByteArrayOutputStream stream = new ByteArrayOutputStream();
////            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
//            byteArray = byteArrayOutputStream.toByteArray();

//            Bitmap bmp = intent.getExtras().get("data");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                FixBitmap.compress(Bitmap.CompressFormat.PNG, 40, stream);
                byteArray = stream.toByteArray();
                FixBitmap.recycle();
                ConvertImage = Base64.encodeToString(byteArray, Base64.DEFAULT);

            } catch (Exception e) {

                ConvertImage = "";
            }

            String is_recommend = "";
            if (!chk_is_recommend.isChecked()) {
                is_recommend = "0";
            } else {
                is_recommend = "1";
            }

            RequestBody formBody = new FormEncodingBuilder()
                    .add("image_data", ConvertImage)
                    .add("store_id", profile.StoreId)
                    .add("categories_id", categories_id)
                    .add("report_cat_id", report_id)
                    .add("id_kit", kitchen_id)
                    .add("product_id", input_product_id)
                    .add("product_name", input_menu.getText().toString())
                    .add("product_detail", input_desc.getText().toString())
                    .add("product_price", input_price.getText().toString())
                    .add("cookingtime", input_time_task.getText().toString())
                    .add("calories", input_calories.getText().toString())
                    .add("is_recommend", is_recommend)
                    //.add("group_choice_id", group_choice_id)
                    .build();

            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                String msg = object.getString("msg"); // success

                //StringUtil.product_menu_id = msg;
                //StringUtil.menu_store_id = msg;


                Toast.makeText(FoodMenuEditActivity.this, "บันทึกข้อมูลเรียบร้อยแล้ว" + msg, Toast.LENGTH_SHORT).show();
//                Intent in = new Intent(FoodMenuAddActivity.this,FoodOptionActivity.class);
////                in.putExtra("menu_id",msg);
//                startActivity(in);

                finish();


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 5) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Now user should be able to use camera

            } else {

                Toast.makeText(FoodMenuEditActivity.this, "Unable to use Camera..Please Allow us to use Camera", Toast.LENGTH_LONG).show();

            }
        }
    }
}
