package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.epson.epos2.Epos2Exception;
import com.epson.epos2.printer.Printer;
import com.epson.epos2.printer.PrinterStatusInfo;
import com.epson.epos2.printer.ReceiveListener;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.os.IBinder;
import android.os.StrictMode;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import recieptservice.com.recieptservice.PrinterInterface;

public class CashireReceiptActivity extends AppCompatActivity implements ReceiveListener {

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {

            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    int check_receipt = 0, payment_type = 1;
    Button btnInvoice, btnShortInvoice, btnNoInvoice, btnSubmit;

    ImageButton btnVisa, btnMastercard;
    CheckBox chkBill, chkCreaditCard;

    int check_vis = 0;
    String bill = "";

    LinearLayout llCreaditCard, llCash, llBankTransfer, llQRcode;

    TextView txt_total, txt_discount_result, txt_service_charge, txtTotalReceipt, txt_balance, txtVatTotal;

    EditText input_receipt_total, input_ref_code;

    String payment_change = "";

    Button btnCash, btnCreadit, btnBank, btnQRcode;

    EditText input_bank_name;
    EditText input_bank_tras_date;
    EditText input_bank_trans_time;
    EditText input_bank_ref_nbr;
    EditText input_transfer_amount;
    EditText input_qrcode_amount;
    EditText input_qrcode_ref;

    String Resultbill;
    double total_price = 0;
    double total_qty = 0;
    double check_order_place = 0;

    String vat_str = "";
    Profile profile;

    private Context mContext = null;
    public static EditText mEditTarget = null;
    public static Spinner mSpnSeries = null;
    public static Spinner mSpnLang = null;
    public static Printer mPrinter = null;
    public static ToggleButton mDrawer = null;

    EpsonMakeErrorMessage epsonMakeErrorMessage;
    final String KEY_Autoprint = "Autoprint";
    final String KEY_PrinterMain = "PrinterMain";
    final String KEY_Printer1 = "Printer1";
    final String KEY_Printer2 = "Printer2";
    final String KEY_Printer3 = "Printer3";
    final String KEY_Printer4 = "Printer4";

    String PrinterAddress = "";

    SharedPreferences sp;
    SharedPreferences.Editor editor;
    final String PREF_NAME = "PrinterPreferences";
    final String KEY_Vat = "Vat";
    double vat_data = 0.0;
    double vat_total = 0.0;

    double total_payment = 0.0;
    double total_discount = 0.0;

    double total_sum = 0.0;

    double total_service_charge;
    DecimalFormat formatter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cashire_receipt);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        epsonMakeErrorMessage = new EpsonMakeErrorMessage(this);
        sp = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sp.edit();

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        profile = new Profile(this);
        input_receipt_total = (EditText) findViewById(R.id.input_receipt_total);
        RadioGroup sex = (RadioGroup) this.findViewById(R.id.rddiscount);
        btnInvoice = (Button) findViewById(R.id.btnInvoice);
        btnInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check_receipt = 0;
                EnableButton();
            }
        });

        btnShortInvoice = (Button) findViewById(R.id.btnShortInvoice);
        btnShortInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check_receipt = 1;
                EnableButton();
            }
        });
        btnNoInvoice = (Button) findViewById(R.id.btnNoInvoice);
        btnNoInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check_receipt = 2;
                EnableButton();
            }
        });

        btnVisa = (ImageButton) findViewById(R.id.btnVisa);
        btnVisa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check_vis = 0;
                PaymentCard();
            }
        });

        btnMastercard = (ImageButton) findViewById(R.id.btnMastercard);
        btnMastercard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check_vis = 1;
                PaymentCard();
            }
        });


        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (payment_type == 1) {
                    String mon = input_receipt_total.getText().toString();
                    if (mon.length() > 0) {
                        int total = Integer.parseInt(mon);
                        if (total < total_sum) {
                            input_receipt_total.setError("กรุณาระบุจำนวนเงิน");

                        } else {
                            new AsyncChoiceProductPrint().execute();
                        }


                    }

                } else {

                    new AsyncChoiceProductPrint().execute();
                }

            }
        });

        llCreaditCard = (LinearLayout) findViewById(R.id.llCreaditCard);
        llCash = (LinearLayout) findViewById(R.id.llCash);
        llBankTransfer = (LinearLayout) findViewById(R.id.llBankTransfer);
        llQRcode = (LinearLayout) findViewById(R.id.llQRcode);
        chkBill = (CheckBox) findViewById(R.id.chkBill);

        // บิลเงินสด
        chkBill.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (chkBill.isChecked()) {
                    bill = "yes";
                } else {
                    bill = "no";
                }
            }
        });

        btnCash = (Button) findViewById(R.id.btnCash);
        btnCash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                payment_type = 1;
                PaymentMethod();
            }
        });

        btnCreadit = (Button) findViewById(R.id.btnCreadit);
        btnCreadit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                payment_type = 2;
                PaymentMethod();
            }
        });

        btnBank = (Button) findViewById(R.id.btnBank);
        btnBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                payment_type = 3;
                PaymentMethod();
            }
        });

        btnQRcode = (Button) findViewById(R.id.btnQRcode);
        btnQRcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                payment_type = 4;
                PaymentMethod();
            }
        });

        txt_total = (TextView) findViewById(R.id.txt_total);
        txt_total.setText(StringUtil.TotalPayment + " บาท");
        txt_discount_result = (TextView) findViewById(R.id.txt_discount_result);
        txt_service_charge = (TextView) findViewById(R.id.txt_service_charge);
        txt_service_charge.setText(StringUtil.custom_service + "");

        txtVatTotal = (TextView) findViewById(R.id.txtVatTotal);
        txtTotalReceipt = (TextView) findViewById(R.id.txtTotalReceipt);

        if (StringUtil.discount_total_payment.equals("")) {
            StringUtil.discount_total_payment = StringUtil.TotalPayment;
        } else {
            StringUtil.discount_total_payment = "" + (Double.parseDouble(StringUtil.TotalPayment) - Double.parseDouble(StringUtil.discount_total_payment));
        }

        total_payment = Double.parseDouble(StringUtil.TotalPayment);

        // เช็คส่วนลดแยกตามประเภทอาหาร
        //แก้ไขเมื่อ 02/07/2563
        editor = sp.edit();
        String vat = sp.getString(KEY_Vat, "");
        float res_vat_total;
        if (vat.equals("")) {
            res_vat_total = 7.0f;
            vat_data = res_vat_total;
            txtVatTotal.setText("Vat (" + vat_data + "%)");
        } else {
            res_vat_total = Float.parseFloat(vat);
            vat_data = Float.parseFloat(vat);
            txtVatTotal.setText("Vat (" + vat_data + "%)");
        }

        if (StringUtil.vat == true) {
            vat_total = (total_payment * 7) / 100;
        }

        //total_payment = total_payment + vat_total;

        int s = Integer.parseInt(StringUtil.custom_service);
        if (s > 0) {
            total_service_charge = (total_payment * Integer.parseInt(StringUtil.custom_service)) / 100;
        }

        total_payment = total_payment + vat_total + total_service_charge;

        //total_sum = total_payment + vat_total + total_service_charge;
        //int d = Integer.parseInt(StringUtil.dicount_total);
        double temp = Double.parseDouble(StringUtil.TotalPayment);
        //if (StringUtil.dicount_type.equals("แยกตามรายการ")) {
        if (StringUtil.dicount_type.equals("เปอร์เซ็น")) {
            txt_discount_result.setText(StringUtil.report_cat_name + " " + StringUtil.dicount_total + " % ");
            total_discount = (temp * StringUtil.dicount_total) / 100;
        } else {
            txt_discount_result.setText(StringUtil.report_cat_name + " " + StringUtil.dicount_total + " บาท");
            total_discount = StringUtil.dicount_total;
        }

        total_payment = total_payment - total_discount;

        formatter = new DecimalFormat("#,###.00");
        //int IntValue = (int) Math.round(total_payment);
        txtTotalReceipt.setText(formatter.format(total_payment));

        txt_balance = (TextView) findViewById(R.id.txt_balance);
        input_receipt_total.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                CalCash();
            }
        });

        input_ref_code = (EditText) findViewById(R.id.input_ref_code);

        input_bank_name = (EditText) findViewById(R.id.input_bank_name);
        input_bank_tras_date = (EditText) findViewById(R.id.input_bank_tras_date);
        ImageButton btnDate = (ImageButton) findViewById(R.id.btnDate);
        btnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment();
                cdp.show(CashireReceiptActivity.this.getSupportFragmentManager(), "Calendar");
                cdp.setOnDateSetListener(new CalendarDatePickerDialogFragment.OnDateSetListener() {
                    @Override
                    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
                        try {
                            SimpleDateFormat date_formatter = new SimpleDateFormat("dd/MM/yyyy");
                            String dateInString = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                            Date date = date_formatter.parse(dateInString);
                            date_formatter = new SimpleDateFormat("yyyy-MM-dd");
                            input_bank_tras_date.setText(date_formatter.format(date).toString());
                        } catch (Exception ex) {
                            //input_date.setText(ex.getMessage().toString());
                        }

                    }
                });

            }
        });

        input_bank_trans_time = (EditText) findViewById(R.id.input_bank_trans_time);
        input_bank_ref_nbr = (EditText) findViewById(R.id.input_bank_ref_nbr);
        input_transfer_amount = (EditText) findViewById(R.id.input_transfer_amount);
        input_qrcode_amount = (EditText) findViewById(R.id.input_qrcode_amount);
        input_qrcode_ref = (EditText) findViewById(R.id.input_qrcode_ref);

        EnableButton();
        PaymentMethod();

    }

    @Override
    public void onPtrReceive(final Printer printerObj, final int code, final PrinterStatusInfo status, final String printJobId) {
        runOnUiThread(new Runnable() {
            @Override
            public synchronized void run() {
                // ShowMsg.showResult(code, epsonMakeErrorMessage.makeErrorMessage(status), CashireReceiptActivity.this);
                // epsonMakeErrorMessage.dispPrinterWarnings(status);
                disconnectPrinter();
            }
        });
    }

    private void disconnectPrinter() {
        if (mPrinter == null) {
            return;
        }

        try {
            mPrinter.endTransaction();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                    //  ShowMsg.showException(e, "endTransaction", CashireReceiptActivity.this);
                }
            });
        }

        try {
            mPrinter.disconnect();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                    //   ShowMsg.showException(e, "disconnect", CashireReceiptActivity.this);
                }
            });
        }

        finalizeObject();
    }


    private class AsyncChoiceProductPrint extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(CashireReceiptActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //this method will be running on UI thread

            pdLoading.setMessage("\tกำลังโหลดข้อมูล...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run("http://www.geerang.com/gpos/api/ReportInvoice/" + profile.StoreId + "/" + StringUtil.OrderId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {
            Resultbill = result;
            pdLoading.dismiss();

            editor = sp.edit();
            String PrinterMain = sp.getString(KEY_PrinterMain, "");
            if (!PrinterMain.equals("")) {
                //OpenPrinter+","+printer_title+","+printer_address+","+printer_mac_address+","+categories_id
                String[] namesList = PrinterMain.split(",");
                String status = namesList[0];
                String title = namesList[1];
                String address = namesList[2];
                String mac_address = namesList[3];
                String categories_id = namesList[4];

                PrinterAddress = address;
                //เลือกเครื่องปริ้นที่กำหนด
                if (!runPrintReceiptSequence()) {

                }


            } else {

            }

            // update ข้อมมูลในระบบ
            new PaymentAddTask().execute(StringUtil.URL + "gpos/api/CashireReceipt");
        }

    }

    private boolean runPrintReceiptSequence() {

        if (!initializeObject()) {
            return false;
        }

        if (!createReceiptData()) {
            finalizeObject();
            return false;
        }

        if (!printData()) {
            finalizeObject();
            return false;
        }

        return true;
    }


    private boolean printData() {
        if (mPrinter == null) {
            return false;
        }

        if (!connectPrinter()) {
            mPrinter.clearCommandBuffer();
            return false;
        }

        try {
            mPrinter.sendData(Printer.PARAM_DEFAULT);
        } catch (Exception e) {
            mPrinter.clearCommandBuffer();
            // ShowMsg.showException(e, "sendData", mContext);
            try {
                mPrinter.disconnect();
            } catch (Exception ex) {
                // Do nothing
            }
            return false;
        }

        return true;
    }

    private boolean connectPrinter() {
        if (mPrinter == null) {
            return false;
        }
        try {
            // mPrinter.connect("192.168.1.103", Printer.PARAM_DEFAULT);
            mPrinter.connect(PrinterAddress, Printer.PARAM_DEFAULT);
        } catch (Exception e) {
            // ShowMsg.showException(e, "connect", mContext);
            return false;
        }

        return true;
    }


    private void finalizeObject() {
        if (mPrinter == null) {
            return;
        }

        mPrinter.clearCommandBuffer();

        mPrinter.setReceiveEventListener(null);
        try {

            mPrinter.addPulse(Printer.DRAWER_HIGH, Printer.PULSE_100);
            mPrinter.sendData(Printer.PARAM_DEFAULT);
        } catch (Epos2Exception e) {

        }
        mPrinter = null;
    }

    private boolean initializeObject() {
        try {
            // ((SpnModelsItem) mSpnLang.getSelectedItem()).getModelConstant()
//            mPrinter = new Printer(((SpnModelsItem) mSpnSeries.getSelectedItem()).getModelConstant(),
            mPrinter = new Printer(Printer.TM_T88, Printer.MODEL_ANK, mContext);
            //

        } catch (Exception e) {
            // ShowMsg.showException(e, "Printer", mContext);
            return false;
        }

        mPrinter.setReceiveEventListener(this);

        return true;
    }

    private boolean createReceiptData() {
        String method = "";

        StringBuffer textData = new StringBuffer();
        final int barcodeWidth = 2;
        final int barcodeHeight = 100;

        if (mPrinter == null) {
            return false;
        }

        try {

            //语言需要
            mPrinter.addTextLang(Printer.LANG_TH);
            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            method = "addFeedLine";
            mPrinter.addFeedLine(1);
            textData.append(Profile.StoreName + "\n" + Profile.store_address + "\n");
            textData.append("ใบเสร็จรับเงิน / RECEIPT\n");
            textData.append("\n");
            textData.append(String.format("%-20s %-20s \n", "Invoice No." + StringUtil.OrderId, "Order Type. " + "Dine In "));
            textData.append(String.format("%-20s %-20s \n", "Invoice By." + profile.Username, "Table." + "" + StringUtil.TableCustomerName));
            textData.append(String.format("%-20s %-20s \n", "Staff." + profile.Username, getDateTime() + ""));

            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            //columns.print();
            total_price = 0;
            total_qty = 0;
            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_RIGHT);
            textData.append("------------------------------------------------\n");
            textData.append(String.format("%-3s %-26s %5s %5s \n", "Qty", "Item", "Price", "Total"));
            textData.append("------------------------------------------------\n");
            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            double sum_price = 0.0;
            JSONArray jArray = null;
            try {


                double sum_row_price = 0.0;
                jArray = new JSONArray(Resultbill);
                // loop ของ report category
                for (int f = 0; f < jArray.length(); f++) {
                    JSONObject json_data_report = jArray.getJSONObject(f);

                    JSONArray jArray_menu = new JSONArray(json_data_report.getString("products"));
                    method = "addTextAlign";
                    mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                    textData.append("   " + json_data_report.getString("report_cat_name") + "\n");
                    //textData.append("------------------------------------------\n");
                    method = "addText";
                    mPrinter.addText(textData.toString());
                    textData.delete(0, textData.length());

                    if (jArray_menu.length() > 0) {
                        // loop ของ products
                        int item_count_row = 0;
                        int total_row_qty = 0;
                        double total_price = 0.0;
                        double total_price_by_cat = 0.0;
                        String line = "";
                        int total_row_price = 0;
                        int product_sale_price = 0;
                        for (int i = 0; i < jArray_menu.length(); i++) {

                            JSONObject json_data = jArray_menu.getJSONObject(i);
                            total_row_qty = Integer.parseInt(json_data.getString("qty"));
                            item_count_row += total_row_qty;
                            String confirm = json_data.getString("is_confirm");
                            String product_name = json_data.getString("product_name");
                            // เก็บตัวแปร Choice
                            String product_choice_name = "";

                            // เก็บราคาใน order
                            total_row_price = Integer.parseInt(json_data.getString("product_sale_price"));
                            product_sale_price = Integer.parseInt(json_data.getString("product_sale_price"));

                            int order_place = Integer.parseInt(confirm);
                            if (order_place == 0) {
                                check_order_place++;
                            }
                            // loop choice
                            JSONArray jArray2 = new JSONArray(json_data.getString("product_choice"));
                            int check_list = jArray2.length();
                            if (check_list > 0) {
                                for (int j = 0; j < jArray2.length(); j++) {
                                    JSONObject json_data2 = jArray2.getJSONObject(j);

                                    // หาราคารวมของแต่ละ row แล้วนำไปยัดใส่ใน header row
                                    int choice_price = Integer.parseInt(json_data2.getString("choice_price"));
                                    total_row_price += choice_price;

                                    String choice_name = json_data2.getString("product_choice_name");
                                    product_choice_name += choice_name + ",";

                                }
                            }


                            total_price = total_row_price;
                            total_qty = total_qty + total_row_qty;
                            // หาผลร่วมระหว่าง หมวดหมู่
                            total_price_by_cat += (total_price * total_row_qty);

                            // หาผลร่วมระหว่าง ทั้งหมด
                            sum_row_price = sum_row_price + total_price;

                            method = "addTextAlign";
                            mPrinter.addTextAlign(Printer.ALIGN_RIGHT);


                            if (product_sale_price > 0) {
                                double temp_total = total_row_qty * product_sale_price;
                                textData.append(String.format("%2s %-25s %7s %7s \n"
                                        , total_row_qty + ""
                                        , product_name
                                        , formatter.format(product_sale_price) + ""
                                        , formatter.format(temp_total) + ""));
                            } else {
                                textData.append(String.format("%2s %-25s %7s %7s \n"
                                        , total_row_qty + ""
                                        , product_name
                                        , " "
                                        , " "));
                            }


                            method = "addText";
                            mPrinter.addText(textData.toString());
                            textData.delete(0, textData.length());


                            if (check_list > 0) {

                                method = "addTextAlign";
                                mPrinter.addTextAlign(Printer.ALIGN_RIGHT);

                                for (int j = 0; j < jArray2.length(); j++) {
                                    JSONObject json_data2 = jArray2.getJSONObject(j);

                                    // หาราคารวมของแต่ละ row แล้วนำไปยัดใส่ใน header row
                                    int choice_price = Integer.parseInt(json_data2.getString("choice_price"));
                                    //total_row_price += choice_price;

                                    String choice_name = json_data2.getString("product_choice_name");
                                    //product_choice_name += choice_name + ",";

                                    //String x = String.format("%.02f", total_price);
                                    //String y = String.format("%.02f", (total_price * total_row_qty));
                                    int qty = (int) Math.round(total_row_qty);

                                    DecimalFormat formatter = new DecimalFormat("#,###.00");
                                    double amount = qty * choice_price;
                                    int IntValue = (int) Math.round(amount);

                                    textData.append(String.format("%2s %-25s %7s %7s \n"
                                            , " "
                                            , " -" + choice_name
                                            , formatter.format(choice_price)
                                            , formatter.format(IntValue) + ""));

                                    // textData.append(filled);
                                    // textData.append(data);

                                }

                                method = "addText";
                                mPrinter.addText(textData.toString());
                                textData.delete(0, textData.length());

                            }

                        }

                        textData.append("------------------------------------------------\n");
                        textData.append(String.format("%-20s %20s \n", "Total " + item_count_row + " items", String.format("%.02f", total_price_by_cat) + ""));
                        //textData.append(String.format("%-20s %-20s \n", "Total " + item_count_row + " items", String.format("%.02f", total_price_by_cat) + ""));
                        sum_price += total_price_by_cat;

                        method = "addText";
                        mPrinter.addText(textData.toString());
                        textData.delete(0, textData.length());

                        // reset ให้เป็น 0 เพื่อหา row ต่อไป
                        total_price_by_cat = 0;


                    } else {

                    }


                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_RIGHT);
            textData.append("------------------------------------------------\n");

            //textData.append("\n");
            textData.append(String.format("%-20s %-20s \n", "Sum total " + total_qty + " items", "", String.format("%.02f", sum_price) + ""));

            // เงินสด
            if (payment_type == 1) {
                double total = 0.0;
                double totalDiscount = 0.0;
                total = total_payment;
                totalDiscount = total_discount;
                // เช็คส่วนลดแยกตามประเภทอาหาร
                if (StringUtil.dicount_type.equals("แยกตามรายการ")) {
                    textData.append(String.format("%-20s %20s \n", "Discount " + StringUtil.dicount_total + StringUtil.report_cat_name, String.format("%.02f", totalDiscount) + ""));
                } else {
                    textData.append(String.format("%-20s %20s \n", "Discount " + StringUtil.dicount_total + StringUtil.dicount_type, String.format("%.02f", totalDiscount) + ""));
                }


                double t = Double.parseDouble(StringUtil.TotalPayment);

                textData.append(String.format("%-20s %20s \n", "Total ", formatter.format(t) + ""));

                //double service_total = (total * Integer.parseInt(StringUtil.custom_service)) / 100;

                textData.append(String.format("%-20s %20s \n", "Service " + StringUtil.custom_service + "%", String.format("%.02f", total_service_charge) + ""));

                //double vat_total = (service_total * 7) / 100;

                if (StringUtil.vat == false) {
                    //textData.append(String.format("%-20s %20s \n", "Vat", vat_total + "%"));
                } else {

                    textData.append(String.format("%-20s %20s \n", "Vat " + vat_data + "%", String.format("%.02f", vat_total) + ""));
                }

                //total = total + service_total + vat_total;

                double _temp = total_payment - totalDiscount;
                //int IntValue = (int) Math.round(_temp);

                textData.append(String.format("%-20s %20s \n", "Grand Total", formatter.format(total_payment) + ""));
                textData.append(String.format("%-20s %20s \n", "Cash", String.format("%.02f", Double.parseDouble(input_receipt_total.getText().toString())) + ""));

                int input_total_cash = Integer.parseInt(input_receipt_total.getText().toString());
                double res_b = input_total_cash - total_payment;
                textData.append(String.format("%-20s %20s \n", "Change cash", formatter.format(res_b) + ""));

                method = "addText";
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());

            } else {
                double total = 0.0;
                double totalDiscount = 0.0;
                total = total_payment;
                totalDiscount = total_discount;
                // เช็คส่วนลดแยกตามประเภทอาหาร
                if (StringUtil.dicount_type.equals("แยกตามรายการ")) {
                    textData.append(String.format("%-20s %20s \n", "Discount " + StringUtil.dicount_total + StringUtil.report_cat_name, String.format("%.02f", totalDiscount) + ""));
                } else {
                    textData.append(String.format("%-20s %20s \n", "Discount " + StringUtil.dicount_total + StringUtil.dicount_type, String.format("%.02f", totalDiscount) + ""));
                }


                textData.append(String.format("%-20s %20s \n", "Total", String.format("%.02f", total_payment) + ""));

                double service_total = (total * Integer.parseInt(StringUtil.custom_service)) / 100;
                textData.append(String.format("%-20s %20s \n", "Service " + StringUtil.custom_service + "%", String.format("%.02f", total_service_charge) + ""));

                //double vat_total = (service_total * 7) / 100;

                if (StringUtil.vat == false) {
                    //textData.append(String.format("%-20s %20s \n", "Vat", vat_total + "%"));
                } else {

                    textData.append(String.format("%-20s %20s \n", "Vat " + vat_data + "%", String.format("%.02f", vat_total) + ""));
                }
                total = total + service_total + vat_total;

                textData.append(String.format("%-20s %20s \n", "Grand Total", String.format("%.02f", total_payment) + ""));
                if (payment_type == 2) {
                    textData.append(String.format("%-20s %20s \n", "Credit/Master Card", ""));
                    textData.append(String.format("%-20s %20s \n", "Ref No.", input_ref_code.getText().toString()));
                } else if (payment_type == 3) {
                    textData.append(String.format("%-20s %20s \n", "โอน", input_transfer_amount.getText().toString()));
                    textData.append(String.format("%-20s %20s \n", "Ref No.", input_bank_ref_nbr.getText().toString()));
                } else {
                    textData.append(String.format("%-20s %20s \n", "QR CODE", ""));
                    textData.append(String.format("%-20s %20s \n", "Ref No.", input_qrcode_ref.getText().toString() + ""));
                }


                //textData.append(String.format("%-20s %20s \n", "Change cash", (input_total_cash - res_banlance) + ""));

                method = "addText";
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());
            }


            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            textData.append("THANK YOU\n");
            textData.append("POWER BY GPOS\n");
            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addCut";
            mPrinter.addCut(Printer.CUT_FEED);
            method = "addPulse";
            mPrinter.addPulse(Printer.PARAM_DEFAULT,
                    Printer.PARAM_DEFAULT);

        } catch (Exception e) {
            //ShowMsg.showException(e, method, mContext);
            return false;
        }


        if (bill.equals("yes")) {
            try {

                //语言需要
                mPrinter.addTextLang(Printer.LANG_TH);
                method = "addTextAlign";
                mPrinter.addTextAlign(Printer.ALIGN_CENTER);
                method = "addFeedLine";
                mPrinter.addFeedLine(1);
                textData.append(Profile.StoreName + "\n");
                textData.append(Profile.store_address + "\n");
                textData.append("(สำนักงานใหญ่)\n");
                textData.append("เลขที่ผู้เสียภาษี " + Profile.StoreTax + "\n");
                textData.append("โทร " + Profile.StorePhone + "\n");

                textData.append(String.format("%-10s %-30s \n", "ได้รับเงินจาก ", "___________________________________"));
                textData.append("\n");
                textData.append(String.format("%-10s %-30s \n", "           ", "___________________________________"));
                textData.append("\n");
                textData.append(String.format("%-10s %-30s \n", "           ", "___________________________________"));
                textData.append("\n");
                textData.append(String.format("%-10s %-30s \n", "ที่อยู่        ", "___________________________________"));
                textData.append("\n");
                textData.append(String.format("%-10s %-30s \n", "           ", "___________________________________"));
                textData.append("\n");
                textData.append(String.format("%-10s %-30s \n", "           ", "___________________________________"));
                textData.append("\n");

                //textData.append("------------------------------------------------\n");
                textData.append("ใบกำกับภาษีอย่างย่อ/ใบเสร็จรับเงิน\n" + StringUtil.OrderId + "\n");
                //textData.append("------------------------------------------------\n");
                textData.append("\n");
                //textData.append(String.format("%-20s %-20s \n", "Invoice No." + StringUtil.OrderId, "Order Type. " + "Dine In "));
                //textData.append(String.format("%-20s %-20s \n", "Invoice By." + profile.Username, "Table." + "" + StringUtil.TableCustomerName));
                textData.append(String.format("%-20s %-20s \n", "พนักงานขาย " + profile.Username, "วันที่ " + getDateTime() + ""));

                method = "addText";
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());

                //columns.print();
                total_price = 0;
                total_qty = 0;
                method = "addTextAlign";
                mPrinter.addTextAlign(Printer.ALIGN_RIGHT);
                textData.append("------------------------------------------------\n");
                textData.append(String.format("%-3s %-26s %5s %5s \n", "Qty", "Item", "Price", "Total"));
                textData.append("------------------------------------------------\n");
                method = "addText";
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());

                double sum_price = 0.0;
                JSONArray jArray = null;
                try {

                    double sum_row_price = 0.0;
                    jArray = new JSONArray(Resultbill);
                    // loop ของ report category
                    for (int f = 0; f < jArray.length(); f++) {
                        JSONObject json_data_report = jArray.getJSONObject(f);

                        JSONArray jArray_menu = new JSONArray(json_data_report.getString("products"));
                        method = "addTextAlign";
                        mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                        textData.append("   " + json_data_report.getString("report_cat_name") + "\n");
                        //textData.append("------------------------------------------\n");
                        method = "addText";
                        mPrinter.addText(textData.toString());
                        textData.delete(0, textData.length());

                        if (jArray_menu.length() > 0) {
                            // loop ของ products
                            int item_count_row = 0;
                            int total_row_qty = 0;
                            double total_price = 0.0;
                            double total_price_by_cat = 0.0;
                            String line = "";
                            int total_row_price = 0;
                            int product_sale_price = 0;
                            for (int i = 0; i < jArray_menu.length(); i++) {

                                JSONObject json_data = jArray_menu.getJSONObject(i);
                                total_row_qty = Integer.parseInt(json_data.getString("qty"));
                                item_count_row += total_row_qty;
                                String confirm = json_data.getString("is_confirm");
                                String product_name = json_data.getString("product_name");
                                // เก็บตัวแปร Choice
                                String product_choice_name = "";

                                // เก็บราคาใน order
                                total_row_price = Integer.parseInt(json_data.getString("product_sale_price"));
                                product_sale_price = Integer.parseInt(json_data.getString("product_sale_price"));

                                int order_place = Integer.parseInt(confirm);
                                if (order_place == 0) {
                                    check_order_place++;
                                }
                                // loop choice
                                JSONArray jArray2 = new JSONArray(json_data.getString("product_choice"));
                                int check_list = jArray2.length();
                                if (check_list > 0) {
                                    for (int j = 0; j < jArray2.length(); j++) {
                                        JSONObject json_data2 = jArray2.getJSONObject(j);

                                        // หาราคารวมของแต่ละ row แล้วนำไปยัดใส่ใน header row
                                        int choice_price = Integer.parseInt(json_data2.getString("choice_price"));
                                        total_row_price += choice_price;

                                        String choice_name = json_data2.getString("product_choice_name");
                                        product_choice_name += choice_name + ",";

                                    }
                                }


                                total_price = total_row_price;
                                total_qty = total_qty + total_row_qty;
                                // หาผลร่วมระหว่าง หมวดหมู่
                                total_price_by_cat += (total_price * total_row_qty);

                                // หาผลร่วมระหว่าง ทั้งหมด
                                sum_row_price = sum_row_price + total_price;

                                method = "addTextAlign";
                                mPrinter.addTextAlign(Printer.ALIGN_RIGHT);

                                if (product_sale_price > 0) {
                                    double temp_total = total_row_qty * product_sale_price;
                                    textData.append(String.format("%2s %-25s %7s %7s \n"
                                            , total_row_qty + ""
                                            , product_name
                                            , formatter.format(product_sale_price) + ""
                                            , formatter.format(temp_total) + ""));
                                } else {
                                    textData.append(String.format("%2s %-25s %7s %7s \n"
                                            , total_row_qty + ""
                                            , product_name
                                            , " "
                                            , " "));
                                }

//                                textData.append(String.format("%2s %-25s %7s %7s \n"
//                                        , total_row_qty + ""
//                                        , product_name
//                                        , formatter.format(product_sale_price) + ""
//                                        , formatter.format(total_row_qty * product_sale_price) + ""));

                                method = "addText";
                                mPrinter.addText(textData.toString());
                                textData.delete(0, textData.length());


                                if (check_list > 0) {

                                    method = "addTextAlign";
                                    mPrinter.addTextAlign(Printer.ALIGN_RIGHT);

                                    for (int j = 0; j < jArray2.length(); j++) {
                                        JSONObject json_data2 = jArray2.getJSONObject(j);

                                        // หาราคารวมของแต่ละ row แล้วนำไปยัดใส่ใน header row
                                        int choice_price = Integer.parseInt(json_data2.getString("choice_price"));
                                        //total_row_price += choice_price;

                                        String choice_name = json_data2.getString("product_choice_name");
                                        //product_choice_name += choice_name + ",";

                                        //String x = String.format("%.02f", total_price);
                                        //String y = String.format("%.02f", (total_price * total_row_qty));
                                        int qty = (int) Math.round(total_row_qty);

                                        DecimalFormat formatter = new DecimalFormat("#,###.00");
                                        double amount = qty * choice_price;
                                        int IntValue = (int) Math.round(amount);

                                        textData.append(String.format("%2s %-25s %7s %7s \n"
                                                , " "
                                                , " -" + choice_name
                                                , formatter.format(choice_price)
                                                , formatter.format(IntValue) + ""));

                                        // textData.append(filled);
                                        // textData.append(data);

                                    }

                                    method = "addText";
                                    mPrinter.addText(textData.toString());
                                    textData.delete(0, textData.length());

                                }

                            }

                            textData.append("------------------------------------------------\n");
                            textData.append(String.format("%-20s %20s \n", "Total " + item_count_row + " items", String.format("%.02f", total_price_by_cat) + ""));
                            //textData.append(String.format("%-20s %-20s \n", "Total " + item_count_row + " items", String.format("%.02f", total_price_by_cat) + ""));
                            sum_price += total_price_by_cat;

                            method = "addText";
                            mPrinter.addText(textData.toString());
                            textData.delete(0, textData.length());

                            // reset ให้เป็น 0 เพื่อหา row ต่อไป
                            total_price_by_cat = 0;


                        } else {

                        }


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                method = "addTextAlign";
                mPrinter.addTextAlign(Printer.ALIGN_RIGHT);
                textData.append("------------------------------------------------\n");
                //textData.append("\n");
                textData.append(String.format("%-20s %-20s \n", "Sum total " + total_qty + " items", "", String.format("%.02f", sum_price) + ""));

                // เงินสด
                if (payment_type == 1) {
                    double total = 0.0;
                    double totalDiscount = 0.0;
                    total = total_payment;
                    totalDiscount = total_discount;
                    // เช็คส่วนลดแยกตามประเภทอาหาร
                    if (StringUtil.dicount_type.equals("แยกตามรายการ")) {
                        textData.append(String.format("%-20s %20s \n", "Discount " + StringUtil.dicount_total + StringUtil.report_cat_name, String.format("%.02f", totalDiscount) + ""));
                    } else {
                        textData.append(String.format("%-20s %20s \n", "Discount " + StringUtil.dicount_total + StringUtil.dicount_type, String.format("%.02f", totalDiscount) + ""));
                    }

                    double t = Double.parseDouble(StringUtil.TotalPayment);

                    textData.append(String.format("%-20s %20s \n", "Total ", formatter.format(t) + ""));

                    //double service_total = (total * Integer.parseInt(StringUtil.custom_service)) / 100;

                    textData.append(String.format("%-20s %20s \n", "Service " + StringUtil.custom_service + "%", String.format("%.02f", total_service_charge) + ""));

                    //double vat_total = (service_total * 7) / 100;

                    if (StringUtil.vat == false) {
                        //textData.append(String.format("%-20s %20s \n", "Vat", vat_total + "%"));
                    } else {

                        textData.append(String.format("%-20s %20s \n", "Vat " + vat_data + "%", String.format("%.02f", vat_total) + ""));
                    }

                    //total = total + service_total + vat_total;

                    double _temp = total_payment - totalDiscount;
                    //int IntValue = (int) Math.round(_temp);

                    textData.append(String.format("%-20s %20s \n", "Grand Total", formatter.format(total_payment) + ""));
                    textData.append(String.format("%-5s %30s \n","","("+new ThaiBaht().getText(total_payment)+")"));

                    textData.append(String.format("%-20s %20s \n", "Cash", String.format("%.02f", Double.parseDouble(input_receipt_total.getText().toString())) + ""));

                    int input_total_cash = Integer.parseInt(input_receipt_total.getText().toString());
                    double res_b = input_total_cash - total_payment;
                    textData.append(String.format("%-20s %20s \n", "Change cash", formatter.format(res_b) + ""));

                    method = "addText";
                    mPrinter.addText(textData.toString());
                    textData.delete(0, textData.length());

                } else {
                    double total = 0.0;
                    double totalDiscount = 0.0;
                    total = total_payment;
                    totalDiscount = total_discount;
                    // เช็คส่วนลดแยกตามประเภทอาหาร
                    if (StringUtil.dicount_type.equals("แยกตามรายการ")) {
                        textData.append(String.format("%-20s %20s \n", "Discount " + StringUtil.dicount_total + StringUtil.report_cat_name, String.format("%.02f", totalDiscount) + ""));
                    } else {
                        textData.append(String.format("%-20s %20s \n", "Discount " + StringUtil.dicount_total + StringUtil.dicount_type, String.format("%.02f", totalDiscount) + ""));
                    }


                    textData.append(String.format("%-20s %20s \n", "Total", String.format("%.02f", total_payment) + ""));

                    double service_total = (total * Integer.parseInt(StringUtil.custom_service)) / 100;
                    textData.append(String.format("%-20s %20s \n", "Service " + StringUtil.custom_service + "%", String.format("%.02f", total_service_charge) + ""));

                    //double vat_total = (service_total * 7) / 100;

                    if (StringUtil.vat == false) {
                        //textData.append(String.format("%-20s %20s \n", "Vat", vat_total + "%"));
                    } else {

                        textData.append(String.format("%-20s %20s \n", "Vat " + vat_data + "%", String.format("%.02f", vat_total) + ""));
                    }
                    total = total + service_total + vat_total;

                    //ThaiBaht thaiBaht = new ThaiBaht();

                    textData.append(String.format("%-20s %20s \n", "Grand Total", String.format("%.02f", total_payment) + ""));
                    //textData.append(String.format("%-5s %20s \n","",new ThaiBaht().getText(total_payment)+""));
                    textData.append(String.format("%-5s %30s \n","","("+new ThaiBaht().getText(total_payment)+")"));

                    if (payment_type == 2) {
                        textData.append(String.format("%-20s %20s \n", "Credit/Master Card", ""));
                        textData.append(String.format("%-20s %20s \n", "Ref No.", input_ref_code.getText().toString()));
                    } else if (payment_type == 3) {
                        textData.append(String.format("%-20s %20s \n", "โอน", input_transfer_amount.getText().toString()));
                        textData.append(String.format("%-20s %20s \n", "Ref No.", input_bank_ref_nbr.getText().toString()));
                    } else {
                        textData.append(String.format("%-20s %20s \n", "QR CODE", ""));
                        textData.append(String.format("%-20s %20s \n", "Ref No.", input_qrcode_ref.getText().toString() + ""));
                    }


                    //textData.append(String.format("%-20s %20s \n", "Change cash", (input_total_cash - res_banlance) + ""));

                    method = "addText";
                    mPrinter.addText(textData.toString());
                    textData.delete(0, textData.length());
                }


                method = "addTextAlign";
                mPrinter.addTextAlign(Printer.ALIGN_CENTER);
                textData.append("VAT INCLUDED\n");
                textData.append("THANK YOU\n");
                textData.append("POWER BY GPOS\n");
                //textData.append("------------------------------------------------\n");
                textData.append(String.format("%-10s %-30s \n", "ผู้รับเงิน ", "_________________________________"));
                textData.append("\n");
                textData.append(String.format("%-10s %-30s \n", "       ", "(_________________________________)"));
                textData.append("\n");
                textData.append(String.format("%-10s %-30s \n", "ตำแหน่ง ", "_________________________________"));
                textData.append("\n");

                method = "addText";
                mPrinter.addText(textData.toString());
                textData.delete(0, textData.length());


                method = "addCut";
                mPrinter.addCut(Printer.CUT_FEED);
                method = "addPulse";
                mPrinter.addPulse(Printer.PARAM_DEFAULT,
                        Printer.PARAM_DEFAULT);


            } catch (Exception e) {
                //ShowMsg.showException(e, method, mContext);
                return false;
            }

        }
        textData = null;

        return true;
    }

    private boolean isPrintable(PrinterStatusInfo status) {
        if (status == null) {
            return false;
        }

        if (status.getConnection() == Printer.FALSE) {
            return false;
        } else if (status.getOnline() == Printer.FALSE) {
            return false;
        } else {
            ;//print available
        }

        return true;
    }


    private String getDateTime() {
        // get date time in custom format
        SimpleDateFormat sdf = new SimpleDateFormat("[yyyy/MM/dd HH:mm]");
        return sdf.format(new Date());
    }


    public void PaymentMethod() {
        if (payment_type == 1) {
            //btnNo.setEnabled(true);
            btnCash.setBackground(ContextCompat.getDrawable(CashireReceiptActivity.this, R.drawable.rounded_button_yellow));
            btnCash.setTextColor(Color.WHITE);
            btnCreadit.setBackground(ContextCompat.getDrawable(CashireReceiptActivity.this, R.drawable.rounded_button_yellow_border));
            btnCreadit.setTextColor(Color.GRAY);
            btnBank.setBackground(ContextCompat.getDrawable(CashireReceiptActivity.this, R.drawable.rounded_button_yellow_border));
            btnBank.setTextColor(Color.GRAY);
            btnQRcode.setBackground(ContextCompat.getDrawable(CashireReceiptActivity.this, R.drawable.rounded_button_yellow_border));
            btnQRcode.setTextColor(Color.GRAY);

            show_layout_payment_method();

        } else if (payment_type == 2) {

            btnCash.setBackground(ContextCompat.getDrawable(CashireReceiptActivity.this, R.drawable.rounded_button_yellow_border));
            btnCash.setTextColor(Color.GRAY);
            btnCreadit.setBackground(ContextCompat.getDrawable(CashireReceiptActivity.this, R.drawable.rounded_button_yellow));
            btnCreadit.setTextColor(Color.WHITE);
            btnBank.setBackground(ContextCompat.getDrawable(CashireReceiptActivity.this, R.drawable.rounded_button_yellow_border));
            btnBank.setTextColor(Color.GRAY);
            btnQRcode.setBackground(ContextCompat.getDrawable(CashireReceiptActivity.this, R.drawable.rounded_button_yellow_border));
            btnQRcode.setTextColor(Color.GRAY);

            show_layout_payment_method();

        } else if (payment_type == 3) {

            btnCash.setBackground(ContextCompat.getDrawable(CashireReceiptActivity.this, R.drawable.rounded_button_yellow_border));
            btnCash.setTextColor(Color.GRAY);
            btnCreadit.setBackground(ContextCompat.getDrawable(CashireReceiptActivity.this, R.drawable.rounded_button_yellow_border));
            btnCreadit.setTextColor(Color.GRAY);
            btnBank.setBackground(ContextCompat.getDrawable(CashireReceiptActivity.this, R.drawable.rounded_button_yellow));
            btnBank.setTextColor(Color.WHITE);
            btnQRcode.setBackground(ContextCompat.getDrawable(CashireReceiptActivity.this, R.drawable.rounded_button_yellow_border));
            btnQRcode.setTextColor(Color.GRAY);

            show_layout_payment_method();

        } else {
            btnCash.setBackground(ContextCompat.getDrawable(CashireReceiptActivity.this, R.drawable.rounded_button_yellow_border));
            btnCash.setTextColor(Color.GRAY);
            btnCreadit.setBackground(ContextCompat.getDrawable(CashireReceiptActivity.this, R.drawable.rounded_button_yellow_border));
            btnCreadit.setTextColor(Color.GRAY);
            btnBank.setBackground(ContextCompat.getDrawable(CashireReceiptActivity.this, R.drawable.rounded_button_yellow_border));
            btnBank.setTextColor(Color.GRAY);
            btnQRcode.setBackground(ContextCompat.getDrawable(CashireReceiptActivity.this, R.drawable.rounded_button_yellow));
            btnQRcode.setTextColor(Color.WHITE);

            show_layout_payment_method();

        }
    }

    public void show_layout_payment_method() {
        if (payment_type == 1) {
            llCash.setVisibility(View.VISIBLE);
            llCreaditCard.setVisibility(View.GONE);
            llBankTransfer.setVisibility(View.GONE);
            llQRcode.setVisibility(View.GONE);
        } else if (payment_type == 2) {
            llCash.setVisibility(View.GONE);
            llCreaditCard.setVisibility(View.VISIBLE);
            llBankTransfer.setVisibility(View.GONE);
            llQRcode.setVisibility(View.GONE);
        } else if (payment_type == 3) {
            llCreaditCard.setVisibility(View.GONE);
            llCash.setVisibility(View.GONE);
            llBankTransfer.setVisibility(View.VISIBLE);
            llQRcode.setVisibility(View.GONE);
        } else {
            llCreaditCard.setVisibility(View.GONE);
            llCash.setVisibility(View.GONE);
            llBankTransfer.setVisibility(View.GONE);
            llQRcode.setVisibility(View.VISIBLE);
        }

    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    private class PaymentAddTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(CashireReceiptActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลังบันทึกข้อมูล");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("order_id", StringUtil.OrderId + "")
                    .add("payment_amount", input_receipt_total.getText().toString())
                    .add("bill_payment", bill)
                    .add("total_qty", StringUtil.TotalOrderQty)
                    .add("amount", StringUtil.TotalPayment)
                    // โอนเงิน
                    .add("bank_name", input_bank_name.getText().toString())
                    .add("bank_tras_date", input_bank_tras_date.getText().toString())
                    .add("bank_trans_time", input_bank_trans_time.getText().toString())
                    .add("bank_ref_nbr", input_bank_ref_nbr.getText().toString())
                    //Creadit card
                    .add("creadit_card_ref", input_ref_code.getText().toString())
                    //meamanee
                    .add("qrcode_amount", input_qrcode_amount.getText().toString())
                    .add("payment_change", payment_change)
                    .add("total_discount", StringUtil.dicount_total + "")
                    .add("discount_type", StringUtil.dicount_type + "")
                    .add("vat", "7")
                    .add("services", StringUtil.custom_service + "")
                    .add("payment_type", payment_type + "")
                    .add("customer_id", StringUtil.TableCustomerId + "")

                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            // ไม่ปริ้นใบเสร็จ
//            if (check_receipt == 2) {
//
//            } else {
//                // ปริ้นใบเสร็จ
//                new AsyncChoiceProductPrint().execute();
//            }


            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(CashireReceiptActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    //AlertDialog alert = builder.create();
                    //alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(CashireReceiptActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    //do things
                                    if (profile.Position.equals("4")) // แคชเชียร์
                                    {
                                        StringUtil.employee_status = "แคชเชียร์";
                                        Intent in = new Intent(CashireReceiptActivity.this, StaffCashireActivity.class);
                                        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                        startActivity(in);
                                        finish();

                                    } else if (profile.Position.equals("2")) {
                                        StringUtil.employee_status = "ผู้จัดการร้าน";
                                        Intent in = new Intent(CashireReceiptActivity.this, ManagerMainActivity.class);
                                        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                        startActivity(in);
                                        finish();

                                    } else {
                                        Intent in = new Intent(CashireReceiptActivity.this, ManagerMainActivity.class);
                                        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                        startActivity(in);
                                        finish();
                                    }

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();


                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public void CalCash() {
        try {
            double receipt_total = Double.parseDouble(input_receipt_total.getText().toString());
            double receipt_total_sum = Double.parseDouble(txtTotalReceipt.getText().toString());
            double result = receipt_total - receipt_total_sum;

            txt_balance.setText("" + String.format("%.02f", result));
            payment_change = result + "";

        } catch (Exception e) {
            txt_balance.setText("");
        }

    }

    public void PaymentCard() {

        if (check_vis == 0) {
            btnVisa.setImageResource(R.drawable.ic_visa);
            btnMastercard.setImageResource(R.drawable.ic_mastercard_grayscale);
        } else {
            btnVisa.setImageResource(R.drawable.ic_visa_grayscale);
            btnMastercard.setImageResource(R.drawable.ic_mastercard);

        }

    }

    public void EnableButton() {
        if (check_receipt == 0) {
            //btnNo.setEnabled(true);
            btnInvoice.setBackground(ContextCompat.getDrawable(CashireReceiptActivity.this, R.drawable.rounded_button_yellow));
            btnInvoice.setTextColor(Color.WHITE);
            //btnYes.setEnabled(false);
            btnShortInvoice.setBackground(ContextCompat.getDrawable(CashireReceiptActivity.this, R.drawable.rounded_button_yellow_border));
            btnShortInvoice.setTextColor(Color.GRAY);

            btnNoInvoice.setBackground(ContextCompat.getDrawable(CashireReceiptActivity.this, R.drawable.rounded_button_yellow_border));
            btnNoInvoice.setTextColor(Color.GRAY);

        } else if (check_receipt == 1) {
            //btnNo.setEnabled(true);
            btnInvoice.setBackground(ContextCompat.getDrawable(CashireReceiptActivity.this, R.drawable.rounded_button_yellow_border));
            btnInvoice.setTextColor(Color.GRAY);
            //btnYes.setEnabled(false);
            btnShortInvoice.setBackground(ContextCompat.getDrawable(CashireReceiptActivity.this, R.drawable.rounded_button_yellow));
            btnShortInvoice.setTextColor(Color.WHITE);

            btnNoInvoice.setBackground(ContextCompat.getDrawable(CashireReceiptActivity.this, R.drawable.rounded_button_yellow_border));
            btnNoInvoice.setTextColor(Color.GRAY);

        } else {
            //btnNo.setEnabled(true);
            btnInvoice.setBackground(ContextCompat.getDrawable(CashireReceiptActivity.this, R.drawable.rounded_button_yellow_border));
            btnInvoice.setTextColor(Color.GRAY);
            //btnYes.setEnabled(false);
            btnShortInvoice.setBackground(ContextCompat.getDrawable(CashireReceiptActivity.this, R.drawable.rounded_button_yellow_border));
            btnShortInvoice.setTextColor(Color.GRAY);

            btnNoInvoice.setBackground(ContextCompat.getDrawable(CashireReceiptActivity.this, R.drawable.rounded_button_yellow));
            btnNoInvoice.setTextColor(Color.WHITE);

        }
    }


    @Override
    protected void onResume() {
        super.onResume();

    }


    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    public class ThaiBaht {
        private final String[] SCALE_TH = {"ล้าน", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", ""};
        private final String[] DIGIT_TH = {"ศูนย์", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า"};
        private final String[] SYMBOLS_TH = {"ลบ", "บาท", "ถ้วน", "สตางค์", "ยี่", "เอ็ด", ",", " ", "฿"};

        private String valueText;

        // ···········Methods··············//
        public String getText(double amount) {
            BigDecimal value = new BigDecimal(amount);
            this.valueText = getThaiBaht(value);
            return this.valueText;
        }

        public String getText(float amount) {
            BigDecimal value = new BigDecimal(amount);
            this.valueText = getThaiBaht(value);
            return this.valueText;
        }

        public String getText(int amount) {
            BigDecimal value = new BigDecimal(amount);
            this.valueText = getThaiBaht(value);
            return this.valueText;
        }

        public String getText(long amount) {
            BigDecimal value = new BigDecimal(amount);
            this.valueText = getThaiBaht(value);
            return this.valueText;
        }

        public String getText(String amount) {
            //ไม่ต้องการเครื่องหมายคอมมาร์, ไม่ต้องการช่องว่าง, ไม่ต้องการตัวหนังสือ บาท, ไม่ต้องการสัญลักษณ์สกุลเงินบาท
            for (String element : SYMBOLS_TH) {
                amount = amount.replace(element, "");
            }

            BigDecimal value = new BigDecimal(amount.trim());
            this.valueText = getThaiBaht(value);
            return this.valueText;
        }

        public String getText(Number amount) {
            BigDecimal value = new BigDecimal(String.valueOf(amount));
            this.valueText = getThaiBaht(value);
            return this.valueText;
        }

        private  String getThaiBaht(BigDecimal amount) {
            StringBuilder builder = new StringBuilder();
            BigDecimal absolute = amount.abs();
            int precision = absolute.precision();
            int scale = absolute.scale();
            int rounded_precision = ((precision - scale) + 2);
            MathContext mc = new MathContext(rounded_precision, RoundingMode.HALF_UP);
            BigDecimal rounded = absolute.round(mc);
            BigDecimal[] compound = rounded.divideAndRemainder(BigDecimal.ONE);
            boolean negative_amount = (-1 == amount.compareTo(BigDecimal.ZERO));

            compound[0] = compound[0].setScale(0);
            compound[1] = compound[1].movePointRight(2);

            if (negative_amount) {
                builder.append(SYMBOLS_TH[0].toString());
            }

            builder.append(getNumberText(compound[0].toBigIntegerExact()));
            builder.append(SYMBOLS_TH[1].toString());

            if (0 == compound[1].compareTo(BigDecimal.ZERO)) {
                builder.append(SYMBOLS_TH[2].toString());
            } else {
                builder.append(getNumberText(compound[1].toBigIntegerExact()));
                builder.append(SYMBOLS_TH[3].toString());
            }
            return builder.toString();
        }

        private  String getNumberText(BigInteger number) {
            StringBuffer buffer = new StringBuffer();
            char[] digits = number.toString().toCharArray();

            for (int index = digits.length; index > 0; --index) {
                int digit = Integer.parseInt(String.valueOf(digits[digits.length
                        - index]));
                String digit_text = DIGIT_TH[digit];
                int scale_idx = ((1 < index) ? ((index - 1) % 6) : 6);

                if ((1 == scale_idx) && (2 == digit)) {
                    digit_text = SYMBOLS_TH[4].toString();
                }

                if (1 == digit) {
                    switch (scale_idx) {
                        case 0:
                        case 6:
                            buffer.append((index < digits.length) ? SYMBOLS_TH[5].toString() : digit_text);
                            break;
                        case 1:
                            break;
                        default:
                            buffer.append(digit_text);
                            break;
                    }
                } else if (0 == digit) {
                    if (0 == scale_idx) {
                        buffer.append(SCALE_TH[scale_idx]);
                    }
                    continue;
                } else {
                    buffer.append(digit_text);
                }
                buffer.append(SCALE_TH[scale_idx]);
            }
            return buffer.toString();
        }
    }

}
