package com.geerang.gcounter;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class ZoneEditActivity extends AppCompatActivity {
    Button btnEdit;
    EditText input_txtName;
    Spinner ddlBuildingEdit;
    String BuildingID = "";
    String Bid = "";
    ArrayList<HashMap<String, String>> MyArrList;
    HashMap<String, String> map;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zone_edit);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }



        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        ddlBuildingEdit = (Spinner) findViewById(R.id.ddlBuildingEdit);
        input_txtName = (EditText) findViewById(R.id.input_NameZoneEdit);
        input_txtName.setText(StringUtil.zone_Name);
        btnEdit = (Button) findViewById(R.id.btnZoneEditSubmit);
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new EditZoneTask().execute(StringUtil.URL + "gpos/api/ZoneEdit");
            }
        });
        new AsyncLoadBuilding().execute();
    }
    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }
    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }
    private class AsyncLoadBuilding extends AsyncTask<String, String, String> {
        //        ProgressDialog pdLoading = new ProgressDialog(FoodMenuAddActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
//            pdLoading.setMessage("\tกำลังโหลดข้อมูล...");
//            pdLoading.setCancelable(false);
//            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            ZoneEditActivity.getHttp http = new ZoneEditActivity.getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/Building/" + Profile.StoreId);
//                response = http.run(StringUtil.URL+"gpos/api/category/1");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread
                int selectIndex = 0;
//            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);
                MyArrList = new ArrayList<HashMap<String, String>>();
                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    map = new HashMap<String, String>();
                    if (StringUtil.zone_Bid.equals(json_data.getString("bid"))){
                        selectIndex = i;
                    }
                    map.put("MemberID", json_data.getString("bid"));
                    map.put("Name", json_data.getString("building_name"));
                    MyArrList.add(map);

                }


                SimpleAdapter sAdap;
                sAdap = new SimpleAdapter(ZoneEditActivity.this, MyArrList, R.layout.activity_menu_ddl_row,
                        new String[]{"Name"}, new int[]{R.id.ColName});

//                final AlertDialog.Builder viewDetail = new AlertDialog.Builder(FoodMenuAddActivity.this);
                ddlBuildingEdit.setAdapter(sAdap);
                ddlBuildingEdit.setSelection(selectIndex);
                ddlBuildingEdit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    public void onItemSelected(AdapterView<?> arg0, View selectedItemView,
                                               int position, long id) {


                        Bid = MyArrList.get(position).get("MemberID")
                                .toString();
                        String sName = MyArrList.get(position).get("Name")
                                .toString();


                    }

                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub
                        Toast.makeText(ZoneEditActivity.this,
                                "Your Selected : Nothing",
                                Toast.LENGTH_SHORT).show();
                    }


                });


            } catch (JSONException e) {
                Toast.makeText(ZoneEditActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }
    private class EditZoneTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
        }

        protected String doInBackground(String... urls)   {
            String response = null;
            ZoneEditActivity.postHttp http = new ZoneEditActivity.postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("bid", Bid)
                    .add("zone_name", input_txtName.getText().toString())
                    .add("building_zone_id", StringUtil.zone_id)
                    .build();


            try {

                response = http.run(urls[0],formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString)  {
            // Dismiss ProgressBar
            //showData(jsonString);

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success

                if(status.equals("0"))
                {
                    String msg = object.getString("msg"); // success
                    androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(ZoneEditActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

                }else
                {

                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(ZoneEditActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    finish();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
