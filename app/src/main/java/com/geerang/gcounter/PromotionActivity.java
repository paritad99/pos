package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.PictureDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.Html;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class PromotionActivity extends AppCompatActivity {

    private final OkHttpClient client = new OkHttpClient();
    private List<FeedItemDrug> feedsList;
    private CategoriesRecyclerAdapter mAdapter;

    RecyclerView rvCatagory;

    Button btnAddPromotion;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {

            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR

            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotion);

// add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        rvCatagory = (RecyclerView) findViewById(R.id.recyclerView);


        //เพิ่มโปรโมชั่น
        btnAddPromotion = (Button) findViewById(R.id.btnAddPromotion);

        if (Profile.Position.equals("5")) {
            btnAddPromotion.setVisibility(View.INVISIBLE);
        } else {
            btnAddPromotion.setVisibility(View.GONE);
        }

        btnAddPromotion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        new AsyncLoadCat().execute();
    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    private class AsyncLoadCat extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(PromotionActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {//http://www.geerang.com/gpos/api/promotion/1/2
                response = http.run(StringUtil.URL + "gpos/api/promotion/" + Profile.StoreId);
//                response = http.run(StringUtil.URL+"gpos/api/category/1");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();

            feedsList = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemDrug item = new FeedItemDrug();

                    item.setPromotion_name(json_data.getString("title"));
                    item.setPromotion_id(json_data.getString("promotion_id"));
                    item.setPromotion_qty(json_data.getString("amount"));
                    item.setPromotion_use_qty(json_data.getString("max_qty"));
                    item.setPromotion_branch_id(json_data.getString("branch_id"));
                    item.setPromotion_type(json_data.getString("promotion_catagory"));
                    feedsList.add(item);
                }

                // Setup and Handover data to recyclerview

                mAdapter = new CategoriesRecyclerAdapter(PromotionActivity.this, feedsList);
                rvCatagory.setAdapter(mAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(PromotionActivity.this, 1);
                rvCatagory.setLayoutManager(mLayoutManager);
                rvCatagory.setItemAnimator(new DefaultItemAnimator());
                rvCatagory.setItemAnimator(new DefaultItemAnimator());

            } catch (JSONException e) {
                Toast.makeText(PromotionActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    public class CategoriesRecyclerAdapter extends RecyclerView.Adapter<CategoriesRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemDrug> feedItemList;
        private Context mContext;

        public CategoriesRecyclerAdapter(Context context, List<FeedItemDrug> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;
        }

        @Override
        public CategoriesRecyclerAdapter.CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.promotion_row, null);

            CategoriesRecyclerAdapter.CustomViewHolder viewHolder = new CategoriesRecyclerAdapter.CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(CategoriesRecyclerAdapter.CustomViewHolder customViewHolder, int i) {
            final FeedItemDrug feedItem = feedItemList.get(i);
            customViewHolder.feedItem = feedItem;


            //Setting text view titlefeedItem.ge
            customViewHolder.tv_title.setText("ชื่อโปรโมชั่น:" + Html.fromHtml(feedItem.getPromotion_name()));
            customViewHolder.tvTotalQty.setText("จำนวนที่ใช้ได้:" + Html.fromHtml(feedItem.getPromotion_qty()));
            customViewHolder.tvTotalUsed.setText("จำนวนที่ใช้ไป:" + Html.fromHtml(feedItem.getPromotion_use_qty()));
            customViewHolder.txt_promotion_type.setText("ประเภทโปรโมชั่น:" + Html.fromHtml(feedItem.getPromotion_use_qty()));
            customViewHolder.btnSelectPromotion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    StringUtil.promotion_discount_percen_type = feedItem.getPromotion_type();
                    StringUtil.promotion_discount_percen_total = Integer.parseInt(feedItem.getPromotion_qty());


                    finish();
                }
            });


            //customViewHolder.imageView.setImageResource(StringUtil.arrImg[i]);


        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            protected ImageView imageView;
            protected TextView tv_title, tvTotalQty, tvTotalUsed, txt_promotion_type;

            FeedItemDrug feedItem;
            protected Button btnSelectPromotion;

            public CustomViewHolder(View view) {
                super(view);

                this.tv_title = (TextView) view.findViewById(R.id.title);
                this.tvTotalQty = (TextView) view.findViewById(R.id.tvTotalQty);
                this.tvTotalUsed = (TextView) view.findViewById(R.id.tvTotalUsed);
                this.txt_promotion_type = (TextView) view.findViewById(R.id.txt_promotion_type);

                this.btnSelectPromotion = (Button) view.findViewById(R.id.btnSelectPromotion);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


//                        StringUtil.CatId = feedItem.getId();
                        //new AsyncProduct().execute();
                        //Toast.makeText(v.getContext(), "Select is: " + feedItem.getId(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }
    }

    // String
    public class FeedItemDrug {

        private String promotion_id = "";
        private String promotion_name = "";
        private String promotion_use_qty = "";
        private String promotion_branch_id = "";
        private String promotion_qty = "";
        private String promotion_type = "";

        public String getPromotion_type() {
            return promotion_type;
        }

        public void setPromotion_type(String promotion_type) {
            this.promotion_type = promotion_type;
        }

        public String getPromotion_qty() {
            return promotion_qty;
        }

        public void setPromotion_qty(String promotion_qty) {
            this.promotion_qty = promotion_qty;
        }

        public String getPromotion_use_qty() {
            return promotion_use_qty;
        }

        public String getPromotion_branch_id() {
            return promotion_branch_id;
        }

        public String getPromotion_id() {
            return promotion_id;
        }

        public String getPromotion_name() {
            return promotion_name;
        }

        public void setPromotion_branch_id(String promotion_branch_id) {
            this.promotion_branch_id = promotion_branch_id;
        }

        public void setPromotion_id(String promotion_id) {
            this.promotion_id = promotion_id;
        }

        public void setPromotion_name(String promotion_name) {
            this.promotion_name = promotion_name;
        }

        public void setPromotion_use_qty(String promotion_use_qty) {
            this.promotion_use_qty = promotion_use_qty;
        }

    }
}
