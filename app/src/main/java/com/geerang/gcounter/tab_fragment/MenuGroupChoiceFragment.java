package com.geerang.gcounter.tab_fragment;

import androidx.appcompat.app.AlertDialog;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.PictureDrawable;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.geerang.gcounter.FoodMenuChoiceActivity;
import com.geerang.gcounter.MenuGroupChoicePriceEditActivity;
import com.geerang.gcounter.R;
import com.geerang.gcounter.StringUtil;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MenuGroupChoiceFragment extends Fragment {


    public static MenuGroupChoiceFragment newInstance() {
        return new MenuGroupChoiceFragment();
    }
    private final OkHttpClient client = new OkHttpClient();
    private List<FeedItemDrug> feedsList;
    private CategoriesRecyclerAdapter mAdapter;

    RecyclerView rvCatagory;
    View  root;

    Button btnChoiceAdd;
    static String choice_id;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_menu_group_choice, container, false);

        btnChoiceAdd = (Button) root.findViewById(R.id.btnChoiceAdd);
        btnChoiceAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(getActivity(), FoodMenuChoiceActivity.class);
                //FoodMenuChoiceActivity
//                Intent in = new Intent(getActivity(), FoodMenuGroupChoiceAddActivity.class);
                startActivity(in);

            }
        });

        rvCatagory = (RecyclerView) root.findViewById(R.id.recyclerView);


        new AsyncLoadCat().execute();

        return root;
    }


    @Override
    public void onResume() {
        super.onResume();

        new AsyncLoadCat().execute();

    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    private void showData(String jsonString) {
        Toast.makeText(getActivity(), jsonString, Toast.LENGTH_LONG).show();
    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    private class AsyncLoadCat extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(getActivity());
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL+"gpos/api/FoodMenuChoice/"+ StringUtil.menu_store_id);
//                response = http.run(StringUtil.URL+"gpos/api/category/1");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();

            feedsList = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemDrug item = new FeedItemDrug();
                    item.setP_group_id(json_data.getString("p_group_id"));
                    item.setEmployee_id(json_data.getString("p_group_id"));
                    item.setFirstname(json_data.getString("detailProductChoiceIdName"));
                    item.setLastname(json_data.getString("product_add_choice_id"));
                    item.setProduct_add_choice_id(json_data.getString("product_add_choice_id"));


                    feedsList.add(item);
                }

                // Setup and Handover data to recyclerview

                mAdapter = new CategoriesRecyclerAdapter(getActivity(), feedsList);
                rvCatagory.setAdapter(mAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
                rvCatagory.setLayoutManager(mLayoutManager);
                rvCatagory.setItemAnimator(new DefaultItemAnimator());
                rvCatagory.setItemAnimator(new DefaultItemAnimator());

            } catch (JSONException e) {
                Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    public class CategoriesRecyclerAdapter extends RecyclerView.Adapter<CategoriesRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemDrug> feedItemList;
        private Context mContext;

        public CategoriesRecyclerAdapter(Context context, List<FeedItemDrug> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_menu_group_choice_row, null);

            CustomViewHolder viewHolder = new CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
            final FeedItemDrug feedItem = feedItemList.get(i);
            customViewHolder.feedItem = feedItem;

            //Setting text view title
            customViewHolder.title_choice.setText(Html.fromHtml(""+feedItem.getFirstname()));
//            customViewHolder.input_choice_price.setText(Html.fromHtml(""+feedItem.getLastname()));

            customViewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent in = new Intent(mContext, MenuGroupChoicePriceEditActivity.class);
                    StringUtil.p_group_id = feedItem.getP_group_id();
//                    in.putExtra("cid",feedItem.getEmployee_id());
//                    in.putExtra("title_choice",feedItem.getFirstname());
//                    in.putExtra("title_choice_order",feedItem.getLastname());
                    startActivity(in);

//                    p_group_id

                }
            });

            customViewHolder.btnChoiceDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    final AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());

                    adb.setTitle("ยืนยันการลบข้อมูล?");
                    adb.setMessage("ยืนยันการลบข้อมูล");
                    adb.setNegativeButton("Cancel", null);
                    adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
                        public void onClick(DialogInterface dialog, int arg1) {
                            // TODO Auto-generated method stub

                            new DeleteItemTask().execute(StringUtil.URL + "gpos/api/MenuGroupChoiceDelete",feedItem.getProduct_add_choice_id());
                        }
                    });
                    adb.show();

                }
            });





        }

        private class DeleteItemTask extends AsyncTask<String, Void, String> {

            ProgressDialog pdLoading = new ProgressDialog(getActivity());

            @Override
            protected void onPreExecute() {
                // Create Show ProgressBar
                pdLoading.setMessage("Deleting..");
                pdLoading.setCancelable(false);
                pdLoading.show();

            }

            protected String doInBackground(String... urls) {
                String response = null;
                postHttp http = new postHttp();


                RequestBody formBody = new FormEncodingBuilder()
                        .add("product_add_choice_id", urls[1])
                        .build();


                try {

                    response = http.run(urls[0], formBody);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
//                txtResult.setText(response);
                return response;
            }

            protected void onPostExecute(String jsonString) {
                // Dismiss ProgressBar
                //showData(jsonString);
                pdLoading.dismiss();
                JSONObject object = null;
                new AsyncLoadCat().execute();
                try {
                    object = new JSONObject(jsonString);

                    String status = object.getString("status"); // success
//                    new AsyncLogin().execute();

                    if (status.equals("0")) {
                        String msg = object.getString("msg"); // success
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage(msg)
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //do things

//                                        new AsyncLoadCat().execute();

                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();



                    } else {

                        //String order_id = object.getString("order_id"); // success

                        // keep to var ORderID
                        ///StringUtil.OrderId = order_id;

//                    Intent in = new Intent(ProductActivity.this, ListOrderActivity.class);
//                    startActivity(in);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }


        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            //            protected ImageView imageView;
            protected TextView title_choice;
            protected Button btnEdit,btnChoiceDelete;

            FeedItemDrug feedItem;
            protected EditText input_choice_price;

            public CustomViewHolder(View view) {
                super(view);

                this.title_choice = (TextView) view.findViewById(R.id.txtCheckIn);
                this.btnEdit = (Button) view.findViewById(R.id.btnEdit);
                this.btnChoiceDelete = (Button) view.findViewById(R.id.btnChoiceDelete);
//                this.input_choice_price = (EditText) view.findViewById(R.id.input_choice_price);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

//                        StringUtil.CatId = feedItem.getId();
                        //new AsyncProduct().execute();
                        //Toast.makeText(v.getContext(), "Select is: " + feedItem.getId(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }
    }

    // String
    public class FeedItemDrug {

        private String employee_id;
        private String firstname;
        private String lastname;
        private String tel;
        private String status_name;
        private String product_add_choice_id;
        private String  p_group_id;

        public String getP_group_id() {
            return p_group_id;
        }

        public void setP_group_id(String p_group_id) {
            this.p_group_id = p_group_id;
        }

        public String getProduct_add_choice_id() {
            return product_add_choice_id;
        }

        public void setProduct_add_choice_id(String product_add_choice_id) {
            this.product_add_choice_id = product_add_choice_id;
        }

        public String getEmployee_id() {
            return employee_id;
        }

        public void setEmployee_id(String employee_id) {
            this.employee_id = employee_id;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getStatus_name() {
            return status_name;
        }

        public void setStatus_name(String status_name) {
            this.status_name = status_name;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }
    }



}
