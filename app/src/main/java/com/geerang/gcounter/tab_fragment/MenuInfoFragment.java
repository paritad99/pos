package com.geerang.gcounter.tab_fragment;

import androidx.appcompat.app.AlertDialog;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.geerang.gcounter.Profile;
import com.geerang.gcounter.R;
import com.geerang.gcounter.StringUtil;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class MenuInfoFragment extends Fragment {

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }


    public static MenuInfoFragment newInstance() {
        return new MenuInfoFragment();
    }
    Bitmap FixBitmap;
    byte[] byteArray;
    String ConvertImage;
    URL url;
    private int GALLERY = 1, CAMERA = 2;
    Spinner ddlMenuCategory;
    Button buttonSelect, btnSave;
    ImageView imgStore;
    CheckBox chk_is_recommend;
    EditText input_menu, input_desc, input_price, input_calories, input_time_task;
    String categories_id;
    final ArrayList<HashMap<String, String>> MyArrList = new ArrayList<HashMap<String, String>>();
    HashMap<String, String> map;
    View root;
    StringUtil stringUtil;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_product_info, container, false);
        chk_is_recommend = (CheckBox) root.findViewById(R.id.chk_is_recommend);
        ddlMenuCategory = (Spinner) root.findViewById(R.id.ddlMenuCategory);

        buttonSelect = (Button) root.findViewById(R.id.buttonSelect);
        imgStore = (ImageView) root.findViewById(R.id.imgStore);

        stringUtil = new StringUtil();
        Picasso.with(getActivity())
                .load(StringUtil.URL+"gpos"+
                        stringUtil.removeFirstChar(StringUtil.menu_image_data))
//                .fit()
                .into(imgStore);


        Toast.makeText(getActivity(),StringUtil.URL+"gpos"+ stringUtil.removeFirstChar(StringUtil.menu_image_data),Toast.LENGTH_LONG).show();

        input_menu = (EditText) root.findViewById(R.id.input_menu);
        input_desc = (EditText) root.findViewById(R.id.input_desc);
        input_price = (EditText) root.findViewById(R.id.input_price);
        input_calories = (EditText) root.findViewById(R.id.input_calories);
        input_time_task = (EditText) root.findViewById(R.id.input_time_task);

        input_menu.setText(StringUtil.menu_name);
        input_desc.setText(StringUtil.menu_product_detail);
        input_price.setText(StringUtil.menu_product_price);
        input_calories.setText(StringUtil.menu_calories);
        input_time_task.setText(StringUtil.menu_cookingtime);

        if(StringUtil.menu_is_recommend.equals("1"))
        {
            chk_is_recommend.setChecked(true);
        }else
        {
            chk_is_recommend.setChecked(false);
        }

        if(!StringUtil.menu_image_data.equals(""))
        {
            FixBitmap = getBitmapFromURL(StringUtil.URL+"gpos/"+ stringUtil.removeFirstChar(StringUtil.menu_image_data));
        }else
        {
           Toast.makeText(getActivity(),"Image cant load",Toast.LENGTH_SHORT).show();
        }


        buttonSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPictureDialog();
            }
        });

        btnSave = (Button) root.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new BookingAddTask().execute(StringUtil.URL + "gpos/api/FoodMenuEdit");
            }
        });

        new AsyncLoadCat().execute();

        return  root;
    }

    private class AsyncLoadCat extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(getActivity());
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/MenuCategory/" +Profile.StoreId);
//                response = http.run(StringUtil.URL+"gpos/api/category/1");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    map = new HashMap<String, String>();
                    map.put("MemberID", json_data.getString("categories_id"));
                    map.put("Name", json_data.getString("categories_name"));
                    map.put("Tel", json_data.getString("categories_color"));
                    MyArrList.add(map);

                }


                SimpleAdapter sAdap;
                sAdap = new SimpleAdapter(getActivity(), MyArrList, R.layout.activity_menu_ddl_row,
                        new String[]{"Name"}, new int[]{R.id.ColName});

//                final AlertDialog.Builder viewDetail = new AlertDialog.Builder(FoodMenuAddActivity.this);
                ddlMenuCategory.setAdapter(sAdap);

//                int spinnerPosition = sAdap.getPosition(StringUtil.);

//set the default according to value
//                ddlMenuCategory.setSelection(spinnerPosition);

                ddlMenuCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    public void onItemSelected(AdapterView<?> arg0, View selectedItemView,
                                               int position, long id) {


                        categories_id = MyArrList.get(position).get("MemberID").toString();
                        String sName = MyArrList.get(position).get("Name")
                                .toString();
                        String sTel = MyArrList.get(position).get("Tel")
                                .toString();


                    }

                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub
                        Toast.makeText(getActivity(),
                                "Your Selected : Nothing",
                                Toast.LENGTH_SHORT).show();
                    }


                });


            } catch (JSONException e) {
                Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }




    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getActivity());
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Photo Gallery",
                "Camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    FixBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), contentURI);
                    // String path = saveImage(bitmap);
                    //Toast.makeText(MainActivity.this, "Image Saved!", Toast.LENGTH_SHORT).show();
                    imgStore.setImageBitmap(FixBitmap);
//                    UploadImageOnServerButton.setVisibility(View.VISIBLE);

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {
            FixBitmap = (Bitmap) data.getExtras().get("data");
            imgStore.setImageBitmap(FixBitmap);
//            UploadImageOnServerButton.setVisibility(View.VISIBLE);
            //  saveImage(thumbnail);
            //Toast.makeText(ShadiRegistrationPart5.this, "Image Saved!", Toast.LENGTH_SHORT).show();
        }
    }


    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    private class BookingAddTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(getActivity());

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tBooking in progress...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            try {
//            FixBitmap.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
//
////            ByteArrayOutputStream stream = new ByteArrayOutputStream();
////            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
//            byteArray = byteArrayOutputStream.toByteArray();

//            Bitmap bmp = intent.getExtras().get("data");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                FixBitmap.compress(Bitmap.CompressFormat.PNG, 40, stream);
                byteArray = stream.toByteArray();
                FixBitmap.recycle();
                ConvertImage = Base64.encodeToString(byteArray, Base64.DEFAULT);

            } catch (Exception e) {

            }

            String is_recommend = "";
            if(!chk_is_recommend.isChecked())
            {
                is_recommend = "0";
            }else
            {
                is_recommend = "1";
            }
            RequestBody formBody = new FormEncodingBuilder()
                    .add("image_data", ConvertImage)
                    .add("store_id", Profile.StoreId)
                    .add("categories_id", categories_id)
                    .add("product_name", input_menu.getText().toString())
                    .add("product_detail", input_desc.getText().toString())
                    .add("product_price", input_price.getText().toString())
                    .add("cookingtime", input_time_task.getText().toString())
                    .add("calories", input_calories.getText().toString())
                    .add("is_recommend", is_recommend)
                    .build();

            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 5) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Now user should be able to use camera

            } else {

                Toast.makeText(getActivity(), "Unable to use Camera..Please Allow us to use Camera", Toast.LENGTH_LONG).show();

            }
        }
    }

}
