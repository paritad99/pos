package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.PictureDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FoodMenuChoiceAddActivity extends AppCompatActivity {

    EditText input_choice_name, input_choice_order;

    EditText input_group_choice, input_choice_desc, input_choice_min, input_choice_max;
    //ListView lisView1;
    Button buttonSelect, btnGotoAddChoice, btnOldChoice;
    ArrayList<HashMap<String, String>> MyArrListChoice;
    HashMap<String, String> map_choice;
    String input_product_id = "";
    String p_group_id = "";
    Profile profile;
    private List<FeedItemDrug> feedsList;
    LinearLayout llSave, llchoice;
    private CategoriesRecyclerAdapter mAdapter;
    static String choice_id = "";
    static String group_choice_id = "";
    RecyclerView rvCatagory;

    TextView txttotal_choice;
    Button btnNewRowChoice;
    boolean is_save = false;
    Button btnSave;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_menu_choice_add);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        profile = new Profile(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        Intent intent = getIntent();
        input_product_id = intent.getStringExtra("input_product_id");
        Log.d("product_id", input_product_id);


        profile = new Profile(this);
        txttotal_choice = (TextView) findViewById(R.id.total_choice);

        input_group_choice = (EditText) findViewById(R.id.input_group_choice);
        input_choice_min = (EditText) findViewById(R.id.input_choice_min);
        input_choice_max = (EditText) findViewById(R.id.input_choice_max);

        btnOldChoice = (Button) findViewById(R.id.btnOldChoice);
        btnOldChoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //new AsyncLoadChoice().execute(group_choice_id);
                Intent in = new Intent(FoodMenuChoiceAddActivity.this, SelectGroupChoiceActivity.class);
                in.putExtra("input_product_id", input_product_id);
                startActivity(in);

            }
        });

        llSave = (LinearLayout) findViewById(R.id.llSave);
        llchoice = (LinearLayout) findViewById(R.id.llchoice);
        rvCatagory = (RecyclerView) findViewById(R.id.recyclerView);


        btnNewRowChoice = (Button) findViewById(R.id.btnNewRowChoice);
        llchoice.setVisibility(View.GONE);

//        StringUtil.StateChoice = "ADDED";
//        if(StringUtil.StateChoice.equals("ADDED"))
//        {
//            btnNewRowChoice.setEnabled(true);
//        }else
//        {
//            btnNewRowChoice.setEnabled(false);
//        }

        Button btnCancel = (Button) findViewById(R.id.btnCancel);
        btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (input_group_choice.length() > 2) {
                    if (input_choice_min.length() > 0) {
                        if (input_choice_max.length() > 0) {

                            llchoice.setVisibility(View.VISIBLE);

                            if (StringUtil.p_group_id.equals("")) {
                                new GroupChoiceAddTask().execute(StringUtil.URL + "gpos/api/GroupChoiceAdd");

                            } else {
                                new GroupChoiceAddTask().execute(StringUtil.URL + "gpos/api/GroupChoiceUpdateAdd", StringUtil.p_group_id);

                            }

                        } else {
                            input_choice_max.setError("กรุณาระบุ Max Choice");
                        }
                    } else {
                        input_choice_min.setError("กรุณาระบุ Min Choice!");
                    }

                } else {
                    input_group_choice.setError("กรุณาระบุ Group Choice!");
                }


            }
        });


        // MyArrListChoice = new ArrayList<HashMap<String, String>>();
        btnNewRowChoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                EditText input_choice = (EditText) findViewById(R.id.input_choice);
                EditText input_price = (EditText) findViewById(R.id.input_price);
                String Choice = input_choice.getText().toString();
                String Price = input_price.getText().toString();
                // เพิ่ม Choice ทีละรายการ

                if (input_choice.length() > 0) {
                    // เพิ่ม Choice ทีละรายการ
                    new ChoiceAddTask().execute(StringUtil.URL + "gpos/api/ChoiceAdd", Choice, Price, StringUtil.p_group_id);

                    input_choice.setText("");
                    input_price.setText("");

                } else {
                    input_choice.setError("กรุณาระบุ Choice ");
                    input_price.setError("กรุณาระบุ ราคา");
                }




            }
        });

    }

    private class GroupChoiceAddTask extends AsyncTask<String, Void, String> {

//        ProgressDialog pdLoading = new ProgressDialog(FoodMenuAddActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
//            pdLoading.setMessage("\tSaving...");
//            pdLoading.setCancelable(false);
//            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            // เพิ่ม GroupChoice
            String group_choice = input_group_choice.getText().toString();
            String choice_min = input_choice_min.getText().toString();
            String choice_max = input_choice_max.getText().toString();

            RequestBody formBody;
            if (StringUtil.p_group_id.equals("")) {

                formBody = new FormEncodingBuilder()
                        .add("UserId", profile.UserId)
                        .add("StoreId", profile.StoreId)
                        .add("name", group_choice)
                        .add("min", choice_min)
                        .add("max", choice_max)
                        .add("description", "")
                        .add("product_id", input_product_id)
                        .build();

            } else {
                formBody = new FormEncodingBuilder()
                        .add("UserId", profile.UserId)
                        .add("StoreId", profile.StoreId)
                        .add("name", group_choice)
                        .add("min", choice_min)
                        .add("max", choice_max)
                        .add("description", "")
                        .add("product_id", input_product_id)
                        .add("p_group_id", urls[1])
                        .build();
            }


            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
//            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(FoodMenuChoiceAddActivity.this);
                    builder.setMessage("ไม่สามารถบันทึกข้อมูลได้")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    //group_choice_id = msg;
                    StringUtil.p_group_id = msg;
                    new AsyncLoadCat().execute();

//                    if (group_choice_id != "") {
//                        llchoice.setVisibility(View.VISIBLE);
//                        new AsyncLoadCat().execute();
//
//                    }
//
//                    if (is_save == true) {
//                        btnSave.setEnabled(false);
//                    } else {
//                        btnSave.setEnabled(true);
//                    }

                    AlertDialog.Builder builder = new AlertDialog.Builder(FoodMenuChoiceAddActivity.this);
                    builder.setMessage("บันทึกเรียบร้อยแล้ว")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
//                                    finish();
                                    // new AsyncLoadChoice().execute();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        input_group_choice.setText(StringUtil.GroupName + "");
//        input_choice_desc.setText(StringUtil.GroupDescription+"");
        input_choice_min.setText(StringUtil.GroupMin + "");
        input_choice_max.setText(StringUtil.GroupMax + "");

        group_choice_id = StringUtil.p_group_id;
        if (StringUtil.p_group_id != "") {
            llchoice.setVisibility(View.VISIBLE);
            new AsyncLoadCat().execute();
        }

        new AsyncLoadCat().execute();

    }

    private class ChoiceAddTask extends AsyncTask<String, Void, String> {
        //        ProgressDialog pdLoading = new ProgressDialog(FoodMenuAddActivity.this);
        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
//            pdLoading.setMessage("\tกำลังบันทึก...");
//            pdLoading.setCancelable(false);
//            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("UserId", profile.UserId + "")
                    .add("choice_name", urls[1])
                    .add("choice_price", urls[2])
                    .add("p_group_id", StringUtil.p_group_id)
                    .add("StoreId", profile.StoreId)
                    .add("BranchId", profile.BranchId)

                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
//            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(FoodMenuChoiceAddActivity.this);
                    builder.setMessage("บันทึกข้อมูลเรียบร้อย")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
//                                    finish();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(FoodMenuChoiceAddActivity.this);
                    builder.setMessage("บันทึกข้อมูลเรียบร้อย")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    //                                    finish();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }

                //CreateChoice();
                new AsyncLoadCat().execute();


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    // เมื่อมีที่สร้างแล้วให้โหลดมาแสดงเลย
    private class AsyncLoadChoice extends AsyncTask<String, String, String> {
        //        ProgressDialog pdLoading = new ProgressDialog(FoodMenuAddActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //this method will be running on UI thread
//            pdLoading.setMessage("\tLoading...");
//            pdLoading.setCancelable(false);
//            pdLoading.show();
        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/GroupChoiceLoadForAdd/" + urls[0]);
//                response = http.run(StringUtil.URL+"gpos/api/category/1");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            //this method will be running on UI thread
            try {

                JSONArray jArray = new JSONArray(result);
                //MyArrListChoice = new ArrayList<HashMap<String, String>>();

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    input_group_choice.setText(json_data.getString("group_name"));
                    input_choice_min.setText(json_data.getString("min"));
                    input_choice_max.setText(json_data.getString("max"));
                    // choice name group_choice
                    MyArrListChoice = new ArrayList<HashMap<String, String>>();
                    JSONArray jArray_2 = new JSONArray(json_data.getString("group_choice"));

                    for (int j = 0; j < jArray_2.length(); j++) {
                        JSONObject json_data_2 = jArray_2.getJSONObject(j);
                        //int r = MyArrListChoice.size() + 1;product_choice_id
                        map_choice = new HashMap<String, String>();
                        map_choice.put("ID", json_data_2.getString("product_choice_name"));
                        //map_choice.put("product_choice_name", json_data_2.getString("product_choice_name"));
                        map_choice.put("Code", json_data_2.getString("choice_price"));
                        MyArrListChoice.add(map_choice);


                    }


                }


            } catch (JSONException e) {
                System.out.println("Error 542 " + e.toString());
            }

        }

    }

    private class AsyncLoadCat extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(FoodMenuChoiceAddActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/MenuGroupChoicePrice/" + StringUtil.p_group_id);
//                response = http.run(StringUtil.URL+"gpos/api/category/1");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;

        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();

            feedsList = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);
                int total_choice = 0;
                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    total_choice++;

                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemDrug item = new FeedItemDrug();
                    item.setEmployee_id(json_data.getString("p_group_add_id"));
                    item.setFirstname(json_data.getString("product_choice_name"));
                    item.setLastname(json_data.getString("choice_price"));

                    feedsList.add(item);
                }

                if (total_choice > 15) {
                    btnNewRowChoice.setEnabled(false);
                } else {
                    btnNewRowChoice.setEnabled(true);
                }

                txttotal_choice.setText(total_choice + "");

                // Setup and Handover data to recyclerview

                mAdapter = new CategoriesRecyclerAdapter(FoodMenuChoiceAddActivity.this, feedsList);
                rvCatagory.setAdapter(mAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(FoodMenuChoiceAddActivity.this, 2);
                rvCatagory.setLayoutManager(mLayoutManager);
                rvCatagory.setItemAnimator(new DefaultItemAnimator());
                rvCatagory.setItemAnimator(new DefaultItemAnimator());

            } catch (JSONException e) {
                Toast.makeText(FoodMenuChoiceAddActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    public class CategoriesRecyclerAdapter extends RecyclerView.Adapter<CategoriesRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemDrug> feedItemList;
        private Context mContext;

        public CategoriesRecyclerAdapter(Context context, List<FeedItemDrug> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;
        }

        @Override
        public CategoriesRecyclerAdapter.CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.choice_list_row, null);

            CategoriesRecyclerAdapter.CustomViewHolder viewHolder = new CategoriesRecyclerAdapter.CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(CategoriesRecyclerAdapter.CustomViewHolder customViewHolder, int i) {
            final FeedItemDrug feedItem = feedItemList.get(i);
            customViewHolder.feedItem = feedItem;


            //Setting text view title
            customViewHolder.title_choice.setText(Html.fromHtml("" + feedItem.getFirstname()));
            customViewHolder.txtPrice.setText(Html.fromHtml("" + feedItem.getLastname()));

            customViewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent in = new Intent(mContext, ChoiceEditActivity.class);
                    in.putExtra("cid", feedItem.getEmployee_id());
                    in.putExtra("title_choice", feedItem.getFirstname());
                    in.putExtra("title_choice_order", feedItem.getLastname());
                    startActivity(in);

                }
            });


            customViewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    choice_id = feedItem.getEmployee_id();
                    new ChoiceDeleteTask().execute(StringUtil.URL + "gpos/api/ChoiceGroupManage_ChoiceDelete");

                }
            });


        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            //            protected ImageView imageView;
            protected TextView title_choice, txtPrice;
            protected Button btnEdit, btnDelete;

            FeedItemDrug feedItem;

            public CustomViewHolder(View view) {
                super(view);

                this.title_choice = (TextView) view.findViewById(R.id.txtCheckIn);
                this.txtPrice = (TextView) view.findViewById(R.id.txtPrice);

                this.btnEdit = (Button) view.findViewById(R.id.btnEdit);
                this.btnDelete = (Button) view.findViewById(R.id.btnDelete);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

//                        StringUtil.CatId = feedItem.getId();
                        //new AsyncProduct().execute();
                        //Toast.makeText(v.getContext(), "Select is: " + feedItem.getId(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }
    }


    private class ChoiceDeleteTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(FoodMenuChoiceAddActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tChoice Delete in progress...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();


            RequestBody formBody = new FormEncodingBuilder()
                    .add("UserId", Profile.UserId + "")
                    .add("ChoiceDeleteID", choice_id)
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(FoodMenuChoiceAddActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    new AsyncLoadCat().execute();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(FoodMenuChoiceAddActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
//                                    finish();
                                    new AsyncLoadCat().execute();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


    public class FeedItemDrug {

        private String employee_id;
        private String firstname;
        private String lastname;
        private String tel;
        private String status_name;

        public String getEmployee_id() {
            return employee_id;
        }

        public void setEmployee_id(String employee_id) {
            this.employee_id = employee_id;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getStatus_name() {
            return status_name;
        }

        public void setStatus_name(String status_name) {
            this.status_name = status_name;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }
    }

    // String
    public class FeedItemChoice {

        private String employee_id;
        private String firstname;
        private String lastname;
        private String tel;
        private String status_name;
        private String group_name;
        private String min;
        private String max;

        public String getGroup_name() {
            return group_name;
        }

        public void setGroup_name(String group_name) {
            this.group_name = group_name;
        }

        public String getMin() {
            return min;
        }

        public String getMax() {
            return max;
        }

        public void setMax(String max) {
            this.max = max;
        }

        public void setMin(String min) {
            this.min = min;
        }


        public String getEmployee_id() {
            return employee_id;
        }

        public void setEmployee_id(String employee_id) {
            this.employee_id = employee_id;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getStatus_name() {
            return status_name;
        }

        public void setStatus_name(String status_name) {
            this.status_name = status_name;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }
    }
}
