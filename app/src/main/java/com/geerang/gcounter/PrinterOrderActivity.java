package com.geerang.gcounter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.StrictMode;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class PrinterOrderActivity extends AppCompatActivity {

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {

//            if (Profile.Position.equals("2")) {
//                StringUtil.employee_status = "ผู้จัดการร้าน";
//                Intent in = new Intent(SelectMenuToTableActivity.this, ManagerMainActivity.class);
//                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                startActivity(in);
//                finish();
//            } else {
            finish();
//            }

        }
        return super.onOptionsItemSelected(item);
    }


    Spinner ddlMenuCategory, ddlKitchen, ddlReport;
    ArrayList<HashMap<String, String>> MyArrList;
    HashMap<String, String> map;

    Profile profile;

    Context context;
    String categories_id;

    Button btnPrinterSearch, btnSubmit;

    final String KEY_Autoprint = "Autoprint";
    final String KEY_PrinterMain = "PrinterMain";
    final String KEY_Printer1 = "Printer1";
    final String KEY_Printer2 = "Printer2";
    final String KEY_Printer3 = "Printer3";
    final String KEY_Printer4 = "Printer4";

    SharedPreferences sp;
    SharedPreferences.Editor editor;
    final String PREF_NAME = "PrinterPreferences";

    EditText input_printer_title, input_printer_address, input_printer_mac_address;

    String OpenPrinter = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_printer_order);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        context = this;
        profile = new Profile(context);
        sp = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sp.edit();

        input_printer_title = (EditText) findViewById(R.id.input_printer_title);
        input_printer_address = (EditText) findViewById(R.id.input_printer_address);
        input_printer_mac_address = (EditText) findViewById(R.id.input_printer_mac_address);

        btnPrinterSearch = (Button) findViewById(R.id.btnPrinterSearch);
        btnPrinterSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(context, PrintDiscoveryActivity.class);
                startActivity(in);
            }
        });

        // initiate a Switch
        final Switch simpleSwitch = (Switch) findViewById(R.id.swPrint);
        // check current state of a Switch (true or false).
        Boolean switchState = simpleSwitch.isChecked();
        simpleSwitch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (simpleSwitch.isChecked()) {
                    //editor.putString(KEY_Autoprint, "ON");
                    OpenPrinter = "ON";
                    Toast.makeText(context, "Printer is on", Toast.LENGTH_LONG).show();
                } else {
                    OpenPrinter = "OFF";
                    //editor.putString(KEY_Autoprint, "OFF");
                    Toast.makeText(context, "Printer is Off", Toast.LENGTH_LONG).show();
                }
                editor.commit();

            }
        });

        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String printer_title = input_printer_title.getText().toString();
                String printer_address = input_printer_address.getText().toString();
                String printer_mac_address = input_printer_mac_address.getText().toString();

                // status,
                editor.putString(KEY_PrinterMain, OpenPrinter + "," + printer_title + "," + printer_address + "," + printer_mac_address + "," + categories_id);
                editor.commit();
                finish();

            }
        });

        ddlKitchen = (Spinner) findViewById(R.id.ddlKitchen);
        new AsyncLoadKitchenCat().execute();


    }

    @Override
    protected void onResume() {
        super.onResume();

        input_printer_title.setText(StringUtil.printer_name);
        input_printer_address.setText(StringUtil.printer_address);
        input_printer_mac_address.setText("");

    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    private class AsyncLoadKitchenCat extends AsyncTask<String, String, String> {
        //        ProgressDialog pdLoading = new ProgressDialog(FoodMenuAddActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
//            pdLoading.setMessage("\tกำลังโหลดข้อมูล...");
//            pdLoading.setCancelable(false);
//            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/KitchenCategories/" + profile.StoreId);
//                response = http.run(StringUtil.URL+"gpos/api/category/1");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

//            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);
                MyArrList = new ArrayList<HashMap<String, String>>();
                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    map = new HashMap<String, String>();
                    map.put("MemberID", json_data.getString("id_kit"));
                    map.put("Name", json_data.getString("kit_cat"));
                    map.put("Tel", json_data.getString("id_printer"));
                    MyArrList.add(map);

                }


                SimpleAdapter sAdap;
                sAdap = new SimpleAdapter(context, MyArrList, R.layout.activity_menu_ddl_row,
                        new String[]{"Name"}, new int[]{R.id.ColName});

//                final AlertDialog.Builder viewDetail = new AlertDialog.Builder(FoodMenuAddActivity.this);
                ddlKitchen.setAdapter(sAdap);
                ddlKitchen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    public void onItemSelected(AdapterView<?> arg0, View selectedItemView,
                                               int position, long id) {


                        categories_id = MyArrList.get(position).get("MemberID")
                                .toString();
                        String sName = MyArrList.get(position).get("Name")
                                .toString();
                        String sTel = MyArrList.get(position).get("Tel")
                                .toString();


                    }

                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub
                        Toast.makeText(context,
                                "Your Selected : Nothing",
                                Toast.LENGTH_SHORT).show();
                    }


                });


            } catch (JSONException e) {
                Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

}