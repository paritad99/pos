package com.geerang.gcounter;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class StoreInfoActivity extends AppCompatActivity {

    String store_name;
    String store_description;
    String business_type_name;
    String store_address;
    String tax;
    String phone;
    String store_image;


    EditText input_Storename, input_Description, input_Address, input_Tax, input_phone;
    Button btnEditStore,buttonSelect;

    ImageView imgStore;

    Bitmap FixBitmap;

    String ImageTag = "image_tag";

    String ImageName = "image_data";

    ProgressDialog progressDialog;

    ByteArrayOutputStream byteArrayOutputStream;

    byte[] byteArray;

    String ConvertImage;

    String GetImageNameFromEditText;

    HttpURLConnection httpURLConnection;

    URL url;

    OutputStream outputStream;

    BufferedWriter bufferedWriter;

    int RC;

    BufferedReader bufferedReader;

    StringBuilder stringBuilder;

    boolean check = true;

    private int GALLERY = 1, CAMERA = 2;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    final String PREF_NAME = "LoginPreferences";
    final String KEY_USERNAME = "Username";
    final String KEY_USERID = "UserId";
    final String KEY_BranchId = "BranchId";
    final String KEY_StoreId = "StoreId";
    final String KEY_Status = "Status";
    final String KEY_REMEMBER = "RememberUsername";
    final String KEY_business_type_id = "business_type_id";
    final String KEY_StoreName = "StoreName";
    final String KEY_BranchName = "BranchName";
    final String KEY_Position = "Position";
    final String KEY_StoreExp = "StoreExp";
    SharedPreferences sp;
    SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_info);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // add back arrow to toolbar
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        if (ContextCompat.checkSelfPermission(StoreInfoActivity.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.CAMERA},
                        5);
            }
        }

        sp = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sp.edit();

        input_Storename = (EditText) findViewById(R.id.input_Storename);
        input_Description = (EditText) findViewById(R.id.input_Description);
        input_Address = (EditText) findViewById(R.id.input_Address);
        input_Tax = (EditText) findViewById(R.id.input_Tax);
        input_phone = (EditText) findViewById(R.id.input_phone);

        imgStore = (ImageView) findViewById(R.id.imgStore);

        btnEditStore = (Button) findViewById(R.id.btnEditStore);
        btnEditStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                new BookingAddTask().execute();
                new BookingAddTask().execute(StringUtil.URL + "gpos/api/StoreUpdate");
            }
        });

        buttonSelect = (Button) findViewById(R.id.buttonSelect);
        buttonSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPictureDialog();
            }
        });




        Intent myIntent = getIntent(); // gets the previously created intent
        store_name = myIntent.getStringExtra("store_name"); // will return "FirstKeyValue"
        store_description = myIntent.getStringExtra("store_description"); // will return "FirstKeyValue"
        business_type_name = myIntent.getStringExtra("business_type_name"); // will return "FirstKeyValue"
        store_address = myIntent.getStringExtra("store_address"); // will return "FirstKeyValue"
        tax = myIntent.getStringExtra("tax");
        phone = myIntent.getStringExtra("phone");
        store_image = myIntent.getStringExtra("store_image");

        StringUtil stringUtil = new StringUtil();
        Picasso.with(StoreInfoActivity.this)
                .load(StringUtil.URL+"gpos"+
                        stringUtil.removeFirstChar(store_image))
//                .fit()
                .into(imgStore);



        SetText(input_Storename,store_name);
        SetText(input_Description,store_description);
        SetText(input_Address,store_address);
        SetText(input_Tax,tax);
        SetText(input_phone,phone);

    }

    @Override
    protected void onResume() {
        super.onResume();

        StringUtil stringUtil = new StringUtil();
        Picasso.with(StoreInfoActivity.this)
                .load(StringUtil.URL+"gpos"+
                        stringUtil.removeFirstChar(store_image))
//                .fit()
                .into(imgStore);

    }

    private void showPictureDialog(){
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Photo Gallery",
                "Camera" };
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }
    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    FixBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    // String path = saveImage(bitmap);
                    //Toast.makeText(MainActivity.this, "Image Saved!", Toast.LENGTH_SHORT).show();
                    imgStore.setImageBitmap(FixBitmap);
//                    UploadImageOnServerButton.setVisibility(View.VISIBLE);

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(StoreInfoActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {
            FixBitmap = (Bitmap) data.getExtras().get("data");
            imgStore.setImageBitmap(FixBitmap);
//            UploadImageOnServerButton.setVisibility(View.VISIBLE);
            //  saveImage(thumbnail);
            //Toast.makeText(ShadiRegistrationPart5.this, "Image Saved!", Toast.LENGTH_SHORT).show();
        }
    }


    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    private class BookingAddTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(StoreInfoActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("กำลังบันทึกข้อมูล...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            try {
//            FixBitmap.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
//
////            ByteArrayOutputStream stream = new ByteArrayOutputStream();
////            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
//            byteArray = byteArrayOutputStream.toByteArray();

//            Bitmap bmp = intent.getExtras().get("data");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                FixBitmap.compress(Bitmap.CompressFormat.PNG, 40, stream);
                byteArray = stream.toByteArray();
                FixBitmap.recycle();
                ConvertImage = Base64.encodeToString(byteArray, Base64.DEFAULT);

            } catch (Exception e) {

            }


            RequestBody formBody = new FormEncodingBuilder()
                    .add("image_data", ConvertImage)//ConvertImage)
                    .add("store_name", input_Storename.getText().toString())
                    .add("store_description", input_Description.getText().toString())
                    .add("store_id", Profile.StoreId)
                    .add("store_address", input_Address.getText().toString())
                    .add("tax", input_Tax.getText().toString())
                    .add("phone", input_phone.getText().toString())
                    .build();



            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                String msg = object.getString("msg"); // success

                StringUtil.product_menu_id = msg;

                editor = sp.edit();
                editor.putString(KEY_USERNAME, "");
                editor.putString(KEY_USERID, "");
                editor.putString(KEY_StoreId, "");
                editor.putString(KEY_BranchId, "");
                editor.putString(KEY_Status, "");
                editor.putString(KEY_REMEMBER, "");
                editor.putString(KEY_business_type_id, "");
                editor.putString(KEY_StoreName, "");
                editor.putString(KEY_BranchName, "");
                editor.putString(KEY_Position, "");
                editor.commit();

                Intent in = new Intent(StoreInfoActivity.this, LoginActivity.class);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(in);
                finish();


                Toast.makeText(StoreInfoActivity.this,"บันทึกข้อมูลเรียบร้อยแล้ว"+msg,Toast.LENGTH_SHORT).show();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 5) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Now user should be able to use camera

            }
            else {

                Toast.makeText(StoreInfoActivity.this, "Unable to use Camera..Please Allow us to use Camera", Toast.LENGTH_LONG).show();

            }
        }
    }
    public void SetText(EditText ed,String val)
    {
        ed.setText(val);
    }



}
