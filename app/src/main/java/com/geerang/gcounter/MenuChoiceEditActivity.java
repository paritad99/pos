package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.PictureDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.StrictMode;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MenuChoiceEditActivity extends AppCompatActivity {
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    Button btnOldChoice;
    Profile profile;
    //RecyclerView recyclerview_choice;
    static String choice_id, group_choice_id;

    EditText input_group_choice, input_choice_min, input_choice_max;

    String input_product_id = "";
    String p_group_id = "";
    String title_choice = "";
    String title_choice_order = "";

    String cmin = "";
    String cmax = "";
    RecyclerView rvCatagory;

    private CategoriesRecyclerAdapter mAdapter;

    Button btnNewRowChoice;
    boolean is_save = false;
    Button btnSave;
    TextView txttotal_choice;
    private List<FeedItemDrug> feedsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_choice_edit);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        profile = new Profile(this);

        Intent intent = getIntent();
        input_product_id = intent.getStringExtra("input_product_id");
        p_group_id = intent.getStringExtra("p_group_id");
        title_choice = intent.getStringExtra("title_choice");
        title_choice_order = intent.getStringExtra("title_choice_order");

        cmin = intent.getStringExtra("cmin");
        cmax = intent.getStringExtra("cmax");

        rvCatagory = (RecyclerView) findViewById(R.id.recyclerView);
        txttotal_choice = (TextView) findViewById(R.id.total_choice);

//        Log.d("product_id", input_product_id);

        input_group_choice = (EditText) findViewById(R.id.input_group_choice);
        input_group_choice.setText(title_choice);

        input_choice_min = (EditText) findViewById(R.id.input_choice_min);
        input_choice_min.setText(cmin);

        input_choice_max = (EditText) findViewById(R.id.input_choice_max);
        input_choice_max.setText(cmax);


        btnOldChoice = (Button) findViewById(R.id.btnOldChoice);

        btnNewRowChoice = (Button) findViewById(R.id.btnNewRowChoice);
        Button btnCancel = (Button) findViewById(R.id.btnCancel);
        btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (input_group_choice.length() > 2) {
                    if (input_choice_min.length() > 0) {
                        if (input_choice_max.length() > 0) {
                            new GroupChoiceAddTask().execute(StringUtil.URL + "gpos/api/GroupChoiceEdit");
                        } else {
                            input_choice_max.setError("กรุณาระบุ Max Choice");
                        }
                    } else {
                        input_choice_min.setError("กรุณาระบุ Min Choice!");
                    }

                } else {
                    input_group_choice.setError("กรุณาระบุ Group Choice!");
                }

            }
        });


        // MyArrListChoice = new ArrayList<HashMap<String, String>>();
        btnNewRowChoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                EditText input_choice = (EditText) findViewById(R.id.input_choice);
                EditText input_price = (EditText) findViewById(R.id.input_price);
                String Choice = input_choice.getText().toString();
                String Price = input_price.getText().toString();

                if (input_choice.length() > 0) {
                    // เพิ่ม Choice ทีละรายการ
                    new ChoiceAddTask().execute(StringUtil.URL + "gpos/api/ChoiceAdd", Choice, Price);

                    input_choice.setText("");
                    input_price.setText("");

                } else {
                    input_choice.setError("กรุณาระบุ Choice ");
                    input_price.setError("กรุณาระบุ ราคา");
                }

            }
        });

//        new AsyncCheckGroupChoiceName().execute(
//                StringUtil.URL + "gpos/api/CheckChoiceGroupName"
//                , input_group_choice.getText().toString()
//        );

        //new AsyncLoadChoice().execute(group_choice_id);
        new AsyncLoadCat().execute();

    }

    @Override
    protected void onResume() {
        super.onResume();

        new AsyncLoadCat().execute();
    }

    private class AsyncLoadCat extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(MenuChoiceEditActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/MenuGroupChoicePrice/" + p_group_id);
//                response = http.run(StringUtil.URL+"gpos/api/category/1");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;

        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();

            feedsList = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);
                int total_choice = 0;
                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    total_choice++;

                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemDrug item = new FeedItemDrug();
                    item.setEmployee_id(json_data.getString("p_group_add_id"));
                    item.setFirstname(json_data.getString("product_choice_name"));
                    item.setLastname(json_data.getString("choice_price"));

                    feedsList.add(item);
                }

                if (total_choice > 15) {
                    btnNewRowChoice.setEnabled(false);
                } else {
                    btnNewRowChoice.setEnabled(true);
                }

                txttotal_choice.setText(total_choice + "");

                // Setup and Handover data to recyclerview

                mAdapter = new CategoriesRecyclerAdapter(MenuChoiceEditActivity.this, feedsList);
                rvCatagory.setAdapter(mAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(MenuChoiceEditActivity.this, 2);
                rvCatagory.setLayoutManager(mLayoutManager);
                rvCatagory.setItemAnimator(new DefaultItemAnimator());
                rvCatagory.setItemAnimator(new DefaultItemAnimator());

            } catch (JSONException e) {
                Toast.makeText(MenuChoiceEditActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }


    public class CategoriesRecyclerAdapter extends RecyclerView.Adapter<CategoriesRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemDrug> feedItemList;
        private Context mContext;

        public CategoriesRecyclerAdapter(Context context, List<FeedItemDrug> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;
        }

        @Override
        public CategoriesRecyclerAdapter.CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.choice_list_row, null);

            CategoriesRecyclerAdapter.CustomViewHolder viewHolder = new CategoriesRecyclerAdapter.CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(CategoriesRecyclerAdapter.CustomViewHolder customViewHolder, int i) {
            final FeedItemDrug feedItem = feedItemList.get(i);
            customViewHolder.feedItem = feedItem;


            //Setting text view title
            customViewHolder.title_choice.setText(Html.fromHtml("" + feedItem.getFirstname()));
            customViewHolder.txtPrice.setText(Html.fromHtml("" + feedItem.getLastname()));

            customViewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent in = new Intent(mContext, ChoiceEditActivity.class);
                    in.putExtra("cid", feedItem.getEmployee_id());
                    in.putExtra("title_choice", feedItem.getFirstname());
                    in.putExtra("title_choice_order", feedItem.getLastname());
                    startActivity(in);

                }
            });


            customViewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    choice_id = feedItem.getEmployee_id();
                    new ChoiceDeleteTask().execute(StringUtil.URL + "gpos/api/ChoiceGroupManage_ChoiceDelete", choice_id);

                }
            });


        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            //            protected ImageView imageView;
            protected TextView title_choice, txtPrice;
            protected Button btnEdit, btnDelete;

            FeedItemDrug feedItem;

            public CustomViewHolder(View view) {
                super(view);

                this.title_choice = (TextView) view.findViewById(R.id.txtCheckIn);
                this.txtPrice = (TextView) view.findViewById(R.id.txtPrice);

                this.btnEdit = (Button) view.findViewById(R.id.btnEdit);
                this.btnDelete = (Button) view.findViewById(R.id.btnDelete);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

//                        StringUtil.CatId = feedItem.getId();
                        //new AsyncProduct().execute();
                        //Toast.makeText(v.getContext(), "Select is: " + feedItem.getId(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }
    }

    private class AsyncLoadChoice extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(MenuChoiceEditActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/GroupChoiceLoadForAdd/" + urls[0]);
//                response = http.run(StringUtil.URL+"gpos/api/category/1");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();

            feedsList = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemDrug item = new FeedItemDrug();
                    item.setEmployee_id(json_data.getString("p_group_add_id"));
                    item.setFirstname(json_data.getString("product_choice_name"));
                    item.setLastname(json_data.getString("choice_price"));

                    feedsList.add(item);
                }

                // Setup and Handover data to recyclerview

                mAdapter = new CategoriesRecyclerAdapter(MenuChoiceEditActivity.this, feedsList);
                rvCatagory.setAdapter(mAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(MenuChoiceEditActivity.this, 2);
                rvCatagory.setLayoutManager(mLayoutManager);
                rvCatagory.setItemAnimator(new DefaultItemAnimator());
                rvCatagory.setItemAnimator(new DefaultItemAnimator());

            } catch (JSONException e) {
                Toast.makeText(MenuChoiceEditActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    // String
    public class FeedItemChoice {

        private String employee_id;
        private String firstname;
        private String lastname;
        private String tel;
        private String status_name;

        public String getEmployee_id() {
            return employee_id;
        }

        public void setEmployee_id(String employee_id) {
            this.employee_id = employee_id;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getStatus_name() {
            return status_name;
        }

        public void setStatus_name(String status_name) {
            this.status_name = status_name;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }
    }


    private class AsyncCheckGroupChoiceName extends AsyncTask<String, Void, String> {

//        ProgressDialog pdLoading = new ProgressDialog(FoodMenuAddActivity.this);

        @Override
        protected void onPreExecute() {
//             Create Show ProgressBar
//            pdLoading.setMessage("\tกำลังตรวจสอบข้อมูลซ้ำ...");
//            pdLoading.setCancelable(false);
//            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("GroupChoiceName ", urls[1] + "")
                    .add("store_id ", profile.StoreId)
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
//            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);
                String status = object.getString("status"); // success
                p_group_id = object.getString("p_group_id"); // success
                new AsyncLoadChoice().execute(p_group_id);

                if (jsonString.length() > 0) {
                    btnOldChoice.setVisibility(View.VISIBLE);
                    btnOldChoice.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                        }
                    });

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class GroupChoiceAddTask extends AsyncTask<String, Void, String> {

//        ProgressDialog pdLoading = new ProgressDialog(FoodMenuAddActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
//            pdLoading.setMessage("\tSaving...");
//            pdLoading.setCancelable(false);
//            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            // เพิ่ม GroupChoice
            String group_choice = input_group_choice.getText().toString();
            String choice_min = input_choice_min.getText().toString();
            String choice_max = input_choice_max.getText().toString();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("description", "")
                    .add("name", group_choice)
                    .add("min", choice_min)
                    .add("max", choice_max)
                    .add("p_group_id", p_group_id)
                    .build();

            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
//            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuChoiceEditActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    group_choice_id = msg;

                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuChoiceEditActivity.this);
                    builder.setMessage("บันทึกเรียบร้อยแล้ว")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    finish();

                                    // new AsyncLoadChoice().execute();

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class ChoiceAddTask extends AsyncTask<String, Void, String> {
        //        ProgressDialog pdLoading = new ProgressDialog(FoodMenuAddActivity.this);
        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
//            pdLoading.setMessage("\tกำลังบันทึก...");
//            pdLoading.setCancelable(false);
//            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("UserId", profile.UserId + "")
                    .add("choice_name", urls[1])
                    .add("choice_price", urls[2])
                    .add("p_group_id", p_group_id)
                    .add("StoreId", profile.StoreId)
                    .add("BranchId", profile.BranchId)

                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
//            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuChoiceEditActivity.this);
                    builder.setMessage("เพิ่มข้อมูลเรียบร้อยแล้ว")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
//                                    finish();

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuChoiceEditActivity.this);
                    builder.setMessage("เพิ่มข้อมูลเรียบร้อยแล้ว")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    //                                    finish();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }

                //CreateChoice();
                new AsyncLoadCat().execute();


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class ChoiceDeleteTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(MenuChoiceEditActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tChoice Delete in progress...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();


            RequestBody formBody = new FormEncodingBuilder()
                    .add("UserId", Profile.UserId + "")
                    .add("ChoiceDeleteID", choice_id)
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuChoiceEditActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    new AsyncLoadCat().execute();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuChoiceEditActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
//                                    finish();
                                    new AsyncLoadCat().execute();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public class ChoiceRecyclerAdapter extends RecyclerView.Adapter<ChoiceRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemChoice> feedItemList;
        private Context mContext;

        public ChoiceRecyclerAdapter(Context context, List<FeedItemChoice> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.choice_list_row, null);

            CustomViewHolder viewHolder = new CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
            final FeedItemChoice feedItem = feedItemList.get(i);
            customViewHolder.feedItem = feedItem;


            //Setting text view title
            customViewHolder.title_choice.setText(Html.fromHtml("" + feedItem.getFirstname()));

            customViewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent in = new Intent(mContext, MenuChoiceEditActivity.class);
                    in.putExtra("cid", feedItem.getEmployee_id());
                    in.putExtra("title_choice", feedItem.getFirstname());
                    in.putExtra("title_choice_order", feedItem.getLastname());
                    startActivity(in);

                }
            });


            customViewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    final AlertDialog.Builder adb = new AlertDialog.Builder(MenuChoiceEditActivity.this);

                    adb.setTitle("ยืนยันการลบข้อมูล?");
                    adb.setMessage("ยืนยันการลบข้อมูล");
                    adb.setNegativeButton("Cancel", null);
                    adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
                        public void onClick(DialogInterface dialog, int arg1) {
                            // TODO Auto-generated method stub

                            choice_id = feedItem.getEmployee_id();
                            new ChoiceDeleteTask().execute(StringUtil.URL + "gpos/api/ChoiceGroupManage_ChoiceDelete");

                        }
                    });
                    adb.show();

                }
            });


        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            //            protected ImageView imageView;
            protected TextView title_choice;
            protected Button btnEdit, btnDelete;

            FeedItemChoice feedItem;

            public CustomViewHolder(View view) {
                super(view);

                this.title_choice = (TextView) view.findViewById(R.id.txtCheckIn);
                this.btnEdit = (Button) view.findViewById(R.id.btnEdit);
                this.btnDelete = (Button) view.findViewById(R.id.btnDelete);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

//                        StringUtil.CatId = feedItem.getId();
                        //new AsyncProduct().execute();
                        //Toast.makeText(v.getContext(), "Select is: " + feedItem.getId(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }
    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    public class FeedItemDrug {

        private String employee_id;
        private String firstname;
        private String lastname;
        private String tel;
        private String status_name;

        public String getEmployee_id() {
            return employee_id;
        }

        public void setEmployee_id(String employee_id) {
            this.employee_id = employee_id;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getStatus_name() {
            return status_name;
        }

        public void setStatus_name(String status_name) {
            this.status_name = status_name;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }
    }

}
