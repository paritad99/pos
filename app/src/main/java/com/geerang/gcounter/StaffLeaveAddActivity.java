package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;

import android.os.StrictMode;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class StaffLeaveAddActivity extends AppCompatActivity {


    List<String> arrList = new ArrayList<String>();

    Button btnSelectDate_start, btnSelectDate_end, btnSave;
    EditText input_leave_title;
    TextView input_date_start, input_date_end;
    String leave_of_day = "";
    RadioButton myOption1, myOption2, myOption3;
    Spinner leave_of_type ;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_leave_add);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }


       leave_of_type = (Spinner) findViewById(R.id.spinner);

//        กิจ,ป่วย,พักร้อน
//        ครึ้งวันเช้า,ครึ่งวันบ่าย,เต็มวัน

        arrList.add("กิจ");
        arrList.add("ป่วย");
        arrList.add("พักร้อน");

        ArrayAdapter<String> arrAd = new ArrayAdapter<String>(StaffLeaveAddActivity.this,
                android.R.layout.simple_spinner_item,
                arrList);

        leave_of_type.setAdapter(arrAd);

        input_date_start = (TextView) findViewById(R.id.input_date_start);
        input_date_end = (TextView) findViewById(R.id.input_date_end);

        input_leave_title = (EditText) findViewById(R.id.input_leave_title);

        btnSelectDate_start = (Button) findViewById(R.id.btnSelectDate_start);
        btnSelectDate_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DialogFragment newFragment = new StartDatePickerFragment();
                newFragment.show(getSupportFragmentManager(), "datePicker");
            }
        });


        btnSelectDate_end = (Button) findViewById(R.id.btnSelectDate_end);
        btnSelectDate_end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new EndDatePickerFragment();
                newFragment.show(getSupportFragmentManager(), "datePicker");
            }
        });

//        กิจ,ป่วย,พักร้อน

//        spin.setOnItemSelectedListener(new  AdapterView.OnItemSelectedListener() {
//
//            public void onItemSelected(AdapterView<?> adapterView,
//                                       View view, int i, long l) {
//                // TODO Auto-generated method stub
//                Toast.makeText(StaffLeaveAddActivity.this,
//                        String.valueOf("Your Selected : " + arrList.get(i)),
//                        Toast.LENGTH_SHORT).show();
//
//            }
//
//            public void onNothingSelected(AdapterView<?> arg0) {
//                // TODO Auto-generated method stub
//                Toast.makeText(StaffLeaveAddActivity.this,
//                        String.valueOf("Your Selected Empty"),
//                        Toast.LENGTH_SHORT).show();
//            }
//
//        });

        myOption1 = (RadioButton)findViewById(R.id.radio1);
        myOption2 = (RadioButton)findViewById(R.id.radio2);
        myOption3 = (RadioButton)findViewById(R.id.radio3);

        btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if( myOption1.isChecked())
                {
                    leave_of_day = "0";
                }

                if( myOption2.isChecked())
                {
                    leave_of_day = "1";
                }

                if( myOption3.isChecked())
                {
                    leave_of_day = "2";
                }


                if(input_leave_title.getText().toString().equals(""))
                {
                    AlertError("กรุณาระบุเหตุผลการขอลา");
                    return;
                } else if (input_date_start.getText().toString().equals("YYYY-MM-DD")) {
                    AlertError("กรุณาเลือกวันที่เริ่มลา");
                    return;
                }else if (input_date_end.getText().toString().equals("YYYY-MM-DD")) {
                    AlertError("กรุณาเลือกวันที่สิ้นสุด");
                    return;
                }else
                {
                    new BookingAddTask().execute(StringUtil.URL + "gpos/api/StaffLeaveAdd");
                }


            }
        });


    }


    public void AlertError(String msg)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(StaffLeaveAddActivity.this);
        builder.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things

                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private class BookingAddTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(StaffLeaveAddActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลังบันทึก...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("leave_title", input_leave_title.getText().toString())
                    .add("start_date",  input_date_start.getText().toString())
                    .add("end_date", input_date_end.getText().toString())
                    .add("leave_of_day", leave_of_day)
                    .add("leave_of_type", leave_of_type.getSelectedItemPosition()+"")
                    .add("create_by_userid", Profile.UserId)
                    .add("employee_id", Profile.UserId)

                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(StaffLeaveAddActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(StaffLeaveAddActivity.this);
                    builder.setMessage("แจ้งขอลางานเรียบร้อยแล้ว")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                   finish();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private void showData(String jsonString) {
        Toast.makeText(this, jsonString, Toast.LENGTH_LONG).show();
    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


}
