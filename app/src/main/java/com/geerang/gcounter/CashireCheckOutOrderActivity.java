package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection;

public class CashireCheckOutOrderActivity extends AppCompatActivity {


    RecyclerView mRecyclerView;
    TextView tvTableName, tvSum_Amount, txtItem_Qty, empty_view;

    private SectionedRecyclerViewAdapter sectionAdapter;
    ArrayList<HashMap<String, String>> MyArrList_GroupChoice;

    ArrayList<HashMap<String, String>> ChoiceArrList = new ArrayList<HashMap<String, String>>();
    HashMap<String, String> map_choice;
    private SimpleAdapter mFirstHeader;

    double total_price = 0;
    double total_qty = 0;
    double total_qty_check = 0;

    int check_receipt = 0;

    boolean vat_check;

    Button btnAddOrder, btnCustomerInfo, btnInvoice, btnReceipt;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    AlertDialog dialog;

    TextView txtTotalDiscount;

    int discount_total = 0;
    String discount_type_menu = "";
    int include_vat = 7;

    int services = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cashire_check_out_order);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        txtTotalDiscount = (TextView) findViewById(R.id.txtTotalDiscount);

        btnAddOrder = (Button) findViewById(R.id.btnAddOrder);
        btnCustomerInfo = (Button) findViewById(R.id.btnCustomerInfo);
        btnInvoice = (Button) findViewById(R.id.btnInvoice);
        btnReceipt = (Button) findViewById(R.id.btnReceipt);

        btnAddOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /* ข้อมูลที่ส่งมา
                StringUtil.TableCustomerName = feedItem.getTitle();
                StringUtil.BranchId = StringUtil.BranchId;
                StringUtil.TableCustomer = feedItem.getTitle();
                StringUtil.TableCustomerId = feedItem.getId();
                StringUtil.TotalCustomer = feedItem.getCustomer_total();
                StringUtil.OrderId = feedItem.getOrder_id();
                StringUtil.payment_status = feedItem.getPayment_status();
                */

                Intent in = new Intent(CashireCheckOutOrderActivity.this, OpenTableOrderActivity.class);
                startActivity(in);


            }
        });

        btnCustomerInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // ทำ Dialog เพื่อใช้ในการเลือก Choice ของเมนูนั้นๆ
                LayoutInflater inflater = getLayoutInflater();
                View alertLayout = inflater.inflate(R.layout.activity_add_info_customer_dialog, null);
                final EditText input_customer_qty = (EditText) alertLayout.findViewById(R.id.input_total_customer);
                Button btnSave = (Button) alertLayout.findViewById(R.id.btnSave);
                Button btnCancel = (Button) alertLayout.findViewById(R.id.btnCancel);
                AlertDialog.Builder alert = new AlertDialog.Builder(CashireCheckOutOrderActivity.this);
                //alert.setTitle("จำนวนลูกค้า\n");
                // this is set the view from XML inside AlertDialog
                alert.setView(alertLayout);
                dialog = alert.create();
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                btnSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        int customer_qty = Integer.parseInt(input_customer_qty.getText().toString());

                        if (customer_qty > 20) {
                            input_customer_qty.setError("ลูกค้าเกิน 20 คน");
                        } else if (customer_qty < 1) {
                            input_customer_qty.setError("กรุณาระบุจำนวนลูกค้า!");
                        } else {
                            new TableCustomerQtyOrderTask().execute(StringUtil.URL + "gpos/api/TableCustomerQty"
                                    , input_customer_qty.getText().toString());
                        }


                    }
                });

                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });


                dialog.show();
            }
        });

        btnInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(CashireCheckOutOrderActivity.this, DiscountActivity.class);
                startActivity(in);

            }
        });

        btnReceipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(CashireCheckOutOrderActivity.this, CashireReceiptActivity.class);
                startActivity(in);

            }
        });

        tvTableName = (TextView) findViewById(R.id.tvTableName);
        tvSum_Amount = (TextView) findViewById(R.id.txtTotalPrice);
        empty_view = (TextView) findViewById(R.id.empty_view);

//        txtCustomer = (TextView) findViewById(R.id.txtCustomer);
//        txtCustomer.setText(StringUtil.TotalCustomer);


//        txt_status = (TextView) findViewById(R.id.txt_status);

        mRecyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);

        new AsyncInvoiceDiscount().execute();
        new AsyncChoiceProduct().execute();

        CheckoutOrder();


    }

    @Override
    protected void onResume() {
        super.onResume();
        CheckoutOrder();
        tvTableName.setText(StringUtil.TableCustomerName);

        new AsyncInvoiceDiscount().execute();
        new AsyncChoiceProduct().execute();

    }


    private class TableCustomerQtyOrderTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(CashireCheckOutOrderActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลังบันทึก...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("customer_qty", urls[1])
                    .add("order_id", StringUtil.OrderId)
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();
            dialog.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                // reload data
                new AsyncChoiceProduct().execute();

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(CashireCheckOutOrderActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(CashireCheckOutOrderActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
//                                    Intent in = new Intent(OpenTableOrderActivity.this, StaffDeliveryActivity.class);
//                                    in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                                    startActivity(in);
//                                    finish();

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public void AlertError(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(CashireCheckOutOrderActivity.this);
        builder.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things

                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }



    public void CheckoutOrder() {
        if (StringUtil.payment_status.equals("CHECKOUT")) {
            btnAddOrder.setVisibility(View.GONE);
        }
    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    // InvoiceDiscount
    private class AsyncInvoiceDiscount extends AsyncTask<String, String, String> {

        ProgressDialog pdLoading = new ProgressDialog(CashireCheckOutOrderActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //this method will be running on UI thread
            pdLoading.setMessage("\tกำลังโหลดข้อมูล...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run("http://www.geerang.com/gpos/api/InvoiceDiscount/" + StringUtil.OrderId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {
            try {
                pdLoading.dismiss();
                String discount = "";
                JSONArray jArray = new JSONArray(result);
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    //total_row_qty = Integer.parseInt(json_data.getString("qty"));

                    String s = json_data.getString("services");

                    if (!s.equals("null")) {
                        services = Integer.parseInt(s);
                    } else {
                        services = 0;
                    }

                    String discount_type = json_data.getString("discount_type");
                    StringUtil.dicount_type = discount_type;

                    String vat_include = json_data.getString("vat_include");



                    if (discount_type.equals("") || discount_type.equals("null")) {
                        discount += "ประเภทส่วนลด : " + "-\n";
                    } else {
                        discount += "ประเภทส่วนลด : " + json_data.getString("discount_type") + "\n";
                    }

                    String total_discount = json_data.getString("total_discount");
                    if (total_discount.equals("") || total_discount.equals("null")) {
                        discount += "จำนวนส่วนลด : " + "-\n";
                    } else {
                        StringUtil.discount_total_payment = json_data.getString("total_discount");
                        StringUtil.dicount_total = Integer.parseInt(total_discount);

                        discount += "จำนวนส่วนลด : " + json_data.getString("total_discount") + "\n";
                    }

                    if(vat_include.equals("yes"))
                    {
                        vat_check = true;
                        String vat = json_data.getString("vat");
                        if (vat.equals("") || vat.equals("null")) {
                            discount += "Vat : " + "7%\n";
                            include_vat = Integer.parseInt("7");
                        } else {
                            discount += "Vat : " + json_data.getString("vat") + "%\n";
                            include_vat = Integer.parseInt(json_data.getString("vat"));
                        }

                    }else
                    {
                        vat_check = false;
                    }

                    discount += "Services charge : " + services + "%";
                    StringUtil.custom_service = services+"";

                    try {
                        if (!StringUtil.discount_total_payment.equals("")) {
                            discount_total = Integer.parseInt(StringUtil.discount_total_payment);
                        }

                    }catch (Exception err)
                    {
                        discount_total = 0;
                    }

                    discount_type_menu = discount_type;

                    StringUtil.dicount_type = discount_type_menu;


                }

                txtTotalDiscount.setText(discount);

            } catch (JSONException e) {
//                txt_status.setVisibility(View.VISIBLE);
                Toast.makeText(CashireCheckOutOrderActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }


    private class AsyncChoiceProduct extends AsyncTask<String, String, String> {

        ProgressDialog pdLoading = new ProgressDialog(CashireCheckOutOrderActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread

            pdLoading.setMessage("\tกำลังโหลดข้อมูล...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run("http://www.geerang.com/gpos/api/MenuOrderList/" + StringUtil.OrderId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {
            try {
//                progressBar.setVisibility(View.GONE);

//                txt_status.setVisibility(View.GONE);

                pdLoading.dismiss();

                total_price = 0;
                total_qty = 0;

                JSONArray jArray = new JSONArray(result);
                if (jArray.length() <= 0) {
                    empty_view.setVisibility(View.VISIBLE);
                    btnInvoice.setVisibility(View.GONE);
                    btnReceipt.setVisibility(View.GONE);
                }

                sectionAdapter = new SectionedRecyclerViewAdapter();
                for (int i = 0; i < jArray.length(); i++) {

                    total_qty_check++;

                    JSONObject json_data = jArray.getJSONObject(i);
                    //ประกาศ array string
                    ArrayList<String> arrSports = new ArrayList<>();
                    // array hasmap
                    MyArrList_GroupChoice = new ArrayList<HashMap<String, String>>();
                    HashMap<String, String> map;


                    double total_row_qty = 0;
                    total_row_qty = Integer.parseInt(json_data.getString("qty"));

                    String confirm = json_data.getString("is_confirm");
                    // เก็บตัวแปร Choice
                    String Choice = "";
                    double total_row_price = 0;
                    // เก็บราคาใน order
                    total_row_price = Integer.parseInt(json_data.getString("product_sale_price"));


                    JSONArray jArray2 = new JSONArray(json_data.getString("product_choice"));
                    int check_list = jArray2.length();
                    if (check_list > 0) {
                        for (int j = 0; j < jArray2.length(); j++) {

                            JSONObject json_data2 = jArray2.getJSONObject(j);
                            arrSports.add(json_data2.getString("product_choice_name"));

                            // หาราคารวมของแต่ละ row แล้วนำไปยัดใส่ใน header row
                            int choice_price = Integer.parseInt(json_data2.getString("choice_price"));
                            total_row_price += choice_price;

                            Choice += json_data2.getString("product_choice_name") + ",";

                            map = new HashMap<String, String>();
                            map.put("product_choice_name", json_data2.getString("product_choice_name"));
                            map.put("orders_item_group_id", json_data2.getString("orders_item_group_id"));
                            map.put("choice_price", json_data2.getString("choice_price"));
                            MyArrList_GroupChoice.add(map);
                        }
                    }

//                        int choice_price = Integer.parseInt(MyArrList_choice.get(position).get("choice_price"));

                    double _total = total_price + total_row_price ;
                    total_price = _total  * total_row_qty;

                    total_qty = total_qty + total_row_qty;


                    mFirstHeader = new SimpleAdapter(MyArrList_GroupChoice
                            , "" + json_data.getString("product_name")
                            , (total_row_price * total_row_qty) + ""
                            , json_data.getString("qty") + ""
                            , json_data.getString("orders_item_id")
                            , Choice
                            , confirm
                            , json_data.getString("remain"));
                    sectionAdapter.addSection(mFirstHeader);


                }

                DecimalFormat formatter = new DecimalFormat("#,###.00");

                //double amount = Double.parseDouble(total_price);
                System.out.println(formatter.format(total_price));

                try {

                    //int result_net = 0;
                    double total_net = 0;
                    //double total_vat = 0;
                    double total_invoice_with_vat = 0;
                    double services_charge = 0;

                    if(vat_check == true)
                    {
                        StringUtil.vat = true;
                        total_invoice_with_vat = (total_price * include_vat) / 100;
                        //total_net = total_price + total_invoice_with_vat;
                    }else
                    {
                        StringUtil.vat = false;
                        total_invoice_with_vat = 7;
                    }

                    if(services >0)
                    {
                        services_charge = (total_price * services)/100;
                        //total_net = total_net + x;
                    }

                    total_net = total_price + services_charge + total_invoice_with_vat;

                    double total_with_discount = 0;
                    if (discount_type_menu.equals("เงินสด")) {
                        total_with_discount = total_price - discount_total;
                       // result_net = total_with_discount;
                    } else {
                        total_with_discount = (total_price * discount_total) / 100;
                        //total_with_discount = total_net - x;
                        //result_net = y;
                    }

                    total_net = total_net - total_with_discount;

//                    total_net = total_net  - total_with_discount;
                    tvSum_Amount.setText("THB " + formatter.format(total_net) + "");

                } catch (Exception e) {
                    tvSum_Amount.setText("THB " + e + "");
                }

//                txtItem_Qty.setText("Total " + total_qty + " Items");
                StringUtil.TotalOrderQty = total_qty + "";
                StringUtil.TotalPayment = total_price + "";

                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(CashireCheckOutOrderActivity.this, LinearLayoutManager.VERTICAL, false);
                mRecyclerView.setLayoutManager(linearLayoutManager);
                mRecyclerView.setAdapter(sectionAdapter);


            } catch (JSONException e) {
//                txt_status.setVisibility(View.VISIBLE);
                Toast.makeText(CashireCheckOutOrderActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }


    public class SimpleAdapter extends StatelessSection {

        //        private ArrayList<String> arrName = new ArrayList<>();
        ArrayList<HashMap<String, String>> MyArrList_choice = new ArrayList<HashMap<String, String>>();
        private String mTitle, Choice;
        private double price;
        private double qty;
        private String confirm;
        private String comment;
        private double select_item_count = 0;
        private double total_price_row;
        private String orders_item_id;

        public SimpleAdapter(ArrayList<HashMap<String, String>> arrName
                , String title, String price, String qty
                , String orders_item_id
                , String Choice
                , String confirm, String comment) {
            super(R.layout.layout_checkout_order_item_header, R.layout.layout_checkout_order_item);

            this.MyArrList_choice = arrName;
            this.mTitle = title;
            this.price = Double.parseDouble(price);
            this.qty = Double.parseDouble(qty);
            this.Choice = Choice;
            this.confirm = confirm;
            this.orders_item_id = orders_item_id;
            this.comment = comment;
        }

        @Override
        public int getContentItemsTotal() {
            return MyArrList_choice.size();
        }

        @Override
        public RecyclerView.ViewHolder getItemViewHolder(View view) {
            return new ItemViewHolder(view);
        }

        @Override
        public RecyclerView.ViewHolder getHeaderViewHolder(View view) {
            return new HeaderViewHolder(view);
        }

        @Override
        public void onBindItemViewHolder(RecyclerView.ViewHolder holder, final int position) {
            final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
            itemViewHolder.txtItem.setText("*" + MyArrList_choice.get(position).get("product_choice_name"));
            itemViewHolder.txtPrice.setText("+฿" + MyArrList_choice.get(position).get("choice_price"));
//            itemViewHolder.checkBox.setTag(MyArrList_choice.get(position).get("Code"));

        }

        @Override
        public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder) {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
            headerViewHolder.txtHeader.setText(mTitle);
            headerViewHolder.txtPrice.setText("฿" + (price));
            headerViewHolder.txtComment.setText(comment);
            int x_qty =(int)qty;
            headerViewHolder.txtItemQty.setText(" " + x_qty + "");

//            headerViewHolder.linearLayout1.Set
            if (!Choice.isEmpty()) {
                headerViewHolder.txtChoice.setText(Choice.substring(0, Choice.length() - 1) + "");
            } else {
                headerViewHolder.txtChoice.setVisibility(View.GONE);
            }
        }

        protected class HeaderViewHolder extends RecyclerView.ViewHolder {

            protected TextView txtHeader, txtDelete;
            protected TextView txtPrice, txtChoice, txtItemQty, txtComment;

            protected CardView cardView;

            private HeaderViewHolder(View itemView) {
                super(itemView);
                txtDelete = (TextView) itemView.findViewById(R.id.txtDelete);

                txtHeader = (TextView) itemView.findViewById(R.id.txtHeader);
                txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
                txtItemQty = (TextView) itemView.findViewById(R.id.txtItemQty);
                txtComment = (TextView) itemView.findViewById(R.id.txtRemain);

                txtChoice = (TextView) itemView.findViewById(R.id.txtChoice);
                cardView = (CardView) itemView.findViewById(R.id.cardView);


            }
        }

        protected class ItemViewHolder extends RecyclerView.ViewHolder {

            protected TextView txtItem, txtPrice;
            protected LinearLayout linearLayout1;

            //            protected CheckBox checkBox;
            private ItemViewHolder(View itemView) {
                super(itemView);
                txtItem = (TextView) itemView.findViewById(R.id.txtChoice);
                txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
                linearLayout1 = (LinearLayout) itemView.findViewById(R.id.linearLayout1);
                linearLayout1.setVisibility(View.GONE);

//                checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);


            }
        }
    }


    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

}
