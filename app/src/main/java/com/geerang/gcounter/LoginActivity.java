package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class LoginActivity extends AppCompatActivity {

    private final OkHttpClient client = new OkHttpClient();

    EditText input_username, input_password;
    Button btnLogin;


    Button btnSignIn;

    final String PREF_NAME = "LoginPreferences";
    final String KEY_USERNAME = "Username";
    final String KEY_USERID = "UserId";
    final String KEY_BranchId = "BranchId";
    final String KEY_StoreId = "StoreId";
    final String KEY_Status = "Status";
    final String KEY_REMEMBER = "RememberUsername";
    final String KEY_business_type_id = "business_type_id";
    final String KEY_StoreName = "StoreName";
    final String KEY_BranchName = "BranchName";
    final String KEY_Position = "Position";
    final String KEY_StoreExp = "StoreExp";
    final String KEY_StoreAddress = "StoreAddress";
    final String KEY_Owner = "Owner";
    final String KEY_package = "package";
    final String KEY_StoreTax = "StoreTax";
    final String KEY_StorePhone = "StoreTax";

    SharedPreferences sp;
    SharedPreferences.Editor editor;

    TextView btnGotoRegister;


    int CheckCashier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //Toolbar toolbar = findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }


        btnGotoRegister = (TextView) findViewById(R.id.btnGotoRegister);
        btnGotoRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "http://geerang.com/gpos/index.php/member_login";
                startOpenWebPage(url);
            }
        });

        sp = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sp.edit();

        input_username = (EditText) findViewById(R.id.input_username);
        input_password = (EditText) findViewById(R.id.input_password);

        btnSignIn = (Button) findViewById(R.id.btnSignIn);
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String uname = input_username.getText().toString();
                String upass = input_password.getText().toString();

                if (uname.equals("")) {
                    input_username.setError("กรุณาใส่ Username");
                } else if (uname.length() < 8) {
                    input_username.setError("Username ต้องไม่น้อยกว่า 8 หลัก");
                } else if (upass.equals("")) {
                    input_password.setError("กรุณาใส่ Password");
                } else if (upass.length() < 8) {
                    input_password.setError("Password ต้องไม่น้อยกว่า 8 หลัก");

                } else {
                    new LoginTask().execute(StringUtil.URL + "gpos/api/login");
                }


            }
        });

        Button btnGotoMamager = (Button) findViewById(R.id.btnGotoMamager);
        btnGotoMamager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(LoginActivity.this, LoginBusinessActivity.class);
                startActivity(in);
            }
        });

    }

    public boolean startOpenWebPage(String url) {
        boolean result = false;
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;

        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);

        try {
            startActivity(intent);
            result = true;
        } catch (Exception e) {
            if (url.startsWith("http://")) {
                startOpenWebPage(url.replace("http://", "https://"));
            }
        }
        return result;
    }


    private class LoginTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(LoginActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลังเข้าสู่ระบบ...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("Username", input_username.getText().toString())
                    .add("Password", input_password.getText().toString())
                    .build();


            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);

            JSONObject object = null;
            try {
                //object = new JSONObject(jsonString);

                //String status = object.getString("status"); // success
                pdLoading.dismiss();

                JSONArray jArray = new JSONArray(jsonString);
                if (jArray.length() == 0) {
                    object = new JSONObject(jsonString);
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                    builder.setMessage("ชื่อผู้ใช้/รหัสผ่านผิด!")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

                } else {
                    // Extract data from json and store into ArrayList as class objects
                    for (int i = 0; i < jArray.length(); i++) {
                        JSONObject object_profile = jArray.getJSONObject(i);
                        //String status = object.getString("status"); json_data.getString("branch_id");
                        String user_id = object_profile.getString("employee_id");
                        String branch_id = object_profile.getString("branch_id");
                        String store_id = object_profile.getString("store_id");
                        String username = object_profile.getString("username");
                        String email = object_profile.getString("email");

                        Profile.BranchId = branch_id;
                        Profile.StoreId = store_id;

                        Profile.Username = object_profile.getString("username");
                        Profile.StoreName = object_profile.getString("store_name");
                        Profile.BranchName = object_profile.getString("branch_name");
                        Profile.Position = object_profile.getString("employee_status_id");
                        Profile.store_exp = object_profile.getString("store_exp");
                        Profile.Manager = object_profile.getString("is_manager");
                        Profile.store_address = object_profile.getString("store_address");
                        Profile.business_type_id = object_profile.getString("business_type_id");

                        Profile.StoreTax = object_profile.getString("store_tax");
                        Profile.StorePhone = object_profile.getString("store_phone");

                        Profile.IsOwner = false;
                        // พนักงานรับรายการอาหาร
                        editor = sp.edit();
                        editor.putString(KEY_USERNAME, username);
                        editor.putString(KEY_USERID, user_id);
                        editor.putString(KEY_StoreId, store_id);
                        editor.putString(KEY_BranchId, branch_id);
                        editor.putString(KEY_Status, Profile.Position);
                        editor.putString(KEY_REMEMBER, "Active");
                        editor.putString(KEY_business_type_id, Profile.business_type_id);
                        editor.putString(KEY_StoreName, Profile.StoreName);
                        editor.putString(KEY_BranchName, Profile.BranchName);
                        editor.putString(KEY_Position, Profile.Position);
                        editor.putString(KEY_StoreExp, Profile.store_exp);
                        editor.putString(KEY_Owner, "Employee");
                        editor.putString(KEY_StoreAddress, Profile.store_address);

                        editor.putString(KEY_StoreTax, Profile.StoreTax);
                        editor.putString(KEY_StorePhone, Profile.StorePhone);


                        JSONArray jArray2 = new JSONArray(object_profile.getString("store_package"));
                        editor.putString(KEY_package, object_profile.getString("store_package"));
                        // Extract data from json and store into ArrayList as class objects
                        for (int k = 0; k < jArray2.length(); k++) {
                            JSONObject json_data2 = jArray2.getJSONObject(k);

                            StringUtil.package_name = json_data2.getString("package_name");
                            StringUtil.package_product = json_data2.getString("package_product");
                            StringUtil.package_branch = json_data2.getString("package_branch");
                            StringUtil.package_cashier = json_data2.getString("package_cashier");
                            StringUtil.package_users = json_data2.getString("package_users");
                            StringUtil.package_zone = json_data2.getString("package_zone");
                            StringUtil.package_table = json_data2.getString("package_table");

                        }

                        editor.commit();

                    }

                    //String msg_data = object.getString("data"); // success
                    //JSONObject object_profile = new JSONObject(msg_data);


                    if (Profile.Position.equals("2")) {
                        StringUtil.employee_status = "ผู้จัดการร้าน";

                        Intent in = new Intent(LoginActivity.this, ManagerMainActivity.class);
                        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(in);
                        finish();
                        // พนักงานครัว
                    } else if (Profile.Position.equals("1")) {
                        StringUtil.employee_status = "พนักงานครัว";

                        Intent in = new Intent(LoginActivity.this, StaffDeliveryActivity.class);
                        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(in);
                        finish();
                        //คนรับ order
                    } else if (Profile.Position.equals("6")) {
                        StringUtil.employee_status = "Customer Service";
                        Intent in = new Intent(LoginActivity.this, StaffDeliveryActivity.class);
                        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(in);
                        finish();

                        //drink
                    } else if (Profile.Position.equals("3")) {
                        Intent in = new Intent(LoginActivity.this, StaffDeliveryActivity.class);
                        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(in);
                        finish();
                    } else if (Profile.Position.equals("4")) // แคชเชียร์
                    {
                        StringUtil.employee_status = "แคชเชียร์";
                        new AsyncLoadProduct().execute();
                    }else
                    {
                        Intent in = new Intent(LoginActivity.this, StaffDeliveryActivity.class);
                        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(in);
                        finish();

                    }


                }

            } catch (JSONException e) {
                e.printStackTrace();

                //object = new JSONObject(jsonString);
                //String msg = object.getString("msg"); // success
                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setMessage("ชื่อผู้ใช้/รหัสผ่านผิด!")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //do things
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

            }

        }
    }

    private class AsyncLoadProduct extends AsyncTask<String, String, String> {

        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/CashierCheck/" + Profile.StoreId + "/" + Profile.BranchId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            try {

                JSONArray jArray = new JSONArray(result);
                if (jArray.length() > 0) {
                    Intent in = new Intent(LoginActivity.this, StaffCashireActivity.class);
                    in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(in);
                    finish();


                } else {

                    Intent in = new Intent(LoginActivity.this, CashierStartActivity.class);
                    in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(in);
                    finish();

                }


            } catch (JSONException e) {
                Toast.makeText(LoginActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    private void showData(String jsonString) {
        Toast.makeText(this, jsonString, Toast.LENGTH_LONG).show();
    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }
}
