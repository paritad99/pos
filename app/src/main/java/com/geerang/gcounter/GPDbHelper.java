package com.geerang.gcounter;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class GPDbHelper  extends SQLiteOpenHelper {

    private static final String DB_NAME = "GposPrinter";
    private static final int DB_VERSION = 3;

    public static final String TABLE_NAME = "printers";
    public static final String COL_PRINTER_NAME = "printer_name";
    public static final String COL_PRINTER_ADDRESS = "printer_address";
    public static final String COL_PRINTER_PRODUCT_ID = "product_id";



    public static final String TABLE_PRODUCT_PRINT = "product_print";
    public static final String COL_PRODUCT_ID = "product_id";
    public static final String COL_PRINT_ADDRESS = "printer_address";


    public GPDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME + " (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COL_PRINTER_NAME + " TEXT, " + COL_PRINTER_ADDRESS + " TEXT, " + COL_PRINTER_PRODUCT_ID + " TEXT);");

        db.execSQL("CREATE TABLE " + TABLE_PRODUCT_PRINT
                + " (_id INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_PRODUCT_ID + " TEXT, " + COL_PRINT_ADDRESS + " TEXT);");


    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCT_PRINT);
        onCreate(db);
    }

}
