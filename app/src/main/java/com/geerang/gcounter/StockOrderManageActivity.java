package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.StrictMode;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection;

public class StockOrderManageActivity extends AppCompatActivity {

    TextView txtMenu, txtMenuDesc;
    RecyclerView recyclerView;
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;
    private List<FeedItemDrug> feedsList;

    static ArrayList<HashMap<String, String>> MyArrList;
    static ArrayList<HashMap<String, String>> MyArrList_stock;
    HashMap<String, String> map;
    Spinner ddlMenuCategory;

    String product_id;


    static ListView lisView1;

    LinearLayout llIngredient;
    static GridView gridview;

    String choice_id = "";

    TextView txtChoice;
    Profile profile;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {

            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private SimpleAdapter mFirstHeader;
    private SimpleAdapter mSecondHeader;
    private SimpleAdapter mThirdHeader;
    private SectionedRecyclerViewAdapter sectionAdapter;
    ArrayList<HashMap<String, String>> MyArrList_GroupChoice;

    ArrayList<HashMap<String, String>> ChoiceArrList;
    HashMap<String, String> map_choice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_srock_order_manage);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
// add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        /*
          in.putExtra("product_name", feedItem.getMenu_name());
                    in.putExtra("product_detail", feedItem.getMenu_product_detail());
                    in.putExtra("product_sale_price", feedItem.getMenu_product_price());
                    in.putExtra("product_image", feedItem.getProduct_image());
                    in.putExtra("calories", feedItem.getMenu_calories());
                    in.putExtra("categories_id", feedItem.getMenu_categories_id());
                    in.putExtra("cookingtime", feedItem.getMenu_cookingtime());
                    in.putExtra("product_id", feedItem.getId());

         */
         product_id = getIntent().getExtras().getString("product_id");


        profile = new Profile(this);

        txtMenu = (TextView) findViewById(R.id.txtMenu);
        txtMenu.setText(StringUtil.menu_name);
        txtMenuDesc = (TextView) findViewById(R.id.txtMenuDesc);
        txtMenuDesc.setText(StringUtil.menu_product_detail);
        txtChoice = (TextView) findViewById(R.id.txtChoice);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        // listView1
        lisView1 = (ListView) findViewById(R.id.listView1);

    }


    private class BookingAddTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(StockOrderManageActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลังบันทึกข้อมูล");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("menu_ingredients_id", urls[1])
                    .add("use_total", urls[2])
                    .add("user_unit", urls[3])
                    .add("choice_id", choice_id)
                    .add("create_by", profile.UserId)

                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

//                choice_id = feedItem.getProduct_id();
//                new AsyncLoadIngredientTable().execute(choice_id);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(StockOrderManageActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(StockOrderManageActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
//                                    Intent in = new Intent(BookingUpdateActivity.this, TableActivity.class);
//                                    startActivity(in);
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class MenuIngredientStockUpdateTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(StockOrderManageActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลังบันทึกข้อมูล");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("menu_stock_ingredient_id", urls[1])
                    .add("use_total", urls[2])
                    .add("user_unit", urls[3])

                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

//                choice_id = feedItem.getProduct_id();
//                new AsyncLoadIngredientTable().execute(choice_id);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(StockOrderManageActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(StockOrderManageActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
//                                    Intent in = new Intent(BookingUpdateActivity.this, TableActivity.class);
//                                    startActivity(in);
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        new AsyncChoiceProduct().execute(product_id);


    }

    private class AsyncChoiceProduct extends AsyncTask<String, String, String> {

        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run("http://www.geerang.com/gpos/api/GroupChoiceList/" + urls[0]);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {
            try {
//                progressBar.setVisibility(View.GONE);

                JSONArray jArray = new JSONArray(result);
                sectionAdapter = new SectionedRecyclerViewAdapter();

                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    //ประกาศ array string
                    ArrayList<String> arrSports = new ArrayList<>();
                    // array hasmap
                    MyArrList_GroupChoice = new ArrayList<HashMap<String, String>>();
                    HashMap<String, String> map;

                    JSONArray jArray2 = new JSONArray(json_data.getString("group_choice"));
                    int check_list = jArray2.length();
                    if (check_list > 0) {
                        for (int j = 0; j < jArray2.length(); j++) {

                            JSONObject json_data2 = jArray2.getJSONObject(j);
                            arrSports.add(json_data2.getString("product_choice_name"));
                            // map data เข้าไป
                            map = new HashMap<String, String>();
                            map.put("product_choice_name", json_data2.getString("product_choice_name"));
                            map.put("p_group_add_id", json_data2.getString("p_group_add_id"));
                            map.put("choice_price", json_data2.getString("choice_price"));
                            map.put("choice_stock", json_data2.getString("choice_stock"));

                            MyArrList_GroupChoice.add(map);

                        }

                        mFirstHeader = new SimpleAdapter(StockOrderManageActivity.this,MyArrList_GroupChoice
                                , "" + json_data.getString("group_name")
                                , json_data.getString("min")
                                , json_data.getString("max")
                                , json_data.getString("p_group_id")
                        );
                        sectionAdapter.addSection(mFirstHeader);
                    }

                }

                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(StockOrderManageActivity.this, LinearLayoutManager.VERTICAL, false);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.setAdapter(sectionAdapter);


            } catch (JSONException e) {
                Toast.makeText(StockOrderManageActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    public class SimpleAdapter extends StatelessSection {

        //        private ArrayList<String> arrName = new ArrayList<>();
        ArrayList<HashMap<String, String>> MyArrList_choice = new ArrayList<HashMap<String, String>>();
        private String mTitle;
        private int min;
        private int max;
        private int select_item_count = 0;
        private String p_group_id;
        private Context context;

        public SimpleAdapter(Context context,ArrayList<HashMap<String, String>> arrName
                , String title, String min, String max, String p_group_id) {
            super(R.layout.layout_header_stock, R.layout.layout_item_stock);

            this.context = context;
            this.MyArrList_choice = arrName;
            this.mTitle = title;
            this.min = Integer.parseInt(min);
            this.p_group_id = p_group_id;
            if (min.equals("0")) {
                this.max = MyArrList_choice.size();
            } else {
                this.max = Integer.parseInt(max);
            }

            ChoiceArrList = new ArrayList<HashMap<String, String>>();

        }

        @Override
        public int getContentItemsTotal() {
            return MyArrList_choice.size();
        }

        @Override
        public RecyclerView.ViewHolder getItemViewHolder(View view) {
            return new ItemViewHolder(view);
        }

        @Override
        public RecyclerView.ViewHolder getHeaderViewHolder(View view) {
            return new HeaderViewHolder(view);
        }

        @Override
        public void onBindItemViewHolder(RecyclerView.ViewHolder holder, final int position) {
            final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
            itemViewHolder.txtItem.setText("*" + MyArrList_choice.get(position).get("product_choice_name"));
            itemViewHolder.txtPrice.setText("+฿" + MyArrList_choice.get(position).get("choice_price"));
            itemViewHolder.btnStockAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent in = new Intent(context, MenuStockIngredientManageActivity.class);
                    in.putExtra("product_choice_name",MyArrList_choice.get(position).get("product_choice_name"));
                    in.putExtra("p_group_add_id",MyArrList_choice.get(position).get("p_group_add_id"));
                    in.putExtra("choice_price",MyArrList_choice.get(position).get("choice_price"));
                    in.putExtra("choice_stock",MyArrList_choice.get(position).get("choice_stock"));
                    in.putExtra("product_id",product_id);
                    startActivity(in);
                }
            });

//            itemViewHolder.checkBox.setTag(MyArrList_choice.get(position).get("Code"));

            // ตรวจสอบของหมด
            // คำนวนหาว่า วัตุดดิบในการทำหาหารเหลือมั้ย
//            String StockQty = MyArrList_choice.get(position).get("choice_stock");
//            JSONArray jArray2 = null;
//            try {
//                jArray2 = new JSONArray(StockQty);
//                int stock_total_by_item = 1;
//                for (int i = 0; i < jArray2.length(); i++) {
//                    JSONObject json_data2 = jArray2.getJSONObject(i);
//                    String Qty =   json_data2.getString("product_onhand");
//                    int x = Integer.parseInt(Qty);
//                    if(x == 0)
//                    {
//                        stock_total_by_item = 0;
//                    }
//
//                }
//
//                if(stock_total_by_item == 0)
//                {
//                    // ซ่อน Checkbox ไป
//                    itemViewHolder.checkBox.setVisibility(View.GONE);
//                    // แสดงข้อมูลว่าวัตุดิบในการประกอบหาหารหมด
//                    itemViewHolder.txtStockOrder.setVisibility(View.VISIBLE);
//                }
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }





        }

        @Override
        public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder) {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
            headerViewHolder.txtHeader.setText(mTitle);


            if (min == 0) {
                headerViewHolder.txtMinMax.setVisibility(View.GONE);

            } else {
                headerViewHolder.txtMinMax.setText("เลือกได้น้อยสุด " + min + " เลือกได้มากสุด " + max);
            }


        }

        protected class HeaderViewHolder extends RecyclerView.ViewHolder {

            protected TextView txtHeader;
            protected TextView txtMinMax;

            private HeaderViewHolder(View itemView) {
                super(itemView);
                txtHeader = (TextView) itemView.findViewById(R.id.txtHeader);
                txtMinMax = (TextView) itemView.findViewById(R.id.txtMinMax);
            }
        }

        protected class ItemViewHolder extends RecyclerView.ViewHolder {

            protected TextView txtItem, txtPrice, txtStockOrder;
            protected CheckBox checkBox;
            protected Button btnStockAdd;

            private ItemViewHolder(View itemView) {
                super(itemView);
                txtItem = (TextView) itemView.findViewById(R.id.txtChoice);
                txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
                checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);
                txtStockOrder = (TextView) itemView.findViewById(R.id.txtStockOrder);
                btnStockAdd = (Button) itemView.findViewById(R.id.btnStockAdd);
            }
        }
    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    // String
    public class FeedItemDrug {
        private String title;
        private String id;
        private String setDetail;
        private String Price;
        private String product_id;
        private String product_image;

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }

        public String getPrice() {
            return Price;
        }

        public void setPrice(String price) {
            this.Price = price;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String name) {
            this.title = name;
        }


        public String getDetail() {
            return setDetail;
        }

        public void setDetail(String Detail) {
            this.setDetail = Detail;
        }


    }

}
