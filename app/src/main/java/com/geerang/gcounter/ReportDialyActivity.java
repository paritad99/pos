package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.PictureDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.epson.epos2.printer.Printer;
import com.epson.epos2.printer.PrinterStatusInfo;
import com.epson.epos2.printer.ReceiveListener;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.IBinder;
import android.os.StrictMode;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;
import recieptservice.com.recieptservice.PrinterInterface;

public class ReportDialyActivity extends AppCompatActivity implements ReceiveListener {


    RecyclerView rvCatagory;
    TextView tvTableName, tvSum_Amount, txtItem_Qty, empty_view;

    private SectionedRecyclerViewAdapter sectionAdapter;
    ArrayList<HashMap<String, String>> MyArrList_GroupChoice;

    ArrayList<HashMap<String, String>> ChoiceArrList = new ArrayList<HashMap<String, String>>();
    HashMap<String, String> map_choice;

    int total_price = 0;
    int total_qty = 0;
    int total_qty_check = 0;

    int check_receipt = 0;
    SimpleDateFormat formatter;
    Button btnPrint, btnDate;

    static String Resultbill;

    static TextView txtReportTypeName;

    IBinder service;
    PrinterInterface mAidl;

    Profile profile;

    private Context mContext = null;
    public static EditText mEditTarget = null;
    public static Spinner mSpnSeries = null;
    public static Spinner mSpnLang = null;
    public static Printer mPrinter = null;
    public static ToggleButton mDrawer = null;
    EpsonMakeErrorMessage epsonMakeErrorMessage;
    final String KEY_Autoprint = "Autoprint";
    final String KEY_PrinterMain = "PrinterMain";
    final String KEY_Printer1 = "Printer1";
    final String KEY_Printer2 = "Printer2";
    final String KEY_Printer3 = "Printer3";
    final String KEY_Printer4 = "Printer4";

    String PrinterAddress = "";

    SharedPreferences sp;
    SharedPreferences.Editor editor;
    final String PREF_NAME = "PrinterPreferences";

    private List<FeedItemChoice> feedsList;
    private ChoiceRecyclerAdapter mAdapter;


    static double sum_dessert = 0.0;
    static double sum_drink = 0.0;
    static double sum_food = 0.0;
    static double sum_other = 0.0;


    TextView txt_sum_dessert;
    TextView txt_sum_drink;
    TextView txt_sum_food;
    TextView txt_sum_other;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    AlertDialog dialog;

    TextView input_date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_dialy);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        epsonMakeErrorMessage = new EpsonMakeErrorMessage(this);
        sp = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sp.edit();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        // Permission StrictMode
        if (Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        txt_sum_dessert = (TextView) findViewById(R.id.txt_sum_sweet);
        txt_sum_drink = (TextView) findViewById(R.id.txt_sum_drink);
        txt_sum_food = (TextView) findViewById(R.id.txt_sum_food);
        txt_sum_other = (TextView) findViewById(R.id.txt_sum_other);

        btnPrint = (Button) findViewById(R.id.btnPrint);
        btnPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new AsyncChoiceProductPrint().execute();

            }
        });

        input_date = (TextView) findViewById(R.id.input_date);

        btnDate = (Button) findViewById(R.id.btnDate);
        btnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment();
                cdp.show(ReportDialyActivity.this.getSupportFragmentManager(), "Material Calendar Example");
                cdp.setOnDateSetListener(new CalendarDatePickerDialogFragment.OnDateSetListener() {
                    @Override
                    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
                        try {

                            formatter = new SimpleDateFormat("dd/MM/yyyy");
                            String dateInString = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                            Date date = formatter.parse(dateInString);

                            formatter = new SimpleDateFormat("yyyy-MM-dd");

                            input_date.setText(formatter.format(date).toString());
                            new AsyncChoiceProduct().execute("http://www.geerang.com/gpos/api/ReportDialy");

                        } catch (Exception ex) {
                            input_date.setText(ex.getMessage().toString());
                        }
                    }
                });
            }
        });


        rvCatagory = (RecyclerView) findViewById(R.id.mRecyclerView);
        new AsyncChoiceProduct().execute("http://www.geerang.com/gpos/api/ReportDialy");


    }

    @Override
    protected void onResume() {
        super.onResume();
        //tvTableName.setText(StringUtil.TableCustomerName);
        //new AsyncChoiceProduct().execute();
        new AsyncChoiceProduct().execute("http://www.geerang.com/gpos/api/ReportDialy");
    }

    @Override
    public void onPtrReceive(final Printer printerObj, final int code, final PrinterStatusInfo status, final String printJobId) {
        runOnUiThread(new Runnable() {
            @Override
            public synchronized void run() {
                //       ShowMsg.showResult(code, epsonMakeErrorMessage.makeErrorMessage(status), DiscountActivity.this);
                //epsonMakeErrorMessage.dispPrinterWarnings(status);
                disconnectPrinter();
            }
        });
    }

    private void disconnectPrinter() {
        if (mPrinter == null) {
            return;
        }

        try {
            mPrinter.endTransaction();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                    //  ShowMsg.showException(e, "endTransaction", ReportDialyActivity.this);
                }
            });
        }

        try {
            mPrinter.disconnect();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                    //  ShowMsg.showException(e, "disconnect", ReportDialyActivity.this);
                }
            });
        }

        finalizeObject();
    }

    private class AsyncChoiceProductPrint extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(ReportDialyActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //this method will be running on UI thread

            pdLoading.setMessage("\tกำลังโหลดข้อมูล...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("store_id", Profile.StoreId)
                    .add("branch_id", Profile.BranchId)
                    .add("report_date", input_date.getText().toString())
                    .build();
            try {
                response = http.run("http://www.geerang.com/gpos/api/ReportDialy", formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {
            Resultbill = result;
            pdLoading.dismiss();

            editor = sp.edit();
            String PrinterMain = sp.getString(KEY_PrinterMain, "");
            if (!PrinterMain.equals("")) {
                //OpenPrinter+","+printer_title+","+printer_address+","+printer_mac_address+","+categories_id
                String[] namesList = PrinterMain.split(",");
                String status = namesList[0];
                String title = namesList[1];
                String address = namesList[2];
                String mac_address = namesList[3];
                String categories_id = namesList[4];

                PrinterAddress = address;
                //เลือกเครื่องปริ้นที่กำหนด
                if (!runPrintReceiptSequence()) {

                }


            } else {
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }

    }

    private boolean runPrintReceiptSequence() {

        if (!initializeObject()) {
            return false;
        }

        if (!createReceiptData()) {
            finalizeObject();
            return false;
        }

        if (!printData()) {
            finalizeObject();
            return false;
        }

        return true;
    }

    private boolean printData() {
        if (mPrinter == null) {
            return false;
        }

        if (!connectPrinter()) {
            mPrinter.clearCommandBuffer();
            return false;
        }

        try {
            mPrinter.sendData(Printer.PARAM_DEFAULT);
        } catch (Exception e) {
            mPrinter.clearCommandBuffer();
            //ShowMsg.showException(e, "sendData", mContext);
            try {
                mPrinter.disconnect();
            } catch (Exception ex) {
                // Do nothing
            }
            return false;
        }

        return true;
    }

    private boolean connectPrinter() {
        if (mPrinter == null) {
            return false;
        }

        try {
            // mPrinter.connect("192.168.1.103", Printer.PARAM_DEFAULT);
            mPrinter.connect(PrinterAddress, Printer.PARAM_DEFAULT);


        } catch (Exception e) {
            // ShowMsg.showException(e, "connect", mContext);
            return false;
        }

        return true;
    }

    private void finalizeObject() {
        if (mPrinter == null) {
            return;
        }

        mPrinter.clearCommandBuffer();

        mPrinter.setReceiveEventListener(null);

        mPrinter = null;
    }

    private boolean initializeObject() {
        try {
            // ((SpnModelsItem) mSpnLang.getSelectedItem()).getModelConstant()
//            mPrinter = new Printer(((SpnModelsItem) mSpnSeries.getSelectedItem()).getModelConstant(),
            mPrinter = new Printer(Printer.TM_T88, Printer.MODEL_ANK, mContext);
            //

        } catch (Exception e) {
            //ShowMsg.showException(e, "Printer", mContext);
            return false;
        }

        mPrinter.setReceiveEventListener(ReportDialyActivity.this);

        return true;
    }

    private boolean createReceiptData() {
        String method = "";

        StringBuffer textData = new StringBuffer();
        final int barcodeWidth = 2;
        final int barcodeHeight = 100;
        double sum_food = 0.0, sum_drink = 0.0, sum_sweet = 0.0, sum_other = 0.0;
        if (mPrinter == null) {
            return false;
        }

        try {
            //语言需要
            mPrinter.addTextLang(Printer.LANG_TH);
            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            method = "addFeedLine";
            //mPrinter.addFeedLine(1);

            textData.append("พิมพ์เมื่อ " + getDateTime());
            textData.append("\n");
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());
            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            textData.append("Daily Report\n");
            textData.append(String.format("%s \n", input_date.getText()));
            textData.append(String.format("%s \n", "" + "" + Profile.StoreName));
            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            //columns.print();
            total_price = 0;
            total_qty = 0;
            // String textLength = "______________________________________________";
            //int lengthStr = textLength.length(); //46
            method = "addText";
            //mPrinter.addTextAlign(Printer.ALIGN_RIGHT);
            //textData.append(String.format("%-5s %-14s %-5s %-5s %-5s %-5s \n", "ที่", "โต๊ะที่", "อาหาร", "เครื่องดื่ม", "ของหวาน", "อื่นๆ"));
            textData.append(String.format("%-3s %-17s %-5s %-5s %-5s %-5s \n", "ที่", "โต๊ะที่", "อาหาร", "เครื่องดื่ม", "ของหวาน", "อื่นๆ"));
            textData.append("------------------------------------------------\n");

            method = "addText";
            // mPrinter.addTextAlign(Printer.ALIGN_RIGHT);
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            double sum_price = 0.0;
            JSONArray jArray = null;
            try {

                double sum_row_price = 0.0;
                jArray = new JSONArray(Resultbill);

                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    sum_sweet += Double.parseDouble(json_data.getString("dessert"));
                    sum_drink += Double.parseDouble(json_data.getString("drink"));
                    sum_food += Double.parseDouble(json_data.getString("food"));
                    sum_other += Double.parseDouble(json_data.getString("other"));


                    String filled = StringUtils.rightPad(String.format("%-3s %-17s", "" + (i + 1), json_data.getString("customer_name")), 20, ' ');
                    String data = String.format("%5s %5s %5s %5s \n"
                            , Double.parseDouble(json_data.getString("food"))
                                    + "", Double.parseDouble(json_data.getString("drink")) + ""
                            , Double.parseDouble(json_data.getString("dessert")),
                            Double.parseDouble(json_data.getString("other")));
                    textData.append(filled);
                    textData.append(data);
                    /*
                    textData.append(String.format("%-4s %-11s %-5s %-5s %-5s %-5s \n"
                            ,"" + (i + 1)
                            , "" + json_data.getString("customer_name")
                            , "" +String.format("%.02f", Double.parseDouble(json_data.getString("food")))
                            , "" +String.format("%.02f", Double.parseDouble(json_data.getString("drink")))
                            ,  "" +String.format("%.02f", Double.parseDouble(json_data.getString("dessert")))
                            , "" +String.format("%.02f", Double.parseDouble(json_data.getString("other")))));

                     */
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());
            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            textData.append("------------------------------------------------\n");
            //textData.append("\n");
            textData.append(String.format("%-3s %-17s %-5s %-5s %-5s %-5s \n"
                    , "รวม"
                    , " "
                    , String.format("%.02f", sum_food)
                    , String.format("%.02f", sum_drink)
                    , String.format("%.02f", sum_sweet)
                    , String.format("%.02f", sum_other)));
            textData.append("\n");
            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());
            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            textData.append("THANK YOU\n");
            textData.append("POWER BY GPOS\n");
            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addCut";
            mPrinter.addCut(Printer.CUT_FEED);


        } catch (Exception e) {
            //ShowMsg.showException(e, method, mContext);
            return false;
        }

        textData = null;

        return true;
    }


    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    private class AsyncChoiceProduct extends AsyncTask<String, String, String> {

        ProgressDialog pdLoading = new ProgressDialog(ReportDialyActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread

            pdLoading.setMessage("\tกำลังโหลดข้อมูล...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("store_id", Profile.StoreId)
                    .add("branch_id", Profile.BranchId)
                    .add("report_date", input_date.getText().toString())
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
//                progressBar.setVisibility(View.GONE);

//                txt_status.setVisibility(View.GONE);

                ReportDialyActivity.Resultbill = result;

                pdLoading.dismiss();

                total_price = 0;
                total_qty = 0;

                sum_dessert = 0;
                sum_drink = 0;
                sum_food = 0;
                sum_other = 0;


                JSONArray jArray = new JSONArray(result);
//                if (jArray.length() <= 0) {
//                    empty_view.setVisibility(View.VISIBLE);
//
//                }

                feedsList = new ArrayList<>();
                try {

                    //JSONArray jArray = new JSONArray(result);

                    // Extract data from json and store into ArrayList as class objects
                    for (int i = 0; i < jArray.length(); i++) {
                        JSONObject json_data = jArray.getJSONObject(i);
                        FeedItemChoice item = new FeedItemChoice();

                        item.setEmployee_id(json_data.getString("order_id"));
                        item.setCustomer_name(json_data.getString("customer_name"));
                        item.setDessert(json_data.getString("dessert"));
                        item.setDrink(json_data.getString("drink"));
                        item.setFood(json_data.getString("food"));
                        item.setOther(json_data.getString("other"));
                        item.setOrder_id(json_data.getString("order_id"));

                        sum_dessert += Double.parseDouble(json_data.getString("dessert"));
                        sum_drink += Double.parseDouble(json_data.getString("drink"));
                        sum_food += Double.parseDouble(json_data.getString("food"));
                        sum_other += Double.parseDouble(json_data.getString("other"));

                        feedsList.add(item);
                    }

                    // Setup and Handover data to recyclerview

                    txt_sum_dessert.setText(sum_dessert + "");
                    txt_sum_drink.setText(sum_drink + "");
                    txt_sum_food.setText(sum_food + "");
                    txt_sum_other.setText(sum_other + "");

                    mAdapter = new ChoiceRecyclerAdapter(ReportDialyActivity.this, feedsList);
                    rvCatagory.setAdapter(mAdapter);
                    //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                    RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(ReportDialyActivity.this, 1);
                    rvCatagory.setLayoutManager(mLayoutManager);
                    rvCatagory.setItemAnimator(new DefaultItemAnimator());
                    rvCatagory.setItemAnimator(new DefaultItemAnimator());


                } catch (JSONException e) {
                    Toast.makeText(ReportDialyActivity.this, e.toString(), Toast.LENGTH_LONG).show();
                }


            } catch (JSONException e) {
//                txt_status.setVisibility(View.VISIBLE);
                Toast.makeText(ReportDialyActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    public class ChoiceRecyclerAdapter extends RecyclerView.Adapter<ChoiceRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemChoice> feedItemList;
        private Context mContext;

        public ChoiceRecyclerAdapter(Context context, List<FeedItemChoice> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.dialyreport_list_row, null);

            CustomViewHolder viewHolder = new CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
            final FeedItemChoice feedItem = feedItemList.get(i);
            customViewHolder.feedItem = feedItem;


            //Setting text view title
            customViewHolder.title_choice.setText(Html.fromHtml("" + feedItem.getCustomer_name()));
            customViewHolder.txt_food.setText(Html.fromHtml("" + feedItem.getFood()));
            customViewHolder.txt_drink.setText(Html.fromHtml("" + feedItem.getDrink()));
            customViewHolder.txt_sweet.setText(Html.fromHtml("" + feedItem.getDessert()));
            customViewHolder.txt_other.setText(Html.fromHtml("" + feedItem.getOther()));
        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            //            protected ImageView imageView;
            protected TextView title_choice, txt_food, txt_drink, txt_sweet, txt_other;

            FeedItemChoice feedItem;

            public CustomViewHolder(View view) {
                super(view);

                this.title_choice = (TextView) view.findViewById(R.id.txtCheckIn);
                this.txt_food = (TextView) view.findViewById(R.id.txt_food);
                this.txt_drink = (TextView) view.findViewById(R.id.txt_drink);
                this.txt_sweet = (TextView) view.findViewById(R.id.txt_sweet);
                this.txt_other = (TextView) view.findViewById(R.id.txt_other);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

//                        StringUtil.CatId = feedItem.getId();
                        //new AsyncProduct().execute();
                        //Toast.makeText(v.getContext(), "Select is: " + feedItem.getId(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }
    }

    private String getDateTime() {
        // get date time in custom format
        SimpleDateFormat sdf = new SimpleDateFormat("[yyyy/MM/dd HH:mm]");
        return sdf.format(new Date());
    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    // String
    public class FeedItemChoice {

        private String employee_id;
        private String firstname;
        private String lastname;
        private String tel;
        private String status_name;

        private String customer_name;

        private String order_id;
        private String food;
        private String drink;
        private String dessert;
        private String other;

        public String getDrink() {
            return drink;
        }

        public void setDrink(String drink) {
            this.drink = drink;
        }

        public void setOther(String other) {
            this.other = other;
        }

        public String getOther() {
            return other;
        }

        public void setFood(String food) {
            this.food = food;
        }

        public String getFood() {
            return food;
        }

        public void setDessert(String dessert) {
            this.dessert = dessert;
        }

        public String getDessert() {
            return dessert;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getCustomer_name() {
            return customer_name;
        }

        public void setCustomer_name(String customer_name) {
            this.customer_name = customer_name;
        }


        public String getEmployee_id() {
            return employee_id;
        }

        public void setEmployee_id(String employee_id) {
            this.employee_id = employee_id;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getStatus_name() {
            return status_name;
        }

        public void setStatus_name(String status_name) {
            this.status_name = status_name;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }
    }

}
