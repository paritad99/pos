package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.StrictMode;
import android.text.InputFilter;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection;

public class OrderCheckoutActivity extends AppCompatActivity {

    Button btnCheckOut;
    RecyclerView mRecyclerView;
    TextView tvTableName, tvSum_Amount, txtItem_Qty;
    Button btnNo, btnYes;
    private SectionedRecyclerViewAdapter sectionAdapter;
    ArrayList<HashMap<String, String>> MyArrList_GroupChoice;

    ArrayList<HashMap<String, String>> ChoiceArrList = new ArrayList<HashMap<String, String>>();
    HashMap<String, String> map_choice;
    private SimpleAdapter mFirstHeader;

    int total_price = 0;
    int total_qty = 0;
    int total_qty_check = 0;

    int check_receipt = 0;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
     AlertDialog alertDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_checkout);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }


        btnNo = (Button) findViewById(R.id.btnNo);
        btnYes = (Button) findViewById(R.id.btnYes);

        EnableButton();

        tvTableName = (TextView) findViewById(R.id.tvTableName);
        tvSum_Amount = (TextView) findViewById(R.id.txtTotalPrice);
        txtItem_Qty = (TextView) findViewById(R.id.txtTotalItemQty);

//        txtCustomer = (TextView) findViewById(R.id.txtCustomer);
//        txtCustomer.setText(StringUtil.TotalCustomer);


//        txt_status = (TextView) findViewById(R.id.txt_status);

        mRecyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);

        btnCheckOut = (Button) findViewById(R.id.btnCheckOut);
        btnCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CheckOrderOrderTask().execute(StringUtil.URL + "gpos/api/CheckOrderOrder",StringUtil.OrderId);
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check_receipt = 0;
                EnableButton();
            }
        });

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check_receipt = 1;
                EnableButton();
            }
        });
    }

    public void EnableButton() {
        if (check_receipt == 0) {
            //btnNo.setEnabled(true);
            btnNo.setBackground(ContextCompat.getDrawable(OrderCheckoutActivity.this, R.drawable.rounded_button_yellow));
            btnNo.setTextColor(Color.WHITE);
            //btnYes.setEnabled(false);
            btnYes.setBackground(ContextCompat.getDrawable(OrderCheckoutActivity.this, R.drawable.rounded_button_yellow_border));
            btnYes.setTextColor(Color.GRAY);
        } else {
            //btnNo.setEnabled(false);
            btnNo.setBackground(ContextCompat.getDrawable(OrderCheckoutActivity.this, R.drawable.rounded_button_yellow_border));
            btnNo.setTextColor(Color.GRAY);
            //btnYes.setEnabled(true);
            btnYes.setBackground(ContextCompat.getDrawable(OrderCheckoutActivity.this, R.drawable.rounded_button_yellow));
            btnYes.setTextColor(Color.WHITE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        tvTableName.setText(StringUtil.TableCustomerName);
        new AsyncChoiceProduct().execute();
        EnableButton();

    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    private class CheckOrderOrderTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(OrderCheckoutActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลัง Chock out...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("orders_id", urls[1])
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);

            new AsyncChoiceProduct().execute();

            pdLoading.dismiss();


            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                //before inflating the custom alert dialog layout, we will get the current activity viewgroup
                ViewGroup viewGroup = findViewById(android.R.id.content);

                //then we will inflate the custom alert dialog xml that we created
                View dialogView = LayoutInflater.from(OrderCheckoutActivity.this).inflate(R.layout.layout_alert_dialog_payment_success, viewGroup, false);
                Button btnOk = (Button) dialogView.findViewById(R.id.btnOk);
                //Now we need an AlertDialog.Builder object
                AlertDialog.Builder builder = new AlertDialog.Builder(OrderCheckoutActivity.this);

                //setting the view of the builder to our custom view that we already inflated
                builder.setView(dialogView);

                //finally creating the alert dialog and displaying it
                alertDialog = builder.create();
                alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                btnOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        /*StringUtil.payment_status = "";

                        Intent in = new Intent(OrderCheckoutActivity.this, StaffDeliveryActivity.class);
                        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(in);
                        finish();*/
                        if (Profile.Position.equals("1")) {
                            StringUtil.employee_status = "พนักงานครัว";

                            Intent in = new Intent(OrderCheckoutActivity.this, ECookListActivity.class);
                            in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(in);
                            finish();
                            //คนรับ order
                        } else if (Profile.Position.equals("6")) {
                            StringUtil.employee_status = "Customer Service";
                            Intent in = new Intent(OrderCheckoutActivity.this, StaffDeliveryActivity.class);
                            in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(in);
                            finish();

                            //drink
                        } else if (Profile.Position.equals("3")) {
                            Intent in = new Intent(OrderCheckoutActivity.this, ETableSelectActivity.class);
                            in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(in);
                            finish();
                        } else if (Profile.Position.equals("4")) // แคชเชียร์
                        {
                            StringUtil.employee_status = "แคชเชียร์";
                                Intent in = new Intent(OrderCheckoutActivity.this, StaffCashireActivity.class);
                                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                startActivity(in);
                                finish();




                        } else if (Profile.Position.equals("2")) {
                            StringUtil.employee_status = "ผู้จัดการร้าน";

                            Intent in = new Intent(OrderCheckoutActivity.this, ManagerMainActivity.class);
                            in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(in);
                            finish();

                        } else {
                            Intent in = new Intent(OrderCheckoutActivity.this, LoginActivity.class);
                            in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(in);
                            finish();
                        }


                        //alertDialog.dismiss();

                    }
                });
                alertDialog.show();


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class AsyncChoiceProduct extends AsyncTask<String, String, String> {


        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run("http://www.geerang.com/gpos/api/MenuOrderList/" + StringUtil.OrderId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {
            try {
//                progressBar.setVisibility(View.GONE);

//                txt_status.setVisibility(View.GONE);

                total_price = 0;
                total_qty = 0;

                JSONArray jArray = new JSONArray(result);
                sectionAdapter = new SectionedRecyclerViewAdapter();
                for (int i = 0; i < jArray.length(); i++) {

                    total_qty_check++;

                    JSONObject json_data = jArray.getJSONObject(i);
                    //ประกาศ array string
                    ArrayList<String> arrSports = new ArrayList<>();
                    // array hasmap
                    MyArrList_GroupChoice = new ArrayList<HashMap<String, String>>();
                    HashMap<String, String> map;


                    int total_row_qty = 0;
                    total_row_qty = Integer.parseInt(json_data.getString("qty"));

                    String confirm = json_data.getString("is_confirm");
                    // เก็บตัวแปร Choice
                    String Choice = "";
                    int total_row_price = 0;
                    // เก็บราคาใน order
                    total_row_price = Integer.parseInt(json_data.getString("product_sale_price"));


                    JSONArray jArray2 = new JSONArray(json_data.getString("product_choice"));
                    int check_list = jArray2.length();
                    if (check_list > 0) {
                        for (int j = 0; j < jArray2.length(); j++) {

                            JSONObject json_data2 = jArray2.getJSONObject(j);
                            arrSports.add(json_data2.getString("product_choice_name"));

                            // หาราคารวมของแต่ละ row แล้วนำไปยัดใส่ใน header row
                            int choice_price = Integer.parseInt(json_data2.getString("choice_price"));
                            total_row_price += choice_price;


                            Choice += json_data2.getString("product_choice_name") + ",";

                            map = new HashMap<String, String>();
                            map.put("product_choice_name", json_data2.getString("product_choice_name"));
                            map.put("orders_item_group_id", json_data2.getString("orders_item_group_id"));
                            map.put("choice_price", json_data2.getString("choice_price"));
                            MyArrList_GroupChoice.add(map);
                        }
                    }


//                        int choice_price = Integer.parseInt(MyArrList_choice.get(position).get("choice_price"));
                    total_price = total_price + (total_row_price * total_row_qty);
                    total_qty = total_qty + total_row_qty;

                    mFirstHeader = new SimpleAdapter(MyArrList_GroupChoice
                            , "" + json_data.getString("product_name")
                            , (total_row_price * total_row_qty) + ""
                            , json_data.getString("qty") + ""
                            , json_data.getString("orders_item_id")
                            , Choice
                            , confirm
                            , json_data.getString("remain"));
                    sectionAdapter.addSection(mFirstHeader);


                }

                txtItem_Qty.setText("Total " + total_qty + " Items");

                DecimalFormat formatter = new DecimalFormat("#,###.00");

                //double amount = Double.parseDouble(total_price);
                System.out.println(formatter.format(total_price));

                tvSum_Amount.setText("THB " + formatter.format(total_price) + "");


//                final GridLayoutManager glm = new GridLayoutManager(CashireTableOrderActivity.this, 5);
//                glm.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
//                    @Override
//                    public int getSpanSize(final int position) {
//                        if (sectionAdapter.getSectionItemViewType(position) == SectionedRecyclerViewAdapter.VIEW_TYPE_HEADER) {
//                            return 5;
//                        }
//                        return 1;
//                    }
//                });

//                mRecyclerView.setLayoutManager(glm);
//
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(OrderCheckoutActivity.this, LinearLayoutManager.VERTICAL, false);
                mRecyclerView.setLayoutManager(linearLayoutManager);
                mRecyclerView.setAdapter(sectionAdapter);


            } catch (JSONException e) {
//                txt_status.setVisibility(View.VISIBLE);
                Toast.makeText(OrderCheckoutActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }


    public class SimpleAdapter extends StatelessSection {

        //        private ArrayList<String> arrName = new ArrayList<>();
        ArrayList<HashMap<String, String>> MyArrList_choice = new ArrayList<HashMap<String, String>>();
        private String mTitle, Choice;
        private int price;
        private int qty;
        private String confirm;
        private String comment;
        private int select_item_count = 0;
        private int total_price_row;
        private String orders_item_id;

        public SimpleAdapter(ArrayList<HashMap<String, String>> arrName
                , String title, String price, String qty
                , String orders_item_id
                , String Choice
                , String confirm, String comment) {
            super(R.layout.layout_menu_order_item_header, R.layout.layout_menu_order_item);

            this.MyArrList_choice = arrName;
            this.mTitle = title;
            this.price = Integer.parseInt(price);
            this.qty = Integer.parseInt(qty);
            this.Choice = Choice;
            this.confirm = confirm;
            this.orders_item_id = orders_item_id;
            this.comment = comment;


        }

        @Override
        public int getContentItemsTotal() {
            return MyArrList_choice.size();
        }

        @Override
        public RecyclerView.ViewHolder getItemViewHolder(View view) {
            return new ItemViewHolder(view);
        }

        @Override
        public RecyclerView.ViewHolder getHeaderViewHolder(View view) {
            return new HeaderViewHolder(view);
        }

        @Override
        public void onBindItemViewHolder(RecyclerView.ViewHolder holder, final int position) {
            final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;


            itemViewHolder.txtItem.setText("*" + MyArrList_choice.get(position).get("product_choice_name"));
            itemViewHolder.txtPrice.setText("+฿" + MyArrList_choice.get(position).get("choice_price"));
//            itemViewHolder.checkBox.setTag(MyArrList_choice.get(position).get("Code"));

        }

        @Override
        public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder) {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
            headerViewHolder.txtHeader.setText(mTitle);

            DecimalFormat formatter = new DecimalFormat("#,###.00");

            //double amount = Double.parseDouble(total_price);
            System.out.println(formatter.format(total_price));

           // tvSum_Amount.setText("THB " +  + "");

            headerViewHolder.txtPrice.setText("฿" + formatter.format(price));
            headerViewHolder.txtComment.setText(comment);

            headerViewHolder.txtItemQty.setText(qty + "");

            headerViewHolder.txtDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(OrderCheckoutActivity.this);
                    builder.setTitle(StringUtil.TableCustomerId + "#ยืนยันการลบข้อมูล!\nกรุณาระบุเหตุผลการขอลบ\n(ความยาวไม่เกิน 120 ตัวอักษร)");
                    final EditText input = new EditText(OrderCheckoutActivity.this);
                    input.setTextSize(14);
                    input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(120)});
                    input.setInputType(InputType.TYPE_CLASS_TEXT);
                    builder.setView(input);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            new ItemDeleteTask().execute(StringUtil.URL + "gpos/api/OrderItemDelete"
                                    , input.getText().toString()
                                    , orders_item_id);

                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    builder.show();


                }
            });
//            headerViewHolder.linearLayout1.Set
            if(!Choice.isEmpty())
            {
                headerViewHolder.txtChoice.setText(Choice.substring(0, Choice.length() - 1) + "");
            }else
            {
                headerViewHolder.txtChoice.setVisibility(View.GONE);
            }

            headerViewHolder.btnReduce.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(OrderCheckoutActivity.this);
                    builder.setTitle("ยืนยันการลดข้อมูลสินค้า!");
                    builder.setPositiveButton("ยืนยัน", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            int sum_qty = qty - 1;
                            new ItemQtyUpdateTask().execute(StringUtil.URL + "gpos/api/OpenTable_UpdateOrderQty"
                                    , orders_item_id, sum_qty + "");
                            new AsyncChoiceProduct().execute();

                        }
                    });
                    builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    builder.show();


                }
            });

            headerViewHolder.btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(OrderCheckoutActivity.this);
                    builder.setTitle("ยืนยันการเพิ่มข้อมูลสินค้า!");
                    builder.setPositiveButton("ยืนยัน", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            int sum_qty = qty + 1;
                            new ItemQtyUpdateTask().execute(StringUtil.URL + "gpos/api/OpenTable_UpdateOrderQty"
                                    , orders_item_id, sum_qty + "");

                            new AsyncChoiceProduct().execute();

                        }
                    });
                    builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    builder.show();



                }
            });

            headerViewHolder.txtConfirStatus.setText("จำนวนถูกต้อง");

            // show status
            if (confirm.equals("1")) {

                headerViewHolder.btnAddComment.setVisibility(View.GONE);
                headerViewHolder.btnAdd.setVisibility(View.VISIBLE);
                headerViewHolder.btnReduce.setVisibility(View.VISIBLE);
                headerViewHolder.txtConfirStatus.setVisibility(View.VISIBLE);
                headerViewHolder.btnAddComment.setVisibility(View.GONE);
                // show button + -
            } else {
                headerViewHolder.btnAddComment.setVisibility(View.VISIBLE);
                headerViewHolder.btnAdd.setVisibility(View.VISIBLE);
                headerViewHolder.btnReduce.setVisibility(View.VISIBLE);
                headerViewHolder.txtConfirStatus.setVisibility(View.VISIBLE);
                headerViewHolder.txtDelete.setVisibility(View.GONE);
                headerViewHolder.btnAddComment.setVisibility(View.GONE);
            }

//            txtConfirStatus


        }

        protected class HeaderViewHolder extends RecyclerView.ViewHolder {

            protected TextView txtHeader, txtDelete;
            protected TextView txtPrice, txtChoice, txtItemQty, txtConfirStatus, txtComment;

            protected Button btnReduce, btnAdd, btnAddComment;

            private HeaderViewHolder(View itemView) {
                super(itemView);
                txtDelete = (TextView) itemView.findViewById(R.id.txtDelete);

                txtHeader = (TextView) itemView.findViewById(R.id.txtHeader);
                txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
                txtItemQty = (TextView) itemView.findViewById(R.id.txtItemQty);
                txtConfirStatus = (TextView) itemView.findViewById(R.id.txtConfirStatus);
                txtComment = (TextView) itemView.findViewById(R.id.txtRemain);

                txtChoice = (TextView) itemView.findViewById(R.id.txtChoice);
                btnReduce = (Button) itemView.findViewById(R.id.btnReduce);
                btnAdd = (Button) itemView.findViewById(R.id.btnAdd);
                btnAddComment = (Button) itemView.findViewById(R.id.btnAddComment);

            }
        }

        protected class ItemViewHolder extends RecyclerView.ViewHolder {

            protected TextView txtItem, txtPrice;
            protected LinearLayout linearLayout1;

            //            protected CheckBox checkBox;
            private ItemViewHolder(View itemView) {
                super(itemView);
                txtItem = (TextView) itemView.findViewById(R.id.txtChoice);
                txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
                linearLayout1 = (LinearLayout) itemView.findViewById(R.id.linearLayout1);
                linearLayout1.setVisibility(View.GONE);

//                checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);


            }
        }
    }

    private class ItemDeleteTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(OrderCheckoutActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลังลบข้อมูล...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("remark", urls[1])
                    .add("orders_item_id", urls[2])
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);

            new AsyncChoiceProduct().execute();

            pdLoading.dismiss();


            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    private class ItemQtyUpdateTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(OrderCheckoutActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tUpdating...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("orders_item_id", urls[1])
                    .add("qty", urls[2])
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);

            new AsyncChoiceProduct().execute();

            pdLoading.dismiss();


            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

}
