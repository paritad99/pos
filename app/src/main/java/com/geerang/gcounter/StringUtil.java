package com.geerang.gcounter;

public class StringUtil {

    public static final int TM_M10 = 0;
    public static final int TM_M30 = 1;
    public static final int TM_P20 = 2;
    public static final int TM_P60 = 3;
    public static final int TM_P60II = 4;
    public static final int TM_P80 = 5;
    public static final int TM_T20 = 6;
    public static final int TM_T60 = 7;
    public static final int TM_T70 = 8;
    public static final int TM_T81 = 9;
    public static final int TM_T82 = 10;
    public static final int TM_T83 = 11;
    public static final int TM_T88 = 12;
    public static final int TM_T90 = 13;
    public static final int TM_T90KP = 14;
    public static final int TM_U220 = 15;
    public static final int TM_U330 = 16;
    public static final int TM_L90 = 17;
    public static final int TM_H6000 = 18;
    public static final int TM_T83III = 19;
    public static final int TM_T100 = 20;

    public static String product_price = "";
    public static String menu_unit = "";

    public static String GroupName = "";
    public static String GroupDescription = "";
    public static String GroupMin = "";
    public static String GroupMax = "";
    public static String dicount_type_report = "";
    public static String StateChoice = "";

//     "package_product": "5",
//             "package_branch": "1",
//             "package_cashier": "1",
//             "package_users": "1",
//             "package_zone": "1",
//             "package_table": "5",

    public static String package_name = "";
    public static String package_product = "";
    public static String package_branch = "";
    public static String package_cashier = "";
    public static String package_users = "";
    public static String package_zone = "";
    public static String package_table = "";

    public static String zone_name = "";

    public static String payment_status = "";

    public static String promotion_discount_percen_type = "";
    public static int promotion_discount_percen_total = 0;
    public static int  promotion_discount_amount = 0;

    public static String id_printer = "";
    public static int is_printer = 0;

    public static String dicount_type = "เปอร์เซ็น";
    public static int dicount_total = 0;
    public static String discount_total_payment = "";

    public static String total_payment_sum = "";
    public static String  TotalOrderQty= "";
    public static String printer_name = "";
    public static String printer_address = "";
    public static String custom_service = "0";
    public static Boolean vat = false;
    public static String report_cat_name = "";


    public static final String userprefernce = "user";
    public static final String UId = "user_id";
    public static final String Username = "username";
    public static final String Store = "store";
    public static final String SId = "store_id";
    public static final String BId = "branch_id";
    public static String total_customer = "";
    public static String employee_status = "";
    public static String zone_id = "";
    public static String zone_Name = "";
    public static String zone_Bid = "";
    public static String product_menu_id = "";
    public static String p_group_id = "";

    public static String OrderId_Delete = "";
    public static String OrderMenuname_Delete = "";
    public static String OrderUsername_Delete = "";
    public static String OrderRemain_Delete = "";
    public static String OrderTel_Delete = "";
    public static String TableCustomerId_new = "";
    public static String Building_Id = "";
    public static String Building_Name = "";
    public static String Building_Person = "";
    public static String Building_Branch = "";
    public static String Id = "";
    public static String CatId = "";
    public static String TableCustomerName = "";
    public static String BranchId = "";
    public static String BranchName = "";
    public static String BranchAddress = "";
    public static String BranchStatus = "";
    public static String TotalCustomer = "";
    public static String OrderId = "";
    public static String ProductQty = "";
    public static String ProductId = "";
    public static String ProductAmount = "";
    public static String ProductPrice = "";
    public static String OrderQty = "";

    public static String payment_change = "";
    public static String input_amount = "";
    public static String cash_payment = "";

    public static String TotalPayment = "";

    public static String Printer_Address = "";
    public static int printerSeries;
    public static int lang;
    public static String TableCustomer = "";
    public static String ItemId = "";
    public static String Orders_item_id = "";

    public static String is_cancel = "0";
    public static String table_is_cancel = "0";

    public static String promotion_id = "";
    public static int promotion_use_qty = 0;
    public static String promotion_branch_id = "";
    public static String  TableCustomerId = "";
    public static String Employee_id = "";


    //for task worker
    public static String ProductName = "";
    public static String URL = "http://www.geerang.com/";
    public static String arr_orderid = "";
    public static int Is_bill_merge = 0;

    public static String BookingTableId = "";
    public static String BookingTableName = "";
    public static String BookingDate = "";

    public static String ProcuctId = "";

    public static String CancelOrderId = "";

    // menu edit

    public static String menu_image_data = "";
    public static String menu_store_id = "";
    public static String menu_categories_id = "";
    public static String menu_name = "";
    public static String menu_product_detail = "";
    public static String menu_product_price = "";
    public static String menu_cookingtime = "";
    public static String menu_calories = "";
    public static String menu_is_recommend = "";


    public String removeFirstChar(String s) {
        return s.substring(1);
    }


}