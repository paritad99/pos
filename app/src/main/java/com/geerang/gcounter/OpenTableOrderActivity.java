package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.app.Service;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.IBinder;
import android.os.StrictMode;
import android.text.InputFilter;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection;
import recieptservice.com.recieptservice.PrinterInterface;


public class OpenTableOrderActivity extends AppCompatActivity {

    RecyclerView mRecyclerView;
    TextView tvTableName, tvSum_Amount, txtItem_Qty, txtCustomer, txt_status;
    Button btnSelectMenu, btnCancel, btnGotoHome, btnDeliveryOrder;
    private SectionedRecyclerViewAdapter sectionAdapter;
    ArrayList<HashMap<String, String>> MyArrList_GroupChoice;

    ArrayList<HashMap<String, String>> ChoiceArrList = new ArrayList<HashMap<String, String>>();
    HashMap<String, String> map_choice;
    private SimpleAdapter mFirstHeader;

    int total_price = 0;
    int total_qty = 0;
    int check_order_place = 0;

    IBinder service;
    PrinterInterface mAidl;

    String Resultbill = "";

    Profile profile;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            //new TableCancelOrderTask().execute(StringUtil.URL + "gpos/api/TableCancelOrder", "");
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //new TableCancelOrderTask().execute(StringUtil.URL + "gpos/api/TableCancelOrder", "");
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    Button btnGotoCheckOut;
    int total_qty_check = 0;
    private CoordinatorLayout coordinatorLayout;

    Boolean check_click_order = false;

    ProgressBar progressBarOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cashire_table_order);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        profile = new Profile(this);

        progressBarOrder = (ProgressBar) findViewById(R.id.progressBarOrder);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);

        tvTableName = (TextView) findViewById(R.id.tvTableName);
        tvSum_Amount = (TextView) findViewById(R.id.tvSum_Amount);
        txtItem_Qty = (TextView) findViewById(R.id.txtItem_Qty);

        txtCustomer = (TextView) findViewById(R.id.txtCustomer);
        txtCustomer.setText(""+ StringUtil.TotalCustomer);


        txt_status = (TextView) findViewById(R.id.txt_status);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        btnSelectMenu = (Button) findViewById(R.id.btnSelectMenu);
        btnSelectMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Profile.Position.equals("6"))
                {
                    if (StringUtil.payment_status.equals("CHECKOUT")) {
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, "ออเดอร์กำลัง Check out ไม่สามารถเพิ่มออเดอร์ได้", Snackbar.LENGTH_LONG)
                                .setAction("OK", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                    }
                                });
                        snackbar.show();
                    } else {
                        Intent in = new Intent(OpenTableOrderActivity.this, MenuCatagoriesActivity.class);
                        startActivity(in);
                    }

                }else
                {
                    Intent in = new Intent(OpenTableOrderActivity.this, MenuCatagoriesActivity.class);
                    startActivity(in);
                }



            }
        });


        btnGotoCheckOut = (Button) findViewById(R.id.btnGotoCheckOut);
        //btnGotoCheckOut.setEnabled(false);
        btnGotoCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(OpenTableOrderActivity.this, OrderCheckoutActivity.class);
                startActivity(in);

            }
        });

//        Intent intent = new Intent();
//        intent.setClassName("recieptservice.com.recieptservice", "recieptservice.com.recieptservice.service.PrinterService");
//        bindService(intent, new ServiceConnection() {
//            @Override
//            public void onServiceConnected(ComponentName name, final IBinder _service) {
//                service = _service;
//                mAidl = PrinterInterface.Stub.asInterface(service);
//
//            }
//
//            @Override
//            public void onServiceDisconnected(ComponentName name) {
//
//            }
//        }, Service.BIND_AUTO_CREATE);


        btnDeliveryOrder = (Button) findViewById(R.id.btnCashireMethod);
        btnDeliveryOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                if (total_qty_check == 0) {
//                    AlertError("กรุณาเลือกสินค้า/บริการ ก่อนการชำระเงิน");
//                } else {
//                    Intent in = new Intent(OpenTableOrderActivity.this, CashierActivity.class);
//                    startActivity(in);

                check_click_order = true;

                AlertDialog.Builder builder = new AlertDialog.Builder(OpenTableOrderActivity.this);
                builder.setTitle(StringUtil.TableCustomerId + "#กรุณายืนยันการส่งออเดอร์");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // print รายการสั่งซื้อไปยังครัว
                        new PlaceOrderConfirmTask().execute(StringUtil.URL + "gpos/api/ConfirmOrderItem", StringUtil.OrderId);

                       // new AsyncChoiceProductPrint().execute();

//                        if(check_click_order == true)
//                        {
//                            btnGotoCheckOut.setEnabled(check_click_order);
//                            btnGotoCheckOut.setBackground(ContextCompat.getDrawable(OpenTableOrderActivity.this, R.drawable.rounded_button_yellow));
//                            btnGotoCheckOut.setTextColor(Color.WHITE);
//                        }

                    }
                });


                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();


//                }

            }
        });


        btnGotoHome = (Button) findViewById(R.id.btnGotoHome);
        btnGotoHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(OpenTableOrderActivity.this);
                builder.setTitle(StringUtil.TableCustomerId + "#กรุณาระบุเหตุผลการยกเลิก\n(ความยาวไม่เกิน 120 ตัวอักษร)");
                final EditText input = new EditText(OpenTableOrderActivity.this);
                input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(120)});
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (input.length() < 5) {
                            AlertError("กรุณาระบุเหตุผลของการยกเลิก");
                            dialog.cancel();

                        } else {
                            new TableCancelOrderTask().execute(StringUtil.URL + "gpos/api/TableCancelOrder", input.getText().toString());

                        }

                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();

            }
        });
    }

    public void AlertError(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(OpenTableOrderActivity.this);
        builder.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things

                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        tvTableName.setText("โต๊ะ: "+StringUtil.TableCustomerName);
        new AsyncChoiceProduct().execute();


    }


//    private class AsyncChoiceProductPrint extends AsyncTask<String, String, String> {
//
//
//        HttpURLConnection conn;
//        URL url = null;
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//
//            //this method will be running on UI thread
//
//        }
//
//        @Override
//        protected String doInBackground(String... urls) {
//            String response = null;
//            getHttp http = new getHttp();
//            try {
//                response = http.run("http://www.geerang.com/gpos/api/MenuOrderList/" + StringUtil.OrderId);
//            } catch (IOException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//
//
////                txtResult.setText(response);
//            return response;
//
//
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            Resultbill = result;
//            try {
//                mAidl.setAlignment(1);
//                try {
//                    mAidl.printBitmap(BitmapFactory.decodeResource(getResources(),R.mipmap.ic_launcher));
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                mAidl.printText(profile.StoreName+"\n");
//                mAidl.printText(profile.store_address+"\n");
//                mAidl.printText("ใบเสร็จรับเงิน/RECEIPT\n");
//                mAidl.printText("\n");
//                mAidl.setTextSize(20);
//                mAidl.printTableText(new String[]{"Receipt No."+ StringUtil.OrderId, "Order Type. "+ "Dine In "}, new int[]{1, 1}, new int[]{0, 0});
//                mAidl.printTableText(new String[]{"Invoice By."+ profile.Username, "Table."+ "" + StringUtil.TableCustomerName}, new int[]{ 1, 1}, new int[]{ 0, 0});
//                mAidl.printTableText(new String[]{"Staff." + profile.Username, getDateTime()+""}, new int[]{1, 1}, new int[]{ 0, 0});
//                mAidl.printText("------------------------------------\n");
//                mAidl.printTableText(new String[]{"", "Qty", "Price", "Total"}, new int[]{1, 1, 1, 1}, new int[]{0, 2, 2, 2});
//                mAidl.setAlignment(1);
//                mAidl.printText("------------------------------------\n");
//
//                //columns.print();
//                total_price = 0;
//                total_qty = 0;
//
//                double sum_price = 0.0;
//                JSONArray jArray = null;
//                try {
//
//                    double sum_row_price = 0.0;
//                    jArray = new JSONArray(Resultbill);
//                    // loop ของ report category
//                    for (int f = 0; f < jArray.length(); f++) {
//                        JSONObject json_data_report = jArray.getJSONObject(f);
//
//                        JSONArray jArray_menu = new JSONArray(json_data_report.getString("products"));
//                        //textData.append("------------------------------\n");
//                        mAidl.setAlignment(0);
//                        mAidl.setTextSize(18);
//                        mAidl.printText(json_data_report.getString("report_cat_name") + "\n");
//
//                        if (jArray_menu.length() > 0) {
//                            // loop ของ products
//                            int item_count_row = 0;
//                            int total_row_qty = 0;
//                            double total_price = 0.0;
//                            double total_price_by_cat = 0.0;
//
//                            int total_row_price = 0;
//                            for (int i = 0; i < jArray_menu.length(); i++) {
//
//                                JSONObject json_data = jArray_menu.getJSONObject(i);
//                                total_row_qty = Integer.parseInt(json_data.getString("qty"));
//                                item_count_row += total_row_qty;
//                                String confirm = json_data.getString("is_confirm");
//                                // เก็บตัวแปร Choice
//                                String Choice = "";
//
//                                // เก็บราคาใน order
//                                total_row_price = Integer.parseInt(json_data.getString("product_sale_price"));
//
//                                int order_place = Integer.parseInt(confirm);
//                                if (order_place == 0) {
//                                    check_order_place++;
//                                }
//                                // loop choice
//                                JSONArray jArray2 = new JSONArray(json_data.getString("product_choice"));
//                                int check_list = jArray2.length();
//                                if (check_list > 0) {
//                                    for (int j = 0; j < jArray2.length(); j++) {
//                                        JSONObject json_data2 = jArray2.getJSONObject(j);
//                                        // หาราคารวมของแต่ละ row แล้วนำไปยัดใส่ใน header row
//                                        int choice_price = Integer.parseInt(json_data2.getString("choice_price"));
//                                        total_row_price += choice_price;
//                                        Choice += json_data2.getString("product_choice_name") + ",";
//                                    }
//                                }
//
//                                total_price = total_price + (total_row_price * total_row_qty);
//                                total_qty = total_qty + total_row_qty;
//
//                                // หาผลร่วมระหว่าง หมวดหมู่
//                                total_price_by_cat +=  (total_price * total_row_qty);
//
//                                // หาผลร่วมระหว่าง ทั้งหมด
//                                sum_row_price = sum_row_price + total_price;
//                                String product_name = json_data.getString("product_name");
//
//                                mAidl.printTableText(new String[]{product_name, total_row_qty + "", String.format("%.02f", total_price)   + "", String.format("%.02f", (total_price * total_row_qty))  + ""}, new int[]{ 1, 1, 1, 1}, new int[]{0, 2, 2, 2});
//
//                            }
//
//
//                            mAidl.printTableText(new String[]{"Total " + item_count_row +" items", String.format("%.02f", total_price_by_cat)  + ""}, new int[]{1, 1}, new int[]{0, 2});
//
//                            mAidl.setAlignment(1);
//                            mAidl.printText("---------------------------------------\n");
//                            // หาผลรวมทั้งหมด
//                            sum_price += total_price_by_cat;
//
//                            // reset ให้เป็น 0 เพื่อหา row ต่อไป
//                            total_price_by_cat = 0;
//
//                        } else {
//                            mAidl.printTableText(new String[]{"-", "", "",""}, new int[]{ 1, 1, 1, 1}, new int[]{0, 2, 2, 2});
//                        }
//                    }
//
//                    mAidl.setAlignment(1);
//                    mAidl.printText("---------------------------------------");
//                    mAidl.nextLine(1);
//                    mAidl.printTableText(new String[]{"Total " + total_qty, " items", "", String.format("%.02f", sum_price) + ""}, new int[]{1, 1, 1, 1}, new int[]{0, 0, 0, 2});
//                    mAidl.setAlignment(1);
//                    mAidl.printText("\n");
//                    try {
////                        Double total_price_result = 0.0;
////                        total_price_result = Double.parseDouble(sum_price+"")  - Double.parseDouble(StringUtil.discount_total_payment);
//                        //// StringUtil.dicount_type StringUtil.dicount_total
//                        // หาค่า ส่วนลด ว่าลดไปเท่าไร
//                        double dis_res = sum_price - Double.parseDouble(StringUtil.TotalPayment);
//
//                        mAidl.printTableText(new String[]{"SubTotal ", String.format("%.02f", sum_price) + ""}, new int[]{1, 1}, new int[]{ 0, 2});
//                        mAidl.printTableText(new String[]{"Discount "+ StringUtil.dicount_type,""+StringUtil.dicount_total}, new int[]{1, 1}, new int[]{0, 2});
//                        mAidl.printTableText(new String[]{"Total ",StringUtil.discount_total_payment+""}, new int[]{1, 1}, new int[]{ 0, 2});
//                        mAidl.nextLine(1);
//                        mAidl.printText("\n");
//                    }catch (Exception e)
//                    {
//                        mAidl.printText(e.toString()+"\n");
//                    }
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//                mAidl.printText("\n");
//                mAidl.nextLine(2);
//
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//        }
//    }


    private class BookingAddTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();


            RequestBody formBody = new FormEncodingBuilder()
                    .add("orders_item_id", urls[1])
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                String msg = object.getString("msg"); // success
                System.out.println(jsonString);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class AsyncChoiceProductPrint extends AsyncTask<String, String, String> {

        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run("http://www.geerang.com/gpos/api/OrderMenuPrint/" + profile.StoreId + "/" + StringUtil.OrderId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

//                txtResult.setText(response);
            return response;

        }

        @Override
        protected void onPostExecute(String result) {
            Resultbill = result;
            try {

                double sum_price = 0.0;
                JSONArray jArray = null;
                try {

                    double sum_row_price = 0.0;
                    jArray = new JSONArray(Resultbill);
                    // loop ของ report category
                    // customer_name
                    for (int f = 0; f < jArray.length(); f++) {
                        JSONObject json_data_report = jArray.getJSONObject(f);

                        mAidl.setAlignment(1);
                        mAidl.printText(json_data_report.getString("customer_name")+"\n");
                        mAidl.printText("\n");

                        JSONArray jArray_menu = new JSONArray(json_data_report.getString("report_cat"));
                        if (jArray_menu.length() > 0) {

                            // loop ของ report_cat
                            for (int i = 0; i < jArray_menu.length(); i++) {
                                JSONObject json_data = jArray_menu.getJSONObject(i);
                                mAidl.printTableText(new String[]{getDateTime(), json_data.getString("report_cat_name")+""}
                                        , new int[]{1, 1}, new int[]{ 0, 0});
                                mAidl.printText("------------------------------------\n");

                                String Choice ="";
                                // loop product
                                JSONArray jArray2 = new JSONArray(json_data.getString("product"));
                                int check_list = jArray2.length();
                                if (check_list > 0) {
                                    for (int j = 0; j < jArray2.length(); j++) {
                                        JSONObject json_data2 = jArray2.getJSONObject(j);
                                        String product_name = json_data2.getString("product_name");
                                        String qty = json_data2.getString("qty");
                                        String  orders_item_id = json_data2.getString("orders_item_id");
                                        // หา Choice ของสินค้าด้วย loop ที่ 3
                                        JSONArray jArray3 = new JSONArray(json_data2.getString("product_choice"));
                                        for (int k = 0; k < jArray3.length(); k++) {
                                            JSONObject json_data3 = jArray3.getJSONObject(k);
                                            Choice += json_data3.getString("product_choice_name") + ",";
                                        }

                                        // update print status
                                        new BookingAddTask().execute(StringUtil.URL + "gpos/api/AutoPrint_UpdateOrder", orders_item_id);

                                        product_name = product_name+" "+Choice;
                                        mAidl.printTableText(new String[]{product_name, qty+""}, new int[]{1, 1}, new int[]{ 0, 0});

                                    }
                                }

                            }

                            mAidl.printText("------------------------------------\n");

                        } else {

                        }
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }

                mAidl.printText("\n");
                mAidl.nextLine(2);


            } catch (Exception e) {
                e.printStackTrace();
            }

        }


    }

    private String getDateTime() {
        // get date time in custom format
        SimpleDateFormat sdf = new SimpleDateFormat("[yyyy/MM/dd HH:mm]");
        return sdf.format(new Date());
    }


    private class AsyncChoiceProduct extends AsyncTask<String, String, String> {


        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressBarOrder.setVisibility(View.VISIBLE);
            //this method will be running on UI thread

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run("http://www.geerang.com/gpos/api/MenuOrderList/" + StringUtil.OrderId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {
            try {
//                progressBar.setVisibility(View.GONE);

                progressBarOrder.setVisibility(View.GONE);
                txt_status.setVisibility(View.GONE);

                total_price = 0;
                total_qty = 0;

                JSONArray jArray = new JSONArray(result);

                int check_row_order = jArray.length();


                if (check_row_order > 0) {

//                    if (check_click_order == true) {
//                        btnGotoCheckOut.setEnabled(true);
//                        btnGotoCheckOut.setBackground(ContextCompat.getDrawable(OpenTableOrderActivity.this, R.drawable.rounded_button_yellow));
//                        btnGotoCheckOut.setTextColor(Color.WHITE);
//                    } else {
//                        btnGotoCheckOut.setEnabled(false);
//                    }

//                    btnGotoCheckOut.setBackground(ContextCompat.getDrawable(OpenTableOrderActivity.this, R.drawable.rounded_button_yellow_border));
//                    btnGotoCheckOut.setTextColor(Color.GRAY);

                } else {
                    check_click_order = false;

                    btnGotoCheckOut.setEnabled(false);
                    btnGotoCheckOut.setBackground(ContextCompat.getDrawable(OpenTableOrderActivity.this, R.drawable.rounded_button_yellow_border));
                    btnGotoCheckOut.setTextColor(Color.GRAY);

                    btnDeliveryOrder.setEnabled(false);
                    btnDeliveryOrder.setBackground(ContextCompat.getDrawable(OpenTableOrderActivity.this, R.drawable.rounded_button_yellow_border));
                    btnDeliveryOrder.setTextColor(Color.GRAY);

                }

                sectionAdapter = new SectionedRecyclerViewAdapter();
                for (int i = 0; i < jArray.length(); i++) {

                    total_qty_check++;

                    JSONObject json_data = jArray.getJSONObject(i);
                    //ประกาศ array string
                    ArrayList<String> arrSports = new ArrayList<>();
                    // array hasmap
                    MyArrList_GroupChoice = new ArrayList<HashMap<String, String>>();
                    HashMap<String, String> map;


                    int total_row_qty = 0;
                    total_row_qty = Integer.parseInt(json_data.getString("qty"));

                    String confirm = json_data.getString("is_confirm");
                    // เก็บตัวแปร Choice
                    String Choice = "";
                    int total_row_price = 0;
                    // เก็บราคาใน order
                    total_row_price = Integer.parseInt(json_data.getString("product_sale_price"));

                    int order_place = Integer.parseInt(confirm);
                    if (order_place == 0) {
                        check_order_place++;
                    }


                    JSONArray jArray2 = new JSONArray(json_data.getString("product_choice"));
                    int check_list = jArray2.length();
                    if (check_list > 0) {
                        for (int j = 0; j < jArray2.length(); j++) {

                            JSONObject json_data2 = jArray2.getJSONObject(j);
                            arrSports.add(json_data2.getString("product_choice_name"));

                            // หาราคารวมของแต่ละ row แล้วนำไปยัดใส่ใน header row
                            int choice_price = Integer.parseInt(json_data2.getString("choice_price"));
                            total_row_price += choice_price;

                            Choice += json_data2.getString("product_choice_name") + ",";

                            map = new HashMap<String, String>();
                            map.put("product_choice_name", json_data2.getString("product_choice_name"));
                            map.put("orders_item_group_id", json_data2.getString("orders_item_group_id"));
                            map.put("choice_price", json_data2.getString("choice_price"));
                            MyArrList_GroupChoice.add(map);
                        }
                    }


//                        int choice_price = Integer.parseInt(MyArrList_choice.get(position).get("choice_price"));
                    total_price = total_price + (total_row_price * total_row_qty);
                    total_qty = total_qty + total_row_qty;

                    mFirstHeader = new SimpleAdapter(MyArrList_GroupChoice
                            , "" + json_data.getString("product_name")
                            , (total_row_price * total_row_qty) + ""
                            , json_data.getString("qty") + ""
                            , json_data.getString("orders_item_id")
                            , Choice
                            , confirm
                            , json_data.getString("remain"));
                    sectionAdapter.addSection(mFirstHeader);

                }

//                if (StringUtil.payment_status.equals("CHECKOUT")) {
//
//                    btnDeliveryOrder.setVisibility(View.GONE);
//                    btnGotoCheckOut.setVisibility(View.GONE);
//
//                }else
//                {
//                    btnDeliveryOrder.setVisibility(View.VISIBLE);
//                    btnGotoCheckOut.setVisibility(View.VISIBLE);
//                }

                // check confirm order
                if(check_order_place == 0)
                {
                    btnDeliveryOrder.setEnabled(false);
                    btnDeliveryOrder.setBackground(ContextCompat.getDrawable(OpenTableOrderActivity.this, R.drawable.rounded_button_yellow_border));
                    btnDeliveryOrder.setTextColor(Color.GRAY);

                }else
                {
                    btnDeliveryOrder.setEnabled(true);
                    btnDeliveryOrder.setBackground(ContextCompat.getDrawable(OpenTableOrderActivity.this, R.drawable.rounded_button_yellow));
                    btnDeliveryOrder.setTextColor(Color.WHITE);

                    check_click_order = true;

                    btnGotoCheckOut.setEnabled(true);
                    btnGotoCheckOut.setBackground(ContextCompat.getDrawable(OpenTableOrderActivity.this, R.drawable.rounded_button_yellow));
                    btnGotoCheckOut.setTextColor(Color.WHITE);


                }

                txtItem_Qty.setText(total_qty + "");
                tvSum_Amount.setText(total_price + "");

                StringUtil.TotalOrderQty = total_qty + "";

//                mRecyclerView.setLayoutManager(glm);
//
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(OpenTableOrderActivity.this, LinearLayoutManager.VERTICAL, false);
                mRecyclerView.setLayoutManager(linearLayoutManager);
                mRecyclerView.setAdapter(sectionAdapter);




            } catch (JSONException e) {
                txt_status.setVisibility(View.VISIBLE);
                Toast.makeText(OpenTableOrderActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }


    public class SimpleAdapter extends StatelessSection {

        //        private ArrayList<String> arrName = new ArrayList<>();
        ArrayList<HashMap<String, String>> MyArrList_choice = new ArrayList<HashMap<String, String>>();
        private String mTitle, Choice;
        private int price;
        private int qty;
        private String confirm;
        private String comment;
        private int select_item_count = 0;
        private int total_price_row;
        private String orders_item_id;

        public SimpleAdapter(ArrayList<HashMap<String, String>> arrName
                , String title, String price, String qty
                , String orders_item_id
                , String Choice
                , String confirm, String comment) {
            super(R.layout.layout_menu_order_item_header, R.layout.layout_menu_order_item);

            this.MyArrList_choice = arrName;
            this.mTitle = title;
            this.price = Integer.parseInt(price);
            this.qty = Integer.parseInt(qty);
            this.Choice = Choice;
            this.confirm = confirm;
            this.orders_item_id = orders_item_id;
            this.comment = comment;


        }

        @Override
        public int getContentItemsTotal() {
            return MyArrList_choice.size();
        }

        @Override
        public RecyclerView.ViewHolder getItemViewHolder(View view) {
            return new ItemViewHolder(view);
        }

        @Override
        public RecyclerView.ViewHolder getHeaderViewHolder(View view) {
            return new HeaderViewHolder(view);
        }

        @Override
        public void onBindItemViewHolder(RecyclerView.ViewHolder holder, final int position) {
            final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
            itemViewHolder.txtItem.setText("*" + MyArrList_choice.get(position).get("product_choice_name"));
            itemViewHolder.txtPrice.setText("+฿" + MyArrList_choice.get(position).get("choice_price"));
//            itemViewHolder.checkBox.setTag(MyArrList_choice.get(position).get("Code"));

        }

        @Override
        public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder) {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
            headerViewHolder.txtHeader.setText(mTitle);
            headerViewHolder.txtPrice.setText("฿" + (price));
            headerViewHolder.txtComment.setText(comment);

            headerViewHolder.txtItemQty.setText(qty + "");

            headerViewHolder.txtDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(OpenTableOrderActivity.this);
                    builder.setTitle(StringUtil.TableCustomerId + "#ยืนยันการลบข้อมูล!\nกรุณาระบุเหตุผลการขอลบ\n(ความยาวไม่เกิน 120 ตัวอักษร)");
                    final EditText input = new EditText(OpenTableOrderActivity.this);
                    input.setTextSize(14);
                    input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(120)});
                    input.setInputType(InputType.TYPE_CLASS_TEXT);
                    builder.setView(input);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            new ItemDeleteTask().execute(StringUtil.URL + "gpos/api/OrderItemDelete"
                                    , input.getText().toString()
                                    , orders_item_id);

                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    builder.show();


                }
            });
//            headerViewHolder.linearLayout1.Set
            if (!Choice.isEmpty()) {
                headerViewHolder.txtChoice.setText(Choice.substring(0, Choice.length() - 1) + "");
            } else {
                headerViewHolder.txtChoice.setVisibility(View.GONE);
            }


            headerViewHolder.btnReduce.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int sum_qty = qty - 1;
                    new ItemQtyUpdateTask().execute(StringUtil.URL + "gpos/api/OpenTable_UpdateOrderQty"
                            , orders_item_id, sum_qty + "");

                    new AsyncChoiceProduct().execute();
                }
            });

            headerViewHolder.btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int sum_qty = qty + 1;
                    new ItemQtyUpdateTask().execute(StringUtil.URL + "gpos/api/OpenTable_UpdateOrderQty"
                            , orders_item_id, sum_qty + "");

                    new AsyncChoiceProduct().execute();
                }
            });

            headerViewHolder.btnAddComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(OpenTableOrderActivity.this);
                    builder.setTitle(StringUtil.TableCustomerId + "#กรุณาระบุหมายเหตุ\n(ความยาวไม่เกิน 120 ตัวอักษร)");
                    final EditText input = new EditText(OpenTableOrderActivity.this);
                    input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(120)});
                    input.setInputType(InputType.TYPE_CLASS_TEXT);
                    builder.setView(input);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                            new AddRecommentOrderTask().execute(StringUtil.URL + "gpos/api/OrderRecomment", input.getText().toString(), orders_item_id);


                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    builder.show();
                }
            });


            // show status
            if (confirm.equals("1")) {

                headerViewHolder.btnAddComment.setVisibility(View.GONE);
                headerViewHolder.btnAdd.setVisibility(View.GONE);
                headerViewHolder.btnReduce.setVisibility(View.GONE);
                headerViewHolder.txtConfirStatus.setVisibility(View.VISIBLE);
                // show button + -
            } else {
                headerViewHolder.btnAddComment.setVisibility(View.VISIBLE);
                headerViewHolder.btnAdd.setVisibility(View.VISIBLE);
                headerViewHolder.btnReduce.setVisibility(View.VISIBLE);
                headerViewHolder.txtConfirStatus.setVisibility(View.GONE);
                //headerViewHolder.txtDelete.setVisibility(View.GONE);
            }

//            txtConfirStatus


        }

        protected class HeaderViewHolder extends RecyclerView.ViewHolder {

            protected TextView txtHeader, txtDelete;
            protected TextView txtPrice, txtChoice, txtItemQty, txtConfirStatus, txtComment;

            protected Button btnReduce, btnAdd, btnAddComment;

            private HeaderViewHolder(View itemView) {
                super(itemView);
                txtDelete = (TextView) itemView.findViewById(R.id.txtDelete);

                txtHeader = (TextView) itemView.findViewById(R.id.txtHeader);
                txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
                txtItemQty = (TextView) itemView.findViewById(R.id.txtItemQty);
                txtConfirStatus = (TextView) itemView.findViewById(R.id.txtConfirStatus);
                txtComment = (TextView) itemView.findViewById(R.id.txtRemain);

                txtChoice = (TextView) itemView.findViewById(R.id.txtChoice);
                btnReduce = (Button) itemView.findViewById(R.id.btnReduce);
                btnAdd = (Button) itemView.findViewById(R.id.btnAdd);
                btnAddComment = (Button) itemView.findViewById(R.id.btnAddComment);


            }
        }

        protected class ItemViewHolder extends RecyclerView.ViewHolder {

            protected TextView txtItem, txtPrice;
            protected LinearLayout linearLayout1;

            //            protected CheckBox checkBox;
            private ItemViewHolder(View itemView) {
                super(itemView);
                txtItem = (TextView) itemView.findViewById(R.id.txtChoice);
                txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
                linearLayout1 = (LinearLayout) itemView.findViewById(R.id.linearLayout1);
                linearLayout1.setVisibility(View.GONE);

//                checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);


            }
        }
    }

    private class PlaceOrderConfirmTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(OpenTableOrderActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลังส่งข้อมูล...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("orders_id", urls[1])
                    .add("customer_id", StringUtil.TableCustomerId)

                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);

            new AsyncChoiceProduct().execute();

            pdLoading.dismiss();
            check_click_order = true;

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class ItemDeleteTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(OpenTableOrderActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลังลบข้อมูล...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("remark", urls[1])
                    .add("orders_item_id", urls[2])
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);

            new AsyncChoiceProduct().execute();

            pdLoading.dismiss();


            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class ItemQtyUpdateTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(OpenTableOrderActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            //pdLoading.setMessage("\tUpdating...");
            //pdLoading.setCancelable(false);
           // pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("orders_item_id", urls[1])
                    .add("qty", urls[2])
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);

            new AsyncChoiceProduct().execute();

            pdLoading.dismiss();


            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class AddRecommentOrderTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(OpenTableOrderActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลังบันทึก...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("comment", urls[1])
                    .add("orders_item_id", urls[2])
                    .build();

            try {
                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(OpenTableOrderActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(OpenTableOrderActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    new AsyncChoiceProduct().execute();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


    private class TableCancelOrderTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(OpenTableOrderActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลังดำเนินการยกเลิก...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("remain", urls[1])
                    .add("order_id", StringUtil.OrderId)
                    .add("create_by", Profile.UserId)
                    .add("table_id", StringUtil.TableCustomerId)
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(OpenTableOrderActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(OpenTableOrderActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
//                                    Intent in = new Intent(OpenTableOrderActivity.this, StaffDeliveryActivity.class);
//                                    in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                                    startActivity(in);
                                    finish();

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


}
