package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnCashier, btnEmployee, btnCheck, btnBooking, btnSetting, btnProduct;
TextView txtStoreName , txtUsername,tvSumAmount,txtStatus;

    final String PREF_NAME = "LoginPreferences";
    final String KEY_USERNAME = "Username";
    final String KEY_USERID = "UserId";
    final String KEY_BranchId = "BranchId";
    final String KEY_StoreId = "StoreId";
    final String KEY_Status = "Status";
    final String KEY_REMEMBER = "RememberUsername";
    final String KEY_business_type_id = "business_type_id";
    final String KEY_StoreName = "StoreName";
    final String KEY_BranchName = "BranchName";
    final String KEY_Position = "Position";
    SharedPreferences sp;
    SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //start service for print
        startService(new Intent(this, TimePrintService.class));

        sp = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sp.edit();

        // create databse
        GPDbHelper mHelper = new GPDbHelper(MainActivity.this);
        SQLiteDatabase mDb = mHelper.getWritableDatabase();
        mHelper.close();
        mDb.close();


        btnCashier = (Button) findViewById(R.id.btnCashier);
        btnCashier.setOnClickListener(this);

        btnEmployee = (Button) findViewById(R.id.btnEmployee);
        btnEmployee.setOnClickListener(this);

        btnCheck = (Button) findViewById(R.id.btnCheck);
        btnCheck.setOnClickListener(this);

        btnProduct = (Button) findViewById(R.id.btnProduct);
        btnProduct.setOnClickListener(this);


        btnBooking = (Button) findViewById(R.id.btnAddBooking);
        btnBooking.setOnClickListener(this);

        btnSetting = (Button) findViewById(R.id.btnSetting);
        btnSetting.setOnClickListener(this);

        txtStatus = (TextView) findViewById(R.id.txtStatus) ;
        txtStatus.setText("ตำแหน่ง: "+StringUtil.employee_status);

        txtStoreName = (TextView) findViewById(R.id.txtStorename) ;
        txtUsername = (TextView)  findViewById(R.id.txtUsername) ;
        tvSumAmount = (TextView)  findViewById(R.id.tvSumAmount) ;
        Button btnExit = (Button) findViewById(R.id.btnExit);
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                stopService(new Intent(MainActivity.this, TimePrintService.class));

                editor = sp.edit();
                editor.putString(KEY_USERNAME, "");
                editor.putString(KEY_USERID, "");
                editor.putString(KEY_StoreId, "");
                editor.putString(KEY_BranchId, "");
                editor.putString(KEY_Status, "");
                editor.putString(KEY_REMEMBER, "");
                editor.putString(KEY_business_type_id, "");
                editor.putString(KEY_StoreName, "");
                editor.putString(KEY_BranchName, "");
                editor.putString(KEY_Position, "");
                editor.commit();

                Intent in = new Intent(MainActivity.this, LoginActivity.class);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(in);
                finish();

            }
        });




    }


    @Override
    protected void onResume() {
        super.onResume();

        txtStoreName.setText("สาขาร้าน:"+Profile.BranchName);
        txtUsername.setText("ชื่อผู้ใช้งาน:"+Profile.Username);

        new AsyncLoadTotalSale().execute();
        // call api total_sale  in link http://www.geerang.com/gpos/api/TotalSale/1

    }

    private class AsyncLoadTotalSale extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(MainActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL+"gpos/api/TotalSale/"+Profile.BranchId);
//                response = http.run(StringUtil.URL+"gpos/api/category/1");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();

           // feedsList = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);

                    // Show total sale
                    tvSumAmount.setText("ยอดขายวันนี้ "+json_data.getString("total_sale")+" บาท");
                }



            } catch (JSONException e) {
                Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnCashier:
                Intent gotoCashierActivity = new Intent(this, CashierActivity.class);
                startActivity(gotoCashierActivity);
                break;
            case R.id.btnEmployee:
                Intent gotoEmployeeActivity = new Intent(this, EmployeeActivity.class);
                startActivity(gotoEmployeeActivity);
                break;

            case R.id.btnCheck:
                Intent gotoStockActivity = new Intent(this, StockActivity.class);
                startActivity(gotoStockActivity);
                break;
            case R.id.btnProduct:
                Intent gotoProductActivity = new Intent(this, CategoryActivity.class);
                startActivity(gotoProductActivity);

                break;
            case R.id.btnAddBooking:

                Intent gotoBookingActivity = new Intent(this, BookingActivity.class);
                startActivity(gotoBookingActivity);

                break;
            case R.id.btnSetting:
                Intent  gotoSettingActivity = new Intent(this, SettingActivity.class);
                startActivity(gotoSettingActivity);
                break;


        }
    }
}
