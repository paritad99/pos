package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.PictureDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class EditProductItemActivity extends AppCompatActivity {

    // CONNECTION_TIMEOUT and READ_TIMEOUT are in milliseconds
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;
    private List<FeedItemDrug> feedsList;
    private RecyclerView recyclerView;
    private DrugRecyclerAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choice);


        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        new AsyncLoadProduct().execute();
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);


    }

    @Override
    protected void onResume() {
        super.onResume();
        new AsyncLoadProduct().execute();

    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }




    private class AsyncLoadProduct extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(EditProductItemActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL+"gpos/api/ProductChoice/22");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();

            feedsList = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemDrug item = new FeedItemDrug();
                    item.setId(json_data.getString("p_group_id"));
                    item.setTitle(json_data.getString("name"));
                    item.setDetail(json_data.getString("description"));
                    item.setProduct_id(json_data.getString("product_id"));

                    feedsList.add(item);
                }

                // Setup and Handover data to recyclerview

                mAdapter = new DrugRecyclerAdapter(EditProductItemActivity.this, feedsList);
                recyclerView.setAdapter(mAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(EditProductItemActivity.this, 1);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setItemAnimator(new DefaultItemAnimator());

            } catch (JSONException e) {
                Toast.makeText(EditProductItemActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    // Adapter
    public class DrugRecyclerAdapter extends RecyclerView.Adapter<DrugRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemDrug> feedItemList;
        private Context mContext;

        public DrugRecyclerAdapter(Context context, List<FeedItemDrug> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.product_choice_row, null);

            CustomViewHolder viewHolder = new CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
            FeedItemDrug feedItem = feedItemList.get(i);
            customViewHolder.feedItem = feedItem;
            //Setting text view title
            customViewHolder.txtChoice.setText(Html.fromHtml(feedItem.getTitle()));
            customViewHolder.txtChoiceDesc.setText(Html.fromHtml(feedItem.getDetail()));

        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {

            protected TextView txtChoice;
            protected TextView txtChoiceDesc;

            FeedItemDrug feedItem;

            public CustomViewHolder(View view) {
                super(view);
                this.txtChoice = (TextView) view.findViewById(R.id.txtChoice);
                this.txtChoiceDesc = (TextView) view.findViewById(R.id.txtChoiceDesc);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        StringUtil.p_group_id = feedItem.getId();


                        // load the dialog_promt_user.xml layout and inflate to view
                        LayoutInflater layoutinflater = LayoutInflater.from(EditProductItemActivity.this);
                        View promptUserView = layoutinflater.inflate(R.layout.content_product_qty, null);

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(EditProductItemActivity.this);

                        alertDialogBuilder.setView(promptUserView);


                        final EditText input_product_qty = (EditText) promptUserView.findViewById(R.id.input_product_qty);

                        input_product_qty.setText( StringUtil.OrderQty);

                        alertDialogBuilder.setTitle("จำนวนสินค้า");

                        // prompt for username
                        alertDialogBuilder.setPositiveButton("บันทึกสินค้า", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // and display the username on main activity layout
                                //String shipping = userAnswer.getText().toString();

                                String product_qty = input_product_qty.getText().toString();
                                StringUtil.ProductQty = product_qty;

                                if (product_qty.length() != 0) {

                                    new AddOrderItemTask().execute(StringUtil.URL+"gpos/api/EditOrderItem");


                                } else {
                                    Toast.makeText(EditProductItemActivity.this, "กรุณาระบุจำนวนสินค้า!", Toast.LENGTH_SHORT).show();
                                }

                            }
                        });

                        // all set and time to build and show up!
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    }
                });

            }
        }
    }

    private class AddOrderItemTask extends AsyncTask<String, Void, String> {
        ProgressDialog pdLoading = new ProgressDialog(EditProductItemActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tOrder add...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();


            RequestBody formBody = new FormEncodingBuilder()
                    .add("orders_item_id", StringUtil.Orders_item_id)
                    .add("orders_id", StringUtil.OrderId)
                    .add("product_id", StringUtil.ProductId)
                    .add("qty", StringUtil.ProductQty)
                    .add("amount", StringUtil.ProductAmount)
                    .add("userid", Profile.UserId)
                    .add("p_group_id", StringUtil.p_group_id)
                    .build();


            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }



        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);

            pdLoading.dismiss();
            AlertDialog.Builder builder = new AlertDialog.Builder(EditProductItemActivity.this);
            builder.setMessage("บันทึกข้อมูลเรียบร้อย")
                    .setCancelable(false)
                    .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //do things

                            finish();

                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();



        }
    }


    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    // String
    public class FeedItemDrug {
        private String title;
        private String id;
        private String setDetail;
        private String Price;
        private String product_id;
        private String product_image;

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }

        public String getPrice() {
            return Price;
        }

        public void setPrice(String price) {
            this.Price = price;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String name) {
            this.title = name;
        }


        public String getDetail() {
            return setDetail;
        }

        public void setDetail(String Detail) {
            this.setDetail = Detail;
        }


    }

}
