package com.geerang.gcounter;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.StrictMode;
import android.text.InputFilter;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection;

public class CashireTableOrderActivity extends AppCompatActivity {

    RecyclerView mRecyclerView;
    TextView tvTableName, tvSum_Amount, txtItem_Qty, txtCustomer, txt_status;
    Button btnSelectMenu, btnCancel, btnGotoHome, btnDeliveryOrder;
    private SectionedRecyclerViewAdapter sectionAdapter;
    ArrayList<HashMap<String, String>> MyArrList_GroupChoice;

    ArrayList<HashMap<String, String>> ChoiceArrList = new ArrayList<HashMap<String, String>>();
    HashMap<String, String> map_choice;
    private SimpleAdapter mFirstHeader;

    int total_price = 0 ;
    int total_qty = 0;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
//            if (Profile.Position.equals("2")) {
//                StringUtil.employee_status = "ผู้จัดการร้าน";
//                Intent in = new Intent(CashireTableOrderActivity.this, ManagerMainActivity.class);
//                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                startActivity(in);
//                finish();
//            }else
//            {
                finish();
//            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR
//            Intent in = new Intent(CashireTableOrderActivity.this, StaffCashireActivity.class);
//            in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//            startActivity(in);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    int total_qty_check = 0;

    Profile profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cashire_table_order);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        profile = new Profile(this);

        tvTableName = (TextView) findViewById(R.id.tvTableName);
        tvSum_Amount = (TextView) findViewById(R.id.tvSum_Amount);
        txtItem_Qty = (TextView) findViewById(R.id.txtItem_Qty);

        txtCustomer = (TextView) findViewById(R.id.txtCustomer);
        txtCustomer.setText(StringUtil.TotalCustomer);


        txt_status = (TextView) findViewById(R.id.txt_status);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        btnSelectMenu = (Button) findViewById(R.id.btnSelectMenu);
        btnSelectMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(CashireTableOrderActivity.this, SelectMenuToTableActivity.class);
                startActivity(in);

            }
        });

        btnDeliveryOrder = (Button) findViewById(R.id.btnCashireMethod);
        btnDeliveryOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(total_qty_check == 0)
                {
                    AlertError("กรุณาเลือกสินค้า/บริการ ก่อนการชำระเงิน");
                }else
                {
                    Intent in = new Intent(CashireTableOrderActivity.this, CashierActivity.class);
                    startActivity(in);
                }

            }
        });


        btnGotoHome = (Button) findViewById(R.id.btnGotoHome);
        btnGotoHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(CashireTableOrderActivity.this);
                builder.setTitle(StringUtil.TableCustomerId + "#กรุณาระบุเหตุผลการยกเลิก\n(ความยาวไม่เกิน 120 ตัวอักษร)");
                final EditText input = new EditText(CashireTableOrderActivity.this);
                input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(120)});
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (input.length() < 5) {
                            AlertError("กรุณาระบุเหตุผลของการยกเลิก");
                            dialog.cancel();

                        } else {
                            new TableCancelOrderTask().execute(StringUtil.URL + "gpos/api/TableCancelOrder", input.getText().toString());

                        }

                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();

            }
        });
    }

    public void AlertError(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(CashireTableOrderActivity.this);
        builder.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things

                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        tvTableName.setText(StringUtil.TableCustomerName);
        new AsyncChoiceProduct().execute();

    }


    private class AsyncChoiceProduct extends AsyncTask<String, String, String> {



        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run("http://www.geerang.com/gpos/api/MenuOrderList/" + StringUtil.OrderId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {
            try {
//                progressBar.setVisibility(View.GONE);

                txt_status.setVisibility(View.GONE);

                total_price = 0;
                total_qty = 0;

                JSONArray jArray = new JSONArray(result);
                sectionAdapter = new SectionedRecyclerViewAdapter();
                for (int i = 0; i < jArray.length(); i++) {

                    total_qty_check ++;

                    JSONObject json_data = jArray.getJSONObject(i);
                    //ประกาศ array string
                    ArrayList<String> arrSports = new ArrayList<>();
                    // array hasmap
                    MyArrList_GroupChoice = new ArrayList<HashMap<String, String>>();
                    HashMap<String, String> map;


                    int total_row_qty = 0;
                    total_row_qty = Integer.parseInt(json_data.getString("qty"));

                    // เก็บตัวแปร Choice
                    String Choice = "";
                    int total_row_price = 0;
                    // เก็บราคาใน order
                    total_row_price = Integer.parseInt(json_data.getString("product_sale_price"));



                    JSONArray jArray2 = new JSONArray(json_data.getString("product_choice"));
                    int check_list = jArray2.length();
                    if (check_list > 0) {
                        for (int j = 0; j < jArray2.length(); j++) {

                            JSONObject json_data2 = jArray2.getJSONObject(j);
                            arrSports.add(json_data2.getString("product_choice_name"));

                            // หาราคารวมของแต่ละ row แล้วนำไปยัดใส่ใน header row
                            int choice_price = Integer.parseInt(json_data2.getString("choice_price"));
                            total_row_price += choice_price;


                            Choice += json_data2.getString("product_choice_name")+",";

                            map = new HashMap<String, String>();
                            map.put("product_choice_name", json_data2.getString("product_choice_name"));
                            map.put("orders_item_group_id", json_data2.getString("orders_item_group_id"));
                            map.put("choice_price", json_data2.getString("choice_price"));
                            MyArrList_GroupChoice.add(map);
                        }


//                        int choice_price = Integer.parseInt(MyArrList_choice.get(position).get("choice_price"));
                        total_price = total_price + (total_row_price*total_row_qty) ;
                        total_qty = total_qty + total_row_qty;

                        mFirstHeader = new SimpleAdapter(MyArrList_GroupChoice
                                , "" + json_data.getString("product_name")
                                , (total_row_price*total_row_qty)+""
                                , json_data.getString("qty")+""
                        , json_data.getString("orders_item_id")
                        ,Choice);
                        sectionAdapter.addSection(mFirstHeader);


                    }

                }

                txtItem_Qty.setText(total_qty+"");
                tvSum_Amount.setText(total_price+"");


//                final GridLayoutManager glm = new GridLayoutManager(CashireTableOrderActivity.this, 5);
//                glm.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
//                    @Override
//                    public int getSpanSize(final int position) {
//                        if (sectionAdapter.getSectionItemViewType(position) == SectionedRecyclerViewAdapter.VIEW_TYPE_HEADER) {
//                            return 5;
//                        }
//                        return 1;
//                    }
//                });

//                mRecyclerView.setLayoutManager(glm);
//
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(CashireTableOrderActivity.this, LinearLayoutManager.VERTICAL, false);
                mRecyclerView.setLayoutManager(linearLayoutManager);
                mRecyclerView.setAdapter(sectionAdapter);



            } catch (JSONException e) {
                txt_status.setVisibility(View.VISIBLE);
                Toast.makeText(CashireTableOrderActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }


    public class SimpleAdapter extends StatelessSection {

        //        private ArrayList<String> arrName = new ArrayList<>();
        ArrayList<HashMap<String, String>> MyArrList_choice = new ArrayList<HashMap<String, String>>();
        private String mTitle,Choice;
        private int price;
        private int qty;
        private int select_item_count = 0;
        private int total_price_row;
        private String orders_item_id;

        public SimpleAdapter(ArrayList<HashMap<String, String>> arrName
                , String title, String price, String qty
                ,String orders_item_id
                ,String Choice) {
            super(R.layout.layout_menu_order_item_header, R.layout.layout_menu_order_item);

            this.MyArrList_choice = arrName;
            this.mTitle = title;
            this.price = Integer.parseInt(price);
            this.qty = Integer.parseInt(qty);
            this.Choice = Choice;
            this.orders_item_id = orders_item_id;


        }

        @Override
        public int getContentItemsTotal() {
            return MyArrList_choice.size();
        }

        @Override
        public RecyclerView.ViewHolder getItemViewHolder(View view) {
            return new ItemViewHolder(view);
        }

        @Override
        public RecyclerView.ViewHolder getHeaderViewHolder(View view) {
            return new HeaderViewHolder(view);
        }

        @Override
        public void onBindItemViewHolder(RecyclerView.ViewHolder holder, final int position) {
            final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
            itemViewHolder.txtItem.setText("*" + MyArrList_choice.get(position).get("product_choice_name"));
            itemViewHolder.txtPrice.setText("+฿" + MyArrList_choice.get(position).get("choice_price"));
//            itemViewHolder.checkBox.setTag(MyArrList_choice.get(position).get("Code"));

        }

        @Override
        public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder) {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
            headerViewHolder.txtHeader.setText(mTitle);
            headerViewHolder.txtPrice.setText("฿"+(price));

            headerViewHolder.txtItemQty.setText(qty+"");

//            headerViewHolder.linearLayout1.Set
            headerViewHolder.txtChoice.setText(Choice.substring(0,Choice.length()-1) +"");
            headerViewHolder.btnReduce.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int sum_qty = qty - 1;
                    new ItemQtyUpdateTask().execute(StringUtil.URL + "gpos/api/OpenTable_UpdateOrderQty"
                            , orders_item_id, sum_qty + "","RETURN");

                    new AsyncChoiceProduct().execute();
                }
            });

            headerViewHolder.btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int sum_qty = qty + 1;
                    new ItemQtyUpdateTask().execute(StringUtil.URL + "gpos/api/OpenTable_UpdateOrderQty"
                            ,orders_item_id, sum_qty + "","ADD");

                    new AsyncChoiceProduct().execute();
                }
            });



        }

        protected class HeaderViewHolder extends RecyclerView.ViewHolder {

            protected TextView txtHeader;
            protected TextView txtPrice,txtChoice,txtItemQty;

            protected Button btnReduce,btnAdd;
            private HeaderViewHolder(View itemView) {
                super(itemView);
                txtHeader = (TextView) itemView.findViewById(R.id.txtHeader);
                txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
                txtItemQty = (TextView) itemView.findViewById(R.id.txtItemQty);
                txtChoice = (TextView) itemView.findViewById(R.id.txtChoice);
                btnReduce = (Button) itemView.findViewById(R.id.btnReduce);
                btnAdd = (Button) itemView.findViewById(R.id.btnAdd);

            }
        }

        protected class ItemViewHolder extends RecyclerView.ViewHolder {

            protected TextView txtItem, txtPrice;
            protected LinearLayout linearLayout1;
//            protected CheckBox checkBox;
            private ItemViewHolder(View itemView) {
                super(itemView);
                txtItem = (TextView) itemView.findViewById(R.id.txtChoice);
                txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
                linearLayout1 = (LinearLayout) itemView.findViewById(R.id.linearLayout1);
                linearLayout1.setVisibility(View.GONE);

//                checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);


            }
        }
    }


    private class ItemQtyUpdateTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(CashireTableOrderActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลังอัพเดทข้อมูล...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("orders_item_id", urls[1])
                    .add("qty", urls[2])
                    .add("orders_status", urls[3])
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);

            new AsyncChoiceProduct().execute();

            pdLoading.dismiss();


            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


    private class TableCancelOrderTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(CashireTableOrderActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tคำสั่งยกเลิกออร์เดอร์กำลังดำเนินการ...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("remain", urls[1])
                    .add("order_id", StringUtil.OrderId)
                    .add("create_by", profile.UserId)
                    .add("table_id", StringUtil.TableCustomerId)
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(CashireTableOrderActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(CashireTableOrderActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    Intent in = new Intent(CashireTableOrderActivity.this, StaffDeliveryActivity.class);
                                    in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                    startActivity(in);
                                    finish();

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }




}
