package com.geerang.gcounter;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.geerang.gcounter.ui.branch.BranchFragment;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class LoginBusinessActivity extends AppCompatActivity {

    private final OkHttpClient client = new OkHttpClient();

    EditText input_username, input_password;
    Button btnLogin;


    Button btnSignIn;

    final String PREF_NAME = "LoginPreferences";
    final String KEY_USERNAME = "Username";
    final String KEY_USERID = "UserId";
    final String KEY_BranchId = "BranchId";
    final String KEY_StoreId = "StoreId";
    final String KEY_Status = "Status";
    final String KEY_REMEMBER = "RememberUsername";
    final String KEY_business_type_id = "business_type_id";
    final String KEY_StoreName = "StoreName";
    final String KEY_BranchName = "BranchName";
    final String KEY_Position = "Position";
    final String KEY_StoreExp = "StoreExp";
    final String KEY_StoreAddress = "StoreAddress";
    final String KEY_Owner = "Owner";
    final String KEY_package = "package";

    SharedPreferences sp;
    SharedPreferences.Editor editor;



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_business);
        //Toolbar toolbar = findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        // add back arrow to toolbar
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        sp = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sp.edit();

        input_username = (EditText) findViewById(R.id.input_username);
        input_password = (EditText) findViewById(R.id.input_password);

        btnSignIn = (Button) findViewById(R.id.btnSignIn);
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String uname = input_username.getText().toString();
                String upass = input_password.getText().toString();

                if (uname.equals("")) {
                    input_username.setError("กรุณาใส่ Username");
                } else if (uname.length() < 8) {
                    input_username.setError("Username ต้องไม่น้อยกว่า 8 หลัก");
                } else if (upass.equals("")) {
                    input_password.setError("กรุณาใส่ Password");
                } else if (upass.length() < 8) {
                    input_password.setError("Password ต้องไม่น้อยกว่า 8 หลัก");
                } else
                {
                    new LoginTask(). execute(StringUtil.URL + "gpos/api/MobileBackendLogin");
                }

            }
        });

        TextView  btnGotoRegister = (TextView) findViewById(R.id.btnGotoRegister);
        btnGotoRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "http://geerang.com/gpos/index.php/member_login";
                startOpenWebPage(url);
            }
        });


    }

    public boolean startOpenWebPage(String url) {
        boolean result = false;
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;

        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);

        try {
            startActivity(intent);
            result = true;
        } catch (Exception e) {
            if (url.startsWith("http://")) {
                startOpenWebPage(url.replace("http://", "https://"));
            }
        }
        return result;
    }

    private class LoginTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(LoginBusinessActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลังเข้าสู่ระบบ...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("Username", input_username.getText().toString())
                    .add("Password", input_password.getText().toString())
                    .build();


            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                pdLoading.dismiss();

                if (status.equals("0")) {
                    String msg = object.getString("data"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginBusinessActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

                } else {

                    String msg_data = object.getString("data"); // success
                    JSONObject object_profile = new JSONObject(msg_data);
                    String user_id = object_profile.getString("user_id2");
                    String branch_id = "";//object_profile.getString("branch_id");
                    String store_id = object_profile.getString("store_id");
                    String username = object_profile.getString("username");
                    String email = object_profile.getString("email");

                    Profile.BranchId = branch_id;
                    Profile.StoreId = store_id;

                    Profile.Username = object_profile.getString("username");
                    Profile.StoreName = object_profile.getString("store_name");
                    //Profile.BranchName = object_profile.getString("branch_name");
                    Profile.Position = "9";//object_profile.getString("level_position");
                    //Profile.Manager = object_profile.getString("is_manager");
                    //Profile.business_type_id = object_profile.getString("business_type_id");
                    Profile.IsOwner = true;

                    // StorePackagere =

                    // พนักงานรับรายการอาหาร
                    editor = sp.edit();
                    editor.putString(KEY_USERNAME, username);
                    editor.putString(KEY_USERID, user_id);
                    editor.putString(KEY_StoreId, store_id);
                    editor.putString(KEY_BranchId, branch_id);
                    editor.putString(KEY_Status, Profile.Position);
                    editor.putString(KEY_REMEMBER, "Active");
                    editor.putString(KEY_business_type_id, Profile.business_type_id);
                    editor.putString(KEY_StoreName, Profile.StoreName);
                    editor.putString(KEY_BranchName, Profile.BranchName);
                    editor.putString(KEY_Position, Profile.Position);

                    editor.putString(KEY_StoreAddress, Profile.store_address);
                    editor.putString(KEY_Owner, "Owner");
                    editor.commit();

                    //load package
                    new AsyncLoadCat().execute();



//                    Intent in = new Intent(LoginBusinessActivity.this, ManagerMainActivity.class);
                    Intent in = new Intent(LoginBusinessActivity.this, SelectBranchActivity.class);
                    in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(in);
                    finish();


                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class AsyncLoadCat extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(LoginBusinessActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/StorePackage/" + Profile.StoreId);
//                response = http.run(StringUtil.URL+"gpos/api/category/1");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;

        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            try {

                JSONArray jArray = new JSONArray(result);
                editor = sp.edit();
                editor.putString(KEY_package, result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    StringUtil.package_name = json_data.getString("package_name");
                    StringUtil.package_product =  json_data.getString("package_product");
                    StringUtil.package_branch =  json_data.getString("package_branch");
                    StringUtil.package_cashier = json_data.getString("package_cashier");
                    StringUtil.package_users =  json_data.getString("package_users");
                    StringUtil.package_zone = json_data.getString("package_zone");
                    StringUtil.package_table = json_data.getString("package_table");
                    Profile.store_exp = json_data.getString("store_exp");
                    editor.putString(KEY_StoreExp, json_data.getString("store_exp"));
                }

                editor.commit();

            } catch (JSONException e) {
                Toast.makeText(LoginBusinessActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    private void showData(String jsonString) {
        Toast.makeText(this, jsonString, Toast.LENGTH_LONG).show();
    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }
}
