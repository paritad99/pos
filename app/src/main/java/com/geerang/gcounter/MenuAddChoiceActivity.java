package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.PictureDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MenuAddChoiceActivity extends AppCompatActivity {
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    Bitmap FixBitmap;
    byte[] byteArray;
    String ConvertImage = "";
    URL url;
    private int GALLERY = 1, CAMERA = 2;
    Spinner ddlMenuCategory, ddlKitchen, ddlReport;

    Button buttonSelect, btnGotoAddChoice, btnOldChoice;
    ImageButton btnSaveMenu, btnCancel;

    ImageView imgStore;
    CheckBox chk_is_recommend;
    EditText input_menu, input_desc, input_price, input_calories, input_time_task;

    ArrayList<HashMap<String, String>> MyArrList;
    HashMap<String, String> map;

    ArrayList<HashMap<String, String>> MyArrListChoice;
    HashMap<String, String> map_choice;

    String categories_id;

    LinearLayout llChoice;

    //RecyclerView recyclerview_choice;
    static String choice_id, group_choice_id;
    EditText input_choice_name, input_choice_order;

    EditText input_group_choice, input_choice_desc, input_choice_min, input_choice_max;
    ListView lisView1;

    AlertDialog dialogChoiceAdd;
    String input_product_id = "";
    String p_group_id = "";

    private List<FeedItemChoice> feedsList;
    private ChoiceRecyclerAdapter mAdapter;

    Profile profile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_add_choice);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        profile= new Profile(this);

        Intent intent = getIntent();
        input_product_id = intent.getStringExtra("input_product_id");
//        Log.d("product_id", input_product_id);

        input_group_choice = (EditText) findViewById(R.id.input_group_choice);
        input_choice_min = (EditText) findViewById(R.id.input_choice_min);
        input_choice_max = (EditText) findViewById(R.id.input_choice_max);
        btnOldChoice = (Button) findViewById(R.id.btnOldChoice);
        lisView1 = (ListView) findViewById(R.id.listView1);
        input_group_choice.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                new AsyncCheckGroupChoiceName().execute(
                        StringUtil.URL + "gpos/api/CheckChoiceGroupName"
                        , input_group_choice.getText().toString()
                );

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {



            }
        });

        Button btnNewRowChoice = (Button) findViewById(R.id.btnNewRowChoice);
        Button btnCancel = (Button) findViewById(R.id.btnCancel);
        Button btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new GroupChoiceAddTask().execute(StringUtil.URL + "gpos/api/GroupChoiceAdd");

            }
        });

        MyArrListChoice = new ArrayList<HashMap<String, String>>();

        btnNewRowChoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                int r = MyArrListChoice.size() + 1;
                map_choice = new HashMap<String, String>();
                map_choice.put("ID", "Choice"+r);
                map_choice.put("Code", "0");
                MyArrListChoice.add(map_choice);
                CreateChoice();

            }
        });
    }

    public void CreateChoice() {

        Toast.makeText(MenuAddChoiceActivity.this, "Update list", Toast.LENGTH_LONG).show();
        lisView1.setAdapter(new ChoiceAdapter(MenuAddChoiceActivity.this));
    }


    // String
    public class FeedItemChoice {

        private String employee_id;
        private String firstname;
        private String lastname;
        private String tel;
        private String status_name;

        public String getEmployee_id() {
            return employee_id;
        }

        public void setEmployee_id(String employee_id) {
            this.employee_id = employee_id;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getStatus_name() {
            return status_name;
        }

        public void setStatus_name(String status_name) {
            this.status_name = status_name;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }
    }

    public class ChoiceAdapter extends BaseAdapter {
        private Context context;

        public ChoiceAdapter(Context c) {
            //super( c, R.layout.activity_column, R.id.rowTextView, );
            // TODO Auto-generated method stub
            context = c;
        }

        public int getCount() {
            // TODO Auto-generated method stub
            return MyArrListChoice.size();
        }

        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.foodmenu_choice_row_add, null);

            }

            // ColID
            EditText txtID = (EditText) convertView.findViewById(R.id.input_choice);
            txtID.setText(MyArrListChoice.get(position).get("ID") + ".");

            // ColCode
            EditText txtCode = (EditText) convertView.findViewById(R.id.input_price);
            txtCode.setText(MyArrListChoice.get(position).get("Code"));

            ImageButton btnDelete_row = (ImageButton) convertView.findViewById(R.id.btnDelete_row);
            btnDelete_row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MyArrListChoice.remove(position);
                    CreateChoice();

                }
            });

            return convertView;

        }

    }

    private class AsyncCheckGroupChoiceName extends AsyncTask<String, Void, String> {

//        ProgressDialog pdLoading = new ProgressDialog(FoodMenuAddActivity.this);

        @Override
        protected void onPreExecute() {
//             Create Show ProgressBar
//            pdLoading.setMessage("\tกำลังตรวจสอบข้อมูลซ้ำ...");
//            pdLoading.setCancelable(false);
//            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("GroupChoiceName ", urls[1] + "")
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
//            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);
                String status = object.getString("status"); // success
                p_group_id = object.getString("p_group_id"); // success

                if(jsonString.length() > 0)
                {
                    btnOldChoice.setVisibility(View.VISIBLE);
                    btnOldChoice.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                        }
                    });

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class GroupChoiceAddTask extends AsyncTask<String, Void, String> {

//        ProgressDialog pdLoading = new ProgressDialog(FoodMenuAddActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
//            pdLoading.setMessage("\tSaving...");
//            pdLoading.setCancelable(false);
//            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            // เพิ่ม GroupChoice
            String group_choice = input_group_choice.getText().toString();
            String choice_min = input_choice_min.getText().toString();
            String choice_max = input_choice_max.getText().toString();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("UserId", profile.UserId)
                    .add("StoreId", profile.StoreId)
                    .add("name", group_choice)
                    .add("min", choice_min)
                    .add("max", choice_max)
                    .add("product_id", input_product_id)
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
//            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuAddChoiceActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    group_choice_id = msg;

                    // บันทึก Choice ต่อหลังจากที่บันทึก GroupChoice เสด
                    int count = lisView1.getAdapter().getCount();
                    for (int i = 0; i < count; i++) {

                        LinearLayout itemLayout = (LinearLayout) lisView1.getChildAt(i); // Find by under LinearLayout
                        EditText input_choice = (EditText) itemLayout.findViewById(R.id.input_choice);
                        EditText input_price = (EditText) itemLayout.findViewById(R.id.input_price);
                        String Choice = input_choice.getText().toString();
                        String Price = input_price.getText().toString();

                        // เพิ่ม Choice ทีละรายการ
                        new ChoiceAddTask().execute(StringUtil.URL + "gpos/api/ChoiceAdd", Choice, Price,  group_choice_id);

                        Log.d(Choice, Price);

                    }

                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuAddChoiceActivity.this);
                    builder.setMessage("บันทึกเรียบร้อยแล้ว")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    finish();

                                   // new AsyncLoadChoice().execute();

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class ChoiceAddTask extends AsyncTask<String, Void, String> {

//        ProgressDialog pdLoading = new ProgressDialog(FoodMenuAddActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
//            pdLoading.setMessage("\tกำลังบันทึก...");
//            pdLoading.setCancelable(false);
//            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("UserId", profile.UserId + "")
                    .add("choice_name", urls[1])
                    .add("choice_price", urls[2])
                    .add("p_group_id",urls[3])
                    .add("StoreId", profile.StoreId)
                    .add("BranchId", profile.BranchId)

                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
//            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuAddChoiceActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
//                                    finish();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuAddChoiceActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    //                                    finish();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


                //new AsyncLoadChoice().execute();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class ChoiceDeleteTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(MenuAddChoiceActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลังลบ...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();


            RequestBody formBody = new FormEncodingBuilder()
                    .add("UserId", profile.UserId + "")
                    .add("p_group_id", urls[1])
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuAddChoiceActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    //new AsyncLoadChoice().execute();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuAddChoiceActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
//                                    finish();
                                    //new AsyncLoadChoice().execute();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public class ChoiceRecyclerAdapter extends RecyclerView.Adapter<ChoiceRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemChoice> feedItemList;
        private Context mContext;

        public ChoiceRecyclerAdapter(Context context, List<FeedItemChoice> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.choice_list_row, null);

            CustomViewHolder viewHolder = new CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
            final FeedItemChoice feedItem = feedItemList.get(i);
            customViewHolder.feedItem = feedItem;


            //Setting text view title
            customViewHolder.title_choice.setText(Html.fromHtml("" + feedItem.getFirstname()));

            customViewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent in = new Intent(mContext, MenuChoiceEditActivity.class);
                    in.putExtra("cid", feedItem.getEmployee_id());
                    in.putExtra("title_choice", feedItem.getFirstname());
                    in.putExtra("title_choice_order", feedItem.getLastname());
                    startActivity(in);

                }
            });


            customViewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    final AlertDialog.Builder adb = new AlertDialog.Builder(MenuAddChoiceActivity.this);

                    adb.setTitle("ยืนยันการลบข้อมูล?");
                    adb.setMessage("ยืนยันการลบข้อมูล");
                    adb.setNegativeButton("Cancel", null);
                    adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
                        public void onClick(DialogInterface dialog, int arg1) {
                            // TODO Auto-generated method stub

                            choice_id = feedItem.getEmployee_id();
                            new ChoiceDeleteTask().execute(StringUtil.URL + "gpos/api/MenuChoiceDelete",feedItem.getEmployee_id());


                        }
                    });
                    adb.show();

                }
            });


        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            //            protected ImageView imageView;
            protected TextView title_choice;
            protected Button btnEdit, btnDelete;

            FeedItemChoice feedItem;

            public CustomViewHolder(View view) {
                super(view);

                this.title_choice = (TextView) view.findViewById(R.id.txtCheckIn);
                this.btnEdit = (Button) view.findViewById(R.id.btnEdit);
                this.btnDelete = (Button) view.findViewById(R.id.btnDelete);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

//                        StringUtil.CatId = feedItem.getId();
                        //new AsyncProduct().execute();
                        //Toast.makeText(v.getContext(), "Select is: " + feedItem.getId(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }
    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

//    private class AsyncLoadChoice extends AsyncTask<String, String, String> {
//        //        ProgressDialog pdLoading = new ProgressDialog(FoodMenuAddActivity.this);
//        HttpURLConnection conn;
//        URL url = null;
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            //this method will be running on UI thread
////            pdLoading.setMessage("\tLoading...");
////            pdLoading.setCancelable(false);
////            pdLoading.show();
//        }
//
//        @Override
//        protected String doInBackground(String... urls) {
//            String response = null;
//            getHttp http = new getHttp();
//            try {
//                response = http.run(StringUtil.URL + "gpos/api/GroupChoiceList/" + input_product_id);
////                response = http.run(StringUtil.URL+"gpos/api/category/1");
//            } catch (IOException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
////                txtResult.setText(response);
//            return response;
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            //this method will be running on UI thread
////            pdLoading.dismiss();
////            List<DataFish> data=new ArrayList<>();
//            feedsList = new ArrayList<>();
//            try {
//
//                JSONArray jArray = new JSONArray(result);
//
//                // Extract data from json and store into ArrayList as class objects
//                for (int i = 0; i < jArray.length(); i++) {
//                    JSONObject json_data = jArray.getJSONObject(i);
//                    FeedItemChoice item = new FeedItemChoice();
//                    item.setEmployee_id(json_data.getString("p_group_id"));
//                    item.setFirstname(json_data.getString("group_name"));
//                    item.setLastname(json_data.getString("group_name"));
//
//                    feedsList.add(item);
//                }
//
//                // Setup and Handover data to recyclerview
//                mAdapter = new ChoiceRecyclerAdapter(MenuAddChoiceActivity.this, feedsList);
//                recyclerview_choice.setAdapter(mAdapter);
//                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
//                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(MenuAddChoiceActivity.this, 1);
//                recyclerview_choice.setLayoutManager(mLayoutManager);
//                recyclerview_choice.setItemAnimator(new DefaultItemAnimator());
//                recyclerview_choice.setItemAnimator(new DefaultItemAnimator());
//
//            } catch (JSONException e) {
//                Toast.makeText(MenuAddChoiceActivity.this, e.toString(), Toast.LENGTH_LONG).show();
//            }
//
//        }
//
//    }

}
