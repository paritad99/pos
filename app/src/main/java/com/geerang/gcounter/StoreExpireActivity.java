package com.geerang.gcounter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import android.view.View;
import android.widget.Button;

public class StoreExpireActivity extends AppCompatActivity {

    final String PREF_NAME = "LoginPreferences";
    final String KEY_USERNAME = "Username";
    final String KEY_USERID = "UserId";
    final String KEY_BranchId = "BranchId";
    final String KEY_StoreId = "StoreId";
    final String KEY_Status = "Status";
    final String KEY_REMEMBER = "RememberUsername";
    final String KEY_business_type_id = "business_type_id";
    final String KEY_StoreName = "StoreName";
    final String KEY_BranchName = "BranchName";
    final String KEY_Position = "Position";
    final String KEY_StoreExp = "StoreExp";
    SharedPreferences sp;
    SharedPreferences.Editor editor;

    Profile profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_expire);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        profile = new Profile(this);
        sp = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sp.edit();


        Button btnGotoStore = (Button)findViewById(R.id.btnGotoStore);
        btnGotoStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://geerang.com/gpos/index.php/member_login";
                startOpenWebPage(url);
            }
        });


        Button btnExitApp = (Button) findViewById(R.id.btnExitApp);
        btnExitApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //stopService(new Intent(StoreExpireActivity.this, TimePrintService.class));

                editor = sp.edit();
                editor.putString(KEY_USERNAME, "");
                editor.putString(KEY_USERID, "");
                editor.putString(KEY_StoreId, "");
                editor.putString(KEY_BranchId, "");
                editor.putString(KEY_Status, "");
                editor.putString(KEY_REMEMBER, "");
                editor.putString(KEY_business_type_id, "");
                editor.putString(KEY_StoreName, "");
                editor.putString(KEY_BranchName, "");
                editor.putString(KEY_Position, "");
                editor.commit();

                Intent in = new Intent(StoreExpireActivity.this, LoginActivity.class);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(in);
                finish();

            }
        });


    }

    public boolean startOpenWebPage(String url) {
        boolean result = false;
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;

        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);

        try {
            startActivity(intent);
            result = true;
        } catch (Exception e) {
            if (url.startsWith("http://")) {
                startOpenWebPage(url.replace("http://", "https://"));
            }
        }
        return result;
    }

}