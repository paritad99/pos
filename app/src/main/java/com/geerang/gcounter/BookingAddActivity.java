package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class BookingAddActivity extends AppCompatActivity {

    private final OkHttpClient client = new OkHttpClient();

    EditText input_firstname, input_lastname, input_phone;
    Button btnSelectDate, btnSelectTime, btnSave;
    TextView input_date, input_time,input_table;


    public void Validete(EditText editText, String param) {
        if (TextUtils.isEmpty(editText.getText())) {
            editText.setError("กรุณากรอกข้อมูล" + param);
            return;
        }
    }

    Profile profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_add);

        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        profile =  new Profile(this);

        input_date = (TextView) findViewById(R.id.input_date);

        input_time = (TextView) findViewById(R.id.input_time);
        input_table = (TextView) findViewById(R.id.input_table);

        input_firstname = (EditText) findViewById(R.id.input_firstname);
        input_lastname = (EditText) findViewById(R.id.input_lastname);
        input_phone = (EditText) findViewById(R.id.input_phone);

        btnSelectDate = (Button) findViewById(R.id.btnSelectDate);
        btnSelectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getSupportFragmentManager(), "datePicker");

            }
        });

        btnSelectTime = (Button) findViewById(R.id.btnSelectTime);
        btnSelectTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogFragment newFragment = new TimePickerFragment();
                newFragment.show(getSupportFragmentManager(), "timePicker");
            }
        });

        btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final AlertDialog.Builder adb = new AlertDialog.Builder(BookingAddActivity.this);

                adb.setTitle("ยืนยันบันทึกการจองโต๊ะ");
                adb.setMessage("กรุณายืนยันบันทึกการจองโต๊ะ");
                adb.setNegativeButton("ยกเลิก", null);
                adb.setPositiveButton("ตกลง", new AlertDialog.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        // TODO Auto-generated method stub

//                        StringUtil.TotalCustomer = input_remain.getText().toString();
                        //new CancelOrderTask().execute(StringUtil.URL + "gpos/api/BookingAdd");

                        Validete(input_firstname, "ชื่อ");
                        Validete(input_lastname, "นามสกุล");
                        Validete(input_phone, "เบอร์โทร");

                        if (input_date.getText().toString().equals("YYYY-MM-DD")) {
                            input_date.setText("กรุณากรอกข้อมูลวันที่");
                            return;
                        }

                        if (input_time.getText().toString().equals("HH:MM")) {
                            input_time.setText("กรุณากรอกข้อมูลเวลา");
                            return;
                        }

                        if (input_table.getText().toString().equals("-")) {
                            input_table.setText("กรุณาเลือกโต๊ะ");
                            return;
                        }




                        new BookingAddTask().execute(StringUtil.URL + "gpos/api/BookingAdd");

                    }
                });
                adb.show();




            }
        });


    }


    private class BookingAddTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(BookingAddActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tBooking in progress...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("customer_id", StringUtil.BookingTableId+"")
                    .add("booking_date",  input_date.getText().toString())
                    .add("booking_time_start", input_time.getText().toString())
                    .add("store_id", profile.StoreId)
                    .add("branch_id", profile.BranchId)
                    .add("employee_id", profile.UserId)
                    .add("firstname", input_firstname.getText().toString())
                    .add("lastname", input_lastname.getText().toString())
                    .add("phone", input_phone.getText().toString())

                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(BookingAddActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(BookingAddActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    Intent in = new Intent(BookingAddActivity.this, TableActivity.class);
                                    startActivity(in);
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private void showData(String jsonString) {
        Toast.makeText(this, jsonString, Toast.LENGTH_LONG).show();
    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

}
