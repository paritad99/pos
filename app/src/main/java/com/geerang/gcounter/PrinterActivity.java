package com.geerang.gcounter;

import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;

public class PrinterActivity extends AppCompatActivity {

    GPDbHelper mHelper;
    SQLiteDatabase mDb;
    Cursor mCursor;
    ListView listStudent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_printer_menu);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }
        public void onResume() {
            super.onResume();

            mHelper = new GPDbHelper(PrinterActivity.this);
            mDb = mHelper.getWritableDatabase();

            mCursor = mDb.rawQuery("SELECT * FROM " + GPDbHelper.TABLE_NAME, null);

            listStudent = (ListView)findViewById(R.id.listStudent);
            listStudent.setAdapter(updateListView());
            listStudent.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                    mCursor.moveToPosition(arg2);

                    String name = mCursor.getString(mCursor.getColumnIndex(GPDbHelper.COL_PRINTER_NAME));
                    String lastname = mCursor.getString(mCursor.getColumnIndex(GPDbHelper.COL_PRINTER_ADDRESS));

//                    Intent intent = new Intent(getApplicationContext(), UpdateStudent.class);
//                    intent.putExtra("NAME", name);
//                    intent.putExtra("LASTNAME", lastname);
//                    startActivity(intent);
                }
            });

            listStudent.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                    mCursor.moveToPosition(arg2);

                    AlertDialog.Builder builder = new AlertDialog.Builder(PrinterActivity.this);
                    builder.setTitle("ลบข้อมูลเครื่องปริ้น");
                    builder.setMessage("คุณต้องการลบข้อมูลเครื่องปริ้นใช่หรือไม่?");
                    builder.setPositiveButton("ใช่", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            String name = mCursor.getString(mCursor.getColumnIndex(GPDbHelper.COL_PRINTER_NAME));
                            String lastname = mCursor.getString(mCursor.getColumnIndex(GPDbHelper.COL_PRINTER_ADDRESS));

                            mDb.execSQL("DELETE FROM " + GPDbHelper.TABLE_NAME
                                    + " WHERE " + GPDbHelper.COL_PRINTER_NAME + "='" + name + "'"
                                    + " AND " + GPDbHelper.COL_PRINTER_ADDRESS + "='" + lastname + "';");

                            mCursor.requery();

                            listStudent.setAdapter(updateListView());

                            Toast.makeText(getApplicationContext(),"ลบข้อมูลเครื่องปริ้นเรียบร้อย"
                                    , Toast.LENGTH_SHORT).show();
                        }
                    });

                    builder.setNegativeButton("ไม่", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();

                    return true;
                }
            });
        }

        public void onStop() {
            super.onStop();
            mHelper.close();
            mDb.close();
        }

        public ArrayAdapter<String> updateListView() {
            ArrayList<String> arr_list = new ArrayList<String>();
            mCursor.moveToFirst();
            while(!mCursor.isAfterLast()){
                arr_list.add(mCursor.getString(mCursor.getColumnIndex(GPDbHelper.COL_PRINTER_ADDRESS)));
                mCursor.moveToNext();
            }

            ArrayAdapter<String> adapterDir = new ArrayAdapter<String>(getApplicationContext()
                    , R.layout.printer_listview, arr_list);
            return adapterDir;
        }

}
