package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.PictureDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ProductActivity extends AppCompatActivity {

    // CONNECTION_TIMEOUT and READ_TIMEOUT are in milliseconds
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;
    private List<FeedItemDrug> feedsList;
    private RecyclerView recyclerView;
    private DrugRecyclerAdapter mAdapter;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

// add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        new AsyncLogin().execute();
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);


    }


    private class AddOrderItemTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();


            RequestBody formBody = new FormEncodingBuilder()
                    .add("orders_id", StringUtil.OrderId)
                    .add("product_id", StringUtil.ProductId)
                    .add("qty", StringUtil.ProductQty)
                    .add("amount", StringUtil.ProductAmount)
                    .add("userid", Profile.UserId)
                    .build();


            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success

                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(ProductActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things


                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();

                } else {
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private void showData(String jsonString) {
        Toast.makeText(this, jsonString, Toast.LENGTH_LONG).show();
    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    private class AsyncLogin extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(ProductActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/products/" + Profile.BranchId + '/' + StringUtil.CatId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();

            feedsList = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemDrug item = new FeedItemDrug();
                    item.setId(json_data.getString("product_id"));
                    item.setTitle(json_data.getString("product_name"));
                    item.setDetail(json_data.getString("product_detail"));
                    item.setPrice(json_data.getString("product_sale_price"));
                    item.setProduct_image(json_data.getString("product_image"));
                    feedsList.add(item);
                }

                // Setup and Handover data to recyclerview

                mAdapter = new DrugRecyclerAdapter(ProductActivity.this, feedsList);
                recyclerView.setAdapter(mAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(ProductActivity.this, 3);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setItemAnimator(new DefaultItemAnimator());

            } catch (JSONException e) {
                Toast.makeText(ProductActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    // Adapter
    public class DrugRecyclerAdapter extends RecyclerView.Adapter<DrugRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemDrug> feedItemList;
        private Context mContext;

        public DrugRecyclerAdapter(Context context, List<FeedItemDrug> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.product_row, null);

            CustomViewHolder viewHolder = new CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
            FeedItemDrug feedItem = feedItemList.get(i);
            customViewHolder.feedItem = feedItem;


            //Setting text view title
            customViewHolder.textView.setText(Html.fromHtml(feedItem.getTitle()));
            customViewHolder.tvPrice.setText(Html.fromHtml(feedItem.getPrice()));

            StringUtil stringUtil = new StringUtil();
            Picasso.with(mContext)
                    .load(StringUtil.URL + "gpos/" + stringUtil.removeFirstChar(feedItem.getProduct_image()))
                    .fit()
                    .into(customViewHolder.imageView);

            //customViewHolder.imageView.setImageResource(StringUtil.arrImg[i]);

        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            protected ImageView imageView;
            protected TextView textView;
            protected TextView tvPrice;

            FeedItemDrug feedItem;

            public CustomViewHolder(View view) {
                super(view);
                this.imageView = (ImageView) view.findViewById(R.id.imageView);
                this.textView = (TextView) view.findViewById(R.id.title);
                this.tvPrice = (TextView) view.findViewById(R.id.tvTotalPrice);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        StringUtil.ProductId = feedItem.getId();


                        // load the dialog_promt_user.xml layout and inflate to view
                        LayoutInflater layoutinflater = LayoutInflater.from(ProductActivity.this);
                        View promptUserView = layoutinflater.inflate(R.layout.content_product_qty, null);

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ProductActivity.this);

                        alertDialogBuilder.setView(promptUserView);


                        final EditText input_product_qty = (EditText) promptUserView.findViewById(R.id.input_product_qty);


                        alertDialogBuilder.setTitle("จำนวนสินค้า");

                        // prompt for username
                        alertDialogBuilder.setPositiveButton("บันทึกสินค้า", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // and display the username on main activity layout
                                //String shipping = userAnswer.getText().toString();

                                String product_qty = input_product_qty.getText().toString();


                                if (product_qty.length() != 0) {

                                    try {


                                        int qty = Integer.parseInt(product_qty);
                                        int price = Integer.parseInt(feedItem.getPrice());

                                        int sum = qty * price;

                                        StringUtil.ProductQty = product_qty;
                                        StringUtil.ProductAmount = feedItem.getPrice();

                                        new AddOrderItemTask().execute(StringUtil.URL + "gpos/api/AddOrderItem");

                                    } catch (Exception error) {

                                    }


                                } else {
                                    Toast.makeText(ProductActivity.this, "กรุณาระบุจำนวนสินค้า!", Toast.LENGTH_SHORT).show();
                                }

                            }
                        });

                        // all set and time to build and show up!
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    }
                });

            }
        }
    }


    // String
    public class FeedItemDrug {
        private String title;
        private String id;
        private String setDetail;
        private String Price;
        private String product_image;

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }

        public String getPrice() {
            return Price;
        }

        public void setPrice(String price) {
            this.Price = price;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String name) {
            this.title = name;
        }


        public String getDetail() {
            return setDetail;
        }

        public void setDetail(String Detail) {
            this.setDetail = Detail;
        }


    }
}