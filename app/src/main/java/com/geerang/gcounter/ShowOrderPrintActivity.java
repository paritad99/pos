package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.PictureDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.epson.epos2.Epos2Exception;
import com.epson.epos2.cashchanger.CashChanger;
import com.epson.epos2.printer.Printer;
import com.epson.epos2.printer.PrinterStatusInfo;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ShowOrderPrintActivity extends AppCompatActivity {

    Button btnPrinterSetup;
    private List<FeedItemOrder> feedsListOrder;

    private static final int REQUEST_PERMISSION = 100;

    private Context mContext = null;
    public static EditText mEditTarget = null;
    public static Printer mPrinter = null;
    static CashChanger mCashChanger = null;

    static String Temp_order = "";

//    TextView txtShowPrint;

    static JSONArray jArray;

    RecyclerView recyclerView;
    String input_amount = "";
    String input_discount = "";

    Button btnReprint,btnOk;

    public int total_qty = 0;
    public int sum_amount = 0;
    TextView tvTableName, tvTotal_qty, tvSum_amount, tvDiscount, txt_receive_total, txtStoreName, txtUsername, txtDatetime, txtOrderNumber;

    private OrderRecyclerAdapter OrdermAdapter;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
//            Intent in = new Intent(CashireTableOrderActivity.this, StaffCashireActivity.class);
//            in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//            startActivity(in);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR
//            Intent in = new Intent(CashireTableOrderActivity.this, StaffCashireActivity.class);
//            in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//            startActivity(in);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    static String menu_order = "";
    static String menu_order_total_price = "";
    static String menu_order_total_qty = "";
    static String menu_order_discount = "";

    private static final int DISCONNECT_INTERVAL = 500;//millseconds

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_showprint_order);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        tvTotal_qty = (TextView) findViewById(R.id.tvItemQty);
        tvSum_amount = (TextView) findViewById(R.id.tvSumAmount);
        tvDiscount = (TextView) findViewById(R.id.tvDiscount);
        tvTableName = (TextView) findViewById(R.id.tvTableName);
        txt_receive_total = (TextView) findViewById(R.id.txt_receive_total);

        txtStoreName = (TextView) findViewById(R.id.txtStoreName);
        txtUsername = (TextView) findViewById(R.id.txtUsername);
        txtDatetime = (TextView) findViewById(R.id.txtDatetime);
        txtOrderNumber = (TextView) findViewById(R.id.txtOrderNumber);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat dateformat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss aa");
        String datetime = dateformat.format(c.getTime());
        System.out.println(datetime);

        txtDatetime.setText("วันที่" + datetime);
        txtStoreName.setText("ร้าน" + Profile.StoreName);
        txtUsername.setText("พนักงาน " + Profile.Username);
        txtOrderNumber.setText("รหัสออร์เดอร์ " + StringUtil.OrderId);

        recyclerView = (RecyclerView) findViewById(R.id.rvOrders);

        new AsyncOrder().execute();


        btnReprint = (Button) findViewById(R.id.btnReprint);
        btnReprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                initializeObject();
//
//                if (!StringUtil.printer_address.equals("")) {
//                    System.out.println(" mPrinter. printer_address : " + StringUtil.printer_address);
//                    initializeObject();
//                } else {
//                    System.out.println(" mPrinter. printer_address : " + StringUtil.printer_address);
//                }

                try {
                    mPrinter.disconnect();
                } catch (Epos2Exception e) {
                    e.printStackTrace();
                }

                try {
                    // เชื่อมต่อเครื่องปริ้น
                    mPrinter.connect(StringUtil.printer_address, Printer.PARAM_DEFAULT);
                } catch (Exception e) {
                    System.out.println("DeliveryOrderActivity mPrinter.connect : " + e +"\n"+StringUtil.printer_address);
//            ShowMsg.showException(e, "connect", getApplicationContext());

                }

                // display toast
                System.out.println(" mPrinter.run : " + Profile.StoreName);
                if (!runPrintReceiptSequence()) {
//                        updateButtonState(true);
                    System.out.println(" runPrintReceiptSequence.run : " + getDateTime());
                }

            }
        });


        btnOk = (Button) findViewById(R.id.btnOk);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }


    private boolean initializeObject() {
        try {
            mPrinter = new Printer(2, 0, getApplicationContext());

            StringUtil.is_printer = 1;

        } catch (Exception e) {
            System.out.println(" Exception initializeObject : " + e);
//            ShowMsg.showException(e, "Printer", getApplicationContext());
            return false;
        }

//        mPrinter.setReceiveEventListener(getApplicationContext());

        return true;
    }


    private boolean runPrintReceiptSequence() {

        if (!createReceiptData()) {

            return false;
        }

        if (!printData()) {
            return false;
        }

        return true;
    }

    private boolean printData() {
        if (mPrinter == null) {
            return false;
        }

        if (!connectPrinter()) {
            mPrinter.clearCommandBuffer();
            return false;
        }

        try {
            System.out.println(" mPrinter : ");
            mPrinter.sendData(Printer.PARAM_DEFAULT);
        } catch (Exception e) {
            mPrinter.clearCommandBuffer();
            System.out.println(" clearCommandBuffer : " + e);

            try {
                mPrinter.disconnect();
            } catch (Exception ex) {
                // Do nothing
            }
            return false;
        }

        return true;
    }

    private boolean connectPrinter() {
        if (mPrinter == null) {
            return false;
        }

        try {
            // เชื่อมต่อเครื่องปริ้น
            mPrinter.connect(StringUtil.printer_address, Printer.PARAM_DEFAULT);
        } catch (Exception e) {
            System.out.println("DeliveryOrderActivity mPrinter.connect : " + e +"\n"+StringUtil.printer_address);
//            ShowMsg.showException(e, "connect", getApplicationContext());
            return false;
        }

        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();
        menu_order = "";
    }

    private boolean createReceiptData() {
        String method = "";
        Bitmap logoData = BitmapFactory.decodeResource(getResources(), R.drawable.store);
        StringBuilder textData = new StringBuilder();
        final int barcodeWidth = 2;
        final int barcodeHeight = 100;

        if (mPrinter == null) {
            return false;
        }

        try {

            // เปิดแกะเก็บเงิน
//            if(mDrawer.isChecked()) {
//                method = "addPulse";
//                mPrinter.addPulse(Printer.PARAM_DEFAULT,
//                        Printer.PARAM_DEFAULT);
//            }

            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);

//            method = "addImage";
//            mPrinter.addImage(logoData, 0, 0,
//                    logoData.getWidth(),
//                    logoData.getHeight(),
//                    Printer.COLOR_1,
//                    Printer.MODE_MONO,
//                    Printer.HALFTONE_DITHER,
//                    Printer.PARAM_DEFAULT,
//                    Printer.COMPRESS_AUTO);
//
//            method = "addFeedLine";
//            mPrinter.addFeedLine(1);

            textData.append("ร้าน " + Profile.StoreName + "\n");
            textData.append("พนักงาน " + Profile.Username + "\n");
            textData.append("\n");
            textData.append("วันที่เวลา " + getDateTime() + "\n");
            textData.append("โต๊ะ#" + StringUtil.TableCustomerName + " รหัสออร์เดอร์#" + StringUtil.OrderId + "\n");
            textData.append("------------------------------\n");
            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            String[] arrB = menu_order.split("#");
            int i = 1;
            for (String order : arrB) {

                textData.append(i + " " + order + "\n");
                i++;
            }

//            textData.append("2 เมล็ดกาแฟดอยปู่หมื่นคั่วกลาง  300$\n");
//            textData.append("3 กาแฟออร์แกนิคชนิดคั่วเข้มบด  30$\n");

            textData.append("------------------------------\n");
            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            textData.append("ราคาสินค้ารวม               " + menu_order_total_price + " บาท\n");
            textData.append("รวมรายการ                 " + menu_order_total_qty + "    รายการ\n");
            textData.append("ส่วนลดจำนวน                " + menu_order_discount + "  บาท\n");
            textData.append("ยอดเรียกเก็บ                " + menu_order_total_price + "   บาท\n");
            textData.append("------------------------------\n");
            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addBarcode";
            mPrinter.addBarcode(StringUtil.OrderId,
                    Printer.BARCODE_CODE39,
                    Printer.HRI_BELOW,
                    Printer.FONT_A,
                    barcodeWidth,
                    barcodeHeight);

            method = "addCut";
            mPrinter.addCut(Printer.CUT_FEED);

            menu_order = "";
        } catch (Exception e) {
            mPrinter.clearCommandBuffer();
            ShowMsg.showException(e, method, getApplicationContext());
            return false;
        }

        textData = null;

        return true;
    }


    private void disconnectPrinter() {
        if (mPrinter == null) {
            return;
        }

        while (true) {
            try {
                mPrinter.disconnect();
                break;
            } catch (final Exception e) {
                if (e instanceof Epos2Exception) {
                    //Note: If printer is processing such as printing and so on, the disconnect API returns ERR_PROCESSING.
                    if (((Epos2Exception) e).getErrorStatus() == Epos2Exception.ERR_PROCESSING) {
                        try {
                            Thread.sleep(DISCONNECT_INTERVAL);
                        } catch (Exception ex) {
                        }
                    } else {
                        runOnUiThread(new Runnable() {
                            public synchronized void run() {
                                ShowMsg.showException(e, "disconnect", ShowOrderPrintActivity.this);
                            }
                        });
                        break;
                    }
                } else {
                    runOnUiThread(new Runnable() {
                        public synchronized void run() {
                            ShowMsg.showException(e, "disconnect", ShowOrderPrintActivity.this);
                        }
                    });
                    break;
                }
            }
        }

        mPrinter.clearCommandBuffer();
    }


    private void dispPrinterWarnings(PrinterStatusInfo status) {
        EditText edtWarnings = (EditText) findViewById(R.id.edtWarnings);
        String warningsMsg = "";

        if (status == null) {
            return;
        }

        if (status.getPaper() == Printer.PAPER_NEAR_END) {
            warningsMsg += getString(R.string.handlingmsg_warn_receipt_near_end);
        }

        if (status.getBatteryLevel() == Printer.BATTERY_LEVEL_1) {
            warningsMsg += getString(R.string.handlingmsg_warn_battery_near_end);
        }

        edtWarnings.setText(warningsMsg);
    }

    private String makeErrorMessage(PrinterStatusInfo status) {
        String msg = "";

        if (status.getOnline() == Printer.FALSE) {
            msg += getString(R.string.handlingmsg_err_offline);
        }
        if (status.getConnection() == Printer.FALSE) {
            msg += getString(R.string.handlingmsg_err_no_response);
        }
        if (status.getCoverOpen() == Printer.TRUE) {
            msg += getString(R.string.handlingmsg_err_cover_open);
        }
        if (status.getPaper() == Printer.PAPER_EMPTY) {
            msg += getString(R.string.handlingmsg_err_receipt_end);
        }
        if (status.getPaperFeed() == Printer.TRUE || status.getPanelSwitch() == Printer.SWITCH_ON) {
            msg += getString(R.string.handlingmsg_err_paper_feed);
        }
        if (status.getErrorStatus() == Printer.MECHANICAL_ERR || status.getErrorStatus() == Printer.AUTOCUTTER_ERR) {
            msg += getString(R.string.handlingmsg_err_autocutter);
            msg += getString(R.string.handlingmsg_err_need_recover);
        }
        if (status.getErrorStatus() == Printer.UNRECOVER_ERR) {
            msg += getString(R.string.handlingmsg_err_unrecover);
        }
        if (status.getErrorStatus() == Printer.AUTORECOVER_ERR) {
            if (status.getAutoRecoverError() == Printer.HEAD_OVERHEAT) {
                msg += getString(R.string.handlingmsg_err_overheat);
                msg += getString(R.string.handlingmsg_err_head);
            }
            if (status.getAutoRecoverError() == Printer.MOTOR_OVERHEAT) {
                msg += getString(R.string.handlingmsg_err_overheat);
                msg += getString(R.string.handlingmsg_err_motor);
            }
            if (status.getAutoRecoverError() == Printer.BATTERY_OVERHEAT) {
                msg += getString(R.string.handlingmsg_err_overheat);
                msg += getString(R.string.handlingmsg_err_battery);
            }
            if (status.getAutoRecoverError() == Printer.WRONG_PAPER) {
                msg += getString(R.string.handlingmsg_err_wrong_paper);
            }
        }
        if (status.getBatteryLevel() == Printer.BATTERY_LEVEL_0) {
            msg += getString(R.string.handlingmsg_err_battery_real_end);
        }


        System.out.println("ErrorStatus " + msg);
        return msg;
    }

    private String getDateTime() {
        // get date time in custom format
        SimpleDateFormat sdf = new SimpleDateFormat("[yyyy/MM/dd - HH:mm:ss]");
        return sdf.format(new Date());
    }


    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    private class AsyncOrder extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(ShowOrderPrintActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();

            try {
                response = http.run(StringUtil.URL + "gpos/api/DeliveryOrderActivity/" + StringUtil.OrderId);//+ StringUtil.OrderId)
//                response = http.run(StringUtil.URL+"gpos/api/ListOrderItems/29");

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            //txtResult.setText(response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {

            pdLoading.dismiss();

            int x = 0;
            int total_sale = 0;
            double total_sale_discount = 0.0;
            double receive_total = 0.0;

            feedsListOrder = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);

                int qty = 0;
                int price = 0;
                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemOrder item = new FeedItemOrder();
                    item.setOrders_item_id(json_data.getString("orders_item_id"));
                    item.setId(json_data.getString("product_id"));
                    item.setTitle(json_data.getString("product_name"));
                    item.setDetail(json_data.getString("product_detail"));
                    item.setPrice(json_data.getString("total_price"));
                    item.setQty(json_data.getString("qty"));
                    item.setOrderid(json_data.getString("orders_id"));
                    item.setProduct_choice(json_data.getString("product_choice_name"));
                    item.setProduct_image(json_data.getString("product_image"));

                    menu_order += json_data.getString("product_name") +"  "+ json_data.getString("product_choice_name")
                            + "              " + json_data.getString("qty")
                            + "              " + json_data.getString("total_price") +"บาท#";

                    try {
                        qty = Integer.parseInt(json_data.getString("qty"));
                        price = Integer.parseInt(json_data.getString("total_price"));

                        x = x + qty;
                        total_sale = total_sale + price;

                        if (StringUtil.promotion_discount_percen_type.equals("เปอร์เซ็น")) {


                        } else {
//                            StringUtil.promotion_discount_percen_type = feedItem.getPromotion_type();
//                            StringUtil.promotion_discount_percen_total = Integer.parseInt(feedItem.getPromotion_qty());
                        }
                        int dis = StringUtil.promotion_discount_percen_total;
                        total_sale_discount = Discount_Percen(total_sale, dis);
                        receive_total = total_sale - total_sale_discount;


                    } catch (Exception e) {
                        Toast.makeText(ShowOrderPrintActivity.this, e.toString(), Toast.LENGTH_LONG).show();
                    }
                    feedsListOrder.add(item);
                }


                StringUtil.TotalPayment = receive_total + "";

                tvSum_amount.setText("ราคาสินค้ารวม " + total_sale + " บาท");
                tvTotal_qty.setText("รวมรายการ " + x);
                tvDiscount.setText("ส่วนลดจำนวน " + total_sale_discount + " บาท");
                txt_receive_total.setText("ยอดเรียกเก็บ " + receive_total + " บาท");

                menu_order_total_price = total_sale+"";
                menu_order_total_qty = x+"";
                menu_order_discount = total_sale_discount+"";

                initializeObject();

                if (!StringUtil.printer_address.equals("")) {
                    System.out.println(" mPrinter. printer_address : " + StringUtil.printer_address);
                    initializeObject();
                } else {
                    System.out.println(" mPrinter. printer_address : " + StringUtil.printer_address);
                }

                // display toast
                System.out.println(" mPrinter.run : " + Profile.StoreName);
                if (!runPrintReceiptSequence()) {
//                        updateButtonState(true);
                    System.out.println(" runPrintReceiptSequence.run : " + getDateTime());
                }


                OrdermAdapter = new OrderRecyclerAdapter(ShowOrderPrintActivity.this, feedsListOrder);
                recyclerView.setAdapter(OrdermAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(ShowOrderPrintActivity.this, 1);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());

            } catch (JSONException e) {


                Toast.makeText(ShowOrderPrintActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }


    public double Discount_Percen(int price, int discount) {
        double res = 0.0;

        res = (discount * price) / 100;

        return res;
    }

    // Adapter
    public class OrderRecyclerAdapter extends RecyclerView.Adapter<OrderRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemOrder> feedsListOrder;
        private Context mContext;

        public OrderRecyclerAdapter(Context context, List<FeedItemOrder> feedItemList) {
            this.feedsListOrder = feedItemList;
            this.mContext = context;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.content_cashier_order_row, null);

            CustomViewHolder viewHolder = new CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(final CustomViewHolder customViewHolder, int i) {
            final FeedItemOrder feedItem = feedsListOrder.get(i);
            customViewHolder.feedItem = feedItem;

//            try {
            int qty = Integer.parseInt(feedItem.getQty());
            int price = Integer.parseInt(feedItem.getPrice());

            int sum = qty * price;

            //Setting text view title
            customViewHolder.textView.setText("สินค้า: " + feedItem.getTitle());
            customViewHolder.txt_price.setText(sum + "฿");
//                customViewHolder.price.setText("ราคา/หน่วย: " + price);
            customViewHolder.txt_order_qty.setText("" + feedItem.getQty());
            customViewHolder.txt_choice_name.setText("Choice : " + feedItem.getProduct_choice());

        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedsListOrder ? feedsListOrder.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            protected ImageView imageView;
            protected TextView textView;
            protected TextView txt_price;
            protected TextView txt_choice_name, txt_order_qty;
            FeedItemOrder feedItem;

            public CustomViewHolder(View view) {
                super(view);
//                this.imageView = (ImageView) view.findViewById(R.id.imageView2);
                this.txt_choice_name = (TextView) view.findViewById(R.id.txt_choice_name);
                this.textView = (TextView) view.findViewById(R.id.txt_menu_name);
                this.txt_price = (TextView) view.findViewById(R.id.txt_price);
                this.txt_order_qty = (TextView) view.findViewById(R.id.txt_order_qty);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        StringUtil.Orders_item_id = feedItem.getOrders_item_id();
                        StringUtil.OrderId = feedItem.getOrderid();

//                        LayoutInflater layoutinflater = LayoutInflater.from(OpenTableOrderActivity.this);
//                        View promptUserView = layoutinflater.inflate(R.layout.content_product_qty, null);
//
//                        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(OpenTableOrderActivity.this);

//                        alertDialogBuilder.setView(promptUserView);
//                        TextView tvTitle = (TextView) promptUserView.findViewById(R.id.tvTitle);
//                        tvTitle.setText(feedItem.getTitle());

//                        final EditText input_product_qty = (EditText) promptUserView.findViewById(R.id.input_product_qty);


                    }
                });

            }
        }
    }


    // String
    public class FeedItemOrder {
        private String title;
        private String id;
        private String setDetail;
        private String price;
        private String qty;
        private String PricePerItem;
        private String orders_item_id;
        private String product_image;
        private String product_choice;

        public String getProduct_choice() {
            return product_choice;
        }

        public void setProduct_choice(String product_choice) {
            this.product_choice = product_choice;
        }


        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }

        private String orderid;

        public String getOrderid() {
            return orderid;
        }

        public void setOrderid(String orderid) {
            this.orderid = orderid;
        }

        public String getOrders_item_id() {
            return orders_item_id;
        }

        public void setOrders_item_id(String orders_item_id) {
            this.orders_item_id = orders_item_id;
        }

        public String getPricePerItem() {
            return PricePerItem;
        }

        public void setPricePerItem(String pricePerItem) {
            this.PricePerItem = pricePerItem;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getPrice() {
            return price;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String name) {
            this.title = name;
        }


        public String getDetail() {
            return setDetail;
        }

        public void setDetail(String Detail) {
            this.setDetail = Detail;
        }


    }

}
