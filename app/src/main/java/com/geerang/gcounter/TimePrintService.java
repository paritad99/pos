package com.geerang.gcounter;

import android.app.Service;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.widget.Toast;

import com.epson.epos2.printer.Printer;
import com.epson.epos2.printer.PrinterStatusInfo;
import com.epson.epos2.printer.ReceiveListener;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;

public class TimePrintService extends Service implements ReceiveListener {
    // constant
    public static final long NOTIFY_INTERVAL = 10 * 1000; // 10 seconds

    // run on another Thread to avoid crash
    private Handler mHandler = new Handler();
    // timer handling
    private Timer mTimer = null;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    static private BluetoothSocket mbtSocket = null;
    public static Printer mPrinter = null;
    static String menu_order = "";

    //    IBinder service;
//    PrinterInterface mAidl;
    String Resultbill = "";

    Profile profile;

    private static final int DISCONNECT_INTERVAL = 1000;//millseconds

    private BroadcastReceiver MyReceiver = null;


    EpsonMakeErrorMessage epsonMakeErrorMessage;
    final String KEY_Autoprint = "Autoprint";
    final String KEY_PrinterMain = "PrinterMain";
    final String KEY_Printer1 = "Printer1";
    final String KEY_Printer2 = "Printer2";
    final String KEY_Printer3 = "Printer3";
    final String KEY_Printer4 = "Printer4";

    String PrinterAddress = "";
    String product_name = "";
    String report_cat_name = "";
    String customer_name = "";
    String username = "";
    String kit_cat = "";
    String create_date = "";

    final String PREF_NAME_PRINT = "PrinterPreferences";
    SharedPreferences sp_print;
    SharedPreferences.Editor editor_print;
    private Handler handler = new Handler();
    Context context;


    @Override
    public void onCreate() {
        // cancel if already existed


//        sp = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
//        editor = sp.edit();

//        if (mTimer != null) {
//            mTimer.cancel();
//        } else {
//            // recreate new
//            mTimer = new Timer();
//        }


        profile = new Profile(getApplicationContext());

        context = getApplicationContext();

        Intent intent = new Intent();
        intent.setClassName("recieptservice.com.recieptservice", "recieptservice.com.recieptservice.service.PrinterService");
        bindService(intent, new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, final IBinder _service) {
//                service = _service;
//                mAidl = PrinterInterface.Stub.asInterface(service);

            }

            @Override
            public void onServiceDisconnected(ComponentName name) {

            }

        }, Service.BIND_AUTO_CREATE);

        // schedule task
       // mTimer.scheduleAtFixedRate(new TimeDisplayTimerTask(), 0, NOTIFY_INTERVAL);

        refresh(true);


    }


    void refresh(boolean b){
        if(b){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    new AsyncChoiceProductPrint().execute();

                    //Toast.makeText(getApplicationContext(),"refresh",Toast.LENGTH_SHORT).show();

                    refresh(true);
                }
            }, 10*1000);
        }
    }

    @Override
    public void onPtrReceive(Printer printer, int i, PrinterStatusInfo printerStatusInfo, String s) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                disconnectPrinter();
            }
        });

    }


//    class TimeDisplayTimerTask extends TimerTask {
//
//        @Override
//        public void run() {
//            // run on another thread
//            mHandler.post(new Runnable() {
//                @Override
//                public void run() {
//
//                    mHandler.postDelayed(this, 3000);
//                    if (NetworkUtil.status.equals("No internet is available")) {
//
//                    } else {
//
//                        disconnectPrinter();
//
//                        System.out.println("Print Load");
//                        new AsyncChoiceProductPrint().execute();
//
//                    }
//
//                }
//
//            });
//        }
//
//
//    }

    private class AsyncChoiceProductPrint extends AsyncTask<String, String, String> {


        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run("http://www.geerang.com/gpos/api/OrderMenuPrint/" + profile.BranchId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

//                txtResult.setText(response);
            return response;

        }

        @Override
        protected void onPostExecute(String result) {

            if(result!=null){
                // Do you work here on success
                Resultbill = result;

                Runnable runnable = new Runnable() {

                    public void run() {
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        handler.post(new Runnable() {
                            public void run() {
                                // Handler thread
                                double sum_price = 0.0;
                                JSONArray jArray = null;
                                double sum_row_price = 0.0;
                                try {
                                    jArray = new JSONArray(Resultbill);


                                    for (int f = 0; f < jArray.length(); f++) {
                                        JSONObject json_data_report = jArray.getJSONObject(f);
                                        //product_name = "";
                                        //mAidl.setAlignment(1);
                                        customer_name = json_data_report.getString("customer_name");
                                        username = json_data_report.getString("username");
                                        kit_cat = json_data_report.getString("kit_cat");
                                        create_date = json_data_report.getString("create_date");
                                        product_name = "";

                                        //mAidl.printText("\n");
                                        JSONArray jArray_menu = new JSONArray(json_data_report.getString("products"));
                                        if (jArray_menu.length() > 0) {

                                            // loop ของ report_cat
                                            for (int i = 0; i < jArray_menu.length(); i++) {
                                                JSONObject json_data = jArray_menu.getJSONObject(i);
                                                //หมวดหมู่อาหาร
                                                //product_name = json_data.getString("product_name");
                                                String qty = json_data.getString("qty");
                                                String remain = json_data.getString("remain");
                                                String orders_item_id = json_data.getString("orders_item_id");

                                                String Choice = "";
                                                // loop product
                                                JSONArray jArray2 = new JSONArray(json_data.getString("product_choice"));
                                                int check_list = jArray2.length();
                                                if (check_list > 0) {
                                                    for (int k = 0; k < jArray2.length(); k++) {
                                                        JSONObject json_data3 = jArray2.getJSONObject(k);
                                                        Choice += json_data3.getString("product_choice_name") + ",";
                                                    }
                                                }

                                                if (remain.equals("")) {
                                                    product_name += json_data.getString("product_name") + " " + Choice + " = " + qty + " \n";
                                                } else {
                                                    product_name += json_data.getString("product_name") + " " + Choice + " = " + qty + " \n (" + remain + ") \n";
                                                }

                                                // update รายการที่ปริ้นแล้ว
                                                new BookingAddTask().execute(StringUtil.URL + "gpos/api/AutoPrint_UpdateOrder", orders_item_id);


                                            }

                                            //mAidl.printText("------------------------------------\n");

                                        } else {

                                        }


                                        PrinterAddress = CheckPrintAddress(kit_cat);

                                        //สั้งปริ้น
                                        if (!runPrintReceiptSequence()) {
                                            System.out.println("Print Start");
                                        }

                                        System.out.println("PrinterAddress  " + PrinterAddress + "\n");
                                        System.out.println(customer_name + "\n");
                                        System.out.println(product_name + "\n");


                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            }
                        });

                    }
                };
                new Thread(runnable).start();

            }else{
                // null response or Exception occur
            }




            // loop ของ report category
            // customer_name


                    /*
                            "username": "pop",
                            "customer_id": "1",
                            "customer_name": "โต๊ะ 1",
                            "kit_cat": "เครื่องดื่ม",
                            "store_id": "1",
                            "orders_id": "119",
                            "products": [
                     */





        }


    }


    public String CheckPrintAddress(String KitchenCat) {
        String status = "";
        String title = "";
        String address = "";
        String mac_address = "";
        String categories_id = "";

        sp_print = getSharedPreferences(PREF_NAME_PRINT, Context.MODE_PRIVATE);
        editor_print = sp_print.edit();

        editor_print = sp_print.edit();
        String PrinterMain = sp_print.getString(KEY_PrinterMain, "");
        String Printer1 = sp_print.getString(KEY_Printer1, "");

        if (!Printer1.equals("")) {
            String[] namesList = Printer1.split(",");
            status = namesList[0];
            title = namesList[1];
            address = namesList[2];
            if (address.equals(KitchenCat)) {
                address = namesList[2];
                System.out.println("Print 1 line 370" + address);
            }


            mac_address = namesList[3];
            categories_id = namesList[4];

            //btnPrinter1.setText("Printer1 (" + title + ") \nStatus " +status+"");
        }

        String Printer2 = sp_print.getString(KEY_Printer2, "");
        if (!Printer2.equals("")) {
            String[] namesList = Printer2.split(",");
            status = namesList[0];
            title = namesList[1];
            address = namesList[2];
            if (address.equals(KitchenCat)) {
                address = namesList[2];
                System.out.println("Print 1 line 370" + address);
            }


            mac_address = namesList[3];
            categories_id = namesList[4];

            //btnPrinter1.setText("Printer1 (" + title + ") \nStatus " +status+"");
        }

        String Printer3 = sp_print.getString(KEY_Printer3, "");
        if (!Printer3.equals("")) {
            String[] namesList = Printer3.split(",");
            status = namesList[0];
            title = namesList[1];
            address = namesList[2];
            if (address.equals(KitchenCat)) {
                address = namesList[2];
                System.out.println("Print 1 line 370" + address);
            }

            mac_address = namesList[3];
            categories_id = namesList[4];

            //btnPrinter1.setText("Printer1 (" + title + ") \nStatus " +status+"");
        }

        String Printer4 = sp_print.getString(KEY_Printer4, "");
        if (!Printer4.equals("")) {
            String[] namesList = Printer4.split(",");
            status = namesList[0];
            title = namesList[1];
            address = namesList[2];
            if (address.equals(KitchenCat)) {
                address = namesList[2];
                System.out.println("Print 1 line 370" + address);
            }


            mac_address = namesList[3];
            categories_id = namesList[4];

            //btnPrinter1.setText("Printer1 (" + title + ") \nStatus " +status+"");
        }


        return address;
    }

    private boolean isPrintable(PrinterStatusInfo status) {
        if (status == null) {
            return false;
        }

        if (status.getConnection() == Printer.FALSE) {
            return false;
        } else if (status.getOnline() == Printer.FALSE) {
            return false;
        } else {
            ;//print available
        }

        return true;
    }


    private String getDateTime() {
        // get date time in custom format
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd - HH:mm");
        return sdf.format(new Date());
    }

    private boolean runPrintReceiptSequence() {

        if (!initializeObject()) {
            return false;
        }

        if (!createReceiptData()) {
            finalizeObject();
            return false;
        }

        if (!printData()) {
            finalizeObject();
            return false;
        }

        return true;
    }


    private boolean initializeObject() {
        try {
            // ((SpnModelsItem) mSpnLang.getSelectedItem()).getModelConstant()
//            mPrinter = new Printer(((SpnModelsItem) mSpnSeries.getSelectedItem()).getModelConstant(),
            mPrinter = new Printer(Printer.TM_T88, Printer.MODEL_ANK, getApplicationContext());
            //

        } catch (Exception e) {
            ShowMsg.showException(e, "Printer", getApplicationContext());
            return false;
        }

        mPrinter.setReceiveEventListener(this);

        return true;
    }

    private void dispPrinterWarnings(PrinterStatusInfo status) {
        //EditText edtWarnings = (EditText) findViewById(R.id.edtWarnings);
        String warningsMsg = "";

        if (status == null) {
            return;
        }

        if (status.getPaper() == Printer.PAPER_NEAR_END) {
            warningsMsg += getString(R.string.handlingmsg_warn_receipt_near_end);
        }

        if (status.getBatteryLevel() == Printer.BATTERY_LEVEL_1) {
            warningsMsg += getString(R.string.handlingmsg_warn_battery_near_end);
        }

        System.out.println("warningsMsg : " + warningsMsg);
        //edtWarnings.setText(warningsMsg);
    }

    private String makeErrorMessage(PrinterStatusInfo status) {
        String msg = "";

        if (status.getOnline() == Printer.FALSE) {
            msg += getString(R.string.handlingmsg_err_offline);
        }
        if (status.getConnection() == Printer.FALSE) {
            msg += getString(R.string.handlingmsg_err_no_response);
        }
        if (status.getCoverOpen() == Printer.TRUE) {
            msg += getString(R.string.handlingmsg_err_cover_open);
        }
        if (status.getPaper() == Printer.PAPER_EMPTY) {
            msg += getString(R.string.handlingmsg_err_receipt_end);
        }
        if (status.getPaperFeed() == Printer.TRUE || status.getPanelSwitch() == Printer.SWITCH_ON) {
            msg += getString(R.string.handlingmsg_err_paper_feed);
        }
        if (status.getErrorStatus() == Printer.MECHANICAL_ERR || status.getErrorStatus() == Printer.AUTOCUTTER_ERR) {
            msg += getString(R.string.handlingmsg_err_autocutter);
            msg += getString(R.string.handlingmsg_err_need_recover);
        }
        if (status.getErrorStatus() == Printer.UNRECOVER_ERR) {
            msg += getString(R.string.handlingmsg_err_unrecover);
        }
        if (status.getErrorStatus() == Printer.AUTORECOVER_ERR) {
            if (status.getAutoRecoverError() == Printer.HEAD_OVERHEAT) {
                msg += getString(R.string.handlingmsg_err_overheat);
                msg += getString(R.string.handlingmsg_err_head);
            }
            if (status.getAutoRecoverError() == Printer.MOTOR_OVERHEAT) {
                msg += getString(R.string.handlingmsg_err_overheat);
                msg += getString(R.string.handlingmsg_err_motor);
            }
            if (status.getAutoRecoverError() == Printer.BATTERY_OVERHEAT) {
                msg += getString(R.string.handlingmsg_err_overheat);
                msg += getString(R.string.handlingmsg_err_battery);
            }
            if (status.getAutoRecoverError() == Printer.WRONG_PAPER) {
                msg += getString(R.string.handlingmsg_err_wrong_paper);
            }
        }
        if (status.getBatteryLevel() == Printer.BATTERY_LEVEL_0) {
            msg += getString(R.string.handlingmsg_err_battery_real_end);
        }

        return msg;
    }

    private boolean printData() {
        if (mPrinter == null) {
            return false;
        }

        if (!connectPrinter()) {
            return false;
        }

        PrinterStatusInfo status = mPrinter.getStatus();

        dispPrinterWarnings(status);

        if (!isPrintable(status)) {
            //ShowMsg.showMsg(makeErrorMessage(status), getApplicationContext());
            try {
                mPrinter.disconnect();
            } catch (Exception ex) {
                // Do nothing
            }
            return false;
        }

        try {
            mPrinter.sendData(Printer.PARAM_DEFAULT);
        } catch (Exception e) {
            ShowMsg.showException(e, "sendData", getApplicationContext());
            try {
                mPrinter.disconnect();
            } catch (Exception ex) {
                // Do nothing
            }
            return false;
        }

        return true;
    }

    private boolean connectPrinter() {
        if (mPrinter == null) {
            return false;
        }

        try {
            // mPrinter.connect("192.168.1.103", Printer.PARAM_DEFAULT);
            mPrinter.connect(PrinterAddress, Printer.PARAM_DEFAULT);

        } catch (Exception e) {
            // ShowMsg.showException(e, "connect", getApplicationContext());
            return false;
        }

        return true;
    }

    private boolean createReceiptData() {

        String method = "";
        StringBuilder textData = new StringBuilder();
        final int barcodeWidth = 2;
        final int barcodeHeight = 100;

        if (mPrinter == null) {
            return false;
        }

        try {

            mPrinter.addTextLang(Printer.LANG_TH);
            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);
            method = "addFeedLine";
            mPrinter.addFeedLine(1);
            textData.append(customer_name + "");
            textData.append("\n");
            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            textData.append(String.format("%-20s %-20s \n", getDateTime(), kit_cat));
            textData.append("------------------------------------------\n");

            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_RIGHT);
            method = "addFeedLine";
            mPrinter.addFeedLine(1);
            textData.append(username + "\n");
            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_LEFT);
            method = "addFeedLine";
            mPrinter.addFeedLine(1);
            textData.append(product_name + "\n");
            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addCut";
            mPrinter.addCut(Printer.CUT_FEED);

            menu_order = "";


        } catch (Exception e) {
            mPrinter.clearCommandBuffer();
            ShowMsg.showException(e, method, getApplicationContext());
            return false;
        }

        textData = null;


        return true;
    }


    private void disconnectPrinter() {
        if (mPrinter == null) {
            return;
        }

        try {
            mPrinter.endTransaction();
        } catch (final Exception e) {
        }

        try {
            mPrinter.disconnect();
        } catch (final Exception e) {
        }

        finalizeObject();
    }

    private void finalizeObject() {
        if (mPrinter == null) {
            return;
        }

        mPrinter.clearCommandBuffer();

        mPrinter.setReceiveEventListener(null);

        mPrinter = null;
    }

    private class BookingAddTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();


            RequestBody formBody = new FormEncodingBuilder()
                    .add("orders_item_id", urls[1])
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                String msg = object.getString("msg"); // success
                System.out.println(jsonString);

                disconnectPrinter();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        //mTimer.cancel();
        Toast.makeText(this, "Invoke background service onDestroy method.", Toast.LENGTH_LONG).show();
    }


    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


}