package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.PictureDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.StrictMode;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection;

public class SelectMenuToTableActivity extends AppCompatActivity {

    // CONNECTION_TIMEOUT and READ_TIMEOUT are in milliseconds
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;
    private List<FeedItemDrug> feedsList;
    private RecyclerView recyclerView;
    private DrugRecyclerAdapter mAdapter;

    static TextView txtShowChoice;

    static ProgressBar progressBar;

    RecyclerView mRecyclerView;
    private SimpleAdapter mFirstHeader;
    private SimpleAdapter mSecondHeader;
    private SimpleAdapter mThirdHeader;
    private SectionedRecyclerViewAdapter sectionAdapter;
    ArrayList<HashMap<String, String>> MyArrList_GroupChoice;

    ArrayList<HashMap<String, String>> ChoiceArrList;
    HashMap<String, String> map_choice;

    AlertDialog dialog;

    int count_select_max = 0;

    Button btnAdd;

    Profile profile;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {

//            if (Profile.Position.equals("2")) {
//                StringUtil.employee_status = "ผู้จัดการร้าน";
//                Intent in = new Intent(SelectMenuToTableActivity.this, ManagerMainActivity.class);
//                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                startActivity(in);
//                finish();
//            } else {
            finish();
//            }

        }
        return super.onOptionsItemSelected(item);
    }


    String CatId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_menu_to_table);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        Intent in = getIntent();
        CatId = in.getStringExtra("CatId");

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        new AsyncLogin().execute();

    }


    @Override
    public void onResume() {
        super.onResume();
        new AsyncLogin().execute();

    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    private class AsyncLogin extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(SelectMenuToTableActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tกำลังโหลดข้อมูล...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                CatId = StringUtil.CatId;
                response = http.run(StringUtil.URL + "gpos/api/Menu/" + CatId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();

            feedsList = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);

                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemDrug item = new FeedItemDrug();
                    item.setId(json_data.getString("product_id"));
                    item.setTitle(json_data.getString("product_name"));
                    item.setDetail(json_data.getString("product_detail"));
                    item.setPrice(json_data.getString("product_sale_price"));
                    item.setProduct_image(json_data.getString("product_image"));

                    item.setMenu_name(json_data.getString("product_name"));
                    item.setMenu_product_detail(json_data.getString("product_detail"));
                    item.setMenu_calories(json_data.getString("calories"));
                    item.setMenu_categories_id(json_data.getString("categories_id"));
                    item.setMenu_cookingtime(json_data.getString("cookingtime"));
                    item.setMenu_image_data(json_data.getString("product_image"));
                    item.setMenu_is_recommend(json_data.getString("is_recommend"));
                    item.setMenu_product_price(json_data.getString("product_sale_price"));
                    item.setMenu_product_price(json_data.getString("product_sale_price"));
                    int choice_group = 0; //
                    JSONArray jArray_2 = new JSONArray(json_data.getString("products_group"));
                    choice_group = jArray_2.length();

                    item.setChoice_group(choice_group);

                    feedsList.add(item);
                }
                // Setup and Handover data to recyclerview
                mAdapter = new DrugRecyclerAdapter(SelectMenuToTableActivity.this, feedsList);
                recyclerView.setAdapter(mAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(SelectMenuToTableActivity.this, 3);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setItemAnimator(new DefaultItemAnimator());

            } catch (JSONException e) {
                Toast.makeText(SelectMenuToTableActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    // Adapter
    public class DrugRecyclerAdapter extends RecyclerView.Adapter<DrugRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemDrug> feedItemList;
        private Context mContext;

        public DrugRecyclerAdapter(Context context, List<FeedItemDrug> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_select_menu_to_table_row, null);

            CustomViewHolder viewHolder = new CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
            final FeedItemDrug feedItem = feedItemList.get(i);
            customViewHolder.feedItem = feedItem;


            //Setting text view title
            customViewHolder.textView.setText(Html.fromHtml(feedItem.getTitle()));
            customViewHolder.tvPrice.setText(Html.fromHtml(feedItem.getPrice()) + " ฿");
            customViewHolder.txtAddorder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });


            StringUtil stringUtil = new StringUtil();
            Picasso.with(mContext)
                    .load(StringUtil.URL + "gpos/" + stringUtil.removeFirstChar(feedItem.getProduct_image()))
                    .fit()
                    .placeholder(R.drawable.noimageavailable)
                    .error(R.drawable.noimageavailable)
                    .into(customViewHolder.imageView);


        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            protected ImageView imageView;
            protected TextView textView;
            protected TextView tvPrice, txtAddorder;

            FeedItemDrug feedItem;

//            protected  Button  btnEdit,btnDelete;

            public CustomViewHolder(View view) {
                super(view);
                this.imageView = (ImageView) view.findViewById(R.id.imageView);
                this.textView = (TextView) view.findViewById(R.id.title);
                this.tvPrice = (TextView) view.findViewById(R.id.tvTotalPrice);
                this.txtAddorder = (TextView) view.findViewById(R.id.txtAddorder);
//                this.btnEdit = (Button) view.findViewById(R.id.btnEdit);
//                this.btnDelete = (Button) view.findViewById(R.id.btnDelete);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        final String product_id = feedItem.getId();

                        if (feedItem.getChoice_group() > 0) {
                            // ทำ Dialog เพื่อใช้ในการเลือก Choice ของเมนูนั้นๆ
                            LayoutInflater inflater = getLayoutInflater();
                            View alertLayout = inflater.inflate(R.layout.activity_selectmenu_choice_dialog, null);


                            //final EditText input_order_qty = (EditText) alertLayout.findViewById(R.id.input_order_qty);
                            final EditText input_remain = (EditText) alertLayout.findViewById(R.id.input_remain);
                            btnAdd = (Button) alertLayout.findViewById(R.id.btnAdd);
                            Button btnCancel = (Button) alertLayout.findViewById(R.id.btnCancel);
                            btnAdd.setEnabled(false);
                            btnAdd.setBackgroundColor(Color.GRAY);

                            //btnAdd.setBackground(R.color.colorPrimary);
                            mRecyclerView = (RecyclerView) alertLayout.findViewById(R.id.rvList);
                            progressBar = (ProgressBar) alertLayout.findViewById(R.id.progressBar);

                            //txtShowChoice = (TextView) alertLayout.findViewById(R.id.txtShowChoice);
                            new AsyncChoiceProduct().execute(product_id);
                            // load choice
                            AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
                            alert.setTitle("กรุณาเลือก Choice \n" + feedItem.getMenu_name());
                            // this is set the view from XML inside AlertDialog
                            alert.setView(alertLayout);
                            dialog = alert.create();
                            btnAdd.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Gson gson = new GsonBuilder().create();
                                    JsonArray myCustomArray = gson.toJsonTree(ChoiceArrList).getAsJsonArray();
                                    System.out.println("Add Data  = " + myCustomArray);

                                    new BookingAddTask().execute(
                                            StringUtil.URL + "gpos/api/AddOrderItem"
                                            , StringUtil.OrderId
                                            , feedItem.getId()
                                            , profile.UserId
                                            , input_remain.getText().toString()
                                            , myCustomArray.toString());
                                }
                            });

                            btnCancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialog.dismiss();
                                }
                            });


                            try {
                                dialog.show();
                            }catch (Exception e)
                            {

                            }

                        } else {

                            new OrderNochoiceAddTask().execute(
                                    StringUtil.URL + "gpos/api/AddOrderItemNochoice"
                                    , StringUtil.OrderId
                                    , product_id
                                    , profile.UserId
                                    , ""
                                    , "");

//                            Toast.makeText(v.getContext(), "ไม่มี Choice ให้เลือก", Toast.LENGTH_LONG).show();
                        }


                    }
                });

            }
        }
    }

    public class SimpleAdapter extends StatelessSection {

        //        private ArrayList<String> arrName = new ArrayList<>();
        ArrayList<HashMap<String, String>> MyArrList_choice = new ArrayList<HashMap<String, String>>();
        private String mTitle;
        private int min;
        private int max;
        private int select_item_count = 0;
        private String p_group_id;

        public SimpleAdapter(ArrayList<HashMap<String, String>> arrName
                , String title, String min, String max, String p_group_id) {
            super(R.layout.layout_header, R.layout.layout_item);

            this.MyArrList_choice = arrName;
            this.mTitle = title;
            this.min = Integer.parseInt(min);
            this.p_group_id = p_group_id;
            if (min.equals("0")) {
                this.max = MyArrList_choice.size();
            } else {
                this.max = Integer.parseInt(max);
            }

            ChoiceArrList = new ArrayList<HashMap<String, String>>();

        }

        @Override
        public int getContentItemsTotal() {
            return MyArrList_choice.size();
        }

        @Override
        public RecyclerView.ViewHolder getItemViewHolder(View view) {
            return new ItemViewHolder(view);
        }

        @Override
        public RecyclerView.ViewHolder getHeaderViewHolder(View view) {
            return new HeaderViewHolder(view);
        }

        @Override
        public void onBindItemViewHolder(RecyclerView.ViewHolder holder, final int position) {
            final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
            itemViewHolder.txtItem.setText("*" + MyArrList_choice.get(position).get("product_choice_name"));
            itemViewHolder.txtPrice.setText("+฿" + MyArrList_choice.get(position).get("choice_price"));
            itemViewHolder.checkBox.setTag(MyArrList_choice.get(position).get("Code"));

            // ตรวจสอบของหมด
            // คำนวนหาว่า วัตุดดิบในการทำหาหารเหลือมั้ย
            String StockQty = MyArrList_choice.get(position).get("choice_stock");
            JSONArray jArray2 = null;
            try {
                jArray2 = new JSONArray(StockQty);
                int stock_total_by_item = 1;
                for (int i = 0; i < jArray2.length(); i++) {
                    JSONObject json_data2 = jArray2.getJSONObject(i);
                    String Qty = json_data2.getString("product_onhand");
                    int x = Integer.parseInt(Qty);
                    if (x == 0) {
                        stock_total_by_item = 0;
                    }

                }

                if (stock_total_by_item == 0) {
                    // ซ่อน Checkbox ไป
                    itemViewHolder.checkBox.setVisibility(View.GONE);
                    // แสดงข้อมูลว่าวัตุดิบในการประกอบหาหารหมด
                    itemViewHolder.txtStockOrder.setVisibility(View.VISIBLE);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


            itemViewHolder.checkBox.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    AppCompatCheckBox cb = (AppCompatCheckBox) v;

                    try {

                        final int ck_lock = ChoiceArrList.size();
                        //check ว่ามีรายการ
                        if (cb.isChecked() == false) {

                            select_item_count = select_item_count - 1;

                            count_select_max = count_select_max - 1;

                            for (int i = 0; i < ck_lock; i++) {
                                String p_group_add_id = ChoiceArrList.get(i).get("p_group_add_id").toString();
                                String group_choice = ChoiceArrList.get(i).get("group_choice").toString();
                                // Removing Key-Value
                                if (p_group_add_id.equals(MyArrList_choice.get(position).get("p_group_add_id"))
                                        && group_choice.equals(mTitle)) {
                                    ChoiceArrList.remove(i);
                                    System.out.println("Element removed is: " + p_group_add_id + "array size = " + ChoiceArrList.size());
                                }
                            }


                        } else {

                            select_item_count = select_item_count + 1;
//                            count_select_max = count_select_max+1;
                            count_select_max = count_select_max + 1;
                            if (select_item_count >= min && select_item_count <= max) {

                                map_choice = new HashMap<String, String>();
                                map_choice.put("p_group_id", p_group_id);
                                map_choice.put("group_choice", mTitle);
                                map_choice.put("p_group_add_id", MyArrList_choice.get(position).get("p_group_add_id"));
                                ChoiceArrList.add(map_choice);

                                System.out.println("Element Add is: "
                                        + MyArrList_choice.get(position).get("p_group_add_id")
                                        + "array size = " + ChoiceArrList.size());

                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(SelectMenuToTableActivity.this);
                                builder.setMessage("เลือกเกินมากกว่าที่กำหนด")
                                        .setCancelable(false)
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                select_item_count = select_item_count - 1;
                                                count_select_max = count_select_max - 1;
                                                itemViewHolder.checkBox.setChecked(false);
                                            }
                                        });
                                AlertDialog alert = builder.create();
                                alert.show();
                            }

                        }

                        // check lock ปุ่มกด
                        if (count_select_max >= min) {
                            btnAdd.setEnabled(true);
                            btnAdd.setBackground(SelectMenuToTableActivity.this.getResources()
                                    .getDrawable(R.drawable.roundedbutton_btn_login_store));


                        } else {
                            btnAdd.setEnabled(false);
                            btnAdd.setBackground(SelectMenuToTableActivity.this.getResources()
                                    .getDrawable(R.drawable.roundedbutton_btn_enable_false));
                        }


                    } catch (Exception error) {
                        btnAdd.setEnabled(false);
                        btnAdd.setBackground(SelectMenuToTableActivity.this.getResources()
                                .getDrawable(R.drawable.roundedbutton_btn_enable_false));
                    }


//                    Toast.makeText(v.getContext(), MyArrList_choice.get(position).get("p_group_add_id") + cb.isChecked(), Toast.LENGTH_LONG).show();
                }
            });

        }

        @Override
        public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder) {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
            headerViewHolder.txtHeader.setText(mTitle);


            if (min == 0) {
                headerViewHolder.txtMinMax.setVisibility(View.GONE);

            } else {
                headerViewHolder.txtMinMax.setText("เลือกได้น้อยสุด " + min + " เลือกได้มากสุด " + max);
            }


        }

        protected class HeaderViewHolder extends RecyclerView.ViewHolder {

            protected TextView txtHeader;
            protected TextView txtMinMax;

            private HeaderViewHolder(View itemView) {
                super(itemView);
                txtHeader = (TextView) itemView.findViewById(R.id.txtHeader);
                txtMinMax = (TextView) itemView.findViewById(R.id.txtMinMax);
            }
        }

        protected class ItemViewHolder extends RecyclerView.ViewHolder {

            protected TextView txtItem, txtPrice, txtStockOrder;
            protected CheckBox checkBox;

            private ItemViewHolder(View itemView) {
                super(itemView);
                txtItem = (TextView) itemView.findViewById(R.id.txtChoice);
                txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
                checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);
                txtStockOrder = (TextView) itemView.findViewById(R.id.txtStockOrder);
            }
        }
    }

    private class AsyncChoiceProduct extends AsyncTask<String, String, String> {

        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run("http://www.geerang.com/gpos/api/GroupChoiceList/" + urls[0]);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {
            try {
                progressBar.setVisibility(View.GONE);

                JSONArray jArray = new JSONArray(result);
                sectionAdapter = new SectionedRecyclerViewAdapter();

                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    //ประกาศ array string
                    ArrayList<String> arrSports = new ArrayList<>();
                    // array hasmap
                    MyArrList_GroupChoice = new ArrayList<HashMap<String, String>>();
                    HashMap<String, String> map;

                    JSONArray jArray2 = new JSONArray(json_data.getString("group_choice"));
                    int check_list = jArray2.length();
                    if (check_list > 0) {
                        for (int j = 0; j < jArray2.length(); j++) {

                            JSONObject json_data2 = jArray2.getJSONObject(j);
                            arrSports.add(json_data2.getString("product_choice_name"));
                            // map data เข้าไป
                            map = new HashMap<String, String>();
                            map.put("product_choice_name", json_data2.getString("product_choice_name"));
                            map.put("p_group_add_id", json_data2.getString("p_group_add_id"));
                            map.put("choice_price", json_data2.getString("choice_price"));
                            map.put("choice_stock", json_data2.getString("choice_stock"));

                            MyArrList_GroupChoice.add(map);

                        }

                        mFirstHeader = new SimpleAdapter(MyArrList_GroupChoice
                                , "" + json_data.getString("group_name")
                                , json_data.getString("min")
                                , json_data.getString("max")
                                , json_data.getString("p_group_id")
                        );
                        sectionAdapter.addSection(mFirstHeader);
                    }

                }

                final GridLayoutManager glm = new GridLayoutManager(SelectMenuToTableActivity.this, 3);
                glm.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(final int position) {
                        if (sectionAdapter.getSectionItemViewType(position) == SectionedRecyclerViewAdapter.VIEW_TYPE_HEADER) {
                            return 3;
                        }
                        return 1;
                    }
                });

                mRecyclerView.setLayoutManager(glm);

//                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
//                mRecyclerView.setLayoutManager(linearLayoutManager);
                mRecyclerView.setAdapter(sectionAdapter);


            } catch (JSONException e) {
                Toast.makeText(SelectMenuToTableActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    private class OrderNochoiceAddTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(SelectMenuToTableActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tSaving...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("orders_id", urls[1])
                    .add("product_id", urls[2])
                    .add("qty", "1")
                    .add("userid", urls[3])
                    .add("remain", urls[4])
                    .add("p_group_add_id", urls[5])
                    .add("branch_id", profile.BranchId)
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(SelectMenuToTableActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    finish();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(SelectMenuToTableActivity.this);
                    builder.setMessage("บันทึกข้อมูลเรียบร้อยแล้ว")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    finish();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();

                String msg = e + ""; // success
                AlertDialog.Builder builder = new AlertDialog.Builder(SelectMenuToTableActivity.this);
                builder.setMessage(msg)
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //do things
                                finish();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();


            }

        }
    }

    private class BookingAddTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(SelectMenuToTableActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tSaving...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();


            RequestBody formBody = new FormEncodingBuilder()
                    .add("orders_id", urls[1])
                    .add("product_id", urls[2])
                    .add("qty", "1")
                    .add("userid", urls[3])
                    .add("remain", urls[4])
                    .add("p_group_add_id", urls[5])
                    .add("branch_id", profile.BranchId)
                    .build();


            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);

                String status = object.getString("status"); // success
                if (status.equals("0")) {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(SelectMenuToTableActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    finish();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    String msg = object.getString("msg"); // success
                    AlertDialog.Builder builder = new AlertDialog.Builder(SelectMenuToTableActivity.this);
                    builder.setMessage("เพิ่มรายการอาหารสำเร็จ")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    finish();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }


            } catch (JSONException e) {
                e.printStackTrace();

                String msg = e + ""; // success
                AlertDialog.Builder builder = new AlertDialog.Builder(SelectMenuToTableActivity.this);
                builder.setMessage(msg)
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //do things
                                finish();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();


            }

        }
    }


    // String
    public class FeedItemDrug {
        private String menu_image_data = "";
        private String menu_categories_id = "";
        private String menu_name = "";
        private String menu_product_detail = "";
        private String menu_product_price = "";
        private String menu_cookingtime = "";
        private String menu_calories = "";
        private String menu_is_recommend = "";
        private int choice_group = 0;

        public int getChoice_group() {
            return choice_group;
        }

        public void setChoice_group(int choice_group) {
            this.choice_group = choice_group;
        }

        public void setMenu_name(String menu_name) {
            this.menu_name = menu_name;
        }

        public String getMenu_name() {
            return menu_name;
        }

        public String getMenu_calories() {
            return menu_calories;
        }

        public String getMenu_categories_id() {
            return menu_categories_id;
        }

        public String getMenu_cookingtime() {
            return menu_cookingtime;
        }

        public String getMenu_image_data() {
            return menu_image_data;
        }

        public String getMenu_is_recommend() {
            return menu_is_recommend;
        }

        public String getMenu_product_detail() {
            return menu_product_detail;
        }

        public String getMenu_product_price() {
            return menu_product_price;
        }

        public String getSetDetail() {
            return setDetail;
        }

        public void setMenu_calories(String menu_calories) {
            this.menu_calories = menu_calories;
        }

        public void setMenu_categories_id(String menu_categories_id) {
            this.menu_categories_id = menu_categories_id;
        }

        public void setMenu_cookingtime(String menu_cookingtime) {
            this.menu_cookingtime = menu_cookingtime;
        }

        public void setMenu_image_data(String menu_image_data) {
            this.menu_image_data = menu_image_data;
        }

        public void setMenu_is_recommend(String menu_is_recommend) {
            this.menu_is_recommend = menu_is_recommend;
        }

        public void setMenu_product_detail(String menu_product_detail) {
            this.menu_product_detail = menu_product_detail;
        }

        public void setMenu_product_price(String menu_product_price) {
            this.menu_product_price = menu_product_price;
        }

        public void setSetDetail(String setDetail) {
            this.setDetail = setDetail;
        }

        private String title;
        private String id;
        private String setDetail;
        private String Price;
        private String product_image;

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }

        public String getPrice() {
            return Price;
        }

        public void setPrice(String price) {
            this.Price = price;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String name) {
            this.title = name;
        }


        public String getDetail() {
            return setDetail;
        }

        public void setDetail(String Detail) {
            this.setDetail = Detail;
        }


    }

}
