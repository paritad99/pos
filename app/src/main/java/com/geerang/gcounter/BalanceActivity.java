package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.PictureDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.epson.epos2.printer.Printer;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class BalanceActivity extends AppCompatActivity {

    TextView tvTotal, tvInputAmount, tvBalance;
    Button btnClose;
    RecyclerView recyclerView;
    private List<FeedItemOrder> feedsListOrder;

    TextView tvTotal_qty, tvSum_amount,
            tvDiscount, txt_receive_total, txtTotalReceive, txtTotalBalance;
    private OrderRecyclerAdapter OrdermAdapter;
    public static Printer mPrinter = null;

    TextView txtStoreName, txtUsername, txtDatetime, txtOrderNumber;

    static String menu_order = "";
    static String menu_order_total_price = "";
    static String menu_order_total_qty = "";
    static String menu_order_discount = "";
    Profile profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_balance);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

       profile = new Profile(this);

        double input_total = Double.parseDouble(StringUtil.input_amount);
        double input_cash = Double.parseDouble(StringUtil.TotalPayment);

        double balance_total = (input_cash - input_total);

        tvTotal = (TextView) findViewById(R.id.tvTotal);
        tvTotal.setText("ยอดรวม " + StringUtil.TotalPayment + " บาท");

        tvInputAmount = (TextView) findViewById(R.id.tvInputAmount);
        tvInputAmount.setText("เรียกเก็บ " + StringUtil.input_amount + " บาท");

        tvBalance = (TextView) findViewById(R.id.tvBalance);
        tvBalance.setText("เงินทอน " + balance_total + " บาท");

        recyclerView = (RecyclerView) findViewById(R.id.rvOrders);

        tvTotal_qty = (TextView) findViewById(R.id.tvItemQty);
        tvSum_amount = (TextView) findViewById(R.id.tvSumAmount);
        tvDiscount = (TextView) findViewById(R.id.tvDiscount);
//        tvTableName = (TextView) findViewById(R.id.tvTableName);
        txt_receive_total = (TextView) findViewById(R.id.txt_receive_total);

        txtStoreName = (TextView) findViewById(R.id.txtStoreName);
        txtUsername = (TextView) findViewById(R.id.txtUsername);
        txtDatetime = (TextView) findViewById(R.id.txtDatetime);
        txtOrderNumber = (TextView) findViewById(R.id.txtOrderNumber);

        txtTotalReceive = (TextView) findViewById(R.id.txtTotalReceive);
        txtTotalBalance = (TextView) findViewById(R.id.txtTotalBalance);

        btnClose = (Button) findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                StringUtil.OrderId = "";
                StringUtil.Id = "";
                StringUtil.TotalPayment = "";
                StringUtil.Is_bill_merge = 0;
                StringUtil.TableCustomerName = "";
                StringUtil.TableCustomer = "";

                if (profile.Position.equals("2")) {
                    StringUtil.employee_status = "ผู้จัดการร้าน";
                    Intent in = new Intent(BalanceActivity.this, ManagerMainActivity.class);
                    in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(in);
                    finish();
                } else {
                    Intent in = new Intent(BalanceActivity.this, StaffCashireActivity.class);
                    in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(in);
                    finish();
                }


            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        Calendar c = Calendar.getInstance();
        SimpleDateFormat dateformat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss aa");
        String datetime = dateformat.format(c.getTime());
        System.out.println(datetime);

        txtDatetime.setText("วันที่" + datetime);
        txtStoreName.setText("ร้าน" + profile.StoreName);
        txtUsername.setText("พนักงาน " + profile.Username);
        txtOrderNumber.setText("รหัสออร์เดอร์ " + StringUtil.OrderId);

//        recyclerView = (RecyclerView) findViewById(R.id.rvOrders);

//        new AsyncOrder().execute();

//        tvTableName.setText(StringUtil.TableCustomerName);
        new AsyncOrder().execute();

    }


    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    private class AsyncOrder extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(BalanceActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {

                response = http.run(StringUtil.URL + "gpos/api/BalanceActivity/" + StringUtil.OrderId);//+ StringUtil.OrderId)
//                response = http.run(StringUtil.URL+"gpos/api/ListOrderItems/29");

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            //txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread
            pdLoading.dismiss();


            //List<DataFish> data=new ArrayList<>();

            int x = 0;
            int total_sale = 0;
            double total_sale_discount = 0.0;
            double receive_total = 0.0;

            feedsListOrder = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);

                int qty = 0;
                int price = 0;
                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {

                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemOrder item = new FeedItemOrder();
                    item.setOrders_item_id(json_data.getString("orders_item_id"));
                    item.setId(json_data.getString("product_id"));
                    item.setTitle(json_data.getString("product_name"));
                    item.setDetail(json_data.getString("product_detail"));
                    item.setPrice(json_data.getString("total_price"));
                    item.setQty(json_data.getString("qty"));
                    item.setOrderid(json_data.getString("orders_id"));
                    item.setProduct_choice(json_data.getString("product_choice_name"));
                    item.setProduct_image(json_data.getString("product_image"));
                    item.setChoice_group_name(json_data.getString("choice_group_name"));

                    menu_order += json_data.getString("product_name") + "  " + json_data.getString("product_choice_name")
                            + "              " + json_data.getString("qty")
                            + "              " + json_data.getString("total_price") + "บาท#";


                    try {
                        qty = Integer.parseInt(json_data.getString("qty"));
                        price = Integer.parseInt(json_data.getString("total_price"));

                        x = x + qty;
                        total_sale = total_sale + price;

                        if (StringUtil.promotion_discount_percen_type.equals("เปอร์เซ็น")) {
                        } else {
//                            StringUtil.promotion_discount_percen_type = feedItem.getPromotion_type();
//                            StringUtil.promotion_discount_percen_total = Integer.parseInt(feedItem.getPromotion_qty());
                        }

                        if (StringUtil.dicount_type.equals("เปอร์เซ็น")) {
                            try {
                                double total = Double.parseDouble(total_sale + "");
                                int dis = StringUtil.dicount_total;
//                                double result = (total * dis) / 100;
                                StringUtil.discount_total_payment = "" + (total * dis) / 100;

                            } catch (Exception e) {
                                StringUtil.discount_total_payment = e + "";
                            }

                        } else {

                            try {

                                double total = Double.parseDouble(total_sale + "");
                                int dis = StringUtil.dicount_total;
//                                double result = total - dis;
                                StringUtil.discount_total_payment = "" + (total - dis);

                            } catch (Exception e) {
                                StringUtil.discount_total_payment = e + "";
                            }


                        }


                    } catch (Exception e) {
                        Toast.makeText(BalanceActivity.this, e.toString(), Toast.LENGTH_LONG).show();
                    }


                    feedsListOrder.add(item);
                }


                StringUtil.TotalPayment = StringUtil.discount_total_payment + "";

                tvSum_amount.setText("ราคาสินค้ารวม " + total_sale + " บาท");
                tvTotal_qty.setText("รวม " + x + "รายการ");
                tvDiscount.setText("ส่วนลด " + StringUtil.dicount_total + " " + StringUtil.dicount_type);
                txt_receive_total.setText("ยอดรวมสุทธิ " + StringUtil.discount_total_payment + " บาท");

                txtTotalReceive.setText("รับเงิน " + StringUtil.input_amount + " บาท");
                txtTotalBalance.setText("เงินถอน " + StringUtil.payment_change + " บาท");
                ;


                menu_order_total_price = total_sale + "";
                menu_order_total_qty = x + "";
                menu_order_discount = total_sale_discount + "";

                initializeObject();

                if (!StringUtil.printer_address.equals("")) {
                    System.out.println(" mPrinter. printer_address : " + StringUtil.printer_address);
                    initializeObject();
                } else {
                    System.out.println(" mPrinter. printer_address : " + StringUtil.printer_address);
                }

                // display toast
                System.out.println(" mPrinter.run : " + profile.StoreName);
                if (!runPrintReceiptSequence()) {
//                        updateButtonState(true);
                    System.out.println(" runPrintReceiptSequence.run : " + getDateTime());
                }


                OrdermAdapter = new OrderRecyclerAdapter(BalanceActivity.this, feedsListOrder);
                recyclerView.setAdapter(OrdermAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(BalanceActivity.this, 1);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());

            } catch (JSONException e) {


                Toast.makeText(BalanceActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    private boolean initializeObject() {
        try {
            mPrinter = new Printer(2, 0, getApplicationContext());

            StringUtil.is_printer = 1;

        } catch (Exception e) {
            System.out.println(" Exception initializeObject : " + e);
//            ShowMsg.showException(e, "Printer", getApplicationContext());
            return false;
        }

//        mPrinter.setReceiveEventListener(getApplicationContext());

        return true;
    }

    private boolean runPrintReceiptSequence() {

        if (!createReceiptData()) {

            return false;
        }

        if (!printData()) {
            return false;
        }

        return true;
    }

    private boolean printData() {
        if (mPrinter == null) {
            return false;
        }

//        if (!connectPrinter()) {
//            mPrinter.clearCommandBuffer();
//            return false;
//        }

        if (mPrinter == null) {
            return false;
        }

        try {
            // เชื่อมต่อเครื่องปริ้น

            mPrinter.connect(StringUtil.printer_address, Printer.PARAM_DEFAULT);

        } catch (Exception e) {
            System.out.println("DeliveryOrderActivity mPrinter.connect : " + e + "\n" + StringUtil.printer_address);
//            ShowMsg.showException(e, "connect", getApplicationContext());
//            return false;
        }

        try {
            System.out.println(" mPrinter : ");
            mPrinter.sendData(Printer.PARAM_DEFAULT);
        } catch (Exception e) {
            mPrinter.clearCommandBuffer();
            System.out.println(" clearCommandBuffer : " + e);

            try {
                mPrinter.disconnect();
            } catch (Exception ex) {
                // Do nothing
            }
            return false;
        }

        return true;
    }

    public double Discount_Percen(int price, int discount) {
        double res = 0.0;

        res = (discount * price) / 100;

        return res;
    }

    private boolean createReceiptData() {
        String method = "";
        Bitmap logoData = BitmapFactory.decodeResource(getResources(), R.drawable.store);
        StringBuilder textData = new StringBuilder();
        final int barcodeWidth = 2;
        final int barcodeHeight = 100;

        if (mPrinter == null) {
            return false;
        }

        try {

            // เปิดแกะเก็บเงิน
//            if(mDrawer.isChecked()) {
//                method = "addPulse";
//                mPrinter.addPulse(Printer.PARAM_DEFAULT,
//                        Printer.PARAM_DEFAULT);
//            }

            method = "addTextAlign";
            mPrinter.addTextAlign(Printer.ALIGN_CENTER);

//            method = "addImage";
//            mPrinter.addImage(logoData, 0, 0,
//                    logoData.getWidth(),
//                    logoData.getHeight(),
//                    Printer.COLOR_1,
//                    Printer.MODE_MONO,
//                    Printer.HALFTONE_DITHER,
//                    Printer.PARAM_DEFAULT,
//                    Printer.COMPRESS_AUTO);
//
//            method = "addFeedLine";
//            mPrinter.addFeedLine(1);

            textData.append("ร้าน " + profile.StoreName + "\n");
            textData.append("พนักงาน " + profile.Username + "\n");
            textData.append("\n");
            textData.append("วันที่เวลา " + getDateTime() + "\n");
            textData.append("โต๊ะ#" + StringUtil.TableCustomerName + " รหัสออร์เดอร์#" + StringUtil.OrderId + "\n");
            textData.append("------------------------------\n");
            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            String[] arrB = menu_order.split("#");
            int i = 1;
            for (String order : arrB) {

                textData.append(i + " " + order + "\n");
                i++;
            }

//            textData.append("2 เมล็ดกาแฟดอยปู่หมื่นคั่วกลาง  300$\n");
//            textData.append("3 กาแฟออร์แกนิคชนิดคั่วเข้มบด  30$\n");

            textData.append("------------------------------\n");
            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());

            textData.append("ราคาสินค้ารวม               " + menu_order_total_price + " บาท\n");
            textData.append("รวมรายการ                 " + menu_order_total_qty + "    รายการ\n");
            textData.append("ส่วนลดจำนวน                " + menu_order_discount + "  บาท\n");
            textData.append("ยอดเรียกเก็บ                " + menu_order_total_price + "   บาท\n");
            textData.append("------------------------------\n");
            method = "addText";
            mPrinter.addText(textData.toString());
            textData.delete(0, textData.length());


            method = "addBarcode";
            mPrinter.addBarcode(StringUtil.OrderId,
                    Printer.BARCODE_CODE39,
                    Printer.HRI_BELOW,
                    Printer.FONT_A,
                    barcodeWidth,
                    barcodeHeight);

            method = "addCut";
            mPrinter.addCut(Printer.CUT_FEED);

            menu_order = "";
        } catch (Exception e) {
            mPrinter.clearCommandBuffer();
            ShowMsg.showException(e, method, getApplicationContext());
            return false;
        }

        textData = null;

        return true;
    }

    private String getDateTime() {
        // get date time in custom format
        SimpleDateFormat sdf = new SimpleDateFormat("[yyyy/MM/dd - HH:mm:ss]");
        return sdf.format(new Date());
    }

    // Adapter
    public class OrderRecyclerAdapter extends RecyclerView.Adapter<OrderRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemOrder> feedsListOrder;
        private Context mContext;

        public OrderRecyclerAdapter(Context context, List<FeedItemOrder> feedItemList) {
            this.feedsListOrder = feedItemList;
            this.mContext = context;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.content_cashier_order_row, null);

            CustomViewHolder viewHolder = new CustomViewHolder(view);
            return viewHolder;
        }


        public void onBindViewHolder(final CustomViewHolder customViewHolder, int i) {
            final FeedItemOrder feedItem = feedsListOrder.get(i);
            customViewHolder.feedItem = feedItem;

//            try {
            int qty = Integer.parseInt(feedItem.getQty());
            int price = Integer.parseInt(feedItem.getPrice());

            int sum = qty * price;

            //Setting text view title
            customViewHolder.textView.setText("สินค้า: " + feedItem.getTitle());
            customViewHolder.txt_price.setText(price + "฿");
//                customViewHolder.price.setText("ราคา/หน่วย: " + price);
            customViewHolder.txt_order_qty.setText("" + feedItem.getQty());
            customViewHolder.txt_choice_name.setText("Choice : " + feedItem.getChoice_group_name() + " : " + feedItem.getProduct_choice());


        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedsListOrder ? feedsListOrder.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            protected ImageView imageView;
            protected TextView textView;
            protected TextView txt_price;
            protected TextView txt_choice_name, txt_order_qty;
            FeedItemOrder feedItem;

            public CustomViewHolder(View view) {
                super(view);
//                this.imageView = (ImageView) view.findViewById(R.id.imageView2);
                this.txt_choice_name = (TextView) view.findViewById(R.id.txt_choice_name);
                this.textView = (TextView) view.findViewById(R.id.txt_menu_name);
                this.txt_price = (TextView) view.findViewById(R.id.txt_price);
                this.txt_order_qty = (TextView) view.findViewById(R.id.txt_order_qty);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        StringUtil.Orders_item_id = feedItem.getOrders_item_id();
                        StringUtil.OrderId = feedItem.getOrderid();

//                        LayoutInflater layoutinflater = LayoutInflater.from(OpenTableOrderActivity.this);
//                        View promptUserView = layoutinflater.inflate(R.layout.content_product_qty, null);
//
//                        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(OpenTableOrderActivity.this);

//                        alertDialogBuilder.setView(promptUserView);
//                        TextView tvTitle = (TextView) promptUserView.findViewById(R.id.tvTitle);
//                        tvTitle.setText(feedItem.getTitle());

//                        final EditText input_product_qty = (EditText) promptUserView.findViewById(R.id.input_product_qty);


                    }
                });

            }
        }
    }

    // String
    public class FeedItemOrder {
        private String title;
        private String id;
        private String setDetail;
        private String price;
        private String qty;
        private String PricePerItem;
        private String orders_item_id;
        private String product_image;
        private String product_choice;
        private String choice_group_name;

        public String getChoice_group_name() {
            return choice_group_name;
        }

        public void setChoice_group_name(String choice_group_name) {
            this.choice_group_name = choice_group_name;
        }

        public String getProduct_choice() {
            return product_choice;
        }

        public void setProduct_choice(String product_choice) {
            this.product_choice = product_choice;
        }


        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }

        private String orderid;

        public String getOrderid() {
            return orderid;
        }

        public void setOrderid(String orderid) {
            this.orderid = orderid;
        }

        public String getOrders_item_id() {
            return orders_item_id;
        }

        public void setOrders_item_id(String orders_item_id) {
            this.orders_item_id = orders_item_id;
        }

        public String getPricePerItem() {
            return PricePerItem;
        }

        public void setPricePerItem(String pricePerItem) {
            this.PricePerItem = pricePerItem;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getPrice() {
            return price;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String name) {
            this.title = name;
        }


        public String getDetail() {
            return setDetail;
        }

        public void setDetail(String Detail) {
            this.setDetail = Detail;
        }


    }

}
