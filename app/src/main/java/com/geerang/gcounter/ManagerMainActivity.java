package com.geerang.gcounter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.view.View;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.widget.Button;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ManagerMainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;

    final String PREF_NAME = "LoginPreferences";
    final String KEY_USERNAME = "Username";
    final String KEY_USERID = "UserId";
    final String KEY_BranchId = "BranchId";
    final String KEY_StoreId = "StoreId";
    final String KEY_Status = "Status";
    final String KEY_REMEMBER = "RememberUsername";
    final String KEY_business_type_id = "business_type_id";
    final String KEY_StoreName = "StoreName";
    final String KEY_BranchName = "BranchName";
    final String KEY_Position = "Position";
    final String KEY_StoreExp = "StoreExp";
    final String KEY_Owner = "Owner";
    final String KEY_package = "package";


    SharedPreferences sp;
    SharedPreferences.Editor editor;

    Profile profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        profile = new Profile(this);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        View headerView = navigationView.getHeaderView(0);

        sp = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sp.edit();


        TextView txtDateExp = (TextView) headerView.findViewById(R.id.txtDateExp);


        try {

            Date c = Calendar.getInstance().getTime();

            SimpleDateFormat df = new SimpleDateFormat("M/dd/yyyy");
            String formattedDate = df.format(c);
            System.out.println("Current time => " + formattedDate);

            String CurrDate = formattedDate;
            String PrvvDate = profile.store_exp;
            Date date1 = null;
            Date date2 = null;

//            date1 = df.parse(CurrDate);
//            date2 = df.parse(PrvvDate);
//            long diff = Math.abs(date1.getTime() - date2.getTime());
//            long diffDays = diff / (24 * 60 * 60 * 1000);

            txtDateExp.setText("สาขา " + profile.BranchName);
            //System.out.println(diffDays);

        } catch (Exception e1) {
            System.out.println("exception " + e1);
        }

        Button btnSwitchBranch = (Button) headerView.findViewById(R.id.btnSwitchApp);

        btnSwitchBranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopService(new Intent(ManagerMainActivity.this, TimePrintService.class));


                Intent in = new Intent(ManagerMainActivity.this, SelectBranchActivity.class);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(in);
                finish();

            }
        });
        if (Profile.IsOwner){
            btnSwitchBranch.setVisibility(View.VISIBLE);
        }else{
            btnSwitchBranch.setVisibility(View.GONE);
        }
        Button btnExitApp = (Button) headerView.findViewById(R.id.btnExitApp);
        btnExitApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //stopService(new Intent(ManagerMainActivity.this, TimePrintService.class));

                editor = sp.edit();
                editor.putString(KEY_USERNAME, "");
                editor.putString(KEY_USERID, "");
                editor.putString(KEY_StoreId, "");
                editor.putString(KEY_BranchId, "");
                editor.putString(KEY_Status, "");
                editor.putString(KEY_REMEMBER, "");
                editor.putString(KEY_business_type_id, "");
                editor.putString(KEY_StoreName, "");
                editor.putString(KEY_BranchName, "");
                editor.putString(KEY_Position, "");
                editor.putString(KEY_Owner, "");

                editor.commit();

                Intent in = new Intent(ManagerMainActivity.this, LoginActivity.class);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(in);
                finish();

            }
        });

        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_attendance,
                R.id.nav_dashbaord,
                R.id.nav_cashire,
                R.id.nav_product_recommend,
                R.id.nav_store_info,
                R.id.nav_product_cat,
                R.id.nav_product,
                R.id.nav_food_menu,
                R.id.nav_product_exp,
                R.id.nav_product_loss,
                R.id.nav_store_update,
                R.id.nav_branch,
                R.id.nav_table,
                R.id.nav_employee,
                R.id.nav_attendance_manage
        )
                .setDrawerLayout(drawer)
                .build();

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);


    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
}
