package com.geerang.gcounter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.PictureDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.StrictMode;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;

public class MenuStoreActivity extends AppCompatActivity {

    // CONNECTION_TIMEOUT and READ_TIMEOUT are in milliseconds
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;
    private List<FeedItemDrug> feedsList;
    private RecyclerView recyclerView;
    private DrugRecyclerAdapter mAdapter;

    static TextView txtShowChoice;

    static ProgressBar progressBar;

    RecyclerView mRecyclerView;
    private SectionedRecyclerViewAdapter sectionAdapter;
    ArrayList<HashMap<String, String>> MyArrList_GroupChoice;

    ArrayList<HashMap<String, String>> ChoiceArrList;
    HashMap<String, String> map_choice;


    int count_select_max = 0;

    Button btnAdd;

    Profile profile;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    TextView txtTotalMenu;
    int Total_Menu = 0;
    Button btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_store);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AsyncNewMenuId().execute("http://www.geerang.com/gpos/api/NewMenuGetProduct");
            }
        });

        profile = new Profile(this);
        txtTotalMenu = (TextView) findViewById(R.id.txtTotalMenu);
        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }


        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        new AsyncLogin().execute();

    }

    public class postHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url, RequestBody body) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }


    private class AsyncNewMenuId extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(MenuStoreActivity.this);

        @Override
        protected void onPreExecute() {
//             Create Show ProgressBar
            pdLoading.setMessage("\tกำลังโหลดข้อมูล...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();

            RequestBody formBody = new FormEncodingBuilder()
                    .add("store_id", profile.StoreId)
                    .build();
            try {
                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
//            pdLoading.dismiss();
            pdLoading.dismiss();
            JSONObject object = null;
            try {
                object = new JSONObject(jsonString);
                String status = object.getString("status"); // success
                String msg = object.getString("msg"); // success
                String product_id = object.getString("product_id"); // success

                Log.d("NEWID", jsonString);

                if (status.equals("1")) {

                    Intent intent = new Intent(MenuStoreActivity.this, FoodMenuAddActivity.class);
                    intent.putExtra("product_id", product_id);
                    startActivity(intent);

                } else {

                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuStoreActivity.this);
                    builder.setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things

                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        new AsyncLogin().execute();

    }

    private class AsyncLogin extends AsyncTask<String, String, String> {
        ProgressDialog pdLoading = new ProgressDialog(MenuStoreActivity.this);
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tกำลังโหลดข้อมูล...");
            pdLoading.setCancelable(false);
            pdLoading.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            String response = null;
            getHttp http = new getHttp();
            try {
                response = http.run(StringUtil.URL + "gpos/api/Menu/" + StringUtil.CatId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;


        }

        @Override
        protected void onPostExecute(String result) {

            //this method will be running on UI thread

            pdLoading.dismiss();
//            List<DataFish> data=new ArrayList<>();

            feedsList = new ArrayList<>();
            try {

                JSONArray jArray = new JSONArray(result);

                Total_Menu = jArray.length();
                // Extract data from json and store into ArrayList as class objects
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);
                    FeedItemDrug item = new FeedItemDrug();
                    item.setId(json_data.getString("product_id"));
                    item.setTitle(json_data.getString("product_name"));
                    item.setDetail(json_data.getString("product_detail"));
                    item.setPrice(json_data.getString("product_sale_price"));
                    item.setProduct_image(json_data.getString("product_image"));

                    item.setMenu_name(json_data.getString("product_name"));
                    item.setMenu_product_detail(json_data.getString("product_detail"));
                    item.setMenu_calories(json_data.getString("calories"));
                    item.setMenu_categories_id(json_data.getString("categories_id"));
                    item.setMenu_cookingtime(json_data.getString("cookingtime"));
                    item.setMenu_image_data(json_data.getString("product_image"));
                    item.setMenu_is_recommend(json_data.getString("is_recommend"));
                    item.setId_kit(json_data.getString("id_kit"));
                    item.setReport_cat_id(json_data.getString("report_cat_id"));
                    item.setMenu_product_price(json_data.getString("product_sale_price"));


                    int choice_group = 0; //
                    JSONArray jArray_2 = new JSONArray(json_data.getString("products_group"));
                    choice_group = jArray_2.length();
                    item.setChoice_group(choice_group);

                    feedsList.add(item);
                }

                int menu = 0;
                try {
                    menu = Integer.parseInt(StringUtil.package_product);
                } catch (Exception err) {
                    menu = 0;
                }

                if (Total_Menu > menu) {
                    btnSave.setEnabled(false);
                } else {
                    btnSave.setEnabled(true);
                }

                txtTotalMenu.setText(Total_Menu + "/" + menu);


                // Setup and Handover data to recyclerview

                mAdapter = new DrugRecyclerAdapter(MenuStoreActivity.this, feedsList);
                recyclerView.setAdapter(mAdapter);
                //mRVFishPrice.setLayoutManager(new LinearLayoutManager(getActivity()));
                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(MenuStoreActivity.this, 2);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setItemAnimator(new DefaultItemAnimator());

            } catch (JSONException e) {
                Toast.makeText(MenuStoreActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            }

        }

    }

    // Adapter
    public class DrugRecyclerAdapter extends RecyclerView.Adapter<DrugRecyclerAdapter.CustomViewHolder> {
        private List<FeedItemDrug> feedItemList;
        private Context mContext;

        public DrugRecyclerAdapter(Context context, List<FeedItemDrug> feedItemList) {
            this.feedItemList = feedItemList;
            this.mContext = context;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_select_menu_row_edit, null);

            CustomViewHolder viewHolder = new CustomViewHolder(view);
            return viewHolder;
        }

        public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
            final FeedItemDrug feedItem = feedItemList.get(i);
            customViewHolder.feedItem = feedItem;

            //Setting text view title
            customViewHolder.textView.setText(Html.fromHtml(feedItem.getTitle()));
            customViewHolder.tvPrice.setText(Html.fromHtml(feedItem.getPrice()));

            customViewHolder.btnGotoStock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // goto edit
                    Intent in = new Intent(mContext, StockOrderManageActivity.class);
                    in.putExtra("product_name", feedItem.getMenu_name());
                    in.putExtra("product_detail", feedItem.getMenu_product_detail());
                    in.putExtra("product_sale_price", feedItem.getMenu_product_price());
                    in.putExtra("product_image", feedItem.getProduct_image());
                    in.putExtra("calories", feedItem.getMenu_calories());
                    in.putExtra("categories_id", feedItem.getMenu_categories_id());
                    in.putExtra("cookingtime", feedItem.getMenu_cookingtime());
                    in.putExtra("product_id", feedItem.getId());
                    startActivity(in);

                }
            });

            customViewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // goto edit
                    Intent in = new Intent(mContext, FoodMenuEditActivity.class);
                    in.putExtra("product_name", feedItem.getMenu_name());
                    in.putExtra("product_detail", feedItem.getMenu_product_detail());
                    in.putExtra("product_sale_price", feedItem.getPrice());
                    in.putExtra("product_image", feedItem.getProduct_image());
                    in.putExtra("calories", feedItem.getMenu_calories());
                    in.putExtra("categories_id", feedItem.getMenu_categories_id());
                    in.putExtra("cookingtime", feedItem.getMenu_cookingtime());
                    in.putExtra("product_id", feedItem.getId());
                    in.putExtra("id_kit", feedItem.getId_kit());
                    in.putExtra("report_cat_id", feedItem.getReport_cat_id());

                    startActivity(in);

                }
            });

            final AlertDialog.Builder adb = new AlertDialog.Builder(mContext);
            customViewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    adb.setMessage("ยืนยันการลบข้อมูล!");
                    adb.setNegativeButton("ยกเลิก", null);
                    adb.setPositiveButton("ยืนยัน", new AlertDialog.OnClickListener() {
                        public void onClick(DialogInterface dialog, int arg1) {
                            // TODO Auto-generated method stub
                            new DeleteMenuTask().execute("http://www.geerang.com/gpos/api/MenuDelete", feedItem.getId());
                        }
                    });
                    adb.show();


                }
            });

            StringUtil stringUtil = new StringUtil();
            try {
                Picasso.with(mContext)
                        .load(StringUtil.URL + "gpos/" + stringUtil.removeFirstChar(feedItem.getProduct_image()))
                        .fit()
                        .placeholder(R.drawable.noimageavailable)
                        .error(R.drawable.noimageavailable)
                        .into(customViewHolder.imageView);
            } catch (Exception e) {

            }


        }

        // get image to bitmap
        private Bitmap pictureDrawableToBitmap(PictureDrawable pictureDrawable) {
            Bitmap bmp = Bitmap.createBitmap(pictureDrawable.getIntrinsicWidth(), pictureDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            canvas.drawPicture(pictureDrawable.getPicture());
            return bmp;
        }


        @Override
        public int getItemCount() {
            return (null != feedItemList ? feedItemList.size() : 0);
        }


        public class CustomViewHolder extends RecyclerView.ViewHolder {
            protected ImageView imageView;
            protected TextView textView;
            protected TextView tvPrice;

            FeedItemDrug feedItem;

            protected Button btnEdit, btnDelete, btnGotoStock;

            public CustomViewHolder(View view) {
                super(view);
                this.imageView = (ImageView) view.findViewById(R.id.imageView);
                this.textView = (TextView) view.findViewById(R.id.title);
                this.tvPrice = (TextView) view.findViewById(R.id.tvTotalPrice);
                this.btnEdit = (Button) view.findViewById(R.id.btnEditMenu);
                this.btnDelete = (Button) view.findViewById(R.id.btnDeleteMenu);
                this.btnGotoStock = (Button) view.findViewById(R.id.btnGotoStock);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });

            }
        }
    }

    private class DeleteMenuTask extends AsyncTask<String, Void, String> {

        ProgressDialog pdLoading = new ProgressDialog(MenuStoreActivity.this);

        @Override
        protected void onPreExecute() {
            // Create Show ProgressBar
            pdLoading.setMessage("\tกำลังลบข้อมูล...");
            pdLoading.setCancelable(false);
            pdLoading.show();
        }

        protected String doInBackground(String... urls) {
            String response = null;
            postHttp http = new postHttp();


            RequestBody formBody = new FormEncodingBuilder()
                    .add("UserId", profile.UserId + "")
                    .add("DeleteId", urls[1])
                    .build();
            try {

                response = http.run(urls[0], formBody);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
//                txtResult.setText(response);
            return response;
        }

        protected void onPostExecute(String jsonString) {
            // Dismiss ProgressBar
            //showData(jsonString);
            pdLoading.dismiss();

            new AsyncLogin().execute();

//            JSONObject object = null;
//            try {
//                object = new JSONObject(jsonString);
//
//                String status = object.getString("status"); // success
//                if (status.equals("0")) {
//                    String msg = object.getString("msg"); // success
//                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuStoreActivity.this);
//                    builder.setMessage(msg)
//                            .setCancelable(false)
//                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    //do things
//                                    new AsyncLogin().execute();
//                                }
//                            });
//                    AlertDialog alert = builder.create();
//                    alert.show();
//                } else {
//                    String msg = object.getString("msg"); // success
//                    AlertDialog.Builder builder = new AlertDialog.Builder(MenuStoreActivity.this);
//                    builder.setMessage(msg)
//                            .setCancelable(false)
//                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    //do things
////                                    finish();
//                                    new AsyncLogin().execute();
//                                }
//                            });
//                    AlertDialog alert = builder.create();
//                    alert.show();
//                }
//
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }

        }
    }


    public class getHttp {
        OkHttpClient client = new OkHttpClient();

        String run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    // String
    public class FeedItemDrug {
        private String menu_image_data = "";
        private String menu_categories_id = "";
        private String menu_name = "";
        private String menu_product_detail = "";
        private String menu_product_price = "";
        private String menu_cookingtime = "";
        private String menu_calories = "";
        private String menu_is_recommend = "";
        private int choice_group = 0;

        private String report_cat_id = "";
        private String id_kit = "";

        public String getId_kit() {
            return id_kit;
        }

        public String getReport_cat_id() {
            return report_cat_id;
        }

        public void setReport_cat_id(String report_cat_id) {
            this.report_cat_id = report_cat_id;
        }

        public void setId_kit(String id_kit) {
            this.id_kit = id_kit;
        }

        public int getChoice_group() {
            return choice_group;
        }

        public void setChoice_group(int choice_group) {
            this.choice_group = choice_group;
        }

        public void setMenu_name(String menu_name) {
            this.menu_name = menu_name;
        }

        public String getMenu_name() {
            return menu_name;
        }

        public String getMenu_calories() {
            return menu_calories;
        }

        public String getMenu_categories_id() {
            return menu_categories_id;
        }

        public String getMenu_cookingtime() {
            return menu_cookingtime;
        }

        public String getMenu_image_data() {
            return menu_image_data;
        }

        public String getMenu_is_recommend() {
            return menu_is_recommend;
        }

        public String getMenu_product_detail() {
            return menu_product_detail;
        }

        public String getMenu_product_price() {
            return menu_product_price;
        }

        public String getSetDetail() {
            return setDetail;
        }

        public void setMenu_calories(String menu_calories) {
            this.menu_calories = menu_calories;
        }

        public void setMenu_categories_id(String menu_categories_id) {
            this.menu_categories_id = menu_categories_id;
        }

        public void setMenu_cookingtime(String menu_cookingtime) {
            this.menu_cookingtime = menu_cookingtime;
        }

        public void setMenu_image_data(String menu_image_data) {
            this.menu_image_data = menu_image_data;
        }

        public void setMenu_is_recommend(String menu_is_recommend) {
            this.menu_is_recommend = menu_is_recommend;
        }

        public void setMenu_product_detail(String menu_product_detail) {
            this.menu_product_detail = menu_product_detail;
        }

        public void setMenu_product_price(String menu_product_price) {
            this.menu_product_price = menu_product_price;
        }

        public void setSetDetail(String setDetail) {
            this.setDetail = setDetail;
        }

        private String title;
        private String id;
        private String setDetail;
        private String Price;
        private String product_image;

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }

        public String getPrice() {
            return Price;
        }

        public void setPrice(String price) {
            this.Price = price;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String name) {
            this.title = name;
        }


        public String getDetail() {
            return setDetail;
        }

        public void setDetail(String Detail) {
            this.setDetail = Detail;
        }


    }


}
