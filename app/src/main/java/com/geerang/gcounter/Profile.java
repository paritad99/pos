package com.geerang.gcounter;

import android.content.Context;
import android.content.SharedPreferences;

public class Profile {

    private Context context;
    final String PREF_NAME = "LoginPreferences";
    final String KEY_USERNAME = "Username";
    final String KEY_USERID = "UserId";
    final String KEY_BranchId = "BranchId";
    final String KEY_StoreId = "StoreId";
    final String KEY_Status = "Status";
    final String KEY_REMEMBER = "RememberUsername";
    final String KEY_business_type_id = "business_type_id";
    final String KEY_StoreName = "StoreName";
    final String KEY_BranchName = "BranchName";
    final String KEY_Position = "Position";
    final String KEY_StoreExp = "StoreExp";
    final String KEY_StoreAddress = "StoreAddress";
    final String KEY_StoreTax = "StoreTax";
    final String KEY_StorePhone = "StoreTax";

    public static  boolean IsOwner = false;
    public static String Username = "";
    public static String StoreTax = "";
    public static String UserId = "";
    public static String Email = "";
    public static String StoreId = "";
    public static String BranchId = "";
    public static String StoreName = "";
    public static String BranchName = "";
    public static String Position = "";
    public static String Manager = "";
    public static String business_type_id = "";
    public static String store_exp = "";
    public static String store_address = "";
    public static String StorePhone = "";

    SharedPreferences sp;
    SharedPreferences.Editor editor;

    public Profile(Context context){
        this.context = context;

        sp = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sp.edit();


        StoreTax = sp.getString(KEY_StoreTax, "");

        BranchId = sp.getString(KEY_BranchId, "");
        StoreId = sp.getString(KEY_StoreId, "");
        Username = sp.getString(KEY_USERNAME, "");
        StoreName = sp.getString(KEY_StoreName, "");
        BranchName = sp.getString(KEY_BranchName, "");
        Position = sp.getString(KEY_Position, "");
        Manager = sp.getString(KEY_BranchId, "");
        business_type_id = sp.getString(KEY_business_type_id, "");
        UserId = sp.getString(KEY_USERID, "");
        store_exp = sp.getString(KEY_StoreExp, "");
        StorePhone = sp.getString(KEY_StorePhone, "");

        String Login = sp.getString(KEY_REMEMBER, "");

        store_address = sp.getString(KEY_StoreAddress, "");


    }
    // เอาไว้เก็บค่าในตัวแปล
    public static void saveInt(Context context, String key, int value) {
        String PREF_NAME = "LoginPreferences";
        SharedPreferences sharedPref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(key, value);
        editor.commit();
    }





    



}
